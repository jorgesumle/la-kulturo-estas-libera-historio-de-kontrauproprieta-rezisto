<section class="citas">
<i>Oni konsideras, ke ne povas esti rilato inter la proprieto de verko
kaj tiu de kampo, kiu povas esti kultivita de nur unu homo, aŭ tiu de
meblo, kiu utilas nur al unu homo; do la ekskluziva proprieto baziĝas
sur la naturo de afero. Tiel la literatura proprieto ne devenas de la
natura ordo, kaj ne estas defendita de la socia forto, sed estas
proprieto fondita de la socio mem. Ne estas vera rajto, sed
privilegio.</i>

<div class="right-align">
Markizo de Condorcet, <cite>Fragmentoj
pri la gazetarlibereco</cite>, 1776</div>
<br>

<i>Se la naturo produktis aĵon malpli taŭgan por ekskluziva proprieto ol
ĉiuj aliaj, tiu estas la ago de la penskapablo, kiun ni nomas ideo,
kiu individuo povas havi ekskluzive nur se ri tenas ĝin por si mem. Sed,
en la momento, en kiu ri diskonigas ĝin, ĉi tiu estas neeviteble havata
de ĉiuj kaj tiu, kiu ricevas ĝin, ne povas forpreni ĝin. Ĝia aparta
karaktero ankaŭ estas, ke neniu posedas ĝin malpli, ĉar ĉiuj aliaj
tute posedas ĝin. Tiu, kiu ricevas ideon de mi ricevas instruon por si
sen esti redukto de tiu mia, same kiel kiu fajrigas lampon per mia,
ricevas lumon, sen ke mia estu estingita.</i>

<div class="right-align">
Thomas Jefferson, en la letero al Isaac McPherson, 1813
</div>
</section>

### I.

La nocio, ke iu havu la proprieton de ideo, kiu iĝis kutima en la okcidenta
socio en la sekvaj jarcentoj, kaj tiam kaj hodiaŭ, ankoraŭ estas iel
stranga: kiel vi povas esti *posedanto* de io, kion mi daŭre havas? Kial
tio estas ŝtelo? Ni pli facile komprenas la ideon de ŝtelo, kiam
ekzemple mi prenas piprujon de la kuirejo de via hejmo. Sed kion mi
ŝtelas, se post pruvi vian pipron en telero, mi prenas la *ideon* uzi
tiun pipron en telero kaj iras al foiro aĉeti pipran vitron sama ol tiu,
kiun vi havas? Kiun mi ŝtelus ĉi-okaze[^51]?

[^51]: Tiu komparo estis kreita de Lessig, <i lang="lt">op.
  cit.</i>, p. 94.

Ni scias, ke ideoj, historioj, muzikaĵoj, poemoj, teatraj verkoj ne
havas la saman naturon ol materiaj aĵoj kiel kampoj, domoj, veturiloj,
muelejoj, plugiloj, juveloj. Ni povas ekzemple aŭskulti la ludadon de
muzikaĵo per iu aparato en iu ajn loko samtempe, kiam la kreinto de la
muzikaĵo ludas en alia &mdash; kaj tio ne senhavigas nek malhelpas la
aŭskultadon de ambaŭ. «Tiu, kiu ricevas ideon de mi ricevas instruon por
si, sen ke estu redukto de tiu mia, same kiel kiu fajrigas lampon per mia
ricevas lumon, sen ke mia estu estingita», diris Thomas Jefferson,
konsiderita unu el la fondintaj patroj de Usono, landa prezidanto inter
1801 kaj 1809, en letero de 1813[^52]. Se la ideoj estas liberaj, ne
konkurantaj, virusaj, asociitaj kaj kombinitaj unu kun la aliaj sen
gravi, kiuj estu iliaj teritorioj aŭ devenoj, modifante sin laŭ la uzo
kaj la kreemo de ĉiu tiel kiel la fajro, kial transformi ilin en
*intelektan propraĵon*[^53]?

[^52]: En la originala versio en la angla: «<i lang="en">He who recieves
  an idea from me, recieves instruction himself, without lessening mine;
  as he who lights his taper at mine, recieves light without darkening
  me</i>». Letero de Thomas Jefferson al Isaac McPherson, 1813.
  Disponebla
  en <https://founders.archives.gov/documents/Jefferson/03-06-02-0322>.
  «Ĉi tiu fragmento estas multfoje citita kiel argumento kontraŭa al la
  intelekta propraĵo, sed la intenco de Jefferson estas nur montri, ke
  la intelekta propraĵo ne estas natura &mdash; kio ne malhelpas [kaj li
  estas defendanto de tio], ke ĝi estu kreita de la socio»
  (Ortellado, <cite>Porque somos contra a propriedade intelectual</cite>,
  p. 29).
[^53]: La disputoj en la anglaj tribunaloj ĉirkaŭ la aŭtorrajto en la
  17-a jarcento, cititaj en la antaŭa ĉapitro, uzis la esprimon
  *literatura proprieto*. La esprimo en la angla
  <i lang="en">intellectual property</i> komencas esti uzita iom da
  tempo poste; laŭ la <cite>Oxford English Dictionary</cite>, ĝia unua
  registro estas tiu de 1769-a artikolo tiam konita, <cite>Monthly
  Review</cite>, dum la uzo kun la signifo, kiun ni hodiaŭ konas,
  datiĝas de 1808, kiel titolo de esearo: <cite>New-England Association
  in favour o Inventors and Discoverers, and Particularly for the
  Protection of Intellectual Property</cite>. Fonto: <cite>Oxford
  English Dictionary</cite>.

Jefferson mem en sia epoko respondis: «por ke la kreantoj de ideoj ne
senkuraĝiĝu krei kaj esprimi siajn ideojn necesas materia stimulo por
tiu, kiu “kreas” aŭ “esprimas la ideojn”. Por ke ili estu asimilitaj de
ĉiuj, kiuj ilin ricevas, la ideoj devas esti speciale protektitaj, por
ke ĉiufoje, kiam iu uzas ilin, la “kreanto” havu sian rekompencon»[^54].
Havante Jefferson-on kiel unu el la farintoj, la usona Konstitucio,
proklamita en 1789, 79 jarojn post la Statuto de Anne kaj en la sama jaro de la
unuaj aŭtorrajtaj leĝoj en Francio, jam havis en unu el siaj klaŭzoj:
«La parlamento devas havi la povon subteni la progreson de la sciencoj
kaj de la utilaj artoj garantiante al la aŭtoroj kaj inventistoj, dum
limigita periodo, la ekskluzivan rajton al iliaj skribaĵoj kaj
malkovroj»[^55].

[^54]: Thomas Jefferson, citita de Ortellado, <i lang="lt">op.
  cit.</i>, p. 29.
[^55]: Aŭtorrajta kaj patenta klaŭzo de la usona Konstitucio, art. I, §
  8, kl. 8.

La unuaj leĝaroj, kiuj klopodas reguligi la intelektan propraĵon leĝe
estigas tiun, kiu ankoraŭ estas la ĉefa konflikto hodiaŭ: konkordigi la
rekompencon de la kreantoj kun la alira rajto al la artaj kreaĵoj.
Estigante la produkton de iu specifa intelekta kreaĵo kiel varon kun
financa interŝanĝa valoro, la materia pago kontraŭ certa ideo
konfliktos, en multaj okazoj de la 19-a jarcento kaj poste, kun la
konservo de vasta publika havaĵo de ideoj komuna por la homaro. La
demando estigita en tiu epoko ankoraŭ aŭdeblas hodiaŭ: ĝis kiu punkto la
kreo de la rajto al la intelekta propraĵo, anstataŭ disvolvi, limigas
la scian, kulturan kaj teknologian progreson?

### II.

Estas substancaj diferencoj inter la ecoj de la intelekta propraĵo kaj
tiuj de la materia propraĵo. Multaj el ili estis estigitaj en la periodo
plena de revolucioj, cirkulado de ideoj kaj teknologiaj kreoj, kiu
daŭras inter mezo de la 18-a jarcento kaj la fino de la 19-a, momento
en kiu la diskuto ĉirkaŭ la proprieto iris tra periodon de ŝanĝoj en
Eŭropo. La dekadenco de la feŭda sistemo, la supreniro de la komerca
burĝaro, la multobliĝo de la presitaj eldonoj, la kreskado de la
individuismo, la navigadoj, kiuj originis la invadon de Ameriko, inter
aliaj rilataj aferoj, estis gravaj por diskuti la statuson de la
proprieto, kiu ĝis tiam, verdire, regis en la eŭropaj landoj. La plej
granda parto de la teroj kaj materiaj havaĵoj ĝis la 18-a jarcento
apartenis al multaj monarkioj, kiuj komandis la eŭropan kontinenton, al
la Katolika Eklezio, al la nobeloj de ĉiuj regionoj kaj, en malpli
granda skalo, al la komunumoj, kiuj kolektive administris siajn terojn
kaj aliajn naturajn resursojn, kiel arbarojn kaj lagojn. La diversaj
militoj en Anglio dum la 17-a jarcento havis kiel unu el siaj temoj la
rompiĝo de la sinjor-vasala rilato, kiu estis en la administrado de la
materiaj proprietoj ĝis tiam kaj, sekve, la estigo de novaj leĝoj, kiuj
bremsis la reĝan kontrolon de la teroj kaj reguligis la proprieton.

Unu el la manieroj intelekte rajtigi la privatan proprieton okazis per
la liberalaj ideoj, kiuj defendis la individuismon kaj la limigon de la
povo de la absolutisma ŝtato de tiu epoko. Unu el la plej gravaj
disvastigantoj de tiuj ideoj, la angla John Locke (1632-1704) diris, ke
la proprieto, kaj ankaŭ la rajto al la vivo kaj al la libereco, estis
*natura rajto*, tio estas, esenca de homo[^56], estigita de Dio, kiam
li kreis la mondon. Locke diris, ke, kiel laŭrajta frukto de sia laboro,
ĉiu homo havus rajton al proprieto; «ĉiu ajn afero, kiun li [la homo] ne
eltiras el la stato, al kiu naturo disvolvis ĝin kaj kiel lasis ĝin,
li miksas ĝin kun sia laboro kaj aldonas al ĝi ion, kiu estas lia,
transformante ĝin en sian proprieton»[^57]. Kiel limo de tiu proprieto
atentigis al la neceso, ke la aferoj en tiu «stato, kiun la naturo
kreis» restu en maniero, ke estu «sufiĉaj por aliaj, kvante kaj
kvalite». Ĉi tie jam estas la embrio de la modernaj kolizioj okazontaj
inter publika kaj privata en la proprieta rajto kaj ĉirkaŭ la koncepto
de komuno[^58].

[^56]: Locke parolis pri tiu rajto kiel ekskluziva de homo de iĉa sekso,
  ignorante inojn, same kiel siaj prauloj de la Antikva Epoko kaj
  Mezepoko kaj kiel daŭre okazis ĝis, almenaŭ, la konkero de la unuaj
  civilaj rajtoj de la inoj, en la 19-a jarcento.
[^57]: Locke, <cite>Dois tratados sobre o governo</cite>, p. 409.
[^58]: Locke uzas ĉi tie la vorton «komunaĵoj» (<i lang="en">commons</i>)
  simile al la romia <i lang="lt">res comunes</i>, en kiu estas unu el
  la unuaj registroj proksimaj al la ideo de komuno, kiun ni hodiaŭ
  konas, kiel «aferoj, kiuj povus esti komune ĝuitaj kaj prizorgitaj de
  la homaro». Ĝi estos la sama komuno, kiu, malpli ol du jarcentoj
  poste, Karl Marx diskutos en <cite>Os despossuídos</cite>, kolekto de
  1842-aj tekstoj, kiu temas pri la rajto al la uzado de la tero post la
  ligna ŝtelo, afero grava en la tiama Germanio. Kaj kiu rilatos al la
  intelektaj kaj ciferecaj havaĵoj, kiel ni vidos en la 5-a Ĉapitro,
  [«Libera kulturo»](#libera-kulturo), de tiu ĉi libro.

Defendante la proprieton kiel naturan rajton, precipe en <cite>Du
traktatoj pri la registaro</cite> (1689), la angla filozofo igis la
nocion de proprieto esenca por la disvolvigo de la individua libereco,
ideo kiu estis gravega, por ke la supreniranta burĝaro liberiĝu de la
sociaj limigoj truditaj de la absolutismaj monarkioj, kiuj malfaciligis
la socian movivecon kaj la liberan komercon. La nocio de privata
proprieto disvastigita de Locke akiris influon kaj disvastiĝis kiel tiu,
kiu anstataŭigis la okcidentan pensmanieron de tiu epoko, la
feŭda koncepto de proprieto, reĝa, hereda kaj neŝanĝebla. Ĝi estis ankaŭ
uzita kiel ideologia bazo por la konstruo de kompreno de privata
materia proprieto kiel frukto de la laboro kaj homa rajto, kiu
disvastiĝus en la sekvaj jardekoj kaj jarcentoj, restante konstanta ĝis
hodiaŭ.

Dum la 17-a kaj 18-a jarcentoj la diskuto pri la proprieto kreskiĝis
ankaŭ en Francio, post la liberalaj ideoj kaj en debatoj, en kiuj
partoprenis tiamaj klerismaj filozofoj, kiel Rousseau, Diderot kaj
Voltaire. Same kiel Anglio, Hispanio kaj aliaj landoj regitaj de
monarkioj en la tiama Eŭropo, Francio havis sian sistemon de
privilegioj, donitaj de la reĝoj al specifaj profesiaj grupoj &mdash;
inter ili la presista, estigita ekde la mezo de la 16-a jarcento. En
1777 la franca monarkio donis la tiel nomitajn «aŭtorajn privilegiojn»
(<i lang="fr">privilèges d’auteur</i>), kiuj, malsamaj ol la «eldonistaj
privilegioj» (<i lang="fr">privilèges en librairie</i>), jam
ekzistintaj, ne temis nur pri la periodo kaj la maniero komerci la
verkojn (kiel la angla aŭtorrajto estigita de la Statuto de Anna), sed
pri la daŭra agnosko de la proprieto de la ideoj. Ĝi estas konsiderita
kiel unua &mdash; kvankam ankoraŭ komenciĝinta &mdash; rajto donita al
la aŭtoroj, frukto de la apliko de la nocio de privata proprieto kiel
natura rajto ankaŭ por la ideoj.

Inter 1763 kaj 1764, sub mendo de la komunumo de la parizaj eldonistoj,
tiam zorgita pri la ebla forigo de la eldonistaj privilegioj, kiuj
garantiis ilin la ekskluzivecon sur la verkoj, la franca Denis Diderot
(1713-1784) skribas la tiel nomitan <cite>Letero pri la komerco de
libroj</cite>. La teksto klopodas proksimigi la *literaturan
proprieton* (kiel ankoraŭ ĝi estis nomita en tiu periodo ankaŭ en
Francio) al tiu de materiaj havaĵoj kaj defendi la daŭran proprieton de
la aŭtoroj kaj, per etendo, de la eldonistoj, sur la kreaĵoj «de la homa
spirito». Li diras:

> Ĉu verko ne apartenas al sia aŭtoro same kiel sia domo aŭ siaj teroj?
> Ĉu ne povas li fremdigi por ĉiam sian proprieton? Ĉu estus permesata,
> pro iu ajn kialo aŭ preteksto, rabi de tiu, kiu libere anstataŭigis
> lin en liaj rajtoj? Ĉu tiu substituito ne meritas havi por tiu rajto
> ĉiun protekton, kiun la registaro donas al la proprietuloj kontraŭ la
> aliaj specoj de uzurpantoj?[^59]

[^59]: Diderot, <cite>Carta sobre o comércio do livro</cite>, p. 52.

Diderot, kiu eldonis kun D’Alembert la unuan enciklopedion inter 1751
kaj 1772, ankaŭ defendis la etendon de la aŭtora rajto por riaj
«substituitoj», la eldonistoj, kiuj, laŭ lia formulado, laŭleĝe aĉetas
la verkojn al iliaj kreintoj, havante tiam la rajtojn sur ili. Estis
diskurso, kiu prenis de Locke la nocion de rajto al la proprieto kiel
naturan kaj klopodis apliki ĝin ankaŭ al la intelektaj havaĵoj, kio
donis al la kreinto absolutan kaj nemalobserveblan proprieton sur lia
verko, senfine. Ankaŭ estis penso, kiu konsentis kun la komerca kaj
industria burĝaro de tiu epoko, kiu klopodis anstataŭigi la reĝan
kontrolon aplikitan per la koncesio de privilegio per alia, bazita sur
la natura rajto kaj aplikita per la merkato.

Kvankam ili trovis akcepton en la tiama franca socio, la ideoj de
Diderot pri la aŭtorrajtoj havis opozicion ene de la superreganta
liberalismo en la intelektulara medio. Marie Jean Antoine Nicolas de
Caritat, konata kiel markizo de Condorcet (1743-1794), ne konsentis en
la ideo, ke la aŭtoro estu la laŭleĝa proprietulo de siaj verkoj
senfine. En libro nomita <cite>Fragmentoj pri la gazetarlibereco</cite>,
Condorcet emfazas la gravecon de la publika intereso, kritikas la ideon
de la eldonista komerca monopolo kaj formetas la ideon egaligi
literaturan proprieton kun la aliaj formoj de materiala proprieto.

> Oni konsideras, ke ne povas esti rilato inter la verka proprieto kaj
> la kampa, kiun povas esti kultivita de nur unu homo, aŭ de meblo, kiu
> utilas nur al unu homo; do la ekskluziva proprieto estas bazita sur
> la naturo de la aĵo. Tiel la literatura proprieto ne devenas de la
> natura ordo, estas defendita de la socia forto, sed estas proprieto
> fondita de la socio mem. Ne estas vera rajto
> (<i lang="en">véritable droit</i>), estas privilegio
> (<i lang="en">privilège</i>).[^60]

[^60]: Condorcet, <cite>Fragments sur la liberté de la presse</cite>, en
<cite>Œuvres de Condorcet</cite>, volumo 11-a, p. 253-314, citita en
Machado Pontes; Sousa Alves, <cite>O direito de autor como um direito de
propriedade: um estudo histórico da origem do copyright e do droit
d’auteur</cite>.

Reprezentinte socian idealon, kiu ankaŭ estis en la Klerismo, tiu de la
scia universaligo, Condorcet kaj aliaj en tiu periodo defendis la
liberan cirkuladon de la tekstoj kaj la finon de la privata alproprigo
de ideo &mdash; ĉiu privilegio estus limigo al la rajto de aliro de
aliaj civitanoj, estante tial noca por la libereco. Ankaŭ en
<cite>Fragmentoj pri la gazetarlibereco</cite>, Condorcet demandas sin,
ĉu la privilegioj estas necesaj, utilaj aŭ nocaj por la progreso de «la
Lumoj» &mdash; kiel oni kutimis nomi la scion en tiu periodo. Li mem
respondis, ke ne; la literatura proprieto estas «malnecesa, malutila kaj
eĉ maljusta»[^61]. Sekve defendas, ke leĝaro, kiu donas al la aŭtoroj la
rajton al proprieto sur iliaj verkoj ne pozitive influas la malkovron de
utilaj veroj, «sed aĉe komprenas la manieron kiel tiuj veroj
disvastiĝas, estante unu el la ĉefaj kaŭzoj de la diferenco en la socio
inter la kleraj aŭ doktaj homoj kaj la maldokta amaso, por kiu la plej
granda parto de la utilaj veroj restas nekonata»[^62]. Condorcet
pensis, ke en mondo, en kiu la ideoj povu libere cirkuli, estus tiu, en
kiu devus esti libereco de kreado, reproduktado kaj disvastigado de la
scio kaj la arto, kio igus maldevan ĉia individua alproprigo de la
kulturaj havaĵoj &mdash; principo, kiu resonos en la ideoj de la libera
kulturo de la 20-a jarcento.

[^61]: <i lang="lt">Ibidem</i>.
[^62]: <i lang="lt">Ibidem</i>.

La kolizio de ideoj inter Diderot kaj Condorcet, inter aliaj, instigis
la kreon de leĝoj dum fundamenta evento por la falo de la reĝaj
privilegioj kaj de la monarkio mem en Eŭropo, la Franca Revolucio
(1789-1799). En siaj unuaj jaroj la revoluciuloj estigis la abolon de la
komercaj privilegioj (kiel diversaj aliaj) donitaj de la registaro de la
reĝo Ludoviko la 16-a &mdash; inter ili, la «eldonistaj privilegioj»
&mdash; kaj kreis leĝojn, kiuj formis la bazojn de la sistemo, kiu, post
tiam, estis konita kiel <i lang="fr">droit d’auteur</i> (aŭtorrajto). La
leĝo «Pri la kongresa laboro pri la literatura kaj arta proprieto»[^63],
de 1791, donas ekspluatadan monopolon de teatraj artistoj pri la prezento
de iliaj verkoj dum iliaj tuta vivo kaj ĝis kvin jaroj post iliaj
mortoj. Du jarojn poste alia leĝo pligrandigas la beneficon por artistoj
de aliaj fakoj kaj al ĝis dek jaroj post la morto de iliaj aŭtoroj.
Inspiritaj per la diskursoj kaj de Diderot kaj de Condorcet, influitaj
ankaŭ de Locke, Rosseau kaj aliaj, la leĝoj klopodis akordigi la
diversajn konfliktintajn interesojn implikitajn. Unuflanke ili aplaŭdis
la ideon de Diderot pri la sankteco de la individua kreivo kaj la
nemalobserveblo de la aŭtorrajto; aliflanke havis ankaŭ lokon la nocio
de Condorcet, ke, post iom da tempo (unue kvin, poste dek jaroj post la
morto de la aŭtoro), la verko devus aparteni al komuna havaĵo, por la
progreso «de la Lumoj» kaj de la universala scio.

[^63]: Tute disponebla en
<https://fr.wikisource.org/wiki/Compte_rendu_des_travaux_du_congr%C3%A8s_de_la_propri%C3%A9t%C3%A9_litt%C3%A9raire_et_artistique/Loi_du_19_juillet_1791>.

Ekde tiam solidiĝis la angla aŭtorrajto kaj la franca aŭtorrajto kiel la
ĉefaj juraj sistemoj, kiuj reguligas ĝis hodiaŭ la kreon de kulturaj
(kaj intelektaj) havaĵoj en Okcidento. Unu el la diferencoj inter la du
sistemoj estis la afero pri la subteno: la <i lang="en">copyright</i>
[angla aŭtorrajto] validis unue por verko nur, kiam ĝi materiiĝas en
fizika formato, kiel presita libro. Tamen en la <i lang="fr">droit
d’auteur</i> [franca aŭtorrajto] tiu forma antaŭkondiĉo ne ekzistis: la
leĝoj komencis protekti la verkan aŭtorecon kaj integrecon (la moralajn
rajtojn), eĉ kiam ĝi ankoraŭ estu ideo kaj ne estu materiigita en iu
formato. Aliaj diferencoj inter la du sistemoj ankoraŭ kunekzistis kaj
estis kompleksigitaj en teoriaj kaj filozofiaj disputoj dum la 19-a
jarcento, periodo kiam diversaj landoj komencis unue adopti leĝarojn
regulantajn la intelektan propraĵon, inter ili Brazilo[^64]. En la jura
teorio iĝis konvencio rilatigi la <i lang="en">copyright</i> al *utilisma* opcio,
licenco donita al la proprietuloj de verkoj por ĝia komerca ekspluatado
dum determinita tempo, kun la celo regajni la kostojn metitajn por la
produktado kaj akiri novajn investaĵojn dum la periodo, dum la
<i lang="fr">droit d’auteur</i>, almenaŭ komence, estus opcio influita
per la *natura rajto*, kiu, se sukcesus, kiel Diderot kaj aliaj
defendis, igus la aŭtorrajton daŭra kaj hereda, kio povus okazigi la
komercigon kaj privatigon de ĉiuj kulturaj havaĵoj kaj la mankon de
publika havaĵo. La regularo kreita en Francio dum la epoko de la Franca
Revolucio limigis tiun rajton al specifa periodo, kio iel miksis la du
influojn, utilisman kaj naturrajtan, kaj en la franca leĝaro kaj en tiu
de landoj, kiuj adoptis la <i lang="en">copyright</i>, kiel Anglio kaj
Usono[^65].

[^64]: Laŭ Paranaguá kaj Branco en <cite>Direitos autorais</cite>, la
  unuaj referencoj al la aŭtorrajtoj en Brazilo datiĝas de 1830, kun
  Krima Kodo, kiu konsideras kiel krimo la malobservon de aŭtorrajtoj.
  La unua leĝo tamen estis la 496/1898-a, ankaŭ nomita Lei Medeiros e
  Albuquerque, omaĝe al ĝia aŭtoro, kiu siavice estis nuligita per la
  Civila Kodo de 1916, kiu klasifikis la aŭtorrajton kiel movebla
  havaĵo, fiksis la templimon de eksvalidiĝo de ago por ofendo al la
  aŭtorrajtoj en kvin jaroj. Nur en 1973 okazis, ke en Brazilo
  publikiĝis unika kaj larĝa statuto regulinte la aŭtorrajton.
[^65]: Kaj proksimigis la nociojn de <i lang="en">copyright</i> kaj
  aŭtorrajto, io kio ĝis hodiaŭ restas en la regularoj de multaj landoj.
  Pri tiu diskuto speciale vidu artikolon de Paulo Rená, <cite>Droit
  d’autor vs. copyright: diferenças conceituais entre direito de autor e
  direito de cópia</cite>, Hiperfície, 28 mar. 2012.  Disponebla en
<https://hiperficie.wordpress.com/2012/03/28/droit-dautor-vs-copyright-diferencas-conceituais-entre-direito-de-autor-e-direito-de-copia>.

Post tiu unua jura solidigo de la intelekta propraĵo, iuj traktatoj de
la sekvaj jardekoj respondecis de la determinigo de internaciaj normoj,
kiuj klopodis akordigi kelkajn komunajn aferojn inter la landoj, kiuj
pleje estis influitaj per la aŭtorrajto (Anglio, Usono kaj granda parto
de la anglosaksaj ekskolonioj), kaj tiuj kun pli da ofteco de la
aŭtorrajtoj (Francio, Germanio, Hispanio kaj la plejparto de
Latinameriko, inkluzive Brazilo). La Konvencio de Berno, subskribita dum
la 1880-a jardeko, estis la ĉefa de tiuj traktatoj, okazigita per la
Literatura kaj Arta Internacia Asocio, grupo kreita en 1878 pere de la
influo de la franca verkisto Victor Hugo. La propono estis difini
jurajn normojn, kiuj utilus por diversaj landoj kaj tiel eviti, ke iu
ajn verko protektita per aŭtorrajto en Anglio, ekzemple, povus esti
kopiita kaj vendita de iu ajn en Francio, ago kiu kutimis en tiu periodo
kaj kiun ne ŝatis al diversaj verkistoj, okazo de Victor Hugo mem (aŭtoro
de, inter aliaj, <cite>La mizeruloj</cite>, de 1862) kaj ankaŭ de
Dickens, kies skribitaj verkoj, unue publikigitaj en Anglio, estis
republikitaj per grandaj eldonkvantoj sen lia permeso en Usono,
kolerigante lin[^66].

[^66]: Kiel, inter aliaj, rakontis la verkisto Ruy Castro, en «Dickens
  los piratas», <cite>Folha de S.Paulo</cite>, 8-a febr. 2012,
  disponebla en
  <https://www1.folha.uol.com.br/fsp/opiniao/24603-dickens-e-os-piratas.shtml>.

La Konvencio de Berno estis subskribita en 1886 de landoj kiel Francio,
Belgio, Hispanio, Svislando, Germanio, Haitio, Tunizio kaj Italio, kaj
havis kiel rezulton la difino de ekskluzivaj rajtoj &mdash; kiuj post
tiam necesis leĝan rajtigon &mdash; por la traduko de verkoj, la adaptoj
kaj reordigoj; la lego kaj prezentado en publikaj lokoj, teatroj kaj
koncertejoj; la reproduktado de presitaj kopioj, inter aliaj uzoj.
Iuj landoj, kiuj adoptis la sistemon influitan per la aŭtorrajto
kontraŭis iujn difinojn, okazo de Anglio, kiu subskribis la konvencion
la sekvan jaron, sed ne sekvis grandan parton de la dispozicioj ĝis post unu
jarcento, en 1988[^67]; kaj de Usono, kiu rifuzis subskribi
motivante, ke la akordo estigita en Berno ege ŝanĝus ĝian aŭtorrajtan
leĝaron &mdash; kaj nur efektivigis ĉiujn regulojn de la internacia
akordo en 1989[^68]. Malgraŭ la opozicioj la Konvencio de Berno
solidiĝis kiel la traktato de intelekta propraĵo plej akceptita en la
mondo; naskigis la internaciaj institucioj[^69] de administrado de tiuj
rajtoj kaj ankaŭ komencis gvidi la ŝanĝojn, kiujn multaj teknologioj
disvolvitaj en la sekvaj jardekoj kaj en la 20-a jarcento kunportis por
la produktado kaj la cirkulado de kulturaj havaĵoj.

[^67]: Post <cite>Copyright, Designs and Patents Act 1988</cite>,
  kiu reformis la aŭtorrajtan leĝon de la lando.
  Kompleta leĝaro disponeblas en
  <https://www.legislation.gov.uk/ukpga/1988/48/contents>.
[^68]: Laŭ la listo de subskribintaj landoj de la World Intellectual
  Property Organization (WIPO). Fonto:
  <https://www.wipo.int/treaties/en/ShowResults.jsp?lang=en&treaty_id=15>.
[^69]: Unue la United International Bureaux for the Protection
  of Intellectual Property (BIRPI), kreita en 1893 por organizi la
  Konvencion de Berno kaj la Parizan, kiu naskigis la internacian nocion
  de intelekta propraĵo. Post 1970 ĝi ŝanĝas sian nomon al tiu, kiun ĝi
  hodiaŭ konservas: World Intellectual Property Organization (WIPO).

### III.

La kreo de nocio de intelekta propraĵo en la 19-a jarcento estas ankaŭ
ligita al la novaj teknologioj de reproduktado kaj esprimo disvolvitaj
en tiu periodo. Same kiel, en la 16-a jarcento, la unuaj privilegioj al
la presistoj kaj la aŭtorrajto naskiĝis post la invento kaj disvastiĝo
de la presilo de moveblaj tipoj en Eŭropo, ankaŭ la novaj formoj
reguligi la kreadon kaj reproduktadon de kulturaj havaĵoj estiĝas per la
enkonduko de novaj teknologioj. Malsame ol presiloj, tamen, kiuj igis
cirkuli ideojn en malsamaj formoj sed nur en unu speco de rimedo, la
teknologioj de la 19-a jarcento pliigas la rimedojn transdoni ideojn al
aŭdaĵoj kaj bildoj, kio grandigas ankaŭ la rapidecon de cirkulado de
informo kaj komencas finigi la presitan kiel la ĉefan rimedon ĝui kaj
konsumi la kulturajn havaĵojn.

La manieroj kiel la teknologiaj inventaĵoj de la 19-a jarcentoj rilatas
kaj influas unu la aliajn estas diversaj kaj kompleksaj. Por faciligi
kaj analizi kelkajn efikojn ni povas dividi tiujn teknologiojn en du
grandajn grupojn: la *komunukadaj teknologioj*, kiuj, mallongigante la
distancojn kaj konektante per pli rapida maniero homojn en diversaj
lokoj, pliigis la interŝanĝon de novaj informoj, okazo de la telegrafo,
la telefono kaj la radio &mdash; ĉiuj ankaŭ multe rilataj al la
disvastiĝo de la transportiloj, kiel la trajno, la vapora ŝipo kaj la
aŭtomobilo &mdash;; kaj la *teknologioj de registrado kaj ludado*, ĉi
tie konsideritaj kaj tiuj sonaj, kiel la gramofono kaj la fonografo, kaj
tiuj bildaj, kiuj kombinis malnovajn tradiciojn faritajn per fizikaj
trukoj kaj kemiaj miksaĵoj de substancoj kun novaj teknikoj kaj
inventaĵoj devenitaj de la scienca &mdash; kaj ankaŭ industria &mdash;
ekspansio en tiu periodo, okazo precipe de la fotografio kaj de la kino.

En la grupo de *komunikadaj teknologioj*, la telegrafo inaŭguris, en la
unua duono de la 19-a jarcento, novan eraon de disvastiĝo de informo
transsendante mesaĝojn per elektraj impulsoj por regionoj apartaj per
miloj da kilometroj. Ĝia kreo estis asociita kun la disvolvo de la
fervojoj, kiuj postulis tujajn metodojn de signado pro sekureco,
«kvankam estu kelkaj telegrafaj kabloj, kiuj sekvis la relojn, ne
fervojajn, sed kanalajn»[^70]. Al la anglaj William Fothergill Cooke kaj
Charles Wheatstonep oni atribuas la genezon de unua sistemo de komerca
uzo de la telegrafo, en 1837, kun la celo akompani la konstruon de la
fervojo inter Londono kaj Birmingham, en Anglio[^71]. En la sekvaj
jardekoj ĝi populariĝis kiel servo liverita de la ŝtato en la plejparto de
Okcidento, kio pliigis al niveloj antaŭe nekonataj la rapidecon de
informa transsendo, publika kaj privata, loka kaj regiona, nacia kaj
imperia.

[^70]: Briggs; Burke, <i lang="lt">op. cit.</i>, p. 140.
[^71]: <i lang="lt">Ibidem</i>.

La jaro, kiu iĝis konata kiel tiu de la unua telegrafa transsendo estas
memorita de ni ĝis hodiaŭ pro tio, ke estis ankaŭ tiu de la publikigo de
la *patento* de la inventaĵo, de la jam cititaj Cooke kaj Wheatstone. Ĉi
tie indas memori: krom la angla kaj franca aŭtorrajtoj, en la 18-a kaj
19-a jarcentoj ankaŭ naskiĝis, solidiĝis per juraj detaloj kaj
disvastiĝis kiel unu el la bazoj de la maniero de kapitalisma produktado
alia jura nocio de alproprigo de ideoj: la patento, kiu post tiam estis
difinita kiel registro de koncesio, publika kaj limigita, por la privata
kaj komerca ekspluatado de ideo. Malsame ol la kulturaj havaĵoj la
patentoj estas aplikitaj al havaĵoj konsideritaj utilecaj &mdash; poste
la nocio inkludis la programojn kaj eĉ matematikan formulon, kiel
algoritmon &mdash;, kiuj, en tiu momento, ekestis amase reproduktitaj
per la ekspansiantaj industriejoj. Dum alia el la internaciaj traktatoj
regulintaj de la intelekta proprieto de tiu periodo, la konvencio de
Parizo de 1883, la patento generas branĉon en la juraj studoj kaj
regularoj pri la intelekta propraĵo, kiu komencis tutmonde reguligi
inventaĵojn kiel la telegrafon, aldone al registroj de industria desegno
kaj markoj (komercaj nomoj), dezajnoj de produktoj kaj pakumoj, inter
aliaj diversaj artefaktoj de listo, kiu nur grandiĝis per la novaj
teknologioj disvolvitaj en la 20-a jarcento.

La telegrafo kontribuis al almenaŭ du inventaĵoj, kiuj helpis akceli la
disvastigadon de ideoj en la tuta mondo en la 19-a jarcento. La unua
estis la telefono, prezentita de Alexander Graham Bell en la Oficejo de
Patentoj de Usono en 1876 kiel «metodo, kaj ilo por, transsendi
voĉajn sonojn aŭ aliajn telegrafe, kaŭzante elektrajn ondojn, similaj
al la aera vibrado, kiu akompanas la voĉan sonon»[^72]. Ĝi utiligis la
kanalojn de mesaĝa transsendo de la telegrafo por transformi akustikan
energion &mdash; la voĉon &mdash; en elektran energion, kio ekpermesus
la interŝanĝon de informoj per la parolado inter du (aŭ pli) lokoj
konektitaj per reto. La dua estis la radio, en 1895, jaro en kiu la
itala Guglielmo Marconi, tiam 21-jara, faris sian unuan transsendon en
sistemo de disvastigo de signaloj en sonaj ondoj per anteno al lokoj iom
pli malproksimaj ol tri kilometroj de la deveno. Ĝi estis speco de
«sendrata telegrafo», kun sonaj informoj ĉifritaj en elektromagneta
signalo, kiu propagiĝas per ondoj, mezurataj per herco, en la fizika
spaco. Unu jaron poste, jam loĝinte en Anglio, Marconi registris sian
patenton kiel «plibonigoj[n] en la transsendo de elektraj impulsoj kaj
signaloj kaj en la respektivaj aparatoj»[^73], la unua liverita por
sendrata telegrafa sistemo bazita sur hercaj ondoj.

[^72]: Fonto: <http://www2.iath.virginia.edu/albell/bpat.1.html>. Por
  pli da informo vidu
  <https://pt.wikipedia.org/wiki/Alexander_Graham_Bell>.
[^73]: «Improvements in Transmitting Electrical Impulses and Signals and in
Apparatus there-for». Fuente: Hong, <code>Wireless: From Marconi’s
Black-box to the Audion</code>. Eblas trovi pli da rilata informo en la
Vikipedia artikolo pri la historio de la radio:
<https://en.wikipedia.org/wiki/History_of_radio#cite_note-34>.

Estas kelkaj proksimaj kaj konkurantaj inventaĵoj naskitaj en tiu
periodo, kiuj povas rilati ĉi tie al *teknologioj de registrado kaj
ludado*. Estas, ili ĉiuj, kulminaj punktoj de multege da klopodoj dum la
historio registri, ludi kaj konservi sonojn kaj bildojn, kiuj, kiam ili
komencas cirkuli en la socio, ŝanĝas la dependecon de simbola perado
per alfabeto, dominanta ĝis tiam, por la kompreno de la realo. Ili estas
metodoj, kiuj komencas konservi kaj transdoni, per la formo de lumaj kaj
sonaj ondoj, vidajn kaj akustikajn efektojn de la realo, igante
orelojn kaj okulojn memstaraj[^74] &mdash; kio kaŭzas aron de
transformoj pri la maniero produkti, cirkuligi, konsumi kaj reguligi la
kulturajn havaĵojn post tiam.

[^74]: Kittler, <cite>Gramofone filme typewriter</cite>, p. 24.

La unua el tiuj inventaĵoj estas la fonografo, kies publika prezento
datiĝas de la 6-a de decembro de 1877 en Usono fare de Thomas Edison, estro
de la tiama unua esplora laboratorio en la teknologia historio, en Parko
Menlo, Nov-Ĵerzejo[^75]. La aparato transformis, per kranka turno,
sonojn diritajn al buŝaĵo en striojn en eta cilindro kun sulkoj, kiuj
poste povis esti reproduktitaj kaj plifortigitaj per konuso kunigita al
la aparato. Jam la gramofono, kreita kaj patentita de la germana Emil
Berliner en 1888, faris la samon, sed uzante platan diskon (vaksan,
ŝelakan, kupran, poste vinilon) anstataŭ la cilindro. La teknologio
antaŭ la du produktoj estis iom malsama, same kiel la inventistaj
intencoj; pli interesita pri la kvalito de registro de klasika muziko,
Berliner elektis uzi matricon por duobligi la sonajn registrojn, ĉar al
li la ripetada ebleco gravis pli ol al Edison kaj ankaŭ al Graham Bell
&mdash; kiu inventis alian similan aparaton en tiu epoko, la
*grafofono* &mdash;, kiuj antaŭvidis la uzon de siaj inventaĵoj por
familiaj situacioj aŭ en oficejoj[^76]. En la unuaj jardekoj de la 20-a
jarcento la plata disko de Berniner venkis la batalon kontraŭ la
cilindroj de Edison kaj iĝis la formo plej uzitaj por tia registra
kaj ludada sona aparato, ĉefe ĉar ĝi estis pli facile industrie
produktebla ol la cilindroj kaj inkludi tavolojn, sigelojn kaj aliajn
akcesoraĵojn.

[^75]: <i lang="lt">Ibidem</i>.
[^76]: Briggs; Burke, <i lang="lt">op. cit.</i>, p. 181-182.

Iom antaŭe, ankoraŭ en la unua duono de la 19-a jarcento, la
dagerotipio, publike prezentita de la franca Louis Daguerre en 1839,
estis la unua procedo de produktado de bildoj, kiuj vaste cirkulis tra
Okcidento. Ĝi konsistis el kupra breto, aŭ alia malpli multekosta, kiu
per arĝenta bano formis spegulitan surfacon, kiu kiam metita en malhela
skatolo kaj elmetita al certa situacio dum iu periodo (kiu povus esti
ĝis dek minutoj en tiu unua momento), kreis «portreton» de tiu situacio,
publike elmontrota post rivelo per kemia procedo. Ĝi ne estis facila
procedo, sed ĝi disvastiĝis tra Okcidento en la 1840-a kaj 1850-a
jardekoj precipe ĉar estis pli praktika kaj malmultekosta ol la pentritaj
portretoj, tre kutimaj en tiu epoko en la burĝaj kaj industriaj
familioj. Apud la *kalotipio* (procedo kiu uzis ârgentan nitraton kaj
produktis «negativojn» sur la papero, disvolvita de la angla William
Henry Fox Talbot unu jaron poste), la dagerotipio estis la plej kutima el
la diversaj fotografaj procedoj ekzistantaj en la periodo ĝis la
plifortiĝo de la metodo de tuja fotografio per rulaj filmoj, en la fino
de la 19-a jarcento. Patentita de George Eastman, bankisto kiu iĝis
negocisto en Usono, tiu metodo estis la bazo por krei kaj komerci
kameraojn de rulaj filmoj, ĉefa produkto de firmao, kiun Eastman fondis
en 1882, Kodak, kiu iĝis preskaŭ sinonimo de fotografio en la 20-a
jarcento.

La enkonduko de la «movanta bildo» per la kino eble estis la plej granda
tiama teknologia ŝanĝo. Ĝi naskiĝis el pluraj novigoj, kiu etendas el la
fortiĝo de la fotografia fako ĝis la movada sintezo; dum la tuta
jarcento okazis eksperimentoj, kiuj, per principoj jam pli malnovaj,
kiel la *senluma kamerao*[^77], klopodis produkti kaj reprodukti
movantajn bildojn, okazo de kelkaj optikaj eksperimentoj kiel la
*zootropo* (en 1828-1832 de William George Horner) kaj la
*praksinoskopo*[^78] (1877 de Émile Reynaud). La jam citita Thomas
Edison laboris pri tiu afero kaj en 1891 eliris, el la teknologia
laboratorio, kiun li komandis, la patento de la *kinetografo*, maŝino
kiu registris movantajn bildojn kaj ilin montris en okula aperturo en ligna
kesto. Du jarojn poste, venis de la ĉefa inĝeniero de la Edison
Laboratories, William Kennedy Laurie Dickson, la patento de la
*kinetoskopo*, ilo de interna projekcio de filmoj kun individua
vidtruo, per kiu eblas vidi, per monera enmeto, malgrandan filman
strion ripetantan. Ejoj kun kinetoskopoj populariĝis en la sekvaj
jardekoj en Usono kaj estis nomitaj <i lang="en">nickelodeons</i>; ili
montris movantajn bildojn de komikaj agoj kun dresitaj bestoj, cirkaj
ekzercoj kaj dancantaj dancistinoj kaj atingis grandan komercan
sukceson.

[^77]: Kun unuaj referencoj, kiuj devenas de la grekaj, la malhela
  kamerao estas speco de optika aparato bazita sur la samnoma principo,
  kiu konsistas el skatolo (kiu povas havi kelkajn centimetrojn aŭ
  atingi la dimensiojn de ĉambro) kun aperturo en unu el ĝiaj flankoj.
  La lumo, spegulita per alia ekstera aĵo, eniras trans tiun aperturon,
  transiras la skatolon kaj atingas enan kontraŭan surfacon, kie
  formiĝas inversigita bildo de tiu aĵo. Fonto:
  <https://pt.wikipedia.org/wiki/C%C3%A2mera_escura>.
[^78]: Eniras en tiun liston de optikaj ludoj ankaŭ la stroboskopo. Vidu
  pli en <https://pt.wikipedia.org/wiki/Hist%C3%B3ria_do_cinema>.

Post du jaroj de la registro de la patento de la kinetoskopo okazis tiu,
kiu eniris en la kinan historion kiel la unua pagita montro de
mallonga filmo, en la Salon Grand Café, en Parizo, la 28-an de decembro
de 1895. Estis publika prezento de aparato inventita &mdash; kaj
patentita en la sama jaro en Francio &mdash; de la fratoj Lumière
(Auguste kaj Louis) nomita *kinematografo*, kiu, bazita sur la
kinetografo de la Edison-laboratorioj, funkciis kiel 3-en-1-a maŝino:
registris, relevis kaj montris la filmojn. Kun granda konatiĝo de la
gazetaro la konsiderita kiel unua kina seanco montris dek filmetojn de
ambaŭ fratoj, ĉiuj kun malpli ol unu minuto, mutaj (sonaj filmoj nur
aperis post 1927) kaj kiuj hodiaŭ estus konsideritaj kiel dokumenta
filmo. Inter la montritaj estis <cite>La Sortie de l’Usine Lumière à
Lyon</cite>, la unua de la seanco kaj ankaŭ la unua filmo de la kina
historio, kiu kunportis scenojn de homoj elirantaj el la fabriko de la
Lumière en Liono.

### IV.

Rigardante tiun epokon kaj la diversajn historiojn pri kie, kiel kaj kiu
kreis tiujn teknologiajn inventojn gravas kelkaj konsideroj pri
patentoj kaj intelekta propraĵo. La unua el tiuj estas, ke la telefono,
la radio, la gramofono, la fotografio kaj la kino estis inventaĵoj
kreitaj «sur la ŝultroj de gigantoj», kiel diras la esprimo atribuita al
la franca Bernardo de Chartres en la 12-a jarcento kaj popularigita de
Isaac Newton en 1675. Diri tion montras, ke ili estis inventaĵoj grande
ebligitaj per aliaj kreaĵoj &mdash; teknikaj aparatoj, ideoj kaj
mekanismoj, kiuj ne venis al ni, ĉar perdiĝis pro la manko de resursoj de
tiuj inventistoj por fari registron, kiu daŭru. Aŭ tiam estis kuplitaj
al aliaj ideoj de tiuj, kiuj havinte pli da teknikaj kaj financaj ebloj,
industrie cirkuligis tiujn inventaĵojn.

La dua konsidero estas, ke, speciale en tiu momento, estas multaj
malakordoj pri kiu fakte inventis la komunikajn, registrajn kaj
reproduktadajn teknologioj identigitaj ĉi tie. La telefono ekzemple jam
havis tre proksiman antaŭaĵon ĉirkaŭ 1860, dek ses jaroj antaŭ la
patenta registro de Graham Bell; ĝi estis speco de «parolanta telefono»
disvolvita de la itala loĝinta en Usono Antonio Meuci, kiu eĉ laboris
kun Bell kaj registris sian inventaĵon en 1871, dum la germana Johan
Philipp Reis, en 1861, kaj la usona Elisha Grey, en la sama 1876 de la
patento de Bell, ankaŭ laboris kun similaj prototipoj. Du jarojn antaŭe
ol la unua transsendo per hercaj ondoj de la itala Marconi, en 1893,
brazila pastro nomita Roberto Landell de Moura faris, en Porto-Alegro,
similajn eksperimentojn de transsendo de voĉo per ondoj, kio estis
oficiale konfirmita kaj dokumentita nur en 1900, jam post la patento de
Marconi. El la kelkaj antaŭaĵoj de la fonografo kaj la gramofono estas
unu speciale tre proksima, nomita *paleofono*, kiu estis registrita de
la franca Charles Cros en lia lando en la sama jaro de registro farita
de Edison en Usono. La diskuto pri la kina invento inter la Lumière,
filoj de eta franca negocisto de Liono, kaj Edison ankoraŭ hodiaŭ naskas
malkonsentojn, ĉar ili ambaŭ faris, en la sama periodo, kelkajn
filmojn.

Ĉi tiuj kaj aliaj similaj tiamaj historioj montras al ni, ke speciale
Graham Bell, Thomas Edison kaj Guglielmo Marconi estis negocistoj kaj
rapidaj patentintoj, kiuj sciis antaŭvidi la eblojn de profitdonajn
negocojn per la inventaĵoj, kiujn ili registris. Per iliaj patentoj, ili
klopodis jure garantii la produktadan kaj uzadan ekskluzivecon de
produktoj, kiujn ili ne necese inventis, sed kiuj, per la produktadaj
strukturoj (aŭ la kontaktoj) jam bone estigitaj, pliigis ilian
cirkuladon per la amasproduktado kaj la amasega distribuo kiel varo. Ili
celis la regajnon de siaj investaĵoj en esplorada kaj disvolvada
strukturo, tio veras, sed ankaŭ la garantion konservi siajn lukrojn dum
multe da tempo &mdash; kio, post la registro de patentoj, fakte okazis.

Ekzemple Graham Bell. De skota familio, kiu laboris en la fako antaŭe
promesplena de la retoriko, Alexander translokiĝis al Kanado en la
komenco de sia plenkreskula vivo kaj faris karieron en Usono kiel
inventisto kaj negocisto; li estis unu el la fondintoj de American
Telephone and Telegraph Company (AT&T), unu el la plej grandaj
telefoniaj firmaoj (poste Interreta kaj ankaŭ de kabla televido) de
Usono en la 20-a jarcento. Thomas Edison, kiu laboris kun Graham Bell,
estis teknologia negocisto, financita de gravaj homoj kiel Henry Ford
kaj Harvey Firestone, kreinto de laboratorio de produktado de
inventaĵoj, kiu iĝis General Electric, unu el la grandaj industriaj
grupoj de la planedo ankoraŭ hodiaŭ. Post la registro de la radia
patento en Anglio en 1896, Marconi kreis Wireless Telegraph & Signal
Company en tiu lando, poste transformita en Marconi Co., firmao kiu
estis unu el la plej gravaj de la britaj telekomunikadoj en la unuaj
jardekoj de la 20-a jarcento.

Malsame ol Graham Bell, Edison, Marconi kaj ankaŭ ol la Lumière, kiuj
jam en tiu epoko havis strukturon por patenti kaj komenci produkti siajn
inventaĵojn en pli granda skalo, Meucci, Landell de Moura, Cros kaj
aliaj gravaj homoj ne tiom memoritaj hodiaŭ estis inventistoj, kiuj, sen
multaj resursoj por produkti kaj disputi en la jam tiam forta patenta
*merkato*, ne transformis siajn ideojn en vendeblajn produktojn. Meucci
ekzemple estis elmigrinto; li naskiĝis en Italio, loĝis dum dek kvin
jaroj kun sia edzino kaj familio en Havano, Kubo, kie estas registroj,
ke li inventis la *parolantan telegrafon* jam en 1849 per elektroŝoka
maŝino[^79]. En 1850, per iom da mono konservita, li elmigris al Usono kun
la celo vivi el siaj inventaĵoj &mdash; en tiu epoko la maljuna nacio
plifortiĝis kiel granda loko de pilgrimado por inventistoj kaj
negocistoj, kiuj volis fari karieron per siaj inventaĵoj. Meucci fondis
fabrikon de kandeloj, dungis aliajn elmigrintajn samlandanojn,
enmiksiĝis en la politiko de sia lando &mdash; Giuseppe Garibaldi,
gvidanto de la unuiĝo de Italio, laboris en lia fabriko kaj luis lian
domon dum kvar jaroj &mdash; fiaskis, fondis alian firmaon, nun bazita
sur sia *parolanta telegrafo*, nomita Telettrofono Company, kiu
eĉ registris lian inventaĵon en 1871, kvin jarojn antaŭ la telefono de
Bell. Sed, sen tiom da resursoj kaj politika povo kiel Bell, Meucci
malvenkis kontraŭ li la jurajn disputojn rilatajn al patentoj kaj ne
atingis disvolvi pli sian inventaĵon[^80].

[^79]: Pli da detaloj pri Meucci kaj la fontoj de informo alportitaj ĉi
  tien en <https://en.wikipedia.org/wiki/Antonio_Meucci>.
[^80]: En eta danko malfrua, en 2002, la U. S. House of Representatives
  (ekvivalenta al la brazila Deputitarejo) omaĝis al Meucci en rezolucio
  (<https://www.congress.gov/bill/107th-congress/house-resolution/269>)
  pro aparteno al disvolvo de la telefono, malgraŭ ke ĝi ne specifis
  kiun kaj estas diversaj diskutoj, pri kiu fakte unue inventis la
  telefonon.

En la antaŭurbo de la mondo de la tiamaj patentoj (kaj ankoraŭ hodiaŭ)
la katolika brazila pastro Landell de Moura faris testojn, multfoje
sole, en siaj paroĥejoj en Porto-Alegro, San-Paŭlo, kun la telegrafo kaj
tio, kio estus la radio en la sama periodo de Marconi en Italio. Estis
nur en 1900, en San-Paŭlo, kiam Landell de Moura atingis fari registron
akceptitan per la tiamaj procedoj, havinte atestantojn kaj estinte
dokumentita per la *Jornal do Commercio*[^81]. En la sekva jaro akiris
unuan brazilan patenton por tio, kion li nomis «aparaton por la fonetika
distanca transsendo, drata aŭ sendrata, trans la spaco, la tero kaj la
akva elemento». Kun ĝi li vojaĝis la sekvajn jarojn al Eŭropo kaj Usono,
kie, en 1904, ankaŭ lasis patentojn de «onda transsendilo», «sendrata
telegrafo» kaj «sendrata telefono», kun iom da influo. Intertempe li
revenis al Brazilo en 1905, kie li daŭrigis siajn eksperimentojn, sed,
sen subteno de la Eklezio, de la komercistoj aŭ de la lokaj regantoj,
li ne plu disvolvas siajn memlernintajn esplorojn; Marconi, Bell kaj
aliaj, en Eŭropo kaj en Usono, daŭris[^82].

[^81]: Jornal do Commercio, 10-an jun. 1899, p. 3. Fonto:
<http://landelldemoura.com.br/>.
[^82]: Ĉi tiuj informoj pri Landell de Moura estas bazitaj sur
<https://pt.wikipedia.org/wiki/Roberto_Landell_de_Moura>.

La 30-an de aprilo de 1877, ok monatojn antaŭe, kiam Thomas Edison
registris la patenton de la fonografo en Usono, la franca bohemia
verkisto kaj inventisto Charles Cros deponis fermitan koverton en la
francan Akademion de Sciencoj kun artikolo pri «Procedo de registrado
kaj ludado perceptitaj per la orelo». Estis la sama funkcimaniero kiel la
aparato de Edison, kiu konis la onidirojn pri la inventaĵo de Cros[^83].
Sed al la franca mankis tio, kio en la alia flanko de la Atlantiko la
laboratorio en Menlo Park abunde havis: teknikajn kaj financajn
kondiĉojn por la praktika realigo de la ideo. Tial ankaŭ la fakto, ke la
fonografo, unu monaton post la publika prezento kaj la registro de
Edison, komencis esti amase produktita, dum la *paleofono*, inventaĵo de
Cros, estis forgesita. Sen kondiĉoj laŭleĝe pretendi ian honoron pro la
ideoj la franco ne atingis vidi la transformojn, kiujn la riĉa
biblioteko de aŭdaĵoj, kiujn li antaŭvidis, faris en la mondo; li mortis
en 1888, 45-jara.

[^83]: Kittler, <i lang="lt">op. cit.</i>, p. 47.

Inter ĉiuj tiuj amerikaj kaj eŭropaj blankaj homoj &mdash; kaj ĉi tie
ankaŭ estas seksa, etna kaj devena distingo de tiuj, kiuj *restis en
la historio*, kaj tiuj, kiuj estis forigitaj aŭ ne cititaj en tiuj
registroj &mdash;, Louis Daguerre eble estis stranga okazo por la afero
de la intelekta propraĵo. Partnero de Joseph Niépce, al kiu oni atribuas
la unua «fotografo de la vivo», nomita *heliografio*[^84] kaj prezentita
almenaŭ unu jardekon antaŭe, Daguerre montris sian inventaĵon al la
Franca Akademio de Sciencoj en 1839. La franca ŝtato akiris la
dagerotipian patenton kaj, tuj poste, igis ĝin publika havaĵo,
«malfermita por la tuta mondo»[^85]. Tiu ago, malofta inter la
teknologioj de komunikado, registrado kaj ludado ĉi tie cititaj,
faciligis, ke estu vera <i lang="fr">daguerréomanie</i> en Francio, kun
granda nombro de dagerotipiistoj ankaŭ en aliaj landoj; «estis dek mil
el ili en Usono en 1853, inter ili Samuel Morse, kaj en Britio estis
ĉirkaŭ du mil registritaj fotografistoj en la 1861-a censo»[^86]. Aliaj
procedoj de fotografa produkto pli malmultekostaj kaj facile
reprodukteblaj, kiel la kalotipio de Henry Fox Talbot, kaj poste la rula
filmo de Eastman kaj de Kodak (ambaŭ registritaj kiel privataj
patentoj), igis la dagerotipion procedo arkaika kaj kiu ne atingis esti
disvolvita amase post 1870.

[^84]: Joseph kovris stanan breton per blanka bitumo de Judeo, kiu havis
  la kvaliton hardiĝi, kiam ĝi estis atingita per la lumo. En la
  neinfluitaj partoj la bitumo estis eltirira per solvaĵo de lavenda
  esenco. En 1826 elmetinte unu el tiuj bretoj dum proksimume ok horoj
  en sia fabrikita senluma kamerao li akiris bildon de la korto de sia
  domo. «Heliografio» signifas gravuron per suna lumo. Fonto: <https://en.wikipedia.org/wiki/Nic%C3%A9phore_Ni%C3%A9pce>.
[^85]: Briggs; Burke, <i lang="lt">op. cit.</i>, p. 166.
[^86]: <i lang="lt">Ibidem</i>, p. 167.

### V.

Antaŭ la solidiĝo de la intelekta propraĵo en la 19-a jarcento indas
refari la demandon de la komenco de ĉi tiu ĉapitro alie: ĉu la enkonduko
de juraj elementoj regulantaj la proprietojn de la ideoj limigis aŭ
instigis la scian, kulturan kaj teknologian progreson? Ebla respondo
estus diri, ke instigis: sufiĉas vidi la kvanton de inventaĵoj
popularigitaj en tiu periodo kaj la grandaj transformoj, kiujn ili
kaŭzis en la socio. Ankaŭ akcepteblas diri, ke la ŝanĝoj kaŭzitaj
per la aŭtorrajtaj leĝoj de tiu epoko, ekzemple, favorigis, ke multaj
artistoj komencu povi vivi de siaj laboroj kaj ne restu plu sub la
kompato de monopoloj kaj reĝaj interesoj, kio sekurigis rajtaron kaj
donis al ili garantiojn, ke niveligis ilin, en kelkaj okazoj, al aliaj
tiamaj profesiaj laboristoj, aldone al doni pli &mdash; almenaŭ teorie
&mdash; eblojn de libereco por la kreado, sen la religia aŭ ŝtata
kontrolo.

Alia ebla respondo estas diri, ke la juraj reguligantaj mekanismoj de la
intelekta propraĵo limigis la progreson kaj la scia aliro. Antaŭ
datumbazo preskaŭ senfina kaj de libera aliro, la publika havaĵo de
ideoj kaj informoj komencis havi siajn ideojn malfermitaj en malgrandaj
feŭdoj, pli grandaj aŭ pli malgrandaj laŭ la ekonomiaj ebloj kaj la
politikaj-instituciaj aranĝoj de tiu, kiu ilin havas. En unua momento la
fermigo de kelkaj ideoj de la publika havaĵo estas malmulte daŭra; la
komencaj aŭtorrajtaj leĝoj estigis 14 jarojn post la eldono kiel la
periodon de komerca ekskluziva ekspluatado de la verko, pro repagi
al la aŭtoro (aŭ al la perantoj, kiuj financis lian produktadon) la
investaĵon akiritan. Sed por ĉiu nova teknologia aparato &mdash; kaj la
lukra mondo malfermita de tiuj &mdash; tiu periodo iĝas pli daŭra: 40,
50, 70, 120 jaroj post la eldono aŭ 70 jaroj post la aŭtora morto, kiel
estiĝis en la aŭtorrajtaj leĝoj en Brazilo kaj en multaj landoj de la
mondo en la 20-a jarcento[^87].

[^87]: Listo de la periodo, post kiu verko eniras en publikan havaĵon
  konsulteblas en Vikipedio:
  <https://pt.wikipedia.org/wiki/Dom%C3%ADnio_p%C3%BAblico>.

Uzita kiel ideologia pravigo de reĝoj, nobelaro kaj Eklezio por la
reguligo de la publikigo de ideoj, la cenzuro cedas lokon, post la 18-a
kaj 19-a jarcentoj, al la merkato kaj al la libera konkurado. Ne plu
estas pro trakti malpermesitaj temoj laŭ la cenzuristoj, ke la cirkulado
de ideoj necesas esti kontrolita; estas por ke homo povu vivi de (kaj
lukri per) siaj inventaĵoj, ekskluzive kaj sen konkurado kun alia
individuo (aŭ firmao). Por tio, la leĝoj; por ilin plenumigi, la Ŝtato.
En kunteksto de plirapidiĝo de la cirkulado de informoj, kaj kun la
ega eblo reprodukti ideojn per la cititaj teknologioj, estis tiel, kiel
la okcidenta kapitalisma socio sin organizis post tiam, kaj ĝis
hodiaŭ, rilate al la produktado kaj cirkulado de ideoj.

Sed la maniero administri la proprieton de ideoj post la nocio de
intelekta propraĵo kaj ĝiaj branĉoj (aŭtoraj rajtoj kaj industria
proprieto) ne estis la sola ĝis tiam. Idea fluo ankaŭ naskita en la dua
duono de la 19-a jarcento, la anarkiismo neis ekde sia komenco la
aŭtorrajtojn; la frazo «la proprieto estas ŝtelo!», prenita de teksto de
Pierre-Joseph Proudhon en 1840 &mdash; unu jaron post la patento de la
dagerotipio estis igita publika havaĵo en Francio &mdash;, estis
aplikita komence al la materia proprieto, sed ne pro tio ĉesis koncerni
ankaŭ la intelektan propraĵon, kiel granda parto de la verkoj (precipe
presitaj) anarkiismaj ekde tiam lasis klare klarigis en siaj unuaj
paĝoj kun mesaĝoj kiel «sen rajtoj rezervitaj», «ĉiuj rajtoj
forlasitaj», inter aliaj eksplicitaj mesaĝoj neantaj la ekziston de
aŭtorrajtoj. Ili faris tion klare kaj kohere kun siaj principoj: la
ideoj, kiel la teroj, devas esti liberaj, cirkuli libere, sen limigoj
nek de reĝaj aŭ religiaj monopoloj nek de juraj reguloj kreitaj de la
Ŝtato por kontroli la merkatan konkuradon. La maniero ekzameni tiujn
filozofajn principojn en la praktiko de la ĉiutaga supervivo en planedo
pli kaj pli proprigita per la kapitalismo kaj ĝia privata proprieto
naskigas nuancojn kaj diversajn diskutojn ĝis hodiaŭ. Rimarkeblas, ke,
konsiderita de multaj naiva, la perspektivo de manko de proprieto, kiun
la anarkiismo defendis, havante la homan memstaron kiel centran akson de
siaj zorgoj, trovos reagon en kodumuloj kaj influos la konstruon de
Interreto &mdash; kaj de la libera programaro &mdash; jardekojn poste. Ĝi
estos ideo kaŝe ĉeesta kaj influa en la socio ĝis hodiaŭ, kiel la
sekva ĉapitro montras.

Ankaŭ frukto de la 19-a jarcento, la socialismo traktis la aŭtorrajtojn
malsame. Kaj en la Soveta Unio (URSS) kaj en Kubo validis la
aŭtorrajtaj leĝoj akordigitaj en Berno, kiam en 1917 kaj 1959
respektive ekis la sociaj kaj sovetaj revolucioj. En 1928 la
aŭtorrajta leĝo en la eŭropa lando, de romia-franca influo, estis
ŝanĝita kaj la periodo de valido de la rajtoj (heredaj) mallongigita al
interspaco pli proksima al la komencaj leĝoj de la 18-a jarcento: al 25
jaroj post la eldono de verko aŭ 15 post la aŭtora morto. Tiel restis
ĝis 1973, kiam la Soveta Unio subskribis la internaciajn traktatojn de
intelekta propraĵo kaj ekadoptis la modelan templimon de 70 jaroj post
la aŭtora morto kiel oficiala por valideco de la heredaj rajtoj; la
moralaj, kiuj parolas pri la aŭtoreca agnosko, estas porĉiamaj kaj
neforigeblaj. Tiu templimo estas ankaŭ hodiaŭ aplikita al Rusio kaj por
eksaj sovetiaj respublikoj kiel Ukrainio, Kartvelio, Estonio, Litovio,
Moldavio, inter aliaj[^88]. En Kubo okazis simila movado: la leĝo estis
modifita en 1977 kaj mallongigis la templimon de la aŭtorrajtoj al 25
jaroj post la aŭtora morto, kio restas ĝis 1994, kiam Kubo subskribis
internaciajn traktatojn kaj komencis adopti la periodon de 50 jaroj post
la aŭtora morto, kio restas en 2022. En Ĉinio kaj Nord-Koreio, aliaj
landoj kiuj adoptis la socialisman reĝimon en la 20-a jarcento, estas
longa socia kolektivisma tradicio, kiu igas, ke la nocioj de kopio,
aŭtoreco kaj intelekta propraĵo estu komprenitaj per malsamaj manieroj,
kiuj estos traktitaj en la 6-a ĉapitro
[«Kolektiva kulturo»](#kolektiva-kulturo).

[^88]: Kiel montras la listo de la antaŭa noto.

Fine imageblas, ke en cirkonstancoj de vasta cirkulado de teknologiaj
aparatoj de reproduktado kaj komunikado, terminoj kiel «plagiato»,
«kopio» kaj «kreaĵo» akiris aliajn signifojn. Se la romantikismo fiksis
en la 19-a jarcento la percepton, ĝis hodiaŭ superreganta, de la aŭtoro
kiel kreiva solecema geniulo, laŭrajta *proprietulo* de kulturaj
havaĵoj, la komenco de la 20-a jarcento regos tiun nocion preskaŭ sankta
de kreaĵo. Artistoj kaj kreantoj ĝenerale tordos kaj renversos la nocion
de plagiato kaj uzos ĝin kiel metodon de arta produktado kaj strategion de
opozicio al la intelekta propraĵo &mdash; kaj sekve al la kapitalismo
mem. La kopio de la kopio de la kopio naskigis aliajn manierojn sin
esprimi, kiuj siavice estis rekombinitaj kaj formis la bazon de multaj
kulturaj havaĵoj tre konataj en la 20-a jarcento kaj ĝis hodiaŭ.

