<section class="citas">
<i>La ideoj perfektiĝas. La signifo de la vortoj partoprenas en la
perfektiĝado. La plagiato necesas. La progreso signifas tion. Ĉi tiu
utiligas frazon de aŭtoro, uzas lian esprimon, forigas malĝustan ideon
kaj anstataŭigas ĝin per ĝusta ideo.</i>

<div class="right-align">
Grafo de Lautréamont (Isidore Lucien Ducasse), <cite>Poesias</cite>, 1870
</div>

<br>

<div class="right-align">
<i>Rajto esti tradukita, ludita kaj deformita en ĉiuj lingvoj.</i>
</div>

<div class="right-align">
Oswald de Andrade, <cite>Serafim Ponte Grande</cite>, 1933
</div>

<br>

<i>En la realo necesas forigi ĉiujn restaĵojn de la nocio de persona
proprieto en tiu ĉi kampo. La apero de la jam malmodernaj novaj necesoj
anstataŭ «inspiritaj» verkoj. Ili iĝas obstakloj, danĝeraj kutimoj. Ne
temas, ĉu vi ŝatas ilin aŭ ne. Ni devas superi ilin. Oni povas uzi
ĉiun ajn eron, ne gravas, el kie ili estas prenitaj por krei novajn
kombinaĵojn. La malkovroj de la moderna poezio rilataj al la analoga
strukturo de la bildoj montras, ke kiam du aĵoj estas kunmetitaj, ne
gravas, kiom disaj ili povas esti de siaj originaj kuntekstoj, ĉiam
estiĝas rilato. Limigi sin al persona aranĝo de vortoj estas nur
konvencio. La reciproka enmiksiĝo en du mondoj de sensacioj, aŭ la
kunveno de du sendependaj esprimoj, anstataŭas la originalajn erojn kaj
produktas sintezan organizadon pli efika. Oni povas uzi ĉion ajn.</i>

<div class="right-align">
Guy Debord; Gil Wolman, <cite>Um guia para os usuários do detournamènt</cite>, 1956
</div>

<br>

<div class="right-align">
<i>La rimedo estas la mesaĝo.</i>
</div>

<div class="right-align">
Marshall McLuhan, <cite>Understand Media</cite>, 1964
</div>

<br>

<i>En hodiaŭaj retoj de multaj ĝenroj, fluo de datumoj algoritme
formaligita povas salti al ĉi ĉiuj. El ĝenro al ĝenro, ĉia modulado iĝis
ebla: en lumaj organoj, akustikaj signaloj kontrolas optikajn signalojn;
en la komputila muziko signaloj en maŝinlingvo kontrolas akustikajn
signalojn; en voĉkodiloj la akustikaj datumoj mem kontrolas aliajn
akustikajn datumojn. Eĉ la diskludistoj de Novjorko kreis per okultaj
bildoj de Moholy-Nagy la kutimon de la
<i lang="en" style="font-style: normal;">scratch</i>-muziko.</i>

<div class="right-align">
Friedrich A. Kittler,
<cite>Gramofone, filme, typewritter</cite>, 1986
</div>
</section>

### I.

La periodo ekde la komenco de la 1900-aj jaroj ĝis la 1970-a jardeko estas
tiu de solidiĝo kaj populariĝo de la diversaj teknologioj de komunikado
kaj ludado patentitaj de homoj kiel Graham Bell, Edison,
Marconi, Eastman kaj Lumière en la dua duono de la 19-a jarcento. La
fino de la «Presa Imperio», kiel ni vidis, donas lokon al maniero
percepti, kiu ĉesas esti nur bazita sur la simbola perado de la
alfabeto, la vortoj kaj la presitaj publikaĵoj kaj komencis esti forte
*sentita*, per okuloj kaj oreloj, pere de la teknikaj aparatoj de
ludado, kiel la gramofono (poste viktrolo, diskoturnilo kaj
kasedoludilo), la filmo (kaj la kino), la televido, la video (kaj la
vidbendo). Kaj kiuj finas per la aparato, kiu unuigas ĉiujn ĉi tiujn en
nur unu, la komputilo, popularigita post la fino de la 1970-aj jaroj per
la ekkreskado de la cifereca nanoteknologio kaj de la PK-j (persona
komputilo) produktitaj en la regiono, kiu estris la mondan konduton
pri la novaj teknologioj en la sekvaj jardekoj, la Silicia Valo, en
Kalifornio, Okcidenta Marbordo de Usono.

Grandegaj industrioj estis kreitaj bazitaj sur la ludado de bildoj
kaj sonoj en la 20-a jarcento. La kino, la radio, la televido, en ĉi tiu
sinsekvo, iĝis iloj de amasaj komunikado kaj ludado kun monda
atingopovo. La konservado de informo iĝis la normo de mondo regita per
la merkata konkurado; la ideoj &mdash; literaturaj, muzikaj, sciencaj
&mdash; estis pakitaj, komercitaj kaj iĝis proprieto, elemento per kiu
la kapitalismo organizas sin kaj antaŭenirigas la tiel nomitan
*progreson*, kiu pli kaj pli dependis de la teknologioj kaj la juraj
procedoj kreitaj por reguligi la interŝanĝojn kaj la publikigon de
ideoj: la patentoj kaj la aŭtorrajto.

Jam en la unuaj jardekoj de la 1900-aj jaroj, la bazoj de la leĝaroj de
intelekta propraĵo, kiuj ankoraŭ hodiaŭ validas, estis difinitaj en
internaciaj akordoj kiel tiu de Berno aŭ tiu de Parizo. Ili igis normala
la ideon de individua kreaĵo sub la mito de la romantika «geniulo»,
bildo reprezentita de tia subjekto, kiu, ŝlosita en sia ĉambro, precipe
sole, havas brilan ideon nur pere de siaj propraj referencoj kaj, kun
tiu ideo bone pakita kaj vendita kiel komercaĵo de peranto, akiros famon
kaj monon. Tio, kio ne enigis en tiun modelon, estis marĝenigita kaj
subpremita per punoj kaj monpunoj en la fino de longaj kaj multekostaj
procesoj komencigitaj de tiuj, kiuj havis kondiĉojn por dungi kaj
konservi advokatojn.

La solidiĝo de la aŭtorrajtaj lêgoj kaj de la nocio, ke la kulturaj
havaĵoj estas privataj proprietoj ankaŭ kaŭzas, aliflanke, rezistadan
movadon. En la ŝanĝo al la 20-a jarcento kaj dum la sekvaj jardekoj,
movadoj de protesto kontraŭ la aŭtorrajto multiĝis en arto kaj en
kulturo fare de la dubo, ke ideoj, sonoj, vortoj, bildoj kaj filmoj povu
esti posedo de iu kaj uzitaj nur per rajtigo de la tiel nomitaj
proprietuloj per iu financa pago. La enketoj proponitaj en la agoj de
tiuj artistoj kaj movadoj alportas, kiel rektan aŭ nerektan konsekvencon,
la proponon kontesti la ideon de kultura havaĵo kiel varo kaj la
konstruon de libera kaj komuna kulturo, kiu devus esti de ĉiuj, sen la
neceso, ke estu rajtigo por esti ĝuita, cirkuligita kaj reuzita.

Proprigante kelkajn ideojn, pakante ilin kaj vendante ilin kiel
fermitajn verkojn, la reĝimo de la intelekta propraĵo klopodis,
unuflanke, krei sistemon, kiu povu rekompenci kreantojn (aŭ iliajn
reprezentantojn) pro iliaj verkoj &mdash; kion ĝi fakte faris egaligante
artistojn al aliaj multaj profesiuloj per rajto havi dignan vivon
pere de iliaj kreaĵoj. Aliflanke, la intelekta propraĵo ankaŭ limigis la
ĥaosecon de la ideoj kaj fermis ilin en spacon, kie ĝi povis ĉerpi
ekskluzivajn profitojn de ilia posedo kaj kontrolo[^89]. En la plejparto
de la okazoj la celo proprigi ideojn kaj el tiuj eltiri resursojn estis
atingita.

[^89]: Nimus, <i lang="lt">op. cit.</i>, p. 27.

Sed ekzistis &mdash; kaj ankoraŭ ekzistas &mdash; kontesto, por ke la
aliro kaj la cirkulado restu pli grandaj ol la limigoj. En la stratoj de
la monda sudo la intelekta propraĵo kutime estis anstataŭita per la
libera disvastiĝo de ideoj, ofte komercitaj aparte de la jura sistemo
por instigi novajn kreaĵojn, kiuj disvastiĝas ĉien, kvankam iniciatoj en
la leĝaroj kaj en la propagando, per ŝtatoj kaj firmaoj, naskiĝis
klopodante la kontrolon kaj krimigon de tiuj praktikoj. Konscie kiel
kontesto al la artista tiama stato aŭ spontane kiel ĉiutaga kaj
kutima, la reproprigo de informoj kaj kulturaj havaĵoj jam ekzistintaj
por la disvolvo de novaj kreaĵoj forte pliiĝis en la 20-a jarcento eĉ
naskigante novajn movadojn, ritmojn, ritojn kaj verkojn en la plej
diversaj lokoj. Estas multnombraj ekzemploj en la arto kaj en la
kontraŭkulturo, ĉiuj prezentantaj iujn manierojn de ribelo kaj movantaj
la aĵojn el unu referenca sistemo al alia, kun (aŭ sen) ŝanĝo de
signifo. La amasaj komunikadaj, registradaj kaj ludadaj
teknologioj ludis gravan rolon en tiu disvastiĝo, plej ofte estiginte
novajn praktikojn de kreado, konsumo kaj disvastigo de kulturaj havaĵoj.
La kapitalismo ludis rolon eĉ pli grandan, estante
ankaŭ la maniero produkti, en kies kerno la teknologioj estas generitaj
kaj popularigitaj, kiel ankaŭ la «malamiko» adoptota, eksplice aŭ ne
tiom, per la kulturaj iniciatoj de konfronto bazitaj sur la remikso kaj
sur la reproprigo de signifoj.

### II.

La kontesto al la leĝoj regulantaj la intelektan propraĵon okazis ekde
la unua momento, ĵus post ilia solidiĝo en la okcidentaj ŝtatoj. Pro
kialoj tre malsamaj kaj foje kontraŭaj: anarkiistoj neante ĉiun ajn
specon de privata proprieto, eĉ la intelektan; socialistoj en la procezo
potencigi la kolektivan proprieton, inkluzive en la artoj, sub la
ŝtata administrado; liberalistoj emfazante la liberan merkaton, kiu
konsideris, ke la publika intereso havi aliron al kulturaj havaĵoj kiel
eble plej malmultekoste, povus havi pli da forto ol la aŭtorrajtoj; kaj
artistoj, de ĉiuj ideologioj, kontestante la tiaman staton de la
romantika kaj proprieta kreado kaj batalante por la libereco uzi ĉiun
ajn specon de verko sen neceso peti rajtigon al iu ajn.

Ankoraŭ dum la dua duono de la 19-a jarcento, samtempe kiam la multaj
inventaĵoj, kiuj iom post iom finigadis la libron kiel ĉefan modelon de
percepto de ideoj kaj realo, unu el la unuaj artistoj publike
kontestintaj la aŭtorrajton kaj la nocion de individua kreanta geniulo
estis la Grafo Lautreamont, naskita kiel Isidore-Lucien Ducasse en
Montevideo, en Urugvajo, en 1846, kaj ekde frue civitano de Parizo, en
Francio, kie li mortis en 1870, kun 24 jaroj. Lautreamont faris el sia
mallonga verko (kaj vivo) daŭran konteston al tio, kio en tiu epoko
estis instituciigita en la literaturo, kaj en la temoj kaj en la procedo
skribi &mdash; la uzo de ortografiaj eraroj, stilaj nedecaĵoj, la
plagiato kaj la ripeto de formuloj, kiuj igas, ke liaj verkoj estu, ĝis
hodiaŭ, adoritaj kaj malinklinaj al la klasifiko[^90]. <cite>La kantoj de
Maldoror</cite> (1869), lia plej fama kaj influa libro, raportas, en
siaj kantoj de poezio iam rakonta kaj iam lirika kaj absurda, sinsekvajn
violentojn, perversaĵojn, kruelaĵojn ĉirkaŭ la malkuraĝo kaj la homa
stulteco.

[^90]: En «O astro negro», prefaco de Cláudio Willer al Lautréamont,
  <cite>Os cantos de Maldoror</cite>.

Lia dua libro kaj lasta laboro, <cite>Poezioj</cite>, «malpli mirinda
kaj eĉ pli stranga»[^91], eldonita en la jaro de lia morto, tekste
kolektis aforismojn, maksimojn, poeziaĵojn, citaĵojn de grekaj poetoj kaj
iliaj samtempuloj en Francio, kiel Charles Baudelaire,
Blaise Pascal kaj Alexandre Dumas. En unu el la fragmentoj de la
publikaĵo, dividita en du partoj (kajeroj), li defendis la revenon de
senpersona poezio, skribita de ĉiuj, kiu referencu la kolektivajn
manierojn produkti en la mezepoka periodo, sed per inkoj de la moderna
industriiĝo de tiu periodo: «La ideoj perfektiĝas. La signifo de la
vortoj partoprenas en la perfektiĝado. La plagiato necesas. La progreso
signifas tion. Ĉi tiu utiligas frazon de aŭtoro, uzas lian esprimon,
forigas malĝustan ideon kaj anstataŭigas ĝin per ĝusta ideo»[^92].
Lautreamont, speciale en tiu fragmento de <cite>Poezioj</cite>[^93],
defias la miton de la individua kreivo &mdash; kiu ekde la komenco iris
tre kune kun la pravigo de la rilatoj de intelekta propraĵo parolante
pri moderna mondo, kie, supozeble, ne estus akceptitaj ideoj sen
posedanto. Lia ago rimarkigas pri la reproprigo de la kulturo kiel
sfero de kolektiva produktado, same kiel en la Antikva Epoko kaj parto
de la Mezepoko, sed «sen ĉesi agnoski la limigojn identigitajn de li
kiel artefaritajn, atribuitajn al la aŭtoreco per la reĝimo jam estigita
de intelekta propraĵo»[^94]. Lia «La poezio devas esti farita de ĉiuj,
ne de unu», estanta en <cite>Poezioj</cite>, antaŭvidas, apud alia
samtempa poeto, Stéphane Mallarmé, la modernan atenton de la supereco de
la teksto super la aŭtoro-leganto, «la transloko el la subjektiveco al
la interteksteco, kiu igas pensi pri la verko ne nur kiel dialogo inter
homoj, sed ankaŭ inter tekstoj»[^95]

[^91]: <i lang="lt">Ibidem</i>.
[^92]: <i lang="lt">Ibidem</i>.
[^93]: Guy Debord eĉ citas lin en la enkonduko de <cite>La
  socio de la spektaklo</cite>, sia plej konata verko, de 1967.
[^94]: Nimus, <i lang="lt">op. cit.</i>, p. 27.
[^95]: En Lautréamont, <i lang="lt">op. cit.</i>, p. 33, Willer, kiu
  bazas sin sur <cite>La filoj de la argilo</cite>, de la poeto kaj
  meksika diplomato Octavio Paz. La fokuso sur la teksto kaj ne tiam sur
  la aŭtoro estis populara en la studoj nomitaj
  <i lang="fr">écriture</i> en Francio post la 1950-aj, 1960-aj kaj
  1970-aj jaroj, kun filozofoj kiel Roland Barthes kaj Jacques Derrida.
  Barther, ne hazarde, estas la aŭtoro de
  <cite>La morto de la aŭtoro</cite>, de 1968, teksto, en kiu li indikas la
  malaperon de la figuro de la aŭtoro post la 19-a jarcento, estante la
  lingvo la vera aganto de la skribita erao, ne unu individuo. Martins,
  <i lang="lt">op. cit.</i>, p. 21.

Lautreamont ankaŭ uzis la plagiaton por komponi sian verkon. En
<cite>Kantoj</cite> estas diversaj fragmentoj, kiuj referencas la
aliajn[^96]. En <cite>Poezioj</cite> eblas eltrovi fragmentojn de
<cite>Pensoj</cite>, de la matematikisto Blaise Paskalo, kaj de
<cite>Maksimoj</cite>, de François de La Rochefoucauld, aldone de la
laboroj de verkistoj kaj filozofoj kiel Jean de La Bruyère, Luc de
Clapiers, Dante Alighieri, Immanuel Kant kaj La Fontaine[^97], inter
aliaj aŭtoroj, kiujn ni trovus, se ni ekzamenus ĝin pli detale. Krom tio, kaj
en maniero eĉ pli stranga komparita kun aliaj kontestintoj de la
aŭtorrajto kaj la aŭtoreco en la 20-a jarcento, ankaŭ la maniero de
cirkulado de la verko de Lautreamont montris ian konteston al la
tradicia merkato de publikaĵoj: la du broŝuroj de <cite>Poezioj</cite>
cirkulis sen prezo tra la stratoj de Parizo, per modelo «pagu kiom vi
volas», kiu estis, preskaŭ unu jarcenton poste, disvastigita per la
punka kulturo de influo anarkiisma kaj de aliaj movadoj en la
kontraŭkulturo, kiuj ne rekonas la sistemon de intelekta propraĵo kiel
rajta por siaj kreaĵoj.

[^96]: Unu precipe videbla, laŭ Willer, estas la morto de la infano
  antaŭ la patroj en la 1-a kanto, fragmento plagiatita de la poemo *La
  reĝo de la alnoj*, de Goethe, kun Maldoror anstataŭ la Arbara Spirito.
[^97]: Willer, Prefaco, en Lautréamont, <i lang="lt">op. cit.</i>; kaj
  <https://en.wikipedia.org/wiki/Comte_de_Lautr%C3%A9amont>.

La libroj de Lautreamont estas ĉi tie cititaj, nur ĉar ili iĝis
referenco de renverso por multaj el la eŭropaj avangardoj en la komenco
de la 20-a jarcento, kiuj tiel konservis kaj popularigis ilin. La
francaj surrealistoj Louis Aragon kaj André Breton metis lin en siajn
templojn de malbenitaj aŭtoroj, ĉe la flanko de Baudelaire kaj Arthur
Rimbaud, kaj republikigis <cite>Poezioj</cite>, en 1919, post malkovri
unu el la malmultaj ekzempleroj de la verko en la Nacia Franca
Biblioteko. Al Lautreamont Aragon kaj Breton ankaŭ dediĉis numeron
de la surrealisma revuo, kiun ili redaktis, <cite>Le Disque Vert</cite>,
en 1925, titolita «Le Cas Lautréamont» («La kazo Lautreamont»). Ambaŭ
iniciatoj igis la verkon de la poeto konata por novaj publikoj. Antaŭ la
revuo, unu el la pioniroj de la modernismo en Usono, Man Ray, en 1920,
faris verkon identigitan al dadaismo nomitan <cite>L’Énigme d’Isidore
Ducasse</cite> (La enigmo de Isidore Ducasse). Ĝi estas fotografaĵo, en
kiu videblas iun aferon kaŝitan en felta bruna peco ligita per sisala
ŝnuro, inspirita per fragmento de <cite>La kantoj de Maldoror</cite>:
«<i lang="en">Beautiful as the chance meeting, on a dissecting table, of
a sewing machine and an umbrella</i>»[^98].

[^98]: Disponebla en
  <https://www.wikiart.org/en/man-ray/the-enigma-of-isidore-ducasse-1920>.
  «Bela kiel la hazarda renkonto, sur dissekcia tablo, de kudrada maŝino
  kaj ombrelo».

La verko de Man Ray estas tio, kion la franca artisto (kaj sia amiko)
Marcel Duchamp nomis en 1913 <i lang="en">ready-made</i>, praktiko kiu
konsistas el preni aĵojn, rilate al kiuj oni estis indiferenta kaj
redoni al ili kuntekston por disloki iliajn signifojn. En la 1910-a
jardeko ambaŭ produktis verkaron, kiu iel estis pionira en la mondo de
la okcidenta arto uzante kaj eksplicante la rekombinadon de informoj kaj
aliaj elementoj por krei novan verkon. Kiel Duchamp rakontas:

> En 1913 mi havis la gajan ideon fiksi radon al kuireja tabureto kaj
> vidi ĝin turniĝi. Kelkajn monatojn poste mi aĉetis malmultekostan
> reproduktaĵon de pejzaĝo de ventra nokto, kiun mi nomis «Apoteko» post
> aldoni du malgrandajn punktojn, unu ruĝan kaj unu flavan, al la
> horizonto. En Novjorko en 1915 mi aĉetis en vendejo de iloj neĝan
> ŝovelilon, en kiu mi skribis «Antaŭ la rompita brako». Estis ĉirkaŭ
> tiu epoko, kiam la vorto «<i lang="en">ready-made</i>» venis al mia
> menso por nomi tiun manieron de esprimo. Unu afero, kiun mi multe
> deziras klarigi estas, ke la elekto de tiuj
> «<i lang="en">ready-mades</i>» neniam estis diktita pro la estetika
> plezuro. Tiu elekto estis bazita sur reago de vida indiferenteco kun
> samtempe tuta manko de bona aŭ malbona gusto... Fakte tuta anestezo.
> [...] Alia aspekto de la «<i lang="en">ready-made</i>» estas ĝia
> neebleco esti unika. La repliko de unu «<i lang="en">ready-made</i>»
> enhavas la saman mesaĝon; fakte preskaŭ neniu el la ekzistantaj
> «<i lang="en">ready-mades</i>» estas originala laŭ la konvencia
> senco.[^99]

[^99]: Originale publikigita en la retejo Iconoclast:
[www.13am.net/iconoclast](https://www.13am.net/iconoclast/), traduko
el la angla de Ricardo Rosas (Arquivo Rizoma) en la cifereca libro
<cite>Recombinação</cite>, p. 17.

En 1917 eliginta pisejon el la necesejo, subskribinta kaj metinta ĝin
sur soklo en artgalerio, lia ĝis hodiaŭ plej konata verko[^100], Duchamp
ankaŭ formetis la signifon de la funkcia interpreto ŝajne konkludita el
la aĵo. Kvankam tiu signifo ne tute malaperis, ĝi estis metita apud alian
eblon &mdash; la signifon kiel artaĵo. La pisejo en galerio instigis
necertecan kaj retaksan momenton kaj kontestis denove la romantikan
esencismon, kiu metas la artaĵon kvazaŭ produkton de dia naturo kaj kiu
privilegias la kreeman individuan laboron. La pisejo kaj la bicikla rado
estis industriaj produktoj faritaj per maŝinoj, kolektitaj en diversaj
lokoj kaj resignifigitaj de Duchamp; kiam metitaj en artejoj kiel
galerioj, ili ne povis esti patentitaj kiel aliaj tiamaj verkoj
(pentraĵoj, skulptaĵoj, fotografaĵoj), ĉar la *aĵo* mem ne havis
valoron, ĝi povis esti forĵetita kaj alia prenus sian lokon en nova
ekspozicio sen signifa perdo. Tio, kio estis valora, estis la ideo
proponita per la sperto vidi aĵon de ĉiutaga uzo kiel pisejon &mdash; aŭ
feltan pecon kaŝantan aliajn aĵojn, aŭ biciklan radon &mdash; en artgalerio.
Naskiĝis tiel la koncepta arto, el sia libera deveno, kontraŭa al la
aŭtorrajtoj kaj kritika al la proprieto de la ideoj, sed kiu ankaŭ
povis, en la sekvaj jardekoj, transformi sin en varon kaj vizitadi
muzeojn kaj galeriojn registrita kun aŭtorrajtoj de miloj da dolaroj.

[^100]: Farita de Duchamp en 1971 sendinte la aĵon al la salono de la
  Asocio de Sendependaj Artistoj de Novjorko kun la kaŝnomo R. Mutt.

Duchamp kaj Man Ray apartenis al unu el la eŭropaj avangardoj de tiu periodo, la
dadaismo, kiu, disvastigita en Svislando, Usono, Franco kaj Nederlando,
sed ankaŭ Kartvelio, Japanio kaj Rusio[^101], disvolvis grandan parton
de siaj laboroj per la kontesto al la ideo de la artisto kaj de la
disiĝo inter arto kaj vivo. La dadaismo, kiu estas vorto kun multaj
devenoj, sed kiu en ĉiuj ili signifas «nenion», multe ŝuldas al alia
nocio de Duchamp: la kontraŭarto, pensita de li bazante sin sur la
<i lang="en">ready-mades</i> en 1913 kaj adoptita de multaj
kontraŭkulturaj movadoj de la 20-a jarcento[^102] kiel metodo inciteti
la bazojn de tiu, kiu estis tradicia arto &mdash; inter ili, temoj kiel
la aŭtoreco, la beleco kaj la intelekta propraĵo. Diversaj verkoj de la
dadaismo dubis pri la ideo de la kreiva solecema geniulo kaj esprimis
ribelon kontraŭ la kapitalismaj principoj ene de la artaj valoroj. La
rumano Tristan Tzara, unu el la ĉefaj homoj de la dadaismo, montris iom
da tiu ribelo ofte uzante la hazardecon, la <i lang="en">nonsense</i>
[sensenceco] kaj la sorton por produkti verkojn, kiuj koliziis kun la
tiama stato de la arta mondo de tiu epoko, kiel en «Por fari dadaisman
poemon», en 1920:

[^101]: Disponeblas multaj detaloj pri dadaismo en <https://en.wikipedia.org/wiki/Dada>.
[^102]: El la movadoj KOBRA kaj Literista Internacio, proksimaj al la
  surrealismo kaj al dadaismo de la unua duono de la 20-a jarcento, al
  Provo (Nederlando), al la punko kaj al la novismo, en la dua duono,
  kaj ankaŭ al la situaciistoj kaj al la <i lang="en">mail art</i> ankaŭ
  ĉi tie cititaj. Tiuj *kontraŭartaj* movadoj estas detalitaj en bela kaj
  malmulte konata verko nomita <cite>Assalto à cultura: utopia subversão
  guerrilha na (anti)arte do século XX</cite> [La rabo al la kulturo:
  utopiaj movadoj el la literismo al la klasmilito], de Steward Home.

> Prenu tondilon.

> Elektu en la gazeto artikolon kun la longeco, kiun vi deziras doni al
> via poemo.

> Tondu la artikolon.

> Tuj atente tondu kelkajn vortojn, kiuj formas tiun artikolon kaj metu
> ilin en sakon.

> Malforte skuu.

> Tuj deprenu ĉiun pecon unu post la alia.

> Konscie kopiu laŭ la ordo, en kiu ili estas deprenitaj de la sako.

> La poemo similos al vi.

> Kaj vi estas verkisto senfine originala kaj de ĉarma sentemeco,
> kvankam nekomprenata de la plebo.[^103]


[^103]: En la originala en la franca: «<i lang="fr">Prenez un jornal.
  Prenez des ciseaux. Choisissez dans ce jornal un article ayant la
  longueur que vou comptez donner à votre poème. Découpez l’article.
  Découpez ensuite avec soin chacun des mots qui forment cet article et
  mettez-le dans un sac. Agitez doucement. Sortez ensuite chaque coupure
  l’une après l’autre dans l’ordre où eles ont quitté le sac. Copiez
  consciencieusement. Le poème vous ressemblera. Et vous voilà un
  écrivain infiniment original et d’une sensibilité charmante, encore
  qu’incromprise du vulgaire</i>».

Prezentitaj ankaŭ en multaj manifestoj produktitaj en la periodo, la
ideoj de la dadaismo[^104] klopodis la forigon de arta sistemo, en kiu
la intelekta propraĵo, surtronigita en la nocio de aŭtoreco, jam ludis
gravan rolon. Ne hazarde, ankaŭ ĉi tie la teknologio komencis havi pli
da graveco en la arto; la kungluaĵoj, la sona poezio kaj la kino estas
artoj, fortigitaj en tiu periodo, en kiuj la teknikaj aparatoj ludas
ĉefan rolon, kaj kiel procedo produktada (okazo de la kungluaĵoj) kaj de
registrado kaj prezentado al la publiko (sona poezio kaj la kino).

[^104]: Indas ankaŭ citi kiel ideojn influajn sur la dadaismo tiujn
  ekspoziciitajn en la toloj de la germana Kurt Schwitters, plenaj de
  hazardaj bildoj de gazetaj tondaĵoj, trajnbiletoj kaj fotografaĵoj,
  kaj en la sonaj poezioj (sen vortoj) de Hugo Ball, fondinto, flanke ĉe
  lia edzino Emmy Hennings, en 1916, de la renkontiĝejo de la dadaistoj,
  la Cabaret [kabaredo] Voltaire, en Zuriko, en Svislando.

La kungluaĵoj forte eniris en la artan mondon danke al la hispana Pablo
Picasso kaj la franca Georges Braque, post 1912, bazitaj sur la evoluo
de la presadaj teknikoj, kiuj ebligis la amascirkuladon de gazetoj
kaj revuoj, el kie la pentristoj tondis fragmentojn kaj miksis
desegnaĵojn kaj inkojn en siaj verkoj. La sona poezio gajnis influon per
la fondinto de la itala futurismo, Filippo Tommaso Marinetti, kiu inter
1912 kaj 1914 eldonis <cite>Zang Tumb Tumb</cite>, sona kaj vida poemo,
en kiu la novaj modernismaj teknikoj de tipografio kaj diagramigo de la
italo estas miksitaj kun riĉa kaj malnova tradicio de parola poezio por
ludi kun la sonoj de la vortoj &mdash; kvankam, en la okazo de Marinetti
kaj de la itala futurismo, la eksperimentadoj, kiuj estis sub tasko de
retoriko de industria rapideco, kiu rezultigis mizoginecon kaj la faŝismon
de Mussolini[^105]. La dadaistaj Hugo Ball kaj Kurt Schwitters ankaŭ
faris sonajn poemojn; la unua estis prezentita de Ball en la malfermo de
la Cabaret Voltaire, en 1916 &mdash; «<i>gadji beri bimba glandridi lauli lonni
cadori</i>»[^106] &mdash;, kiu, sen gramofono disponebla en tiu epoko,
estis registrita multajn jarojn poste kiel pop-muziko dancebla en mikso
de afrikaj ritmoj en «I Zimbra»[^107], de la disko <cite>Fear of Music</cite>,
de Talking Heads, en 1979. La germana Schwitters estis pli bonŝanca; en
la 1920-aj jaroj iris kun Tzara, Hans Arp kaj Raoul Hausmann tra
diversaj literaturaj salonoj por deklami (kaj provoki) al la
aŭskultantaroj per «Ursonate» (ankaŭ nomita «Praa sonato»), sona poemo
bazita sur unu frazo «<i lang="de">Fümms bö wö tää zää Uu</i>», ripetita
kaj aldonita de aliaj fragmentoj dum la tempo, en kiu Schwitters ĝin
prezentis &mdash; unu el tiuj prezentoj estis registrita en radio de
Frankfurto, en Germanio, en 1932, kaj ankoraŭ hodiaŭ disponeblas[^108].

[^105]: La unua «Futurisma manifesto», skribita de Marinetti kaj
  publikigita en la franca gazeto <cite>Le Figaro</cite> en 1909,
  «laŭdis la rapidecon kiel novan estetikan valoron destinitan riĉigi la
  grandiozecon de la mondo», kiel Franco «Bifo» Berardi skribas en
  <cite>Depois do futuro</cite> [Post la estonteco], libro kiu detalas la konceptojn de
  «estonteco» dum la 20-a jarcento kaj hodiaŭ. Kun la rapideco venas la
  apologio de la aŭtomobilo, de la militemaj virtoj kaj la malvalorigo
  de ĉio, kio estas ina, elmontrita en ĉi tiu fragmento de la manifesto
  de Marinetti eltirita de Bifo: «Ni volas celebri la iĉon, kiu
  sekurigas la stirradon, kies ideala tigo trairas la Teron, lanĉita
  plenrapide al la ciklo de ĝia propra orbito» (Bernardi, <cite>Depois do futuro</cite>, p. 24).
[^106]: Pli da informo pri la poemo en
  <https://www.nealumphred.com/hugo-ball-sound-poetry-gadji-beri-bimba>.
[^107]: Videblas en <https://www.youtube.com/watch?v=3tyVn2ZDJ-Y>.
[^108]: Tradukista noto: La ligita video ne plu disponeblas en YouTube,
  sed oni tie povas trovi prezentadojn de la poemo kiel ĉi tiu: <https://www.youtube.com/watch?v=AAcKb24dVOg>.

La radio, cetere, komencas aparteni al la monda ĉiutago post la 1920-a
jardeko, kio fortigas la sonan eksperimentadon. La registro de la
gramofonoj, de loka kaj efemera atingo, komencas havi eblon de
aŭskultantaroj kaj interago kun miloj da homoj. La influo de la radio en
la 1930-a kaj 1940-a jardekoj igis, ke la nura lego de literatura
teksto por publika transsendo per la sistemo de radiotranssendado
transformiĝu en alia maniero krei. Famiĝis la paniko de multaj, kiuj
aŭdis <cite>La milito de la mondoj</cite>, la 30-an de oktobro de
1938 en la radio CBS, de Usono, transsendon gviditan de Orson Welles,
produktitan per The Mercury Theater on the Air, bazitan sur teksto de H. G.
Wells. Post la unuaj 15 minutoj, post aŭdi rakontojn de aperoj de
eksterteranoj en diversaj bienoj de Usono, la transsendo ŝajnas fali,
donante al multaj la impreson, ke CBS en Novjorko estadis *vere*
invadita de la eksterteranoj, pri kiuj la tujelsende aŭditaj rakontoj
parolis[^109]. Kvankam ankoraŭ hodiaŭ estas malakordoj pri la vera
influo de la transsendo, estis memorinda epizodo en la historio de la
komunikiloj por ilustri, ke adapto de presita teksto al sona
komunikilo de publika atingo neniam plu estis nura transsendo, sed alia
afero &mdash; «la perilo estas la mesaĝo», kiel resumis Marshall McLuhan
du jardekojn kaj duonon poste[^110], analizante la influon de la formo
de la komunikiloj sur ilia enhavo.

[^109]: Tute disponebla en
  <https://en.wikipedia.org/wiki/File:War_of_the_Worlds_1938_Radio_broadcast_full.flac>.
[^110]: En <cite>Understanding Media</cite> [Kompreni komunikilojn].

Ankoraŭ en la 1930-a jardeko, la populariĝo de la teknikoj de komuniko
kaj reproduktado kiel la radio, la gramofono, la fotografio kaj la kino
naskis unu el la plej konataj tekstoj de la germana historiisto Walter
Benjamin, <cite>Das Kunstwerk im Zeitalter seiner technischen
Reproduzierbarkeit</cite> [La artverko en la epoko de sia teknika
reproduktado]. Skribita en 1935 kaj eldonita en 1936, ĝi
argumentas, ke la teknika reproduktado tiam atingis nivelon, «tiel, ke
ĝi komencis igi propra la tuton de la artverkoj de antaŭaj epokoj, sed
ankaŭ krei propran lokon inter la artaj procedoj»[^111]. Benjamin
skribas, ke tiuj teknikoj de reproduktado liberigus la reproduktitan
aĵon de la superrego de la tradicio kaj, multobligante tion reproduktitan,
metus en la lokon de la unika okazo la okazon amasan[^112]. Tiel la
neordinara «aŭro» de la artverkoj perdiĝus kaj la kopioj, amase
(re)produktitaj, komencus havi valoron per si mem &mdash; kio, kiel ni
vidos post kelkaj jardekoj, kutimis en la novaj artaj kaj kulturaj
esprimmanieroj post la naskiĝo de elektraj teknologioj, poste ciferecaj
kaj, fine, rete ciferecaj.

[^111]: Benjamin, <cite>A obra de arte na era de sua reprodutibilidade
  técnica</cite> [La artverko en la epoko de sia teknika
reproduktado], p. 24.
[^112]: <i lang="lt">Ibidem</i>.

### II½.

La kontesto de la eŭropaj avangardoj al la aŭtorrajto ankaŭ havis eĥon
en la Brazilo de la unua duono de la 20-a jarcento. Preter la kunlaboraj
kaj liberaj praktikoj de la sudamerikaj originaj popoloj, temo de la [6-a
ĉapitro](#kolektiva-kulturo) de ĉi tiu libro, la brazila modernismo
prenis kelkajn elementojn de la eŭropaj movadoj kaj readaptis ilin al la
regionaj ecoj, jam alkutimigitaj al la rekombinado de elementoj por
la kreo de novaj kulturaj havaĵoj.

Tiurilate la san-paŭla Oswald de Andrade ludis gravan rolon kiel
disvastiginto kaj adaptinto de la ideoj de kontesto al la aŭtoreco kaj
al la aŭtorrajto. Por Oswald, la garantio de postvivo de la brazila
kulturo estis en la ebleco kontakti aliajn kulturojn kaj sorbi ilin per
gluta procedo, kiel estas esprimita en fragmentoj de la «Hommanĝula
manifesto», publikigita en la unua eldono de la <cite>Revista de
Antropofagia</cite> [Revuo de antropofagismo], en 1928: «Mi nur
interesiĝas pri tio, kio ne estas mia. Homa Leĝo. Hommanĝula Leĝo». La
antropofagismo proponita de la verkisto estis la renverso de la mito de
la bona sovaĝulo atribuita al klerista Rousseau: anstataŭ pura kaj
senkulpa, indiĝeno lerta kaj fripona, kiu kanibalas la eksterlandanon
kaj digestas la okcidentan koloniganton kaj lian kulturon.

Alia kontesta spuro kontraŭ la aŭtorrajto kaj ankaŭ kontraŭ tiuj nocioj
de aŭtoreco lasitaj de Oswald estas <cite>Serafim Ponte Grande</cite>,
libro eldonita en 1933. En la loko, kie kutimas indiki la «Aŭtoraj[n]
verkoj[n]», en la komenco de la publikaĵo, li metas la rubrikon
«Forlasitaj verkoj», kaj la libro mem, kiu estas legota estas
inkluzivita inter la titoloj «forpuŝita». La frazo, kiu komencas la
kovrilfolion de la eldonaĵo parafrazas la kutiman aŭtorrajtan indikon:
«*Rajto esti tradukita, reproduktita kaj misformigita en ĉiuj lingvoj*».
La libra formo, kiel komentas Haroldo de Campos en la prefaco de la dua
eldono (1971), estas farita per la kungluaĵo, la apudmetado de diversaj
materialoj, kio en la kina tekniko ŝajnas egali iumaniere al muntado.

> La kungluaĵo &mdash; kaj ankaŭ la muntado &mdash; ĉiam, kiam ili
> laboru el aro jam konsistigita de iloj kaj materialoj, inventarante
> ilin kaj reŝanĝante al ili la primitivajn funkciojn, povas eniĝi en
> tiun specon de ago, kiun Lévi-Strauss difinas kiel
> <i lang="fr">bricolage</i> (ellaboro de strukturitaj aroj, ne rekte
> pere de aliaj strukturitaj aroj, sed per la utiligo de restaĵoj kaj
> fragmentoj), kio, se estas karakteriza de la
> «<i lang="fr">pensée sauvage</i>», ne ĉesas esti tre rilata al la
> logiko de konkreta speco, kombineca, de la poezia penso.[^113]

[^113]: Campos, <cite>Serafim: um grande não livro</cite>, p. 2.

La influo de la kungluaĵo de la dadaismo kaj de la surrealisma
aŭtomatismo en Oswald estas konsiderinda, sed ekzistas ankaŭ diversaj
artistoj dum la historio, kiuj uzis similajn artifikojn de kungluaĵo kaj
de kontesto al la propra libro kiel rakonta kaj metodika aĵo. En la
prefaco de <cite>Serafim Ponte Grande</cite> jam citita, Haroldo de
Campos citas unu el tiuj, la idiosinkrazian <cite>La vivo kaj opinioj de
la kavaliro Tristram Shandy</cite>, de Laurence Sterne, skribitan inter
1759 kaj 1767 en Anglio, «pionira mejloŝtono de la revolucio de la
librobjekto, kiu estas projekciita en superforta kaj nerenversebla
maniero en nia jarcento, nun havante kiel apogilojn la novajn
teknikojn de reproduktado de informtranssendo.»[^114]. La praktiko de
Oswald krei per la kungluado kaj per la rekompilado de diversaj
fragmentoj de aliaj artistoj aŭ de amaskomunikiloj, miksante ĝenrojn kaj
diversajn formojn por estigi specifan verkon estis, kiel aŭguras
Haroldo de Campos, abunde vidita en la 20-a jarcento en Brazilo, el la
vidaj artoj kaj prezentoj de Hélio Oiticica, Paulo Bruzcky kaj Adriana
Varejão ĝis la literaturo de Valêncio Xavier kaj, pli lastatempe,
Angélica Freitas, Verônica Stigger, Cristiane Costa kaj Leonardo
Villa-Forte[^115].

[^114]: <i lang="lt">Ibidem</i>, p. 4.
[^115]: Villa-Forte ankaŭ eldonis <cite>Escrever sem escrever:
  literatura e apropriação no século XXI</cite>, laboro, kiu pensas pri
  la literaturo en kunteksto de reproprigo favorigita per la ciferecaj
  teknologioj kaj Interreto.

La influo plej eksplicita de la dadaismo rifuzi la originalecon kaj,
ĉefe, ke ĉiu arta produkto konsistas el la reciklado kaj en la remuntado
estas montrita kaj en Oswald kaj en aliaj artistoj ĉi tie cititaj, pli
en la estetiko ol en la maniero licenci la verkon. Forlasi la
aŭtorrajton &mdash; aŭ eĉ licenci per manieroj malpli limigaj &mdash;
estas, en la kunteksto de la arto de la 20-a jarcento, estetika
distingilo, kiu praktike ŝajnas estiĝi kiel tro radikala eĉ por
eksperimentaj artistoj.

### III.

La apero de artverkoj amase daŭris dum la 20-a jarcento kaj komencis
ankaŭ inkludi aspektojn de la teknologioj de komunikado, registrado kaj
reproduktado dum ili populariĝis. Ankaŭ novaj signifoj kaj artaj praktikoj
naskiĝis per la rekombino de dateno (literatura teksto registrita per
aŭdioformo ekzemple) transportita al alia speco de registro kaj
rekombinita laŭ la diversaj eblaj teknikoj en alia komunikilo &mdash; la
sonaj enmetoj, tranĉoj kaj redaktaj paŭzoj, kiuj transformis <cite>La
milito de la mondoj</cite> en *alian aferon*, kiam tujelsende
transsendita ekzemple. El la pura eksperimentado per teknika inventaĵo
ankaŭ naskiĝas aliaj rekreaĵoj; la gramofono de Edison en la manoj de la
hungara vida artisto kaj profesoro en la germana Bauhaus László
Moholy-Nagy povis ŝanĝi produktivan ilon «tiel ke la aŭda fenomeno
estiĝu per si mem danke al la registro de la necesaj spuroj sur disko
sen antaŭaj aŭdaĵoj»[^116]. La sugesto de Moholy-Nagy (kaj de aliaj) en
tiu periodo por produkti muzikon per la gramofono efektiviĝas per la
konkreta muziko post 1948, kun la franca Pierre Schaefer farante sonajn
eksperimentojn per mikrofonoj, aktoraj voĉoj kaj aliaj sonoj registeblaj
en tiama radia studio. Estas en tiu jardeko ankaŭ, kiam la registrilo de
magneta bendo komencas esti komercita kaj, per la portebleco, kiun iom
post iom ĝi komencis ebligi, realigi novajn sonajn eksperimentojn &mdash; kiel
tiuj de la egipta Halim El-Dabh, kiu, per registrilo de magneta bendo de
radio-studio de Kairo, en Egiptio, produktis en 1944 «The Expression of
Zaar», konsiderita unu el la unuaj elektraj muzikoj faritaj per ŝanĝoj
en studio de registritaj sonoj (per la registrilo) de religia ceremonio
en tiu epoko[^117].

[^116]: Moholy-Nagy, citita de Kittler, <i lang="en">op. cit.</i>, p.
  79.
[^117]: El-Dabh iris tra la stratoj registri eksterajn sonojn de malnova
  zar-kulta ceremonio, speco de ekzorcismo publike farita. Fascinita per
  la ebloj ŝanĝi la registritan sonon per muzikaj celoj, li pensis, ke
  li povis malfermi la enhavon de kruda aŭdaĵo de la zar-kulta ceremonio
  por pli profunda esploro pri la «interna sono» enhavita en ĝi. Fonto:
  <https://en.wikipedia.org/wiki/Halim_El-Dabh>.

Estinta ekde la komenco tranĉaĵo, de la daŭra movado aŭ de historio
irigita antaŭ la lenso[^118], la kino estis unu arto eĉ pli
rekombinema. La datenoj registritaj per du malsamaj manieroj (bildo kaj
sono, movantaj) gajnis la eblon de diversaj specialaj efikoj en kompleksaj
studioj de redaktado (kaj ankaŭ produktado) per la plej bonaj
registriloj, mikrofonoj kaj aliaj sonaj aparatoj, kiun la plej riĉa el
la artoj jam ebligis en la 1940-aj kaj 1950-aj jaroj, precipe en la
Okcidenta Marbordo de Usono, en Holivudo. Ĝi estis la arto, en kiu la
rekombinado povis atingi plej da efikeco kaj beleco, laŭ la francaj Guy
Debord kaj Gil Wolman, en <cite>Mode d'emploi du
détournement</cite>[^119], de 1956: «la povoj de la filmo estas tiel
grandaj, kaj la manko de kunordigo de tiuj povoj estas tiel evidenta, ke
preskaŭ ĉiu ajn filmo, kiu estu super la mizera mezboneco provizas temon
pri senfinaj polemikoj inter spektantoj kaj profesiaj
kritikistoj»[^120]. Heredantoj de Lautreamont, de la dadaismo kaj de la
Bauhaus, Debord kaj Wolman estas ligitaj al la situaciistoj, grupo
estigita en Francio post 1957 de diversaj poetoj, arkitektoj, filmistoj,
artistoj, kiuj difinis sin kiel «arta kaj politika avangardo» centrita
al la kritiko al la konsumsocio kaj al la komercaĵigita kulturo[^121].

[^118]: Kittler, <i lang="lt">op. cit.</i>, p. 177.
[^119]: Unue eldonita en la oka numero de la belga surrealisma revuo
  <cite>Les Lèvres Nues</cite>, en 1956.
[^120]: Ĉi tiu fragmento estis *turnigita* de la enkonduko por <cite>O
  guia dos usuários do detournamènt</cite> publikigita per BaixaCultura
  en la Interreto kaj en zina formo, 2015.
[^121]: «La ideo de “situacionismo” estas rilatata al la kredo, ke la
  individuoj devas konstrui la situaciojn de siaj vivoj en la ĉiutago,
  ĉiu esplorante sian eblon, por rompi la regantan fremdigon kaj akiri
  propran plezuron». Fonto:
  <https://enciclopedia.itaucultural.org.br/termo3654/situacionismo>.

<cite>Um guia para os usuários do detournamènt</cite> estas unu el la
unuaj tekstoj havante kiel fokuson la disvolvon de kreema metodo bazita
sur la plagiato. Debord kaj Wolman parolas ekzemple de la praktiko en la
literaturo, pli uzita en la procedo skribi ol en la fina rezulto: «Ne
estas multe da estonteco en la turnigo de tutaj romanoj, sed dum la
transira fazo povus esti certa nombro de tiaj entreprenoj». En la poezio
ili citas la *metagrafion* &mdash; tekniko de grafika kungluaĵo
disvolvita de la rumana Isidore Isou kaj adoptita de la movado de la
Literismo[^122], kiu ilin inspiris. Kiel «fundamentaj leĝoj de la
turnigo» estas: 1) la perdo de graveco de ĉiu turnigita ero, kiu
povas iri tiom malproksime, ke ĝi povas perdi sian originan sencon; kaj
samtempe, la 2) reorganizigo en alia signifaro, kiu donas al ĉiu ero
novan aliron aŭ efikon. Unu fragmento:

[^122]: Kultura movado lokita en Francio kaj kreita en la 40-aj jaroj
  de Isidore Isou, kun influoj de la dadaismo kaj de la surrealismo.

> Ne temas ĉi tie pri reveno al la estinteco, kio estas reakcia; eĉ la
> «modernaj» kulturaj celoj estas per fina analizo reakciaj en la senco,
> ke ili dependas de ideologiaj formuligoj de estinteca socio, kiu
> daŭrigis sian agonion de morto ĝis la nuntempo. La sola taktiko
> historie pravigita estas la ekstremista novigo. [...] Vere necesas
> forigi ĉiujn restaĵojn de la nocio de persona proprieto en tiu fako.
> La apero de la jam malmodernaj novaj necesoj per «inspiritaj» verkoj.
> Ili iĝis bariloj, danĝeraj kutimoj. Ne gravas, ĉu ŝati aŭ ne ŝati tiujn.
> Ni devas preterpasi ilin. Eblas uzi ĉiun ajn eron, ne gravas el kie ili
> estis prenitaj, por fari novajn kombinaĵojn. La malkovroj de la
> moderna poezio rilataj al la analoga strukturo de la bildoj montras,
> ke kiam du aĵoj estas kunigitaj, ne gravas kiom disaj ili povu esti de
> siaj originaj kuntekstoj, ĉiam formiĝas rilato. Limigi sin al persona
> aranĝo de vortoj estas nura konvencio. La reciproka enmiksiĝo de du
> sensaj mondoj, aŭ la kunigo de du malsamaj esprimoj, anstataŭas la
> originalajn erojn kaj produktas pli efikan sintezan organizon. Oni
> povas uzi ĉion ajn.[^123]

[^123]: Debord; Wolman, <i lang="lt">op. cit.</i>

Kiel praktiko, la turnigo ne estis antagonisto al la tradicio, sed
emfazo de la reinventado de nova mondo bazante sin sur la estinteco, en
momento en kiu, postmilite, Eŭropo (kaj Francio) vivis artan eksplodon
kaj de rekomenco de la avangardoj de la komenco de la jarcento, kiu iel
lernigis al ĉiuj «lerni vivi per malsama maniero per la kreado de novaj
praktikoj kaj kondutaj manieroj»[^124]. La ideo de turnigo proponita en
la teksto de Debord kaj Wolman ŝajnis funkcii pli por malkaŝi ol por
kaŝi ĝiajn devenojn. Estis unu maniero, inter aliaj multaj, rekte enigi
en la longan dialogon de la scio, elmontri referencojn kaj montri al
ĉiuj tion, kion oni volas absorbi de tiuj. De la unuiĝo de tiu, kiun oni
profitis de unu flanko, kun tio, kion oni profitas de alia, ni lernas
ekde nul, naskiĝas io malsama. En la sama 1950-a jardeko ankaŭ akcidente
naskiĝis alia influa tekniko bazita sur la plagiato, la
<i lang="en">cut-up</i>. La pentristo kaj poeto Brion Gysin, kiu
apartenis al la grupo de André Breton en la franca surrealismo, metis
gazetajn tavolojn kiel maton por protekti tablon, dum li tondis paperojn
per lameno por razi. Tondante la gazetojn Gysin rimarkis, ke la
tranĉitaj tavoloj prezentis interesajn apudmetojn de teksto kaj bildo;
li komencis tiam dividi gazetartikolojn en sekcioj, hazarde
reorganigitaj same kiel Tzara proponis en «Por fari dadaisman poemon».
Multaj poetoj eble jam faris similajn agojn por la kreado, sed Gysin
konis la verkiston William Burroughs kaj prezentis al li la teknikon en
1958, en la Beat Hotel, en Parizo. Naskiĝis partneraro, kiu produktis
diversajn verkojn en teksto kaj en sonoj, inter ili la libron <cite>The
Third Mind</cite>, kolekton de <i lang="en">cut-ups</i> subskribitaj de
ambaŭ. Unu el la plej neordinaraj verkistoj de la 20-a jarcento,
Burroughs ankaŭ uzis la teknikon en trilogio de libroj, kiu enhavas
<cite>The Soft Machine</cite>, <cite>The Ticket That Exploded</cite> kaj
<cite>Nova Express</cite>, eldonitaj inter 1961 kaj 1964, rakontoj ĉiuj,
en kiuj ekzistantaj tekstoj eltonditaj kaj muntitaj per pecoj estis
hazarde metitaj, kombinitaj per pentraĵoj de Gysin kaj eksperimentaj
sonoj eltonditaj de Ian Sommervile &mdash; en registrilojn de magneta
bendo, tiam pli popularaj ol en la Egiptio de Halim El-Dabh en 1944.

[^124]: Nimus, <i lang="en">op. cit.</i>, p. 79.

Burroughs ankaŭ priskribis la <i lang="en">cut-up</i> didaktike:

> La metodo simplas. Jen estas maniero fari tion. Prenu paĝon. Kiel ĉi tiun
> paĝon. Nun tranĉu el la mezo malsupren. Vi havas kvar sekciojn: 1,
> 2, 3, 4, ... unu du tri kvar. Nun reorganizu la sekciojn metante
> sekcion kvar kun sekcio unu kaj sekcion du kun sekcio tri. Kaj vi
> havas novan paĝon. Kelkfoje ĝi diras la saman aferon. Kelkfoje alian
> multe malsaman aferon &mdash; eltondi kaj alglui diskursojn estas
> interesa ekzerco &mdash; ĉiuokaze vi malkovros, ke tio diras iun
> aferon kaj iun aferon bone difinitan. Algluu ajnan poeton aŭ
> verkiston, kiun vi admiras, ekzemple, aŭ poemojn, kiujn vi multfoje
> legis. La vortoj perdis la signifon kaj vivon per jaroj de ripetado.
> Nun algluu la poemon kaj tajpu elektitajn pecojn. Plenigu paĝon
> per fragmentoj. Nun tondu la paĝon. Vi havas novan poemon. Tiom da
> poemoj, kiom vi volas. Tristan Tzara diris: «La poezio estas por
> ĉiuj». Kaj André Breton nomis lin policisto kaj forpelis lin de la
> movado. Diru denove: «La poezio estas por ĉiuj». La poezio estas
> loko kaj estas libera, por ke ĉiuj eltondu kaj algluu Rimbaud-on, kaj
> vi metu vin en la loko de Rimbaud.
>
> La metodo de la <i lang="en">cut-up</i> kondukas la verkistojn al
> kungluaĵo, kio estis uzita de pentristoj dum sepdek jaroj. Kaj uzita
> per la fotografaj kaj kinaj kameraoj. Fakte ĉiuj la strataj tranĉoj de
> la kino aŭ de fotografaj kameraoj estas, per neantaŭvideblaj faktoroj
> de pasantoj kaj apudmeto, <i lang="en">cut-ups</i>. Kaj fotografistoj
> diros al vi, ke ofte iliaj plej bonaj tujfotografaĵoj estas
> akcidentoj... verkistoj diros la samon. La plej bonaj skribaĵoj
> ŝajnas esti tiuj faritaj preskaŭ akcidente de verkistoj, ĝis kiam oni
> igis la metodon de la <i lang="en">cut-up</i> eksplica &mdash; ĉiu
> skribaĵo estas fakte <i lang="en">cut-ups</i>; mi revenos al ĉi tiu
> afero &mdash; estis neniu maniero produkti la akcidenton de la
> spontaneeco. Vi ne povas decidi la spontaneecon. Sed vi povas
> enkonduki la neantaŭvideblan kaj spontanean faktoron per
> tondilo.[^125]

[^125]: Burroughs, <cite>O método cut-up</cite>, p. 85.

En la 1950-a jarcento ankoraŭ videblas, en agokampo malpli
<i lang="en">underground</i> [netendenca], la poparto antaŭeniri per la
rekombinado de la modernisma kaj dadaisma kungluaĵo kun pli kaj pli
alproprigo de la aĵoj de la amaskulturo, nun ankaŭ inkluzivante la
televidon, ekde 1930 ebla en Eŭropo kaj en Usono, sed fakte popularigita
kaj igita simbolo en la okcidentaj hejmoj en la 1950-a jardeko. Andy
Warhol kaj Roy Lichtenstein, du el la nomoj (ne hazarde el Usono) plej
konataj de tiu movado, iĝis <i lang="en">pop</i> [popiĝis] uzante
elementojn de la historioj en kadretoj, de la reklama propagando kaj de
la distraj televidaj programoj por parodii kaj komenti la apation, kion
la konsumo (ankaŭ de amaskomunikilaj produktoj) povas naskigi en la
homoj. Kiel en Duchamp, la arta verko en la poparto estas produktita
bazante sin sur pecoj de aliaj verkoj amase komercigitaj, kio ankoraŭ
hodiaŭ levas diversajn demandojn pri intelekta propraĵo kaj aŭtoreco.
Warhol, speciale, faris siajn plej konatajn verkojn bazante sin sur
bildoj kaj âjoj ne produktitaj de li, sed proprigitaj, okazo de
<cite>Marilyn Monroe</cite> (1967), 250 buntaj serigrafioj faritaj en
sia <cite>Factory</cite> bazinte sur disvastiga fotografaĵo de la
aktorino simbolo de la Ora Erao de Holivudo. Kaj ankaŭ de la skatoloj de
<cite>Brillo Box</cite>, populara lesivo en Usono, alportita de Warhol
al la mondo de la artoj en 1964 kaj registrita per aŭtorrajto ekde tiam.

En la plejparto de la juraj procesoj, kiujn Warhol devis alfronti en tiu
epoko, la tribunaloj agnoskis sian proprigan agon kiel arta. Por
fundamenti sian rezolucion ili memorigis lian trajektorion, la tiaman
kuntekston kaj atestojn de specialistoj pri la afero, kiel kritikistoj,
historiistoj kaj profesoroj, agantoj de la arta fako, kiuj povus doni
«difinon» de tio, kion la socio konsideras kiel artiston. La proprigoj de
Warhol pasis la «estetikan teston» ankaŭ ĉar montris «originecon» en lia
ago; kiel antaŭ faris Duchamp, en <cite>Brillo Box</cite> Warhol signife
kaj nerenverseble modifis la fakon (kaj la regulojn) de la arto. La
paradokso de la historio estas, ke Warhol (kaj liaj heredantoj poste)
montris sin nefleksebla pri la modifoj kaj uzoj de siaj verkoj[^126].

[^126]: Kiel oni montras en, <i lang="lt">op. cit.</i>, p. 448.

Warhol ne estis la unua, nek la lasta, agi en kontraŭdira maniero uzante
ĉion kreante, sed ne lasi, ke aliaj uzu ion, kiun li produktis. Estas
termino en psikologio por tiu praktiko, nomita «malamego al la perdo»
(<i lang="en">loss aversion</i>, en la angla), kiu diras: «ni ne ŝatas
perdi kion ni havas». Ĝi parolas pri emo doni valoron pri altan al la
perdoj ol al la gajnoj; la profitoj, kiujn ni akiras kopiante laboron de
aliaj ne kreas en ni grandan emocion, sed, kiam niaj ideoj estas
kopiitaj, ni sentas preskaŭ perdon kaj iĝas preskaŭ gardohundoj
soifantaj je venĝo[^127]. La Studioj Disney estas, eble, la okazo plej
konata de tiu praktiko: ili multe uzis la publikan havaĵon por savi
kelkajn el siaj ĉefaj verkoj &mdash; <cite>Neĝulino kaj la sep
nanoj</cite>, <cite>Pinokjo</cite>, <cite>Alico en Mirlando</cite>,
<cite>Cindrulino</cite>, <cite>La dormanta belulino</cite>,
<cite>Aladino</cite> &mdash; kaj transformi ilin en
videorakontojn kun komerca sukceso[^128]. Sed, kiam estis la tempo de la
eksvalidiĝo de la aŭtorrajtoj de la unuaj filmoj de Disney, ili faris
fortan premon, por ke la aŭtorrajta periodo estu prokrastita en Usono.
La lasta plilongigo, <cite>Copyright Term Extension Act</cite>, de 1998,
famiĝis ankaŭ kiel «Mickey Mouse Protection Act» [Leĝo Protekta de
Mickey Mouse] kaj prokrastis la eniron al la publika havaĵo de verko al
70 jaroj post la aŭtora morto, 120 jaroj post la kreo aŭ 95 jaroj post
la verka publikigo. Ĉi tiu lasta periodo estas tiu de Mickey Mouse,
publikigita en 1928, kiu estos en publika havaĵo en 2024 &mdash; krom se
Disney faru premon por prokrasti tiun daton.

[^127]: Kiel estas montrita en sceno de la kvara parto de la dokumenta
  filmo <cite>Everything Is a Remix</cite> [Ĉio estas remiksaĵo], de
  Kirby Ferguson (2015). Disponebla en
  <https://vimeo.com/baixacultura>.
[^128]: Pli da informo pri la okazoj de historioj akiritaj el la publika
  havaĵo, kiuj iĝis sukcesaj videorakontoj en Disney en
  <https://baixacultura.org/a-armadilha-disney>.

### IV.

Kiel kontesto deklarita al la sistemo de intelekta proprieto aŭ por
komenci esti kutima parto de la procezo de kreado, la okazoj de la
reproprigo de ideoj komencis esti tiom oftaj, kiom ni volus rimarki post
la 1960-aj jaroj. Estas en tiu jardeko, kiam la teknologiaj agoj bazitaj
sur la registro kaj ludado komencis esti komercitaj en la plejparto
de la planedo kaj ĉesis esti nur aparatoj de specialistoj, por iĝi iom
post iom pli porteblaj kaj malpli grandaj, komencante esti en multaj
mezklasaj domoj de la okcidenta mondo. La registriloj kaj la
aŭdaj reproduktiloj en magneta bendo ekzemple komencis esti facile
trovitaj en la merkatoj de la grandaj urbaj centroj per la produktado
kaj komercado de la kasedaj bendoj &mdash; kaj «virga», kiu povas esti
uzitaj por registri, kaj de antaŭregistrita muziko &mdash; post 1964.
Patento de Philips de 1963[^129], la «kompakta» formo de la kaseda bendo
konkuris (kaj poste anstataŭigis) kun la Stereo 8 (kartoĉo, aŭ
8-track), kreita en Usono en 1958 kaj iomete pli granda ol la kasedo, kaj
popularigis la kutimon aŭskulti kaj registri muzikaĵojn en bendoj
anstataŭ la voĉaj registraĵoj kaj diktaĵoj, kiel kutimis ĉe la unuaj
registriloj. Ĝi estis la fuelo de la porteblaj registriloj, kiuj
disvastiĝis kiel komercaj produktoj en la fino de tiu jardeko &mdash;
unu el la unuaj estas la modelo Typ EL 3302, de Philips, de 1968 &mdash;
kun la eblo konservi aŭdaĵoj de ĝis tridek minutoj en ĉiu el siaj
flankoj.

[^129]: Fonto: <https://en.wikipedia.org/wiki/Cassette_tape>.

Pli multekosta kaj stranga, la specimenilo aperas post 1969 kiel
aparato, kiu registras kaj ebligas modifon de malsamaj muzikaj specimenoj,
kaj poste kiel maniero tondi kaj intermeti muzikaĵojn &mdash; la
<i lang="en">sampling</i>. Ĝi naskiĝas el la sinteziloj, aparatoj kiuj,
bazitaj sur la aranĝo de la piano, kunigas kaj ludas malsamajn sonojn,
kiel la unuaj disvolvitaj de la inĝeniero Robert Moog en 1964, ankoraŭ
analogaj kaj kiuj popularigis nomojn kiel oscililojn, kovertojn,
brugenerilojn, filtrilojn kaj sekvencilojn kun tensia kontrolo kiel
vortoj (kaj efikoj) uzotaj por sona modifo. Samtempulo de Moog estas
la Mellotron, de 1963, unue vendita kiel klavaro kun antaŭregistritaj
akompanoj (en magnetaj bendoj) por animi la anglajn domojn, poste uzita
en diversaj rokbandoj kiel The Moody Blues, Genesis, King Crimson kaj
The Beatles &mdash; ĝi estas la sono, kiu malfermas la klasikaĵon
«Strawberry Fields Forever», komponita de John Lennon kaj Paul McCartney
en 1967. Ankoraŭ estis Electronic Music Studios (EMS), produktita en
Anglio en 1969, la Minimoogs kaj aliaj antaŭ Fairlight CMI, kreita de la
aŭstraliaj Kim Rydie kaj Peter Vogel en 1979, ĉefa responda de la
populariĝo de la specimenilo, uzita kaj en la disvastigado de la elektra
muziko kaj en la repo &mdash; muzika stilo tute naskita el la
reproprigo kaj havanta ĉi tiun kiel bazon[^130]. La specimenilo igis la
komponadon, en la popa muziko, ankaŭ la arto kombini sonojn kaj
pecojn de muzikaĵoj[^131].

[^130]: Pri la deveno de la specimenilo en la repo, vidu la dokumentan
  filmon <cite>Copyright Criminals</cite>, produktita kaj direktita de
  Benjamin Franzen kaj publikigita en 2009, disponebla
  <https://www.pbs.org/independentlens/copyright-criminals>.
[^131]: Bastos, La cultura del reciclaje, en Rosas; Salgado,
  <cite>Recombinação</cite>, p. 10.

Ankaŭ tiuepoka, la videokasedo komerce aperis inter 1959 kaj 1963 kun
diversaj modeloj venditaj per markoj kiel Toshiba, Philips kaj Sony.
Bazitaj en mekanismo de registro kaj konservo de informoj simila al
tiu de aŭdaj magnetaj bendoj, la unuaj ankoraŭ estis pezaj aparatoj,
bruaj kaj multekostaj, pli uzitaj en firmaoj, lernejoj, malsanulejoj.
Sed, en la 1970-a jardeko, malgrandiĝis ilia grando kaj ili disponeblis en la
bazaro por ekzemple esti uzitaj por la hejma registrado de televidaj
programoj. Post 1969, per la komercigo de la hejmaj kameraoj
kun kuplitaj baterioj (modelo nomita Portapak), la videokasedoj ankaŭ
komencis ekspozicii hejmajn videojn de tiuj kameraoj &mdash; la unuaj,
kiuj uzis magnetajn bendojn, kiuj povas esti luditaj en tiuj aparatoj
estas de la komenco de la 1970-a jardeko.

La 1950-a, 1960-a kaj 1970-a jardekoj estis, fakte, tiuj de disvastigo
kaj uzo de teknologioj de ludado kaj registrado de aŭdaĵoj kaj videoj.
Sed ni ne povas forgesi, ke la presa kopio ne nur restis, sed ankaŭ estis
antaŭenpelita per la teknikaj inventaĵoj de tiu periodo. Nomo iĝis
sinonima de praktiko: Xerox, naskita en 1948 kiel registrita marko de
kopiiloj bazitaj en la elektrofotografia metodo, praktiko kiu, en 1947,
ricevis novan nomon pli facile difinebla: kserografio (de la greka
<i lang="grc">xeros</i>, seka, kaj <i lang="grc">grafia</i>, skribo).
Xerox lanĉis al la merkato sian unuan presmaŝinon en 1960, la Xerox 914,
kaj post tio, kvankam ekzistis aliaj markoj kaj modeloj de kopiiloj, kiuj sekvis
ĝin, iĝis sinonimo de kopio kaj referenco de arta praktiko &mdash; la
<i lang="en">copy art</i>. En Brazilo ĝi estis renomita kiel kserografio
kaj tre praktikita, proksima al la poŝta arto (aŭ <i lang="en">mail
art</i>), de artistoj kiel la jam citita Paula Bruscky, Hudnilson Jr.
(Rio-de-Ĵaneiro) kaj Hugo Pontes (Minas Gerais). Ĉi tiu lasta skribis:

> Eble la plej grava aspekto de la kserografio estas, ke ĝi oferas al la
> artisto, kiu ne havas kapablojn por la desegno, kondiĉojn ellabori la
> muntadon de siaj projektoj, miksante planojn, liniojn kaj ombrojn, sen
> iu ajn helpa ilo, kiun la desegna tekniko postulas. Per tiu
> elektronika procezo ni povas transpasi al la diversaj gradoj de
> denseco de blanka kaj nigra kolorajn bildojn, retikulajn kaj eĉ en
> reliefo (ĉi-okaze aĵoj aliformigitaj en figuroj), kio tre
> alproksimiĝas al la kungluaĵoj, ebligante revenon al la spertoj de la
> taĉistoj[^132].

[^132]: Pontes, O que é arte xerox?, en Rosas; Salgado, <i lang="lt">op.
  cit.</i>, p. 18.

La bendo kaj la videokasedo, la aŭdioregistrilo kaj la porteblaj
videokameraoj kaj la kopiiloj kunportis novan aspekton ĝis tiam nova por
la demando de la intelekta propraĵo kaj de la disvastigo de la libera
kulturo: la ludado de muzikaĵoj, videoj kaj tekstoj por hejmaj kaj nekomercaj celoj. Preter la kopioj nomitaj piratoj, kiuj, kiel ni vidis, ĉiam
akompanis la ludadojn leĝe permesitajn, la alveno de la registradaj
kaj ludadaj teknologioj al la domoj de la homoj popularigis la privatan
kopion, kiu pagis aŭtorrajtojn al neniu ajn. Pli ol populara, kutimo:
registri kasedan bendon per elektitaj muzikaĵoj de unu aŭ pli radioj,
ekzemple, iĝis unu el la plej bonaj donacoj, kiam oni volis konkeri iun
en la 1980-aj jardekoj.

La rekombina ebleco de la teknologioj de registrado kaj ludado
disvolvitaj en la dua duono de la 20-a jarcento kreis problemon ankaŭ
por la industrio, kiu, ekde la disvastiĝo de la aŭtorrajtoj ĉirkaŭ la
duono de la 19-a jarcento, estiĝis bazante sin sur la intelekta
propraĵo. Estis tiel, kiam en 1969 Phillips lanĉis la sonkasedon kaj la
fonografa industrio unue klopodis eviti la lanĉon de la produkto kaj
poste faris influon en la Kongreso de Usono, por ke estu kreita imposto
sur la virgaj kasedoj por kompensi la malgajnojn de la industrioj
kaŭzitajn per la kopioj, kiujn la uzantoj faris de siaj LP-j, al kasedoj.
Same okazis en 1976, kiam Sony lanĉis la videokasedon de tipo Betamax
kaj Universal Studios kaj la Studioj Disney komencis proceson kontraŭ
la firmao akuzante ĝin, ke la produktoj rezultantaj de tiuj aparatoj
instigis al la malobservo de la aŭtorrajtoj[^133].

[^133]: <i lang="lt">Ibidem</i>.

En ĉi tiu lasta kazo, jura batalo, kiu daŭris ok jarojn fine agnoskis,
ke la homo, kiu registris la lastan ĉapitron de dramserio en la
videokasedo Betamax (aŭ aliaj specoj, kiuj sekvis) ne piratis[^134]. En
multaj aliaj similaj situacioj same okazis: neniu leĝo atingis efike
reteni la privatan kaj komunuman uzon de la verkoj sen la pago de la
respektivaj aŭtorrajtoj. Ne eblis kontroli la hejman ludadon sen
komercaj celoj, kiam la teknologioj de ludado kaj registrado ne nur
permesas, sed ankaŭ havas la kopion por ĉiu ajn celo, inkluzive la persona,
kiel bazan metodon funkcii.

[^134]: <i lang="lt">Ibidem</i>.

Kiel maŝino, kiu unuigas registradon kaj tekstan registradon, aŭdaĵojn
kaj bildojn, la persona komputilo komencas esti vendita kaj popularigita
de firmaoj kreitaj en Silicon Valley post 1975. Du jardekojn poste, ĝi
kunigis sin kun la Interreto por iĝi, ambaŭ, respondecaj por igi la
procedon krei eĉ pli bazite sur la kopio, kio plivastigis la debaton pri
intelekta propraĵo, pirateco kaj libera kulturo al niveloj ĝis tiam ne
konitaj.

