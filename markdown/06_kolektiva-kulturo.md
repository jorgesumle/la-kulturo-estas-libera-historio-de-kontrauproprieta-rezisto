<section class="citas">
<div class="right-align">
<i>Mi estas, ĉar ni estas.</i>

Ubuntu
</div>

<br>

<i>La Majstro diris: «Mi komunikas, sed mi ne faras ion novan; mi estas vera en tiu, kiu mi diras, kaj dediĉita al la Malnovepoko».</i>

<div class="right-align">
Konfuceo, la <cite>Analektoj</cite>, ĉirk. s. IV-II a.K.
</div>

<br>

<i>La indiana rigardo traktas ekzemple la aĵojn kiel registrojn malpli
pasivajn de la eblecoj de subjekto ol la objektigoj personigitaj de tiuj
rilatoj. Tiel, ke la kreado okazas distribuita en la rilato inter la
pluraj objektoj kaj homoj, sen tiu disiĝo inter subjekto kaj objekto,
intelekto kaj materio, kiun ni kutimas fari en Okcidento. La
subjektiveco ankaŭ ekzistas en la objektoj kaj kreas vivan pejzaĝon
formitan de malsamaj specoj de niveloj de homaj agoj.</i>

<div class="right-align">
Marcela Stockler Coelho de Souza, <cite>The Forgotten Pattern and the
Stolen Design: Contract, Exchange and Creativity among the
Kĩsêdjê</cite>, 2016
</div>

<br>

<i>Kiel feministoj ni devas kontraŭi la patriarkan karakteron de la
aŭtorrajto, krei novan paradigmon, kiu valorigu la kreadon kiel agon
socian kaj komunuman, kaj precipe klopodi ŝanĝi la leĝojn, kiuj hodiaŭ
kriminaligas aŭ malleĝigas fundamentajn agojn por la parollibereco, por
la interŝanĝo, por la disdonado kaj la reproprigo de la kulturo. Ni
devas silentigi la muzojn, kiuj inspiras al la geniulo, por ke povu
finfine paroli la inoj.</i>

<div class="right-align">
Evelin Heidel (Scann), <cite>Que se callen las musas</cite>, 2017
</div>
</section>

### I.

Ke ni, okcidentuloj, klare scias, ke la organizo de ideoj en certa
objekto, kiu iras al aliaj homoj, igas nin aŭtoroj &mdash; do supozeble
laŭrajtaj proprietuloj de aŭtorrajtoj sur nia kreaĵo &mdash;, estas
frukto de filozofa veturo kaj de maniero vidi la mondon, kiu mallonge
estis prezentita en ĉi tiu libro. Sed tiu maniero vidi la mondon kaj la
aferojn ne estas la sola ekzistanta en ĉi tiu planedo konata kiel Tero, sed
nur *perspektivo*, emfazita ĝenerale ĉi tie kiel okcidenta. Kvankam tiu estas
dominanta perspektivo, regulanta de la vivo en la kapitalismaj socioj,
oni devas memori, ke ekzistas aliaj, kiuj estas en multaj ejoj kaj
tradiciaj komunumoj, kiu konfliktas kun certaj ideoj kaj agmanieroj
komunaj en la kapitalisma socio, en kiu la intelekta havaĵo naskiĝis.
Zorgo, solidareco, kunlaboro kaj kolektiveco ekzemple estas valoroj
ankoraŭ gravaj kaj dominantaj en multaj komunumoj kaj miljaraj
kulturoj, kiuj konservis iujn el siaj kutimoj bazitaj sur la sufiĉeco,
kaj ne sur la akumulado, kaj sur la kultivado de kunlabora kaj kolektiva
scio aparta de serĉo individuista kaj proprieta de la kulturo dominanta
en Okcidento.

Kiel paroli pri origino kaj kopio ekzemple, se kulturo de du jarmiloj de
la Ekstrema Oriento inspiras reproduktadon kaj traktas kiel pli gravan ol
la deveno de ideo ĝian enhavon kaj ĝian daŭron, eĉ se ĝi estas modifita
aŭ rekreita per ĉiu okazo? Aŭ kiel diri, ke estas nur unu homo
posedanta de ideoj, kiam por multaj originaj popoloj, inter ili iuj
indianoj, ne estas aparteco inter subjekto kaj objekto kiel ni konas en
Okcidento, kaj la krea subjektiveco, al kiu devus esti atribuita la
«aŭtoreco» aŭ la «posedo» de la havaĵoj, estas dividita en granda reto,
kiu enhavas homojn kaj aĵojn, naturon kaj socion preskaŭ simetrie?

Ekzemple en Afriko ekde kelkaj jarcentoj humanisma antaŭkolonia filozofio
konata kiel *Ubuntu*[^245] diras: «*mi estas, ĉar ni estas*». Esprimo de
deveno de *nguni banto*, influa en la batalo kontraŭ la *apartheid*
[rasa apartigo] en
Sudafriko kaj el ĉi tie al Subsahara Afriko, *Ubuntu* signifas la
humanecon kun viaj similuloj per la kulto al viaj prauloj, frate kaj
kompate, inkludante ĉi tiel kiel similajn ĉiujn vivaĵojn &mdash;
biocentrismo, kiu ankaŭ kontraŭas la antropocentrismon kutiman de la
okcidenta socio[^246], en kiu naskiĝas la intelekta havaĵo kaj la
aŭtorrajto. «La <i lang="grc">episteme</i> kaj la nigra filozofio de
Ubuntu havas sencon de scio malsaman ol tiu Okcidenta, scio integranta de
la racio kaj de la emocio, tiel ke la subjekto ne nur vidas la objekton,
sed ankaŭ sentas ĝin; do “subjekto kaj objekto estas reciproke
efikitaj en la scia ago”»[^247].

[^245]: Konsiderata filozofio de parola disvastigo kaj ne ligita al
  iu specifa teksto, ĝi estas vorto, kiu havas variojn en diversaj
  afrikaj lingvoj: <i lang="ki">umundu</i> en kikuja kaj
  <i lang="mer">umuntu</i> en kimera, du lingvoj parolataj en Kenjo;
  <i lang="suk">bumuntu</i> en sukuma kaj haja, parolataj en Tanzanio;
  <i lang="ts">vumuntu</i> en conga kaj cŭa en Mozambiko;
  <i lang="bni">bomoto</i> en bobanga, parolata en la Demokratia
  Respubliko Kongo; <i lang="kg">gimuntu</i> en konga kaj en kvesa,
  parolataj en la sama Kongo kaj en Angolo, respektive. Fonto: Eze,
  <cite>Intelectual History in Contemporary South Africa</cite>, p. 91.
[^246]: Negreiros, Ubuntu: considerações acerca de uma filosofia
  africana em contraposição à tradicional filosofia ocidental,
  <cite>Problemata: R. Intern. Fil.</cite>, v. 10, n. 2, p. 123.
[^247]: Mance, Filosofia africana: autenticidade e libertação, em Serra,
  <cite>O que é filosofia africana?</cite>, p. 75.

Mondvidmaniero, kiu havas valorojn kiel respekton, ĝentilecon,
kunhavadon, komunumon, malavarecon, konfidon, altruismon[^248], en kiu
la ni triumfas kaj la mi eĉ estas en la ni, malfacile povas enigi sin en
filozofan kaj ĵuran sistemon kiel la Okcidentan. Kiel igi produkton
unika kaj apartenanta al unu homo, se ĝi estas frukto de kolektiva
praa klopodo kaj ĝia fino estas transdono de ideo (aŭ objekto)
konstruita per multaj manoj? Komunumoj, kiuj havas manieron kaj
agon koni la mondon gviditaj de la kolektivo kaj de la komunumo restas,
kvankam kun malfacilaĵoj kaj multaj frapoj ricevitaj, konservante siajn
kulturajn havaĵojn kaj siajn tradiciojn ekde longa tempo antaŭe, en kiu
malgraŭ la konflikto kun la okcidenta ekskluziva vidpunkto, kiu vidas
praajn produktojn nur kiel havaĵojn vendeblajn en bazaro. Pli ol teni
ilin kaptitaj en idealigita estinto, rigardi iom pli kelkajn el tiuj
perspektivoj povas lumigi alternativajn vojojn por la nuntempo kaj
ebligi al ni kompreni iom pli kiel oni igis kaj igas la kulturon tra la
tempoj libera.

[^248]: Kiel rimarkis Nelson Mandela en klariga video pri Ubuntu
  disponebla en
  <https://pt.wikipedia.org/wiki/Ficheiro:Experience_ubuntu.ogv>.

### II.

<i lang="zh">Shanzai</i> estas neologismo kreita en la 2000-aj jaroj por
nomi tion, kio estas falsa, <i lang="en">fake</i>. Ĝi inkludas el
literaturo ĝis Nobelaj premioj, deputitoj, amuzparkoj, sportŝuoj,
muzikaĵoj, filmoj, tre diversaj historioj. Komence la termino aludis
nur al telefonoj (saĝtelefonoj) aŭ al la falsigo de produktoj de markoj
kiel Nokia aŭ Samsung kaj kiuj estis komercigitaj per la nomoj Nokir,
Samsing aŭ Anycat. Poste, tamen, ĝi disvastiĝis al ĉiuj fakoj, en ludoj,
kiuj simile al Dadaismo uzis la kreemecon kaj parodiajn efikojn kun la
«originaj» markoj por krei aliajn nomojn &mdash; Adidas ekzemple iĝas
Adidos, Adadas, Adis, Dasida[^249]... Ili estas tamen nur simplaj
falsaĵoj: iliaj desegnoj kaj ecoj ŝuldas nenion al la originalaj, kaj la
teknikaj aŭ estetikaj modifoj faritaj donas al ili propran
identecon[^250]. La <i lang="zh">shanzai</i> varoj estas karakterizitaj
ĉefe per siaj grandaj flekseblecoj, adaptiĝemaj laŭ la necesoj kaj
specifaj situacioj, «io, kio ne estas atingebla por granda firmao, ĉar
ĝiaj produktadaj procezoj estas fiksitaj longetempe»[^251].

[^249]: Han, <cite>Shanzai: el arte de la falsificación y la
  deconstrucción en China</cite>, p. 73.
[^250]: <i lang="lt">Ibidem</i>.
[^251]: <i lang="lt">Ibidem</i>.

En <cite>Shanzai: el arte de la falsificación y la deconstrucción en
China</cite>, la surkorea filozofo loĝanta en Germanio Byung-Chul Han
analizas kelkajn ĉinajn artaĵojn, <i lang="zh">shanzai</i> aŭ ne, kaj
okcidentajn por labori per la ideo, kiel estas konstruitaj la nociojn de
aŭtoreco kaj originaleco en la Ekstrema Oriento. Por ilustri la
diferencon rilate al Okcidento li citas la ideon de
<i lang="grc">ádyton</i>, kiu en antikva greka signifas «nealirebla» aŭ
«netransirebla». La origino de la vorto sendas al ena spaco de templo de
antikva Grekio, kiu estis tute aparta de la ekstero, kie religiaj kultoj
estis celebritaj. «La izolado difinas la sanktecon», diras Han: la nocio
esti izolita por povi renkonti Dion, aŭ onin mem, estas malsama en la
Ekstrema Oriento, komencante per la arkitekturo de la spacoj nomitaj
sanktaj: «La budhisma templo estas karakterizita per la permeableco aŭ
per la tuta malfermeco. Iuj temploj havas pordojn kaj fenestrojn, kiuj
izolas nenion»[^252].

[^252]: <i lang="lt">Ibidem</i>.

En la ĉina penso ne estas <i lang="grc">ádyton</i>, nek kiel spaco nek
kiel ideo. Nenio disigas nek malfermas sin: la penso, ke io estu aparta
aŭ izolita de la tuto estas fremda por la pensmaniero superreganta en la
Ekstrema Oriento[^253]. Do ne estas ideo de *originalo* kiel oni ĝin
komprenas en Okcidento, ĉar la originaleco postulas komencon
striktasence, kiu parto de la ĉina penso neas, ĉar ili ne konceptas
kreaĵon bazitan sur absoluta kaj individua komenco, sed jes sur la
senĉesa procezo, sen komenco nek fino, sen naskiĝo nek morto,
fundamente kolektiva. La malfido de la principoj neŝanĝeblaj kaj de la
kreemaj individuaj «geniuloj» originas el la manko de esenco kaj el ia
malpleno, kiu, por okcidentaj okuloj &mdash; ilustritaj de Han en la
penso kritika kontraŭ tiuj nocioj orientaj de la germana filozofo Hegel,
unu el la plej influaj okcidentaj pensuloj &mdash;, povas esti rigardita
kiel hipokrito, ruzo aŭ eĉ malmoraleco.

[^253]: <i lang="lt">Ibidem</i>.

Paroli pri aŭtoreco, originaleco kaj sekve pri aŭtorrajto, kopirajto kaj
libera kulturo en la Ekstrema Oriento estas do malsama ol paroli pri tio
en la okcidenta mondo. Tiu pensmaniero, kiu ignoras la
<i lang="grc">ádyton</i>, tiun «nealireblan», estu kiel ejo aŭ kiel ideo,
naskiĝas el nocio de *vero kiel procezo*, tio estas, pli bazita sur la
daŭra *inkludo* de diversaj eroj ol sur la ekskludo kaj sekva unuigo ĉirkaŭ
sola ero aparta el ĉio, kiel ni kutime pensas en Okcidento. Se la vero
estas en daŭra procezo de produktado, la nocio, el kiu ĝi venas &mdash;
ĝia *originaleco* &mdash; perdas gravecon; ne estas plu la sola origino,
tio kio gravas, sed ke la enhavo de tiu vero transcendu, disvastiĝu,
estu reorganizita kaj suplementita laŭ malsamaj situacioj, celoj kaj
intencoj. Ĉiu ero estas grava kiel parto de ideo, kiu transcendas
kolektive, ne kiel parto izolita, kiu havas originon kaj aŭtorecon.

Dirinte tion, la kreema procezo de arta kaj kultura verko en tiu regiono
estas karakterizita per la daŭreco kaj la silentaj ŝanĝoj, ne per la
rompo, kiun genia ideo havigita de artisto propragandas, kiel
konsekritan en la okcidenta vidpunkto post la romantikismo. Ne estas
valorata tiel la *utero* de la ideo, ĝia origino aŭ ĝia aŭtoro, sed kiel
ĝi estos &mdash; aŭ necesas esti &mdash; daŭrigita. Se la ideo restas en
la kopio, estas kiel se la verko daŭru, seninterrompe, sen «nova verko».
Ĝi estas kiel en la naturo: malnovaj ĉeloj estas anstataŭitaj per nova
ĉela materialo. Oni ne demandas pri la origina ĉelo; «la maljuna mortas
kaj estas anstataŭita per la nova. La identeco kaj la noveco ne estas
ekskluzivaj»[^254].

[^254]: <i lang="lt">Ibidem</i>, p. 64.

Parto de tiu maniero vidi la veron, la originalecon kaj la krean
procezon kiel ion pli kolektivan ol individuan, originas el la
konfuceismo (儒學), aro de moralaj, etikaj, filozofaj kaj religiaj
doktrinoj kreitaj de la disĉiploj de Konfuceo post lia morto, en 479 a.
K., kiu havis grandan influon sur la penso de Ĉinio kaj de landoj kiel
la Koreioj, Japanio, Tajvano kaj Vjetnamio, precipe ĝis la komenco de la
20-a jarcento. Origina de la provinco de Lu, hodiaŭ Ŝandongo, ĉina
oriento, Konfuceo devenis de dekadencinta nobela familio kaj havis
diversajn profesiojn dum sia vivo &mdash; profesoro, ŝtata funkciulo,
politikisto, pentristo, paŝtisto &mdash; ĝis esti ĉirkaŭ 50-jara, kiam
li komencis ofte vojaĝi tra la ĉinaj provincoj kaj varbi disĉiplojn
al sia filozofio bazita sur la simpla vivo, la kolektivo kaj la
altruismo[^255]. Ĝi estis filozofia propono, kiu reprenis iujn kutimojn de
pli malnovaj ĉinaj dinastioj kiel la Shang (1600-1046 a.K.) kaj la Zhou
mem (1046-256 a.K.), periodo en kiu ekzistis morala kaj etika dekadenco en
la ĉina socio.

[^255]: Malgraŭ lia graveco en la ĉina tradicio, malmultaj informoj pri
  Konfuceo estas fakte pruvitaj. Tiuj tie ĉi cititaj estas bazitaj sur la
  registro de la angla Vikipedio
  (<https://en.wikipedia.org/wiki/Confucius>) pri li kaj sur la
  enkonduko al la brazila eldono de la <cite>Analektoj de Konfuceo</cite>
  (<cite>Os analectos</cite>), skribita ankaŭ de la tradukisto de la
  verko el la ĉina al la angla, D. C. Lau.

Post la dinastio Han (206 a.K. ĝis 220 p.K.) la instruoj de Konfuceo
komencis havi grandan influon sur la registaroj kaj sur la ĉina socio,
provizante la planon de tio, kio estus ideala vivo kaj regulo sur kiu
oni devus mezuri la homajn rilatojn[^256]. Reinventitaj kaj
reinterpretitaj de diversaj homoj dum jarcentoj, liaj ideoj iĝis modeloj
de kutimaro en la edukada, kultura, politika fako kaj tiu de la landaj
sociaj rilatoj dum malsamaj momentoj de la imperia Ĉinio. Ili nur perdis
forton dum la komenco de la 20-a jarcento, kiam la ĉina imperia periodo
finas kaj la konfuceismo komencas esti akuzita esti «tro tradicia» por
kunvivi kun la vigleco de la tiama moderna ĉina socio.

[^256]: Yu, <cite>Intellectual Property and Confucianism: Diversity in Intellectual
Property: Identities, Interests and Intersection</cite>, p. 5.

La influo de la instruoj de Konfuceo sur la maniero vidi la kreadon en
Ĉinio kaj en la Ekstremaj Orientaj landoj komencas akiri influon en la
studioj pri intelekta propraĵo ĉefe post la libro <cite>To Steal a Book
is An Elegant Offense Intelectual Property Law in Chinese
Civilization</cite>, de William P. Alford, eldonita en 1995. La libra
titolo, «Ŝteli libron estas eleganta krimo» venas el populara ĉina
koncepto (<i lang="zh">Qie Shu Bu Suan Tou</i>) post <cite>Kong Yiji</cite>,
libro eldonita en 1919 de konata tiama verkisto nomita Lu Xun. La verka
historio temas pri ĉefa rolulo, kies nomo titolas la libron, memlerninta
intelektulo alkoholula kaj malsukcesa, kiu kutimas iri al taverno en la
urbo de Luzhen (魯鎮), bazo de aliaj fikcioj de Xun. Li ne trapasis la
ekzamenon de <i lang="zh">xiucai</i>, unu el la multaj de la tiama
imperia Ĉinio, kaj uzas en sia parolado konfuzajn klasikajn frazojn,
kiuj naskigas malestimon inter la frekventantoj de la ejo, kiuj ankaŭ
mokis ĝin pro «fari laboretojn» kaj ŝteli por manĝi kaj trinki. Unu el
liaj plej ŝatataj agoj estis kopii manuskriptojn por riĉaj aĉetantoj;
multfoje li ŝtelis librojn por
interŝanĝi ilin kontraŭ vino en la taverno. «Ŝteli libron estas eleganta
krimo» estis la argumento, kiun li uzis, kiam li estis insultita de la
frekventantoj de la ejo.

*Kong Yiji*, la ĉefa rolulo, estis kreita kiel «karikatura arlekeno»,
kiu reprezentis memlernintan intelektulon de la dekadenca ĉina klasika
periodo[^257] &mdash; en la libra fino la rolulo mortas bastonita kaj
forgesita. La verko estis farita en kunteksto de la Movado de la 4-a de
majo, al kiu Lu Xun apartenis, kiu populariĝis pro la protestoj en la
ĉefurbo Pekino kaj pro la kontraŭimperiisma kritiko (la lasta imperia
dinastio, Qing, finiĝis en 1911), en kiu la konfuceismo kaj siaj
tradiciaj, hierarkiaj kaj kolektivistaj manieroj estis konsideritaj kiel
baroj por la modernigo en la «konkurado kun aliaj nacioj de la okcidenta
mondo»[^258]. Per tiu vidmaniero la literaturo de la Movado de la 4-a de
majo, ligita la la modernigemaj ideoj de aliaj mondaj lokoj de tiu
periodo, devus klopodi eviti la kliŝojn de la tradicia ĉina lingvistiko,
kiuj limigis la kreeman pensadon de homoj dum jarcentoj, kaj veti por la
novigado kaj de enhavo, rigardita kiel eksmoda, kaj de formo.

[^257]: En Yu, <i lang="lt">op. cit.</i>, kiu ĉi tie ankaŭ citas Feng-on,
  <cite>Intellectual Property in China</cite>, p. 167.
[^258]: Yu, <i lang="lt">op. cit.</i>, p. 15.

Sed kiuj estis tiuj valoroj kaj tradiciaj ideoj, kiujn la modernismaj de
la 4-a de majo batalis? Por la konfuceismo la estinteco donis sociajn,
etikajn kaj moralajn valorojn &mdash; ekzemple la ideon de familio kiel
baza unuo, kiu organizas la komunumon &mdash;, kiuj devus esti
integritaj en la nuntempan socion. La neceso koni la estintecon por la
persona kreskado postulis, ke estu granda aliro al la komuna heredaĵo de
ĉiuj ĉinoj[^259]. Ĉar havi proprieton de tiuj estintecaj verkoj permesis
al malmultaj monopoligi tiel esencan scion por ĉiuj, ekzistis tiam
kontraŭdiro inter la rajtoj de intelekta propraĵo kaj la tradiciaj ĉinaj
moralaj valoroj defenditaj de Konfuceo. Altigante la familiajn valorojn
kaj la kolektivajn rajtojn la ĉinoj ne disvolvis la koncepton de
individuaj rajtoj; do ili ne konsideris la kreemecon kaj la novigadon
kiel individuan proprieton, sed kiel komunan profiton por la komunumo kaj la
estonteco. Por tiuj, kiuj konsideris la individuismon antaŭkondiĉon por
la disvolvo de la rajtoj de intelekta propraĵo, tiu vidpunkto de la
mondo signifis grandan defion[^260].

[^259]: Alford, <cite>Steal a Book Is an Elegant Offense: Intellectual
  Property Law in Chinese Civilization</cite>, p. 20.
[^260]: Yu, <i lang="lt">op. cit.</i>, p. 4.

Estis alia grava afero starita en la ĉina kulturo kaj en tiu de la
popoloj de la Ekstrema Oriento post la konfuceismo. En tiu filozofio
ekde tre junaj la infanoj estis instruitaj pensi per la memorigo kaj
kopiado de la klasikaj, procedo kiu, laŭ iliaj instruistoj, lernigus al
la junuloj familiajn valorojn, filan fiecon kaj praan respekton[^261].
La memorigo kaj la kopiado, speciale de la verkoj de Konfuceo, iĝis
necesaj procedoj por garantii la sukceson en la ekzamenoj de la Imperia
Publika Servo, faritaj dum tridek jarcentoj (inter 605 kaj 1905,
proksimume) kaj kiuj konsistis el testaro, kiu servis por elekti al kiu,
inter la loĝantaro (iĉa kaj aristokratarida), oni permesus la eniron al
la ŝtata burokratio &mdash; kio donus povon kaj gloron al la kandidatoj
kaj honorojn al iliaj familioj, distriktoj kaj provincoj.

[^261]: <i lang="lt">Ibidem</i>.

Kiam tiuj infanoj kreskis, ili iĝis pli *kompilantoj* ol *kreantoj*. Ili
memoris tiom da klasikaj verkoj, ke ili komencis konstrui siajn proprajn
rakontojn per vasta procezo kopii kaj alglui (<i lang="en">cut-and-paste</i>)
frazojn, fragmentojn kaj partojn de tiuj malnovaj tekstoj. Kiam por
vidpunkto de okcidentulo tio estus rigardita kiel plagiato, por tiamaj
ĉinoj estis rigardita kiel distinga trajto de intelekteco kaj kultura
scio. «Kiam ĉinaj tradiciaj aŭtoroj prenas pecojn de antaŭekzistanta
teksto kaj, precipe, de klasika, oni atendas, ke la leganto rekonu la
fonton de la prenita materialo tuje. Se leganto estas sufiĉe malbonŝanca
ne rekoni tiun cititan materialon, estas ria kulpo, ne de la aŭtoro»[^262].

[^262]: En Yu, <i lang="lt">op. cit.</i>, kaj Stone, What Plagarism Was
  Not: Some Preliminary Observations on Classical Chinese Attitudes
  Toward What the West Calls Intellectual Property, <cite>Marquette Law
  Review</cite>, 2008.

En *Lún Yǔ* (論語, konata en Esperanto kiel la <cite>Analektoj</cite>),
ĉefa kolekto de eseoj kaj ideoj atribuitaj al Konfuceo, estas skribita:
«La Majstro diris: “Mi komunikas, sed mi ne faras ion novan; mi estas
vera en tiu, kiu mi diras, kaj dediĉita al la Malnovepoko” (shù ér bù
zuò)»[^263]. Kvankam tiu aserto povas iom senkuraĝigi la kreemon, ĝi
havis kiel kialon emfazi la rolon de ĉiu kiel portanto de tradicio, kaj
ne kiel fondinto aŭ originanto de nova doktrino[^264]. En alia loko de
la <cite>Analektoj</cite> estas atribuita al Konfuceo la frazo: «Meritas
esti profesoro homo, kiu malkovras la novan revigligante en sia menso
tion, kion li jam scias (<i lang="zh">wēn gù é zhīxīn / kěy ǐ wéi shī yǐ</i>)»[^265].
Laŭ Yu, la penso de Konfuceo manifestis vidpunkton, ke «la eblo
transforme uzi antaŭekzistantajn verkojn povas demonstri la komprenon
kaj la piecon al la kerno de la ĉina kulturo, kaj ankaŭ la eblon
distingi la estantecon de la estinteco per originalaj pensoj»[^266].

[^263]: Confucio, <cite>Os analectos</cite>, libro VII, ĉap. 1, p. 62.
[^264]: Yu, <i lang="lt">op. cit.</i>, p. 7.
[^265]: Confucio, <i lang="lt">op. cit.</i>, p. 44.
[^266]: Yu, <i lang="lt">op. cit.</i>, p. 69.

Lasta faktoro, kiu influis la ĉina malsamecon rilate al la intelekta
propraĵo kiel ĝi estis konceptita en Okcidento, estas certa malestimo de
la konfuceanoj al la komerco kaj al la kreo de verkoj nur pro lukro.
Denove la <cite>Analektoj</cite>: «La okazoj, en kiu la Majstro parolis
pri lukro, Destino kaj bonvolemo estis maloftaj» (<i lang="zh">Zi hǎn yá
lì</i>)»[^267]. En lia vaste uzita traduko al la angla, Arthur Waley
klarigis la majstran instruon aldonante la piednoton «Ni povas
aldoni: malofte ni parolas pri aferoj el la vidpunkto, kio pagus
pli bone, sed nur el la vidpunkto, kio estas ĝusta»[^268]. La
komercistoj (<i lang="zh">shāng</i>) estis konsideritaj la plej malalta
inter la kvar sociaj klasoj de la tradicia ĉina socio, post la
studema-oficisto (<i lang="zh">shì</i>), la
terkulturisto (<i lang="zh">nóng</i>) kaj la metiisto
(<i lang="zh">gōng</i>). Ne estis do surprizo, ke la konfuceanoj ne
emfazis, ĝis la 20-a jarcento, la nocion de intelekta propraĵo kaj la
koneksan ideon de la komercaj ekskluzivaj rajtoj[^269].

[^267]: Confucio, <i lang="lt">op. cit.</i>, p. 69.
[^268]: Yu, <i lang="lt">op. cit.</i>, p. 8.
[^269]: Kiel Yu asertas, «La intelekta propraĵo povas esti rigardita
  ankaŭ en la ĉina esprimo de la termino «patento»,
  <i lang="zh">zhuānlì</i> (专利), kiu povas esti tradukita laŭlitere
  kiel «ekskluziva profito» aŭ «ekskluziva lukro». Kurioze la mortinta
  d-ro Arpad Bogsch, la ĉefa longatempa direktoro de la Monda Organizaĵo
  de Intelekta Propraĵo, konsideris la terminon tiel problema, ke
  «proponis, ke iu alia ĉina termino devus esti uzita por anstataŭigi la
  du ĉinajn signojn, por eviti miskomprenojn» (Yu
  <i lang="lt">op. cit.</i>, p. 8).

En la budhisma kaj ŝintoisma flanko, la aliaj du idearoj plej
disvastigitaj en Ekstrema Oriento, la influo de la konfuceismo en la
ĉina kulturo igis, ke la perspektivo de la aŭtorrajto estu turnita, dum
multe da tempo, pli por la defendo de bazo de publika informo, de libera
aliro kaj reuzo &mdash; kio en Okcidento estis nomita publika havaĵo. La
ĉina prokrasto subskribi internaciajn traktatojn de intelekta propraĵo
(post la 1980-a jardeko, kiam ankaŭ la lando komencas aparteni al la
World Intelectual Property Organization) rilatas al kolektiva kulturo
kaj defenda de la publika havaĵo radikita ekde multe da tempo en ĝia
socio. Kaj ankaŭ rilatas al la disvastigo de la
<i lang="zh">shanzai</i> kulturo jam citita, kiu havas la kopion kiel
bazon por la rekreo de malsamaj produktoj kaj markoj per kreiva
*kompilanta* praktiko radikita en la kutimoj de la popolo de la regiono,
eĉ post la perdo de influo de la konfuceismo.

Tamen, en la lastaj jardekoj de la 20-a jarcento kaj en la unuaj de la
21-a jarcento, Ĉinio ne nur adaptiĝis al la okcidenta rigardo de la
intelekta propraĵoj, sed, en 2020, iĝis la gajnanto de internaciaj petoj
de patentoj, antaŭ Usono[^270]. Aliflanke ĝi estas la lando, al kiu plej
estas atribuitaj malobservoj de aŭtorrajoj en la muzika fako; la
International Federation of the Phonographic Industry [Internacia Federacio de la Aŭdvida Industrio] (IFPI) asertas, ke
95% de la muzikaĵoj, kiuj cirkulas en la lando, okazas post
elŝutoj sen rajtigo de la proprietuloj[^271]. Preter la duboj pri la
datumoj de tiuj esploroj, ni povas demandi nin kiel estus leĝaro, kiu
respektus la kolektivisman estintecon de la konfuceisma kulturo de Ekstrema
Oriento kaj dialogus kun nuntempa nocio de aŭtorrajto de la ekzistanta
situacio de la kapitalismo? Yu vetas por nocioj, kiuj ne klopodas
maksimumcelan leĝaron &mdash; tio estas, ke ne havu *ĉiujn la rajtojn
rezervitajn* al la posedantoj de aŭtorrajto, ekzemple. Tio estas la okazo
de Creative Commons, citita de li kiel ekzemplo de opcio, en kiu «la
konfuceanoj povas havi malestimon por la komerco kaj eĉ tiel akcepti la
rajtojn de intelekta propraĵo»[^272].

[^270]: Afero diskonigita de la AFP per komunikaĵo de Wipo en 2019
  diris, ke Ĉinio «konkeris la titolon» unuafoje. «En 1999 la OMPI
  ricevis 276 petojn de Ĉinio, kontraŭ 58&nbsp;990 en 2019, 200-oble pli
  hodiaŭ ol antaŭ 20 jaroj», detalis la ĉefa direktoro de la organizaĵo,
  Francis Gurry. Fonto: AFP: China se torna campeã
  de pedidos internacionais de patentes, <cite>UOL Notícias</cite>, 7-an
  apr. 2020, disponebla en
  <https://noticias.uol.com.br/ultimas-noticias/afp/2020/04/07/china-se-torna-campea-de-pedidos-internacionais-de-patentes.htm>.
[^271]: La IFPI en sia batalo kontraŭ la tiel nomita pirateco ŝatas
  montri tiujn datumojn dirante kiel kaj kiom gajnus la kulturaj
  industrioj, se estus adekvateco al la okcidentaj aŭtorrajtaj normoj.
  Ekzemploj disponeblas en la retejo de la organizaĵo:
  <https://www.ifpi.org>.
[^272]: Yu, <i lang="lt">op. cit.</i>, p. 7.

### III.

«Donaco de indiano» (<i lang="en">Indian giver</i>) estas termino,
kiu aperas en la okcidentaj lingvoj post la kontakto de la eŭropaj
kolonigintoj kun la originaj popoloj de la kontinento, kiu tiam estis
baptita kiel *Ameriko*. En la angla ĝi estas unue registrita en 1765 por
difini specon de donaco, kontraŭ kiu oni atendas samvaloran repagon. Unu
jardekon poste <i lang="en">Indian giver</i> estis kompilita en vortaro
de amerikaj vortoj kiel komuna frazo inter novjorkaj infanoj por
esprimi donacon, kiun iu donas kaj denove ricevas[^273], la saman
signifon, kiun ĝi akiris en la portugala lingvo kaj kiun, aparte en Brazilo,
oni aldonas al la senco de «nedezirata donaco», kiun la esprimo akiris
en sia kutima uzo.

[^273]: Observita en 1765 de Thomas Hutchinson en <cite>History of
  Massachusetts: From the First Settlement thereof in 1628, until the
  Year 1750</cite> kaj poste difinita en la citita vortaro, skribita de
  John Russell Bartlett. Fonto:
  <https://en.wikipedia.org/wiki/Indian_giver>.

La kutima uzo de la esprimo venas de ago, kiu komprenas kiel «normalan»,
ke iu ricevu donacon kaj konsumu ĝin kiel alian ajnan aferon ricevitan aŭ
akiritan. Okcidenta kaj de eŭropa deveno tiu normalo ne estas la kutimo
de multaj originaj popoloj de la amerika kontinento kaj de aliaj
regionoj de la planedo. Por tiuj, kio ajn prezentita aŭ donita devas havi
ian repagon, esti transdonita, minimume anstataŭita, ne konservita por
ĉiam aŭ reinvestita por ekskluziva profito de unu homo. Donaco estas la
komenco de cirkla rilato de iro kaj reveno, kiu antaŭsupozas reciprokajn
respondecojn kaj ne finas per la ago ricevi, konservi kaj uzi iam, kiel
kutimas en la okcidentaj socioj, kie la kapitalismo superregas kiel
ĉefa maniero organizi la vivon. Estante la komenco de rilato ĝi ne povas
esti konsumita kaj forĵetita kvazaŭ simpla komercaĵo. Se ĝi tio estas, ĝi
povos esti reprenita: «donaco de indiano».

La jena okazo rakontita de Viveiros de Castros estas ilustra.

> Tre kutimas, ke filma skipo alvenu al indiĝena loko kaj oferu 30 mil
> dolarojn por filmi, kaj ke la indianoj konversaciu inter si kaj faru
> kontraŭproponon, 40 mil dolarojn, kaj fermu la negocon. Estas farita
> la akordo. Tiam estas farita la filmo kaj la skipo pensas, ke ili solvis
> la problemon. Ili rekte pagas kaj tiel plu. Kiam la filmo estas
> eldonita, la reĝisoro ricevas telefonvokon, kiu jen diras: «Vi ŝuldas
> al ni monon, vi ŝtelis de la homoj!». Tiam li diras: «Atendu, mi
> subskribis paperon, mi jam donis la 40 mil», kaj la indianoj: «Ne, sed
> vi ne pagis ion ajn», aŭ tiam «ne estis por ĉiuj». Tiam li subite
> rimarkas, ke la indianoj havas koncepton de transakcio, de socia
> rilato ĝenerale, treege kontraŭan al nia. Kiam ni faras transakcion,
> ni komprenas, ke ĝi havas komencon, mezon kaj finon, mi donas al vi
> aferon, vi pagas ĝin al mi, ni pacas, vi iras ien, mi iras aliloken.
> Tio estas, la transakcio estas farita rigardante ĝian finon. La
> indianoj, malsame: la transakcio neniam finas, la rilato neniam finas,
> komencis kaj finos ja neniam, estas dum la tuta vivo. Petante pli da
> mono ne estas ekzakte la mono, kiun la indianoj volas, sed la rilaton.
> Li ne akceptas, ke la propono finis, nenion ja finis, nun komencos.
> De kie la famaj kliŝoj: la indianoj ĉiam petas. Jes, ili petas. Kaj ni
> plendas, ke tio, kion ili akiras estas subite forlasita: la vilaĝoj
> pleniĝas de aĵoj forĵetitaj, kiujn la indianojn petis al ni, ili insistis
> ĝis atingi, kaj kiam ili atingas ne prizorgas ilin, lasas putri,
> rustiĝi. Kaj la blankuloj restas kun tiu ideo, ke tiuj indianoj estas
> eĉ sovaĝuloj, ne scias prizorgi la aferojn. Sed klaras, la problemo de
> ili ne estas la aĵo, kion ili volas estas la rilato.[^274]

[^274]: Viveiros de Castro, Economia da cultura digital, en Savazoni;
  Cohn (org.). <cite>Cultura digital.br</cite>, p. 90.

En la antropologio estas multaj studoj pri la speco de transakcio, kiu
okazas per la interŝanĝo (aŭ dono) de donacoj. Unu el la plej
malnovaj kaj influaj estas tiu de la franca Marcel Mauss, la hodiaŭ
klasika «Esesai sur le don» [Eseo pri la dono], publikigita en Francio
en 1924[^275], en kiu li komparas malsamajn sistemojn de donacoj inter
socioj de Polinezio, Melanezio kaj norokcidento de la amerika
kontinento por klarigi la interŝanĝon de donacoj kiel fenomenon, kiu
antaŭsupozas diversajn transakciojn &mdash; jurajn, moralajn,
estetikajn, religiajn, mitologiajn &mdash;, aldone al la ekonomiaj.
Mauss asertas, ke la sistemo de interŝanĝo de donacoj en tiuj socioj
havas komunan regulantan principon: la devigo doni, ricevi kaj
rekompenci. Anstataŭ redukti tiujn transakciojn al simplaj interŝanĝoj
de donacoj, la franca montras, ke tiu procedoj portas kun si moralan
dimension, kiu karakterizas ilin kiel provizon de servoj, kiuj klopodas
estigi novajn aliancojn kaj fortigi malnovajn[^276].

[^275]: La origina referenco estas Mauss, «Essai sur le don»,
  <cite>L’Année Sociologique I</cite>, p. 30-186. La antropologo
  Marshall Sahlins metas la ideojn de donaco en la politika filozofio
  post Mauss en <cite>Stone Age Economics</cite> (Ŝtonepoka ekonomio),
  eldonita en 1972. Lewis Hyde parolas pri la temo en la kultura kaj
  arta fakoj en <cite>The Gift</cite> (La donaco), en 1983; inter
  diversaj aliaj konsekvencaj verkoj, kiuj disvolvas, perfektigas kaj
  rekombinas la ideojn de Mauss pri donaco.
[^276]: Sertã; Almeida, Ensaio sobre a dádiva, en <cite>Enciclopédia de
  Antropologia</cite>. Disponebla en: <http://ea.fflch.usp.br/obra/ensaio-sobre-dádiva>.

Mauss rimarkas, ke la cirkulantaj havaĵoj en tiuj sistemoj estas
nedisigeblaj de siaj proprietuloj kaj havas propran moralan substancon
rilatitan kun la spirita materio de la donanto de iu ajn aĵo[^277]. Tiu
substanco estas donita, kiam li interŝanĝas donacon kaj ĝi komencas
cirkuli kun tiu aĵo, kiu tiam neniam nur estos «simpla aĵo», estu ĝi kio
ajn estu, sed io, kiu havas *intencon* kaj kiu vivas egalece kun la
homoj. Per tiu senco la sistemo de komercaĵo konata en Okcidento iĝas
malsama por la vidpunktoj de la tradiciaj popoloj. Laŭ vortoj de la
antropologino Marilyn Strathern (1984), estas la opozicio de la vara
[<i lang="en">commodity</i>] ekonomio, en kiu la homoj kaj aĵoj
alprenas la socian formon de aĵoj, kun la donaca ekonomio
(<i lang="en">gift</i>), en kiu homoj kaj aĵoj alprenas la socian
formon de la homoj[^278]. Estas per tiu senco, ke en originaj socioj
de diversaj partoj de la mondo la modelo de proprieto (specife tiu de
intelekta propraĵo), paŭsita en la rilato de arta verko kiel konsuma
komercaĵo, iĝas nesufiĉa por trakti rilaton pli daŭran kaj kompleksan de
la cirkulado de aĵoj/havaĵoj[^279]. En la kultura sistemo de la originaj
socioj percepteblas unue la centrecon de la kolektivaj valoroj,
ligitaj al la diverseco kaj al la postvivo de la komunumo, rilate al la
individuaj valoroj, de ekskluziva uzo kaj individua elekto. Kio siavice
igas, ke la kulturaj kaj sciaj havaĵoj en tiu kunteksto pli malfacile
iĝu nur *unu varo pli* vendita kiel komercaĵo, ĉar estas principoj kaj
respondecoj de reciprokeco kaj solidareco, kiuj klopodas valorigi la
propran moralan substancon &mdash; kiun ankaŭ ni povus nomi «animo»
&mdash; de la aĵoj en iliaj rilatoj kun la homoj kaj la mondo. Citante
unu ekzemplon, la rajtoj de la guarania popolo, unu el la plej
ekzistantaj en Brazilo kaj en Sudameriko, estas gviditaj per la
principoj de valorigo de kolektivaj rajtoj malprofitante la individuajn,
kio estigas normojn pli molajn, diskutitajn de tempo al tempo en
komunumoj en la <i lang="gn">Aty Guassu</i> (granda asembleo), bazitaj
sur iliaj kulturaj praktikoj kaj kiuj klopodas konservi la ekvilibron de
la kunvivado kaj la respekton al la tradicioj[^280].

[^277]: <i lang="lt">Ibidem</i>.
[^278]: Strathern, <cite>The Gender of the Gift: Problems with Women and
  Problems with Society in Melanesi</cite>. Citita, per tiuj terminoj,
  de Coelho de Souza, The Forgotten Pattern and the Stolen Design:
  Contract, Exchange and Creativity among the Kĩsêdjê, en Brightman;
  Fausto; Grotti, <cite>Ownership and Nurture: Studies in Native
  Amazonian Property Relations</cite>.
[^279]: Coelho de Souza, <i lang="lt">op. cit.</i>, p. 183.
[^280]: «La indiĝena rajto estas praktiko naskita per la socia konsento,
  modifante sin en la praktiko mem. Tiuj principoj estas kiuj gvidas la
  komunuman moralecon, tabuojn kaj mitojn, kiuj limigas kaj alidirektas
  la socian kunvivadon al ekvilibra ebeno kaj kiam poste okazas
  malekvilibro, tiuj principoj estas direktitaj al sani la okazitan
  rompaĵon». Indas ekzameni la artikolon: Machado; Ortiz, Direito
  e cosmologia Guarani: um diálogo impreterível, <cite>Revista de
  Direito: trabalho, sociedade e cidadania</cite>.

Due oni devas emfazi, ke en la kultura sistemo de la originaj socioj
la nocio de kolektiveco estas eĉ pli kompleksa ol ĝi aspektas. La
antropologino Marcela S. Coelho de Souza (2016) diras, ke, por indianaj
popoloj kiel la suia [<i lang="suy">Kisêdjê</i>], de la regiono proksima
al parko de Xingu, nordo de la ŝtato de Mato-Groso, en la brazila
Amazona arbaro, nek subjekto nek objekto, nek kreanto nek estaĵo
kondutas laŭ la okcidentaj atendoj enhavitaj en tiuj sencoj.
La kolektivo ĉi tie ne implicis nur homojn, sed ankaŭ aĵojn kaj la
malsamajn reciprokajn rilatojn inter ili, kio igas la vortaron
proponitan per la nocio de intelekta propraĵo, bazita sur la klara
apartigo inter subjekto kaj objekto, kreanto kaj estaĵo, pli malriĉa por
uzi ĝin en tiuj okazoj.

Ankaŭ ne facilas harmoniigi la logikon de kolektivaj rajtoj de iu
subjekto sur ria kreaĵo, ĉar, por multaj indianaj popoloj, preskaŭ ĉio,
kio difinas la homan kulturon, venas de ekstero, aŭ estas akirita per
eksteraj fortoj. En la okazo de la suia [<i lang="suy">Kisêdjê</i>]
popolo ekzemple la maizo venas de la rato; la fajro de la jaguaro; nomoj
kaj korpaj ornamaĵoj de raso de nanoj hommanĝuloj; muzikaĵoj de la
abeloj, vulturoj, arboj kaj akvaj testudoj, inter aliaj okazoj,
kiuj permesas aserti, ke «se la kulturo de la suiaj
[<i lang="suy">Kisêdjê</i>] apartenas al la suiaj
[<i lang="suy">Kisêdjê</i>], estas precize ĉar ne estas ili la
kreantoj»[^281]. Coelho de Souza asertas, ke, kiam la rajtoj
ĉirkaŭigantaj la kulturajn kaj sciajn havaĵojn estas riskitaj, ili
neniam naskiĝas kiel kolektivaj rajtoj, kiuj povas esti sendube
atribuitaj al homoj aŭ grupoj, sed antaŭe al vasta reto de heterogenaj
prerogativoj, rajtoj kaj devoj, kiuj ne facile enmeteblas en la
formujojn de la jura reprezento postulita en la formoj de jura
kontrakto[^282].

[^281]: Coelho de Souza, <i lang="lt">op. cit.</i>, p. 183.
[^282]: Coelho de Souza, <i lang="lt">op. cit.</i>, p. 181, citante
  alian konatan tekston de la antropologio en Brazilo:
  Carneiro da Cunha, «Cultura» e cultura: conhecimentos tradicionais e
  direitos intelectuais, en <cite>Cultura com aspas e outros
  ensaios</cite>, p.  331-373.

Kreado kaj intelekta propraĵo en la okcidenta penso, speciale post la
Klerismo kaj John Locke en la 17-a jarcento, estas nocioj ligitaj al
la ideo, ke aĵoj (historioj, rakontoj) estas faritaj per la homa
intelekto, frukto de nia subjektiveco kaj kiel io, kio estas etendo de
nia memeco. Per tiu ideo la kreivo esta specifigita per la produkto de
iu ajn aĵo, sed ne naskiĝas de tiu, kiu restas inerta kaj senviva
&mdash; almenaŭ en ĝia jura kaj tradicia koncepto. La intelekta
propraĵo, en tiu senco, estas fiksita kiel rilato inter homoj rilate al
(kaj perita per) aferoj[^283], kun klara disigo inter tio, kio estas
materio, kaj tio, kio estas spirito, subjekto kaj objekto.

[^283]: Coelho de Souza, <i lang="lt">op. cit.</i>, p. 182. Mia traduko
  de «<i lang="en">Property is anchored as a relation among people
  with respect to (mediated by) things</i>».

La indiana vido estas tre malsama. Ĝi traktas ekzemple la aĵojn kiel
registrojn «malpli pasivajn de la kapabloj de subjekto ol la
personigitaj objektigoj de tiuj rilatoj»[^284]. Tiel, ke la kreado
okazas *dise* en la rilato inter la multaj objektoj kaj personoj, sen
tiu disigo inter subjekto kaj objekto, intelekto kaj materio, kiun ni
kutime faras en Okcidento. La subjektiveco ankaŭ ekzistas en objektoj
kaj formas vivan pejzaĝon konsistantan el malsamaj specoj de niveloj de
homaj agoj[^285]. Por la indiĝenaj ĉiu aĵo, kaj ankaŭ besto, estas eble
subjekto, kio igas, ke ni reiru al la *indiana perspektivismo*
proponita de Eduardo Viveiros de Castro kaj Tânia Stolze Lima, koncepto
vaste disvastigita en la antropologio kaj kiun ni povas difini kiel
ideo, ke «estaĵoj provizitaj per animo rekonas sin mem kaj tiujn, kiuj
aspektas kiel homoj, sed estas perceptitaj de aliaj estaĵoj en la formo
de bestoj, spiritoj aŭ kategorioj de nehomoj»[^286].

[^284]: <i lang="lt">Ibidem</i>, p. 182.
[^285]: Adapto mia, en la originala: «<i lang="en">In this
  distributed mode, recombinations are not the work of an intellect
  separated from matter; here, subjectivity exists distributed in
  objects, forming “an animated landscape composed of different kinds
  of bodies in which change and effect are events with meaning on the
  same level as human actions” (Leach, 2004, p.169). […] “People” and
  “things” appear then as indexes of capacities and powers the
  apprehension of which becomes the focus of the explicit practice of
  subjects</i>» (Coelho de Souza, <i lang="lt">op. cit.</i>, p. 183).
[^286]: Ĉi tiu difino venas per la helpo de Marcial, «Perspectivismo
  amerindio», en <cite>Enciclopédia de antropologia</cite>. Disponebla
  en <https://ea.fflch.usp.br/conceito/perspectivismo-amer%C3%ADndio>.
  La koncepto estas detalita en, inter aliaj verkoj, <cite>Um peixe olhou
  para mim: o povo Yudjá e a perspectiva</cite>, de Tânia Stolze Lima; kaj
  <cite>A inconstância da alma selvagem e outros ensaios de antropologia
  e Metafísicas canibais: elementos para uma antropologia
  pós-estrutural</cite>, de Eduardo Viveiros Castro.

Kvankam tiu koncepto estas specifa de la indiana perspektivismo, tiu
ankaŭ povas proksimiĝi al nocio pli vasta, kiun ankaŭ havas aliaj
originaj popoloj de Latinameriko kaj de aliaj lokoj de la mondo, de ne
divido inter naturo kaj socio. Centra por tio, kion oni kutimas nomi
moderna epoko, tiu divido estis kontestita en la antropologio antaŭ
multaj jardekoj[^287], eĉ pli forte post la fenomenoj ligitaj al la
tutmonda varmiĝo, post la 1990-aj jaroj, en kiuj politikaj decidoj
faritaj de la registaroj, homoj kaj firmaoj («socio») kaŭzis
neinversigeblajn ŝanĝojn al la planeda klimato («naturo»). Por la
originaj popoloj, kiel la suioj [<i lang="suy">Kisêdjê</i>] kaj la
gvaranioj ĉi tie cititaj, tiu divido neniam ekzistis; la sama monda
koncepto, kiu disigas subjekton kaj objekton kaj kiu inkludas ilin en
tio, kio estas nomita kolektivo, ankaŭ ne vidas diferencon inter naturo
kaj socio.

[^287]: Inter aliaj aŭtoroj, Bruno Latour, en <cite>Jamais fomos
  modernos</cite>, diskutas la ideon, ke la moderna epoko komencas kun
  la divido inter naturo kaj socio en la okcidenta mondo, kio
  malfermis la vojon por la detruo de la naturo. «La naturo kaj la
  socio ne estas du malsamaj polusoj, sed antaŭe unu sama produkto
  de socioj-naturoj, de kolektivoj» (p. 138).

Ne estas hazarde, do, ke la indiĝena influo en latinamerikaj landoj
kondukis al debato pri la naturaj rajtoj[^288], perspektivo kiu
kontestas tiun dividon kaj klopodas enhavigi arbojn, riverojn, montojn
kaj ĝangaloj kiel jurajn estaĵojn ene de aktiva okcidenta jura sistemo.
La Konstitucio de Montecristi de la Ekvatora Respubliko ekzemple,
proklamita en 2008, asertas en la 71-a artikolo de la 7-a ĉapitro:

> La naturo aŭ Paĉamama, kie sin reproduktas kaj plenumas la vivo,
> rajtas, ke oni plene respektu ĝian ekziston kaj ĝiajn konservadon kaj
> regeneradon de ĝiaj vivaj cikloj, strukturo, funkcioj kaj evoluaj
> procezoj.

> Ĉiu homo, komunumo, vilaĝo aŭ nacio povos postuli al la
> publika aŭtoritato la realigon de la rajtoj de la naturo.
> [...]
>
> La Ŝtato kuraĝigos al la naturaj kaj juraj homoj kaj al la kolektivoj,
> por ke ili protektu la naturon kaj propagandu la respekton al ĉiuj
> elementoj, kiuj formas ekosistemon.[^289]

[^288]: Aŭ «nehomaj rajtoj». Vidu Gudynas, <cite>Direitos da
  natureza: ética biocêntrica e políticas ambientais</cite>.
[^289]: La Konstitucifara Asembleo estis prezidita de Alberto Acosta,
  kaj inkludis ankaŭ 99 artikolojn, kiuj specife traktas la aferon. Tiu
  laboro apartenas al movado de latinamerika diferencigo de la tradicia
  eŭropa konstitucia teorio, nomita en tiu fako «nova latinamerika
  konstituciismo», kiu inkludas ankaŭ la konstituciojn de Kolombio (1994)
  kaj de Venezuelo (1999). Por pli da detaloj vidu:
  Shiraishi Neto; Tapajós Araújo, «“Buen vivir”: notas
  de un concepto constitucional en disputa», <cite>Pensar</cite>, p.
  379-403, majo-aŭg. 2015. Disponebla en
  <https://periodicos.unifor.br/rpen/article/viewFile/2886/pdf>

La inkludo (aŭ klopodo) de nehomaj estaĵoj en leĝaroj kaj konstitucioj,
kiel en la okazo de Ekvatoro, reflektas ankaŭ la ideon de la bona vivo
(<i lang="es">buen vivir</i>), komuna nocio ekde longe inter originaj
popoloj de Latinameriko[^290] kaj kiu estis reakirita en la lastaj
jardekoj kiel kritiko al la disvolvismo kaj al la neceso kreski kaj
akumuli riĉaĵon koste de la naturo kaj de multaj de la loĝantaroj, kiuj
vivas rekte de ĝi. Por la bona vivo, ankaŭ ne devas esti divido inter
naturo kaj socio, kio siavice ne devas esti konfuzita kiel «reiro al
la estinteco», sed la serĉo, en la praaj radikoj de la originaj popoloj,
por pli harmonia kunvivo inter homoj kaj naturo, kiu donu al ni
solvojn, partikularaj por ĉiu komunumo, por la media kaj ankaŭ socia
krizo, en kiu ni hodiaŭ vivas.

[^290]: La termino devenas de originaj lingvoj: <i lang="qu">suma
  kawsay</i> en keĉua; <i lang="ay">suma qamaña</i> en ajmara, krom
  aperi ankaŭ kiel <i lang="gn">handereko e teko porã</i> en
  guarania. Estas similaj nocioj ankoraŭ inter la mapuĉaj popoloj en
  Ĉilio, la tuloj en Panamo, la ŝuaroj kaj la aĉŭaroj de la ekvatora
  Amazona arbaro, kiel ankaŭ la majaaj tradicioj de Gvatemalo kaj de
  Chiapas en Meksiko. Vidu <cite>O bem viver: uma oportunidade
  para imaginar outros mundos</cite>.

En popoloj, en kiuj ne estas divido inter naturo kaj kulturo, subjekto
kaj aĵo, kaj en kiu la kolektivo, en ĝia tuta komplekseco, kiun la
termino povas havi por tiuj popoloj, estas prioritata rilate al la
individuo, estas pli malfacile paroli pri nocioj kiel intelekta
propraĵo, aŭtorrajtoj kaj libera kulturo same kiel ni komentis ĝis ĉi tiu
ĉapitro[^291]. La ideoj de kopio, plagiato, proprigo kaj remikso estas
prezentita ene de *proprieta* koncepto de la mondo, aktuala en Okcidento
ekde la kapitalisma komenco, sed kiu por tiuj cititaj popoloj ne
normalas. Tamen ne eblas nei, ke la kapitalisma sistemo klopodas, ofte,
proprigi la sciojn kaj la kulturajn havaĵojn de tiuj originaj popoloj
por el tiuj elpreni valoron kaj tiam vendi ilin kiel komercaĵojn &mdash;
kiel okazas el ĝangalaj produktoj uzitaj kiel praaj rimedoj ĝis dezajnaj
ŝablonoj rekonitaj kiel <i lang="en">design</i>. Kiel do krei
mekanismojn, kiuj instigu la kolektivan kaj komunuman pensadon
inspiritan de tiuj popoloj, respektu ilian mondkoncepton kaj, samtempe,
protektu ilian kulturon de generi varojn vendotajn en bazaro, kie la
plej granda parto de la valoro akirita ne iros al ili?

[^291]: Aldone al Brightman; Fausto; Grotti (originalaj), <cite>Ownership and
  Nurture: Studies in Native Amazonian Property Relations</cite>, de
  2016, kolekto kiu havas la artikolon de Marcela Coelho de Souza ĉi tie
  citita, estas almenaŭ du gravaj verkoj por tiuj, kiuj volas profundiĝi en la
  temo: Strathern, <cite>Property, Substance, and
  Effect: Anthropological Essays on Persons and Things</cite>, kaj
  Hirsch; Strathern, <cite>Transactions and Creations: Property Debates
  and the Stimulus of Melanesia</cite>. Mi dankas al Eduardo Viveiros de
  Castro pro tiuj ĉi tri rekomendoj.

Malfacilas sola respondo al tiu demando. Se, kiel ni jam vidis,
programo havas produktadajn kondiĉojn malsamaj ol tiuj de muzikaĵo aŭ
desegno, ankaŭ la kontraktoj de tiuj havaĵoj necesas esti malsamaj, laŭ
kuntekstoj kaj agantoj ĉirkaŭe. La protekto de vasta publika havaĵo ne
ekskluzivas la rekompencon de tiu, kiu (re)kreas, kiel pruvas la licencoj
Libera Arto, Creative Commons kaj Copyfarleft cititaj en la antaŭa
ĉapitro. La egaligo de aĵoj kaj homoj kaj la estigo de longa rilato de
donaco, kiu ne finas sen milito, povas esti rigarditaj per negocadoj pli
kompleksaj, kiuj inventu aliajn terminojn, kaj ne tiujn kutime uzitajn en la
fako de la intelekta propraĵo. Kelkaj el la vortoj de tiu nova vortaro
neniel estas novaj; la 1980-jara rajtocedo kaj la miljara komuno
devena de la romia
<i lang="lt">res communes</i> ekzemple povas esti uzitaj kaj rekreitaj
per la indianaj vidpunktoj cititaj por estigi novajn kaj protekti
komunojn malnovajn per ĉiutagaj praktikoj de prizorgo kaj rezisto
&mdash; inkluzive en la jura flanko, tiu mediacia procedo, kiu
multfoje necesas, por la garantio de bona vivo por ĉiuj.

### IV.

La kultivo de liberaj kulturaj havaĵoj aparte de la individua kaj
proprieta motivo de la superreganta kulturo en Okcidento postulas
reziston kaj kreivon. Rezisti estas necesa procedo por la konservado de
granda kolektiva datumbazo de kreo, dum krei estas baza neceso por
reinventi konceptojn kaj praktikojn por konstrui alternativajn vojojn de
kulturaj produktado, cirkulado kaj rekompenco malpli limigaj kaj pli
memstaraj. Rilate al tio estas malsamaj proponoj. Por Creative Commons
reformi la aŭtorrajtajn leĝojn donante la elektan rajton de la liberecoj
uzi, cirkuligi, kaj produkti por tiuj identigitaj kiel aŭtoroj kaj vojon
por konstrui publikan havaĵon viglan kaj alireblan. Por la rajtocedo
proponita de Stallman, igi programojn kaj iujn kulturajn havaĵojn
liberaj estas solvo por batali kontraŭ monopoloj, kiuj forprenas la
liberecon de kreado kaj memstara elekto de la uzoj de iu specifa verko.

Aliaj homoj, kiel la aktivulo de la libera scio Evelin Heidel (Scann),
diras, ke la feminismo devus kontraŭi la patriarkan karakteron de la
aŭtorrajto. Li proponas do modifi la leĝaron ne pune por doni pli da
protekto al la inaj kreaĵoj, kiuj restis for de tiuj leĝaroj, sed jes
por krei paradigmon, kiu valorigu la kreadon kiel socian kaj komunuman
praktikon. «Klopodi modifi la leĝojn, kiuj hodiaŭ krimigas aŭ malpermesas
fundamentajn praktikojn por la gazetarlibereco, por la interŝanĝo, la
distribuo kaj la reproprigo de la kulturo», tiel ke silentu la muzoj,
kiuj inspiras al geniuloj, por ke finfine oni povu paroli pri la
inoj[^292].

[^292]: Heidel (Scann), <i lang="lt">op. cit.</i>. Disponebla en
  <https://www.genderit.org/es/feminist-talk/columna-que-se-callen-las-musas-por-que-el-feminismo-debe-oponerse-al-copyright>.

Por kelkaj, ankaŭ ĉi tie cititaj, kiel Anna Nimus, aboli la aŭtorrajton
ankaŭ povas esti la solvo. Joost Smiers kaj Marieke van Schijndel imagis
mondon sen aŭtorrajto, kiu havas kiel ĉefan ideon la fakton, ke la
protekto donita per la aŭtorrajtoj ne necesas por la procedo de
ekspansio de la arta kreo. Ili citas diversajn argumentojn, kiuj faras,
ke estu mallogike subteni la aŭtorrajton kiel modelon de reguligo de la
kultura produktado: la fakto, ke ĝi estas ekskluziva kaj monopolema
rajto de verko, privatigas esencan parton de nia komunikado kaj damaĝas
la demokration, ekzemple[^293]; la demando, ĉu ĝi estas fakte ekonomia
instigo al la kreanto, motivo asertita ekde la komenco de la aŭtorrajto,
sed, kiel kelkaj cititaj ekonomiaj studoj montras, el la enspezoj
akiritaj per venditaj kopioj, 10% iras al 90% da la artistoj kaj 90%
iras al 10%[^294]; la falsa ideo de originaleco kiel individua kaj
ekskluziva esprimo de iu kreanto; la fiasko de la batalo kontraŭ la
nomita pirateco de ciferecaj dosieroj de kulturaj verkoj en la Reto. Kiel
solvo, Smiers kaj Van Schijndel indikas ion proksiman al la komuno: «Ni
kredas, ke eblas krei kulturajn merkatojn tiel, ke la proprieto de la
rimedoj de produktado kaj distribuado estu en la manoj de multaj homoj.
En tiu kondiĉoj, ni pensas, neniu povos kontroli la enhavon aŭ la uzon
de la manieroj de kultura esprimo per la ekskluziva kaj monopola reteno
de proprietaj rajtoj»[^295].

[^293]: Smiers; Van Schijndel, <cite>Imagine um mundo sem direitos do
  autor nem monopólios</cite>, p. 10.
[^294]: <i lang="lt">Ibidem</i>, p. 13.
[^295]: <i lang="lt">Ibidem</i>, p. 6.

En la Encontro de Cultura Livre do Sul [Renkontiĝo de Libera Kulturo de
la Sudo], organizita de latinamerikaj kulturaj kolektivoj la tagojn
21-an, 22-an kaj 23-an de novembro de 2018[^296], aro da aktivuloj kaj
esploristoj kaj mi diskutis kaj serĉis respondojn al iuj el la demandoj
traktitaj en ĉi tiu libro. Dum la ses tabloj de debato de la renkontiĝo
ni parolis pri publikaj politikoj kaj juraj kadroj de aŭtorrajtoj;
ciferigado de kolektoj kaj aliro al la kultura heredaĵo en liberaj
deponejoj; pri laboratorioj, kunlaboraj produktantoj, kodumulejoj,
kodumulaj laboratorioj kaj aliaj formoj de organizaĵoj, kiuj defendas
kaj ĉiutage praktikas la liberan kulturon; pri kiel ni enmetu nin en
internacian reton, kiu ankaŭ defendu la komunajn havaĵojn; pri multaj
manieroj de kultura produktado &mdash; eldona, muzika, aŭdvida,
fotografa &mdash;, kiuj estas farataj en la agokampo de la licencoj kaj
de la libera kulturo; kaj pri la platformoj de enhavo kaj edukadaj
praktikoj, kiuj havas la liberan kiel paradigmon de ago kaj disvastigo.

[^296]: Ĉiuj debatoj videblas tie ĉi:
  <http://baixacultura.org/encontro-de-cultura-livre-do-sul-todos-os-videos-y-relatos>

Apud la pli ol ducent partoprenantoj, ni pensis pri la specifecoj de la
libera kulturo en la monda sudo rilate al la nordo. Kiel unu el la
rezultoj, ni skribis la Manifeston de Libera Kulturo de la Monda
Sudo[^297], kiu proponas kelkajn principojn, konceptajn kaj praktikajn,
kiujn ni konsideras gravaj por la disvastigo kaj prizorgo de libera
kulturo en tiu sudo, kiu ne estas nur geografia. La jena fragmento de la
manifesto estis konkludo por la renkontiĝo &mdash; kaj ankaŭ indas, ke
estu ĉi tie, adaptita, ne por fermi, sed por konservi aktiva la longan
kaj daŭran diskuton pri la libera kulturo tra la tempoj.


[^297]: Disponebla en <https://www.articaonline.com/2018/12/cultura-libre-del-sur-global-un-manifiesto/>.

> La diskuto pri la libereco de uzoj kaj produktado de liberaj
> teknologioj estis fundamentaj por la libera kulturo ekde la komenco,
> sed ni pensas, ke, en la sudo, ni havas la pli grandan urĝecon demandi
> nin kial kaj por kiu utilas niaj liberaj teknologioj. Ne nur sufiĉas
> diskuti, ĉu ni uzos ilojn produktitajn en liberaj programoj aŭ ĉu ni
> elektos liberajn licencojn en niaj kulturaj produktaĵoj: ni necesas
> pensi pri teknologioj, iloj kaj liberaj procedoj, kiuj estu uzotaj por
> doni spacon, aŭtonomecon kaj respekton al la malplej favorataj,
> finance kaj teknologie, el niaj kontinentoj, kaj por malpliigi la
> sociajn malegalecojn en niaj lokoj, malegalecoj tiuj eĉ pli videblaj
> en la kunteksto de monda faŝista supreniro, kiun ni vivas ĉi tiun 2020.

> El la sudo, ni devas pensi pri la libera kulturo kiel movado kaj
> kultura praktiko, kiu intense dialogu kun la popularaj kulturoj de
> niaj kontinentoj; kiu respektu kaj konversu kun la originaj popoloj de
> Ameriko, kiuj estas ĉi tie en nia kontinento vivantaj en libera
> kulturo multe pli antaŭe ol la alveno de la «latinaj»; kiu defendu la
> feminismon kaj la samajn rajtojn por ĉiuj, sen distingo de raso,
> koloro, seksa orientiĝo, identeco kaj seksa esprimo, handikapo, fizika
> aspekto, korpa grando, aĝo aŭ religio; kiu dialogu kun la rekombina
> kreemo de la antaŭurboj de niaj kontinentoj, korinklinaj al la
> komunuma kunhavado kaj estantaj ĉefa celo de la ekstermado praktikita
> per niaj regionaj policoj; kiu klopodu ŝirmi nian privatecon per
> kontraŭkontroladaj teknikoj kaj la defendo de la rajto al la anonimeco
> kaj al la ĉifrado; kaj kiu batalu por la disvastigado de la fendoj en
> la kapitalisma sistemo, serĉante, per kultura praktiko kaj
> kontraŭaŭtorrajta teknologio, alternativajn kaj solidarajn manierojn
> vivi harmonie kun Paĉamama sen elĉerpi la jam malabundajn resursojn de
> nia planedo.

> Pensi kaj fari la liberan kulturon el la sudo postulas pensi pri la
> urĝecon de la supervivaj necesoj de nia popolo; postulas, ke ni
> alproksimiĝu al la diskuto pri la komuno, ĉefa koncepto, kiu unuigas
> nin en la batalo kontraŭ la privatigo de la naturaj resursoj kiel la
> oceanoj kaj la aero, sed ankaŭ de la liberaj programoj kaj la
> malfermaj kaj senpagaj protokoloj, sur kiuj la Interreto organizas
> sin. Ke ni Alproksimiĝu al la komuno, pliigas nian disputan agokampon
> en la monda sudo kaj alproksimigas nin al la kutimo de komunumoj,
> centraj kaj periferiaj, kiuj ĉiutage batalas por la konservado de la
> komunaj havaĵoj.

> Gravas memori, ke la koncepto «komuno», al kiu ni klopodas
> alproksimiĝi, devas esti pensita kiel ion en procezo, kiel komune fari
> (<i lang="en">commoning</i> en la angla). Tio estas, ke ni
> ne observu nur la produkton mem &mdash; libron, videon, muzikaĵon,
> liberajn aparatarojn aŭ programarojn &mdash;, sed ankaŭ niajn proprajn
> praktikojn kaj dinamikojn, per kiuj ni kune kreas novajn manierojn vivi,
> kunvivi kaj ankaŭ produkti. Tia estas la komuna faro. Pro tio gravas,
> ke ni konservu la konektojn, kiuj trairis ĉiujn vortojn, ligilojn,
> referencojn kaj homojn cititajn, debatitajn, registritajn kaj
> partoprenintaj en la paĝoj de tiu libro, kiu ĉi tie daŭras.

<div class="right-align">
Interreto, Iberoameriko,

~~monda sudo, 23-a de novembro de 2018~~

remiksita en vintro de 2020
</div>

