Ĉi tiu libro naskas el peno date de 2008, jaro de la kreado de
BaixaCultura[^1], blogo, retejo, projekto, reta esplorejo kreita de mi
kaj de poeto Reuben da Cunha Rocha, tiam majstriĝontaj studentoj de publikaj
universitatoj de Brazilo, kiuj ankoraŭ kredis en la estonteco. Klopodante
skribi pri kulturaj produktoj, kiujn oni povus konsumi (aprezi, ŝati,
taksi) en la Interreto, post malmultaj monatoj ni renkontis kun la
libera kulturo, tiam ideo, kiu parolis pri la ĉefa diskuto en la
tiama monda Interreto: la kunhavigo de dosieroj en la reto kaj la
disputoj pri la (mal)leĝeco de tiu ago. Poste ni esploris pli: libera
programaro, rajtocedo, cifereca kulturo, kodumuloj kaj cifereca
aktivismo venis el unu flanko; remiksoj, plagiato, alproprigo, radikala
arto, kontraŭkulturo, el alia. Ni kunigis ambaŭ flankojn kun la
pirateco, la kunhavado kaj la teknopolitika diskuto.

[^1]: Disponebla en <https://baixacultura.org>.

Dek unu jarojn poste, post pli ol tri cent tekstoj publikitaj kaj multaj
debatoj, filmaj projekcioj, atelieroj, prelegoj, konversacioj kaj
intervjuoj faritaj, BaixaCultura restis. Sen Reuben ekde 2010, mi devis,
helpe de kelkaj homoj dum tiu periodo, teni la spacon malfermitan, nun
en alia Interreto kaj kun la emo kunhavi dosierojn kaj kun la libera
kulturo kun malpli da spaco ĉie. La promesoj de radikalaj ŝanĝoj de la
socio, kiun la Interreto instigis en multaj el ni en ĉi tiu epoko
transformiĝis en io proksima al inkubsonĝo. En 2020 ne estis maniero fuĝi
el vorto por priskribi ĝin: *malutopio*. Tamen la kunhavigo de dosieroj
en la reto daŭras firma en la kodumulaj kaj kontraŭkulturaj spacoj; la
libera kulturo daŭras kiel movado ne nur por kulturo, sed ankaŭ por
libera scio kaj komunaĵoj; la rajtocedo restas kiel unu el la plej
grandaj <i lang="en">hacks</i> en pli ol tri jarcentoj de aŭtorrajtoj en
Okcidento; la libera programaro restas kiel utopio de kunlabora kaj
solidara konstruo de teknologioj, kiu, nuntempe kaj preskaŭ ne, perdis
la okazon esti monda realaĵo; la remikso iĝis la ĉefa formo de arta
kreaĵo en mondo, kiu, pli konektita ol ĉiam, ne dubas plu, ke oni
kreas nur rekreante.

Pro tiuj ĉi kialoj, ankoraŭ estas grave paroli pri libera kulturo.
Rezulte de la celo debatita de multaj homoj en BaixaCultura en tiu
periodo kaj en aliaj diversaj lokoj, tio, kion ĉi tiu libro intencas,
estas analizi ideon, kiu komencis multe antaŭ ol la Interreto kaj kiu
restos dum estas homoj vivaj kreante. Tamen estus tasko ega kaj
herkula paroli pri la tutaj aspektoj, kiuj estas en ideo de historio
tiel granda. Pro tio la elekto trakti temon helpe de multaj fakoj
&mdash; historio, juro, komunikado, arto, sociologio, antropologio,
politika scienco, teknologiaj kaj sciencaj studoj, komputado &mdash;.

Disvolvita kaj disvastigita kiel ideo en la 1990-a jardeko, en la unuaj
jaroj de Interreto en la mondo, la libera kulturo nutras sin rekte de la
koncepto de libera programaro kaj de la rajtocedo, ambaŭ kreaĵoj
rilatitaj al teknologiaj produktoj &mdash; la programaro &mdash; de la
komenco de la 1980-aj jaroj. Ĝia bazo do rilatas al disvolviĝo de la
cifereca teknologio, kaj ankaŭ ĝia popularigo estas rezulto de kunteksto
de ekspansio de la informa aliro pro Interreto. Sed la ideo de libera
kulturo, almenaŭ kun la perspektivo, kiun mi ĉi tie traktas, havas
historion, kiu komencas multe antaŭ ol la libera programaro kaj
Interreto. Paroli pri liberaj manieroj de kreado, uzado, modifado,
konsumado, protekto kaj reprodukto de kulturo eblas nur komprenante la
manierojn produkti kaj disvastigi informon kaj kulturon en malsamaj
historiaj periodoj, kiel la Antikva Epoko, la Mezepoko kaj la Moderna
Epoko; konsideri la mekanismojn kreitajn per la okcidenta juro;
rimarkante kiel teknologiaj inventoj kiel la preso, la kino, la radio,
la fotografio, la komputiloj kaj ĉefe la Interreto havas grandan
gravecon en la ŝanĝoj de ĉiuj aspektoj de kultura kreado. Paroli pri
libera kulturo ankaŭ estas rigardi kiel estis konstruataj la ideoj de
aŭtoreco, intelekta havaĵo, originalo kaj kopio, sen forgesi la nociojn
de la Ekstrema Oriento kaj de la indiĝenaj povoj de Ameriko pri tiuj
aferoj; ankaŭ estas vidi kiel homoj, grupoj kaj movadoj renversis la
tiaman staton de la kulturaj kreado kaj disvastigo en siaj epokoj,
speciale dum la 20-a jarcento, kaj de la politikaj implicoj de iliaj
agoj.

Pensita dum multe da tempo kaj fine komencita ĝia skribado en 2019, ĉi
tiu libro esploras la liberan kulturon ankaŭ pri du konataj aspektoj:
tiu pri la pago al la kreantoj, kiu devus garantii la daŭrigon de la
produktado de iliaj verkoj, kaj tiu pri la aliro, (re)uzado, disvastigo
de la verkoj, kiu promesus al la homaro la rajton ĝui kaj rekrei ilin.
En tiuj du vidoj, multfoje rigardataj kiel malaj, estas nuancoj kaj
demandoj, inter ili tiu pri la propra nocio, ke iu povu esti proprietulo
de ideo, melodio, frazo, bildo, teknologio, kaj tiu pri la opinio, ke
verko ne povu esti kunhavita aŭ konsumita kontraŭ neniu pago al kiu kreis
ĝin. Por mi la celo de ĉi tiu libro estos atingita, se fine oni
komprenus, ke estas multaj pliaj nuancoj (kaj diferencoj) por vidi kaj
kompreni la liberan kulturon ol kiujn vi pensas.

<br><p class="right-align">Leonardo Foletto</p>

<p class="right-align">São Paulo, vintro de 2020</p>

