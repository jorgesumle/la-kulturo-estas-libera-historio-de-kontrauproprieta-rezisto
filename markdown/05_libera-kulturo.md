<section class="citas">
<i>La vendantoj de programoj volas disigi la uzantojn kaj domini ilin,
por ke ĉiu uzanto akceptu ne kunhavi kun aliaj. Mi rifuzas rompi la
solidarecon inter aliaj uzantoj tiel. Mi ne povas kun bona konscienco
subskribi interkonsenton pri konfidenceco aŭ programan interkonsenton pri
uzado. Dum jaroj mi laboris en la Laboratorio de Artefarita Intelekto
[de la MIT] por rezisti tiujn tendencojn kaj aliajn malĝentilaĵojn, sed
fine ili iris tro malfermen: mi ne povis resti en institucio, kie tiaj
aferoj estas faritaj kontraŭ mia volo. Por ke mi plu povu uzi
komputilojn sen malhonoro, mi decidis grupigi sufiĉe da liberaj
programoj, por ke mi povu vivi sen iu ajn mallibera programo.</i>

<div class="right-align">
Richard Stallman, <cite>La manifesto de GNU</cite>, 1985
</div>

<br>

<i>Unuflanke ĉi tiuj pintoteknologiaj metiistoj ne nur kutimas esti bone
pagitaj, sed ankaŭ havas konsiderindan aŭtonomecon pri sia ritmo de
laboro kaj laborejo. Tial la kultura divido inter la hipio kaj la viriĉo
de organizo iĝis nun tre malklara. Tamen aliflanke tiuj laborantoj estas
ligitaj per termoj de iliaj kontraktoj kaj havas neniun garantion de
daŭra laboro. Malhavante la liberan tempon de la hipioj, la laboro iĝis
la ĉefa vojo al memkontentiĝo por granda parto de virtuala klaso.</i>

<div class="right-align">
Richard Barbrook; Andy Cameron, <cite>The Californian Ideology</cite>,
1995
</div>

<br>

<i>Registaroj de la industria mondo, pezecaj gigantoj de karno kaj
ŝtalo, mi venas de la retlando, la nova hejmo de Menso. En la nomo de la
estonteco, mi petas al vi de la pasinteco, lasu nin trankvilaj. Vi ne
estas bonvenaj ĉe ni. Vi ne havas suverenecon, kie ni kunvenas. Ni
formas nian propran socialan kontrakton. Tiu rego leviĝos laŭ kondiĉoj
de nia mondo, ne de via. Nia mondo estas malsama. Viaj juraj konceptoj
de posedaĵo, esprimo, identeco, movado, kaj kunteksto ne aplikeblas al ni.
Ĉiuj ĉi bazas sur materio, kaj ĉi tie ne ekzistas materio.</i>

<div class="right-align">
John Perry Barlow, <cite>Deklaracio de sendependeco de la retlando</cite>, 1996
</div>

<br>

<i>Facilas, kiam vi preterpasas la perantojn.</i>

<div class="right-align">
Creative Commons, <cite>Get Creative!</cite>, 2000
</div>
<br>

<i>Kiam vi elŝutas MP3-dosierojn, vi ankaŭ elŝutas komunismon.</i>

<div class="right-align">
Record Industry Association of America, <cite>Kontraŭpirateca kampanjo</cite>,
2000-aj jaroj
</div>


<br>

<i>La malfermitkodo kaj la rajtocedo etendas nune multe pli foren de la
programado de programoj: la «malfermitaj permesiloj» estas ĉie kaj
tendence povas iĝi la paradigmo de nova modo de produktado, kiu fine
liberigu la socian kunlaboron (jam ekzistantan kaj videble praktikitan) de
la parazita kontrolo, la eksproprietigo kaj la «rento» por la grandaj
industriaj kaj firmaaj riĉuloj.</i>

<div class="right-align">
Wu Ming, <cite>Copyright e maremoto</cite>, 2002
</div>

<br>

<i>La ideo estas, ke la aŭtorrajto signifas «<span lang="en">all rights
reserved</span>» [ĉiuj rajtoj rezervitaj] kaj Creative Commons signifas «<span lang="en">some
rights reserved</span>» [kelkaj rajtoj rezervitaj]. Kaj vi diras, kiuj estas tiuj. Estas kelkaj
procedoj, kelkaj specoj de malfermitaj permesiloj. Tio temas pri krei
manieron kunvivi en la informa medio, kiu estu tolerema, kaj kiu evitu
tion, kio okazas, kio estas la kontrolo de la informo per la grandaj
firmaoj. Nun ĉi ĉio ankoraŭ estas, iel, paliativo. Creative Commons
povas esti vidita, kiel ĝi estas efektive de la plej, ni diru,
radikalaj, kiel kapitalisma strategio. La vera anarkiisto ne volas scii
pri Creative Commons nek pri rajtocedo, ri estas tute radikala. Unue mi
estas kun ili, mi pensas, ke la privata proprieto, kaj intelekta kaj
neintelekta, estas monstraĵo sed mi ankaŭ scias, ke oni ne antaŭeniras
frapante muron, kovrante la sunon per kribrilo. Mi pensas, ke vi devas
cedi, devas fari ian intertraktadon.</i>

<div class="right-align">
Eduardo Viveiros de Castro, Economia da cultura digital, en Savazoni;
Cohn, <cite>Cultura digital.br</cite>, 2009
</div>
</section>

### I.

«La presilo estas denove blokita!»

<br>

Richard M. Stallman, programisto de programoj en la laboratorio de
Artefarita Intelekto de la Massachusetts Institute of Technology (MIT),
en la Orienta Marbordo de Usono diras, ke li eltrovis la problemon unu
horon post sendi el sia komputilo kvindek-paĝan dosieron por printado
kaj rimarki, ke la maŝino metis inkon sur kvar paĝojn de alia laboro, kiu
ne estis lia. Ne estas vere novaĵo, ĉar li vivis alian similan
situacion, en kiu la ĵus diplomiĝita fizikisto de Hardvard uzis ĝian
kapablon de programado de programoj por eviti ĝin, kreante kodan
ŝanĝeton en la kodo de la presila programo, kiu permesis sciigi,
distance, kiam ĝi estis blokita, per la frazo «<i lang="en">The printer
is jammed, please fix it</i>[^135]. Sed ĉi-foje la presilo estis nova,
unu el la eldonoj de Xerox (modelo 9700), kiu presis po tricent pontojn
por coloj en solaj paperaj paĝoj per rapideco de ĝis po du paĝoj en
sekundo, sur unu aŭ du flankoj, laŭ pejzaĝa aŭ portreta aranĝo. Estis donita
al laboratorio kiel testa maniero, kutimo de la firmao kaj de aliaj bazitaj
sur teknologiaj aparatoj al lokoj, kie tiu komunumo de programistoj
kuniĝis &mdash; se ili povus kodumi, ili multfoje estis alvokitaj por labori
en tiuj firmaoj. En la 1960-aj kaj 1970-aj jaroj, la MIT estis unu el la
unuaj lokoj, kie tiu komunumo de programistoj de
programoj kaj aparataroj defendinta la ideon, ke «ĉiu informo devas
esti libera», kiu kondukas nin al Thomas Jefferson, al markizo de
Condorcet kaj al la naskiĝo de la liberalismo, renkontiĝis por inventi kaj
kunhavigi kodojn por la pli kaj pli potencaj kaj malgrandaj
komputiloj, kiuj loĝis en la esplorejoj. Inter pico antaŭfrumatene kaj
komputpasiula kaj diletanta intereso, kiu eniris en ĉiun ajn temon ŝajne
banalan, kiel la formon de karoto, aŭ iom kompleksan, kiel la manierojn
fari telefonajn ŝercojn, ili penis krei kreemajn solvojn por malfacilaj
problemoj. Resume: ili <i lang="en">hackumis</i> [lerte trovis kaj
aplikis solvojn].

[^135]: Libere tradukita «La presilo estas blokita, bonvolu ripari ĝin».
Ĉi tiu historio estas ĉi tie rakontita el la 1-a ĉapitro, «<span lang="en">For
Want a Printer</span>», de <cite>Free as in Freedom: Richard Stallman and the
  Free Software Revolution</cite>, biografio de Richard Stallman
  skribita de Sam Williams.

Kiam Stallman vidis la problemon kun la nova presilo de Xerox, li pensis
apliki la malnovan korekton kaj kodumi ĝin denove. Tamen serĉante la
maŝinan programaron ili eltrovis, ke la firmao ne sendis, kiel
kutimis ĝis tiam, ĝentilecan programon, por ke la programistoj povu legi
la kodon, sed nur preskaŭ senfinan dosieron de nuloj kaj unuj nomitan
*duuman*. Li povus ŝanĝi la nulojn kaj unujn en malaltnivelajn ordonojn
por maŝinoj per programoj nomitaj retrotradukiloj kaj tiam provi plenumi ĝin
en la *organoj* de la presilo, sed ĝi estus malrapida kaj malfacila
tasko, kiu povus kaŭzi jarojn de kongestitaj kaj ĝenaj presadoj.

Tio, kion Stallman faris tiam, estis iri al la programo. Li eltrovis, ke
alia programisto, en la universitato Carnegie Mellon, ankaŭ en la
Orienta Marbordo de Usono, havis la programon. Vizitinte lin kun la
insigno de «esploristo de la MIT», li parolis amikece kun aliaj
inĝenieroj partoprenintaj en la produktado de Xerox kaj petis la aliron
al la kodo de la presila programaro. Li estis tiam informita, ke la kodo
estis novaĵo konsiderita kiel avangarda, tial ĝi devis resti sekrete kaj ne
esti kunhavita. Stallman eliris el la universitato parolinte nenion,
kun rabio kaj sen kopio, kun sensacio, ke tio, kio antaŭe estis libera
kaj kunhavebla fariĝis, fine de la 1970-a jardeko, konfidenca. Ne pro iu
jura registara cenzuro, sed pro merkataj interesoj; ĝis tiam ne estis
interkonsento pri konfidenceco (angle <i lang="en">nondisclosure
agreement</i>, NDA) en la programa industrio, kio faris, ke ĉiu
programo estu libera, kun sia fontkodo disponebla por ĉiu, kiu volu ĝin
legi kaj modifi.

Programo de komputilo &mdash; aŭ de presilo &mdash; funkcias kiel aro de
komandoj, por ke la maŝino plenumu funkciojn. Ĝi estis skribita per lingvo,
kiun tiuj inventaĵoj sciis legi kaj trakti; ju pli en la organoj, des
pli *malaltnivela* estas la lingvo; ju pli proksima al la interfaco kun la
homo, des pli *altnivela*. Finita aro de proceduroj plenumota per maŝino
nomiĝas *algoritmo*, araba vorto (لخوارزمية) latinigita en la kunteksto
de la matematiko en la 8-a jarcento, sed kies unua uzado destinita al
komputilo estis farita de la grafino Ada Lovelace[^136] por la analitika
maŝino de Charles Babbage &mdash; ega aparato esence por solvi
logaritmojn kaj trigonometriajn funkciojn &mdash; en la fino de la 19-a
jarcento. Kiel ĉio bazita sur komandoj, tiuj estantaj en algoritmo
funkcias per la trafiko de informoj, tiuokaze inter maŝino kaj homoj
peritaj de la lingvo; ne havi aliron al la kodo, kiu regas la trafikon
de informoj inter ĉi tiuj flankoj signifas ne scii, kio estas
interŝanĝita, tial ankaŭ ne scii kiel proceduro plenumas, ne ebli modifi
ĝin, nek por ripari eraron nek por proponi plibonigon, kaj fine ne povi
doni ĝin al aliaj &mdash; kiuj, ne havante la ŝlosilon por malfermi la
*nigran skatolon* de la algoritmo, malmulton povas fari per ĝi.

[^136]: La angla Augusta Ada King, grafino de Lovelace (1815-1852),
  idino de la poeto konita kiel Lord Byron (kun kiu ŝi kunvivis malmulte
  ĝis siaj ok jaroj, kiam Byron mortis), estis la unua konstati, ke
  analitika maŝino de Babbage havis aplikojn krom kalkulaj kaj tiam
  publikigis la unuan algoritmon, en 1843, destinitan al plenumado per
  tiu maŝino. Rezulte ŝi estas konsiderita kiel unu el la unuaj
  programistinoj. Ŝi estas, ankaŭ, unu el la raraj virinoj en la
  historio de la teknologioj, kies historio estis rakontita, inter
  multaj aliaj, kiuj, havinte gravajn rolojn, estis forigitaj de la
  rakontoj, kiuj hodiaŭ estas la plej elektitaj en la dokumentado de la
  teknologia historio. Pri Ada Lovelace, vidu: <https://en.wikipedia.org/wiki/Ada_Lovelace>.

Same kiel kultura havaĵo, programo havas en sia genezo la kunhavigon de
informo kaj la rekombinadon de ideoj. Kiam Stallman, fine de la 1970-a
jardeko, rimarkis, ke la programaj informoj fariĝis malfermitaj pro
konfidencecaj kialoj, kaj nur alireblaj kontraŭ pago, okazis movado en
kelkaj aspektoj simila al tiu, kiu okazis dum la fortiĝo de la kopirajto
kaj la aŭtorrajto en la Eŭropo de la 18-a jarcento: la privata malfermiĝo
de tio, kio antaŭe estis komuna kaj libere alirebla. Kiam ili fariĝas pli
altvalora en kapitalisma merkato, la programo komencis havi
proprietulon; ĝia kodo, nur malfermita, estas la kerno de la valoro de
produkto, la sekreto plej bone konservita, kiu determinas ĝian
ekskluzivecon.

Malsame tamen ol kultura havaĵo, programo estas komandaro por maŝino.
Kiel povas ni komuniki kun maŝino, se ni ne konas ĝian kodon kaj ĝian
lingvon? Ni ne povas. Aŭ pli bone dirite, kiu havos la ekskluzivecon komuniki
estos kiu havas la proprieton de la fontkodo. Problemo de komunikado
estas solvita per garantio de privilegio de la eldonanto: nur kiu
produktis, uzinte la komunajn informojn, havas tiun rajton. Post la okazo
de Xerox 9700, Stallman estis incitita demandi: sed ĉu la rajto de
aliro, uzo kaj reuzo de la necesaj informoj, por ke iu teknika aparato
funkciu, ne estas ankaŭ gravaj? Por li, rifuzi oferi la fontkodon de
programoj ne estas nur interrompo de regulo estigita post la fino de la
Dua Mondmilito, kiam, post Alan Turning kaj aliaj, la programoj komencis
esti gravaj, sed malobservo de la Ora Regulo, la morala regulo, kiu
diris: «agu kun la aliaj kiel vi ŝatus, ke oni agu kun vi»[^137].

[^137]: Williams, <cite>Free as in Freedom</cite>, p. 11.

Pro lia persona malkontento kaj deziro klopodi konservi la informojn
malfermitaj kaj liberaj, Stallman kreis, fine de la 1970-a jardeko, la
ideon de libera programo kiel komputilprogramo, kiu donus al ĝia uzanto
liberecon, kiel dum la unuaj jaroj de la komputilprogramoj, *(0) uzi la
programon, por iu ajn celo; (1) studi kiel la programo funkcias kaj
ŝanĝi ĝin por viaj bezonoj; (2) disdoni kopiojn de la programo; (3)
modifi (plibonigi) la programon kaj disdoni tiujn modifojn*[^138].
Silente sed necesege la libera programaro disvastiĝis per la Interreto
kaj per la populariĝo de la komputiloj en la 1980-aj kaj 1990-aj jaroj kaj
estis kondukita al aliaj kampoj, kiel la kulturo, en kiu ĝi trovis
fekundan grundon por vastiĝi. Post la libera programaro estiĝis la
aŭtorrajto, en la 1980-aj jaroj, kiu poste faris, ke la libera kulturo
disvastiĝu en la unuaj jaroj de la komerca Interreto kiel ideo, movado
de homoj kaj praktiko ligita al la kunhavado de ĉia dosiero en la
Interreto (aŭ elŝutado), libera rekombinado de ideoj por la kreado de
kulturaj havaĵoj kaj defio al la ŝanĝoj en la aŭtorrajta leĝo post la
ŝanĝoj kaŭzitaj per la Interreto.

[^138]: En ĝia unua difino, angle, en la retejo de la Free Software
  Foundation, disponebla en
  <https://www.gnu.org/philosophy/free-sw.html>.

La elsendflua kreskiĝo kaj la populariĝo de la sociaj retejoj en la
Interreto, tiam fine de la 2000-aj jaroj, igas la signifajn diferencojn,
kiuj karakterizas programon kaj kulturan havaĵon, kiel libron aŭ muzikon,
pli videblaj ol en la unuaj jaroj de la Interreto. Antaŭ la teknologio kaj
la libera kunhavado estas energio &mdash; viva energio[^139] de
senmateria laboro, kiun en multaj okazoj la libera kulturo de la Interreto
ne atentis. «La misuzo antaŭas la uzon», diras la franca Michel
Serres[^140], bona frazo por klarigi la senton de *postebrio*, kiu
la post-2016-a Interreto donis al ni ĉiuj, kiuj ebriiĝis per la «liberado de
la sendejo de informo» de la unuaj jaroj de la reto kaj kiuj ne sukcesis
atenti ekonomiajn kaj politikajn alternativojn de konstruado de reto,
kiu, en la fino de la 2010-a jardeko, helpis disvastigi faŝisman venĝon
konsistantan el politikaj iniciatoj de koloniigo de la reto kaj
disvastigo de malamo ekzistantan en multaj lokoj de la planedo.

[^139]: Pasquinelli, A ideologia da cultura livre e a gramática da
  sabotagem, en Belisário; Tarim (originaj). <cite>Copyflight</cite>, p.
  52.
[^140]: <i lang="lt">Ibidem</i>.

### II.

La 27-an de septembro de 1983 Stallman sendis retmesaĝon per la tiama
Arpanet, reto antaŭinta la Interreton, kiu ligis ĉefe esplorejojn en
usonaj universitatoj:

> Post la proksima Dankotago mi komencos skribi kompletan programaran
> sistemon kongruan kun Unikso nomitan GNU (kiu signifas «Gnu ne estas
> Unikso»), kaj mi distribuos ĝin libere, por ke ĉiuj povu uzi ĝin. [...]
> Mi konsideras, ke la ora regulo postulas, ke se mi ŝatas programon, mi
> devas kunhavi ĝin kun aliaj homoj, kiujn mi ŝatas. Mi ne povas kun
> bona konscienco subskribi interkonsenton pri konfidenceco aŭ programan
> interkonsenton pri uzado. Por ke mi povu plu uzi komputilojn sen
> malobservi miajn principojn, mi decidis kunmeti sufiĉe da liberaj
> programoj, por ke mi povu interagi sen iu programo, kiu ne estas
> libera.[^141]


[^141]: Fragmento de <cite>Initial Announcement</cite>. La historio la
  Projekto de GNU malsamas de ĉi tiu komenca plano &mdash; la komenco
  ekzemple estis prokrastita ĝis januaro de 1984. Multaj el la filozofaj
  konceptoj de libera programaro ne estis detalitaj ĝis iom da jaroj
  poste, kiel deklaras la teksto, kiun kuntekstigas la manifesto,
  disponebla, kiel ankaŭ la manifesto, en
  <https://www.gnu.org/gnu/initial-announcement.html>.

La retmesaĝo finis kun la subskribo, kiun Stallman kutimis uzi en
Arpanet (*RMS*), kaj la leterkesto por komunikado, en Cambridge. Ĝi estis
la unua paŝo de la Projekto de GNU, iniciato kiu komencis la ideon de
programaro, kiu kontraŭe de la pli kaj pli malfermitaj programoj
eldonitaj en la komenco de la 1980-aj jaroj estus libera por malsamaj
manieroj de uzo kaj modifo, kun sia kodo disponebla, por ke ĉiu aliru.
Ĝi estis projekto, en kiu la programisto laboris antaŭ iuj jaroj,
inspirita de la kodumula etiko, kiu influis la MIT, bazita sur la totale
libera aliro kaj kunhavado de informo kaj sur la kunlaboro anstataŭ
konkuro; kaj kiu havis kiel principojn: 1) la aliro al komputiloj &mdash;
kaj ĉia alia ilo, kiu kapablu lernigi ion pri kiel la mondo funkcias
&mdash; devas esti mallimigita kaj totala; 2) ĉiu informo devas esti
libera; 3) ne kredu en la aŭtoritato kaj antaŭenigu la malcentralizon; 4)
kodumuloj devas esti taksita laŭ sia kodumado, kaj ne laŭ kriterioj
bazitaj sur antaŭjuĝoj kiel akademiaj titoloj, raso, koloro, religio,
pozicio kaj aĝo; 5) vi povas krei arton kaj belecon en la komputilo; 6)
la komputiloj povas ŝanĝi vian vivon pozitive[^142].

[^142]: Fonto: <https://pt.wikipedia.org/wiki/%C3%89tica_hacker>.
  Estas multaj difinoj de kodumulo; unu el la plej ĝustaj venas el
  Gabriella Coleman en <cite>Coding Freedom: The Ethics and
  Aesthetics of Hacking</cite>, libro produktita el etnografio en
  kodumulaj komunumoj, kiu tiel montras ilin: «Obseditaj per la
  komputilo movitaj per kurioza pasio por alĝustigi kaj lerni teknikajn
  sistemojn, kaj kutime dediĉitaj al etika versio de libereco de informo»
  (libera traduko bazita sur la originalo «<i lang="en">computer
  aficionados driven by an inquisitive passion for tinkering and
  learning technical systems, and frequently committed to an ethical
  version of information freedom</i>»). Por alproksimiĝo al la kudumula
  etiko kompare al la protestanta etiko vidu Himanen, <cite>La ética del
  hacker y el espíritu de la era de la información</cite>.

Estante kiel agmaniero en la komunumo de kodumuloj de la 60-aj kaj 70-aj
jaroj, en kiuj Stallman edukiĝis, tiu kodumula etiko komencis, laŭ li,
ŝanĝi, kiam multaj membroj foriris al privataj teknologiaj firmaoj, kiuj
amase komencis naskiĝi en la fino de la 1970-a jardeko kaj la komenco de
1980 por komercigi personajn komputilojn, programojn kaj aparatarojn
diversajn.

La amasfuĝo de la kodumula komunumo en la laboratorio, kie Stallman
laboris[^143], bone reprezentis tiun movadon: en la komenco de 1980
granda parto de la membroj de la AI Lab (Laboratorio de Artefarita
Intelekto) estis dungitaj de la firmao Symbolics, kreita de Russ
Noftsker, membro de la laboratorio, kiu gvidis la grupon, kiu komencis
forlasi kelkajn kodumulajn principojn, kiel lasi malfermita kaj kunhavi
la fontkodon, por komerci siajn produktojn. La disputo de Noftsker
estis speciale kontraŭ la grupo gvidita de Richard Greenblatt, ankaŭ de
la MIT, kiu kreis en 1979 la projekton LISP Machine, firmon kiu
produktis komputilojn bazitajn sur la lingvo de artefarita intelekto LISP
kaj klopodis resti fidela al la kodumula spirito, sen forlasi la
malfermitan kodon. Greenblatt kredis, ke la enspezoj, kiuj venis de la
konstruo kaj vendo de kelkaj maŝinoj, povus esti reinvestitaj por la
financado de la firmao, dum Noftsker vetis por vojo, tradicia en la
kapitalismo kaj kiu iĝis regulo en la mondo de la teknologiaj ekfirmaoj
post tiam, serĉi investantojn kaj subtenon en investfondusoj. La opcio
de Noftsker varbis pli da homoj, rezultante en la kreado de Symbolics
kaj en la eliro de multaj membroj de la AI Lab, historio kiun Steven Levy
rakontas en sia libro <cite>Hackers: Heroes of the Computer
Revolution</cite>, en kiu li nomis Stallman, kiu en la disputo restis en
la flanko de Greenblatt, «La lasta de la veraj kodumuloj», ankaŭ titolo
de la ĉapitro, kiu detalas la okazon.

[^143]: Stallman donas pli da detajloj pri tiuj ŝanĝoj en «The Project
  GNU», unu el la tekstoj, kiuj estas en la kolekto eldonita kiel
  <cite>Free Software, Free Society</cite> en 2002, de la GNU Press. La
  ĉi tie citita estas la hispana versio eldonita de la hispana eldonejo
  Traficantes de Sueños en 2004.

La propono de Stallman por la Projekto GNU estis doni al uzantoj la
liberecon, kiun Unikso, firmaa operaciumo kaj la plej uzita en tiu epoko,
kreita en 1969, ne donis. Por tio li profitis la eblojn, kiujn Unikso
ankoraŭ permesis en tiu epoko, kiel la aliron al sia fontkodo, kaj
komencis krei sian propran operaciumon, kiu devus esti kongrua kun la
plej uzita (Unikso) en tiu epoko, sed, malsame ol ĝi, ĝi devus esti
«100% libera programaro». Ne 95, ne 99,5%, sed 100%
&mdash;«por ke la uzantoj estu liberaj redistribui la tutan sistemon
kaj liberaj ŝanĝi kaj kontribui per ĉia parto»[^144]. En tiu momento
Stallman jam kreis unu el siaj laboroj plej konataj, redaktilan
programon nomitan Emacs (mallongigo de «redaktado de makrooj»), kiu
prezentis ekzemplon de tio, kion li faris poste per la Projekto GNU kaj
kio «estis libere kunhavita kun ĉiuj, kiuj akceptus solan postulitan
kondiĉon: ĉiuj modifoj kaj plibonigoj faritaj de la uzantoj al la
programo devus esti ankaŭ kunhavitaj»[^145].

[^144]: Stallman, en teksto festa de la dek kvin jaroj de GNU.
  Disponebla en
  <https://www.gnu.org/philosophy/15-years-of-free-software.html>.
[^145]: En Torres, <cite>A tecnoutopia do software livre: uma história
  do projeto técnico e político do GNU</cite>, p. 128.

En la komenco de 1984, monatojn post anonci la kreon de la Projekto
GNU, tiam sen la fekunda kaj kunlabora medio, en kiu li vivis dum multaj
jaroj, Stallman eliris el la MIT kaj komencis tute dediĉi sin al
disvolvigo de sia operaciumo. Por li, eliri el la instituto estis
bezona, se li volus, ke neniu interrompu la distribuon de GNU kiel
liberan programaron: «La MIT povus esti propriginta al si mian laboron
kaj trudi siajn proprajn kondiĉojn de distribuo, aŭ eĉ aliformigi la
laboron en proprietan programaron»[^146]. La saman jaron li komencis la
disvolvigon de la nova operaciumo, kiun sekvis kelkaj aliaj en la sekvaj
jaroj, kiel programtradukiloj de kodo de diversaj programlingvoj (GCC),
erarserĉiloj (*GNU Debugger*), inter aliaj.

[^146]: Stallman, <cite>Software libre para una sociedad libre</cite>,
  pp. 250-251.

En oktobro de 1985 Stallman fondis la Free Software Foundation (FSF),
ne-profitcelan organizaĵon, kiu ĝis hodiaŭ estas responsa pri la projekto
GNU. En tiu sama jaro li publikigis la GNU-Manifeston, en kiu li
prezentas la ideojn rilatajn al sia projekto kaj vokas al programistoj,
por ke ili helpu lin en la disvolvigo de la sistemo. Kun frazoj de la
unua anonco de antaŭ du jaroj kaj kun daŭraj modifoj ĝis 1987 estas ĝis
hodiaŭ centra dokumento en la filozofio de la libera programaro. Iuj
fragmentoj:

> La vendantoj de programoj volas disigi la uzantojn kaj domini ilin,
> por ke ĉiu uzanto akceptu ne kunhavi kun aliaj. Mi rifuzas rompi la
> solidarecon inter aliaj uzantoj tiel. Mi ne povas kun bona konscienco
> subskribi interkonsenton pri konfidenceco aŭ programan interkonsenton pri
> uzado. Dum jaroj mi laboris en la Laboratorio de Artefarita Intelekto
> [de la MIT] por rezisti tiujn tendencojn kaj aliajn malĝentilaĵojn, sed
> fine ili iris tro malfermen: mi ne povis resti en institucio, kie tiaj
> aferoj estas faritaj kontraŭ mia volo. Por ke mi plu povu uzi
> komputilojn sen malhonoro, mi decidis grupigi sufiĉe da liberaj programoj,
> por ke mi povu vivi sen iu ajn mallibera programo. [...]
>
> Multaj programistoj estas malkontentaj pri la komercigo de la
> sistema programaro. Ĝi povas ebligi al ili gajni pli da mono, sed ĝi
> necesigas, ke ili ĝenerale sentu sin en konflikto kun aliaj
> programistoj anstataŭ en kamaradeco. La fundamenta ago de amikeco
> inter programistoj estas la kunhavado de programoj; merkatikaj
> kontraktoj nune kutime uzitaj esence malpermesas al programistoj
> trakti la aliajn kiel amikojn. La aĉetanto de programoj devas elekti
> inter amikeco kaj obei la leĝon. Nature multaj decidas, ke la amikeco
> estas pli grava. Sed tiuj, kiuj kredas en la leĝo ofte ne sentas sin
> komfortaj kun ambaŭ elektoj. Ili iĝas cinikaj kaj pensas, ke
> la programado estas nur maniero gajni monon. [...]
>
> Kiam GNU estu skribita, ĉiu povos ricevi bonan sisteman programaron
> senpage, same kiel aero.
>
> Tio signifas multe pli ol nur ŝpari al ĉiuj la prezon de Uniksa
> licenco. Ĝi signifas, ke multo da malnecesa duobligo de sistema
> programada peno estos evitita. Ĉi tiu peno povas iri anstataŭe al
> antaŭenigi la staton de la arto.
>
> La fontkodo de la tuta sistemo estos disponebla por ĉiuj. Kiel
> rezulto, uzanto, kiu bezonu ŝanĝojn en la sistemo ĉiam estos libera
> fari tion mem, aŭ dungi iun ajn disponeblan programiston aŭ firmaon
> por fari ilin. La uzantoj ne plu estos sub la jugo de unu programisto
> aŭ firmao, kiu posedas la fontojn kaj havas la solan pozicion fari
> ŝanĝojn.[^147]

[^147]: Stallman, <cite>The GNU Manifiesto</cite>. Disponebla en
<https://www.gnu.org/gnu/manifesto.html>.

La procezo de disvolvigo de GNU post 1985 donis al Stallman kelkajn
lernaĵojn. La unua el tiuj estas la fakto, ke ne sufiĉis krei projekton,
kiu havu kiel celon la liberecon kaj la liberan uzon kaj hunhavadon, se
ne estus iu maniero protekti kaj garantii tiun liberecon ankaŭ leĝe. Do
en 1989 estis publikigita la General Public License (GPL), permesilo
ĝenerala, kiu kovris ĉiujn kodojn de la projekto de GNU kaj kiu klopodis
estigi liberecojn de uzo, kiujn la aŭtorrajto en modo en Usono ne
permesis. Stallman bezonis «garantii al la uzantoj de GNU la bazajn
rajtojn de aliro, kopio, modifo kaj redistribuo de la programoj kaj por
tio necesis limigi la limigojn de tiuj rajtoj. Li tiam estigis, helpe de
la aŭtorrajto, sistemon, kiu permesis al ĉiuj la rajton aliri al liaj
programoj kaj al neniu la rajton limigi tiun aliron»[^148]. Li registris
la aŭtorrajton de la programo por, tiam, liberigi ĝin, kreante specon de
kontaĝa procezo, en kiu ĉiuj uzoj eblas, se estas donitaj al aliaj.
Li tial Garantiis, ke neniu proprigis al si la programaron.

[^148]: Torres, <i lang="lt">op. cit.</i>, p. 133.

En la origina teksto de la GPL troviĝas la liberecojn, kiuj karakterizas,
post tiam, kio estas libera programo, kaj ankaŭ la motivon por uzi la
sistemon de aŭtorrajto por protekti tiun de ĉi tiu mem:

> Por protekti viajn rajtojn ni bezonas eviti, ke aliaj rifuzu al vi ĉi
> tiujn aŭ petu al vi forlasi la rajtojn. Do vi havas kelkajn
> respondecojn, se vi distribuas kopiojn de la programo aŭ se vi modifas
> ĝin: respondecojn respekti la liberecon de aliaj.
>
> Ekzemple, se vi distribuas kopiojn de tia programo, aŭ senkoste aŭ
> kontraŭ pago, vi devas doni al la ricevantoj la samajn liberecojn,
> kiujn vi ricevis. Vi devas certigi, ke ili, ankaŭ, ricevas aŭ povas
> akiri la fontkodon. Kaj vi devas montri al ili ĉi tiujn kondiĉojn, por
> ke ili sciu siajn rajtojn.[^149]

[^149]: GNU General Public License, disponebla en
  <https://www.gnu.org/licenses/gpl-3.0.html>.

La <i lang="en">hack</i> [ruzo] en la jura sistemo por garantii la liberecojn
de la libera programaro, kiu naskis la GPL-n akiris la nomon de
<i lang="en">copyleft</i> [rajtocedo]. Ĝi estis vortludo per la vorto
<i lang="en">copyright</i> [aŭtorrajto] proponita, laŭ Stallman
rakontas[^150], de lia amiko Don Hopkins en letero sendita al li en 1984
(aŭ 1985), en kiu Hopkins skribis la jenan frazon en la mesaĝa fino:
«<i lang="en">Copyleft – all rights reversed</i>» (aŭtorrajto – ĉiuj
rajtoj inversigitaj), en klara rilato al la sciigoj de aŭtorrajtoj, kiuj
enhavis la frazon «<i lang="en">All rights reserved</i>» (Ĉiuj rajtoj
rezervitaj). Dum la jaroj, diversaj ebloj de interpreto de la vortludo
krom ĉi tiu estis kreitaj, inter iliaj ke la rajtocedo estus «kopio de
maldekstro» paralele de la aŭtorrajto, «kopio de dekstro».

[^150]: Stallman, <cite>Free Software, Free Society</cite>; Gay,
  <i lang="lt">op. cit.</i>; Stallman, <cite>Software libre para una
  sociedad libre</cite>, p. 293.

Per la vortludo aŭ laŭvorte la rajtocedo estis la koncepto, esprimita en
la GPL-permesilo kaj aliaj ligitaj al la GNU-Projekto, kiuj daŭras ĝis
hodiaŭ, postuli la juran posedon por, praktike, rezigni ĝin permesante,
ke ĉiuj faru la uzon, kiujn ili deziru, el la verko, dum ili donas siajn
proprajn liberecojn al aliaj. La formala bezono de posedo signifas, ke
neniu alia homo povos meti aŭtorrajton sur rajtocedan verkon kaj klopodi
limigi ĝian uzon. Stallman jam diris, ke lia komenca celo estis
idealisma: disvastigi la liberecon kaj la kunlaboradon, propagandante la
liberan programaron, kaj anstataŭigi la proprietan programaron, kiu
malpermesas la kunlaboron. Lia klopodo estis provi akordigi la
konservadon de la uza kaj modifada libereco de programaro kun protekto,
por ke ĝi ne estu proprigita libere de iu ajn. Kiel li mem diris:

> La maniero plej facila liberigi programon estas meti ĝin en la
> publikan havaĵon, sen aŭtorrajtoj. Tio permesas, ke la homoj kunhavu la
> programon kaj ĝiajn plibonigojn, se ili volu. Sed ankaŭ ĝi permesas,
> ke tiuj, kiuj ne kredas en kunlaboro, igu la programon proprieta
> programo. Ili povas fari modifojn, multajn aŭ malmultajn, kaj distribui
> siajn rezultojn kiel proprietan produkton. Homoj, kiuj ricevas la
> programon kun tiuj modifoj, ne ĝuas la liberecon, kiun la origina
> aŭtoro donis al ili; la peranto senigis ilin al ili.[^151]

[^151]: Stallman, <cite>Software libre para una sociedad libre</cite>,
  p. 125.

Post la GPL kaj la rajtocedo estis kreita leĝilo, kiu en la sekvaj jaroj
iĝis ideo ebla praktiki ne nur en la komputila mondo, sed ankaŭ en aliaj
sciaj kaj kulturaj fakoj, kunigante kelkajn aliajn grupojn ĉirkaŭ malnova
deziro montrita en la socio de demokratiigo de la kulturaj
havaĵoj[^152]. Igi la rajton de aliro pli granda ol la rajto de limigo
estis io, kion ĝis tiam oni kutimis manifesti per diversaj manieroj: en la
neado de la intelekta propraĵo, en la kontraŭaŭtorrajtaj praktikoj, kiuj
kritikis la pozicion vidi la kulturajn havaĵojn nur kiel varojn, en la
sendistinga uzo de partoj de aliaj verkoj sen fari pagon aŭ eĉ sen
agnoski la fonton (kiel en la malsamaj uzoj de kreema plagiato) kaj en
la rifuzo al la aŭtoreco per la anonimeco aŭ per kolektiva identigo. La
ideo uzi la sistemon de intelekta propraĵo mem por trompi ĝin montris sin
kiel novaĵo, kiu per la populariĝo de Interreto poste disvastiĝis al
diversaj lokoj kaj fakoj tre malproksimaj de sia deveno.

[^152]: Torres, <i lang="lt">op. cit.</i>, p. 131.

### III.

En la fino de la 1990-aj jaroj la rajtocedo disvastiĝas almenaŭ per du
malsamaj manieroj. La unua kiel ideo kaj praktiko de alfrontado al la
tiama stato de la aŭtorrajto kaj de la scio konsiderita kiel varo, vojo
elektita de aktivulaj movadoj de fakoj kiel la medio kaj la homaj
rajtoj; anarkiistoj, aŭtonomistaj marksistoj kaj membroj de iniciatoj
ligitaj al kontraŭliberala maldesktro; kaj artistoj, kiuj estas anoj de
kontraŭkulturo de respondo al la aŭtoritateco en diversaj fakoj, kiel
multaj el la nomitaj en la antaŭa ĉapitro. La dua vojo de disvastiĝo de
la rajtocedo okazas kiel parolado, kiu kunigas praktikojn por la defendo de
la informada kaj alira libereco post la ciferecigo de Interreto, la okazo
de multaj kodumuloj ligitaj al la libera programaro kaj al malfermitkodo
kaj de retaj aktivuloj, kiuj en tiu epoko vastiĝis en fakoj kiel la
libera kunhavado de dosieroj rete kaj la defendo de liberaj
amaskomunikiloj, kiuj serĉu malsamajn perspektivojn ol la ĵurnalismo de
la grandaj retoj.

En kelkaj okazoj la du manieroj miksas sin, kiel ni vidos poste. Sed
unue estas grave diri kiel dek jarojn post la kreo de la GPL, en 1999,
la rajtocedo fariĝis ĉefa inspiro por la kreo de movado ĉirkaŭ libera
kulturo (<i lang="en">free culture</i>), precipe pro Usono kaj Eŭropo.
Projektoj, kiuj aperis en tiu epoko, kiel Science Commons, Open
Access kaj Open Educational Resources (OER) &mdash; en Esperanto
tradukita kiel liberaj edukaj rimedoj[^153] &mdash;, disvastigis la
liberan aliron, uzon kaj kunhavadon de resursoj en malsamaj fakoj same
kiel estis estigita post la liberecoj de la libera programaro proponitaj
de Stallman. En socio, kie la informo, kodo kaj leĝo komencis formi
triopon pli kaj pli potenca, ideoj kiel la libereco, la komunaj bonoj
kaj la malfermeco disvolviĝas kiel ŝlosiloj en movado de libera kulturo,
kiu klopodas oferi alternativojn al la kreskanta malfermado kaj kontrolo
de la kultura en tiu epoko[^154].

[^153]: Pli da informo pri liberaj edukaj rimedoj en
  <https://eo.wikipedia.org/wiki/Liberaj_lerniloj>.
[^154]: Mansoux, Livre como queijo: confusão artística acerca da
  abertura, en Belisário; Tarim (originaj), <cite>Copyfight</cite>, p. 195.

Artistoj rilataj al la kontraŭkulturo kaj la scia libereco komencas
rigardi la ideon de la rajtocedo kaj vidi ĝin kiel taktikon, proprigante
ĝin kaj disvolvigante ĝin por diversaj celoj, inkluzive leĝaj. Tio estas
la okazo de la naskiĝo de la unua libera permesilo ekster la agokampo de la
programoj, la Licenco de Libera Arto[^155], kreita en la komencoj de
2000 de grupo de francaj artistoj en la reta diskutejo nomita Ataque
Copyleft. Publikigita en julio de 2000 ĝi estas bazita sur la samaj
principoj de la origina rajtocedo kaj aperas pro la deziro kaŭzi
kreajn procezojn, kaj ne pro aferoj ligitaj al la aŭtorrajtoj aŭ al la
uzo de programoj[^156]. Laŭ la opinio de tiuj, kiuj proponis la
permesilon, la libera programaro malfermis la realan vojon por la
disvastiĝo de la kreaj teknikoj pere de la ciferecaj amaskomunikiloj, kaj
la libera arto (la permesilo) helpus eviti la ekskluzivan proprigon de la
libera arto (kiel praktiko): «Se ni difinas la aŭtorrajton kiel
gvidantan principon, la Libera Arto konektas sin kun tiu, kiu la arto
ĉiam estis, ekde antikvaj tempoj, eĉ antaŭ kiam oni agnoskis, ke ĝi posedas
historion: mensa kreo kontraŭ kulturo, kiu ŝatus regi kaj kompreni
ĝin»[^157].

[^155]: Disponebla en <https://artlibre.org/>.
[^156]: Moreau, Sobre arte livre e cultura livre, en Belisário; Tarim,
  <i lang="lt">op. cit.</i>, p. 159.
[^157]: <i lang="lt">Ibidem</i>, p. 162.

Tradicie kontraŭaŭtorrajta kaj kun kolektivaj nomoj de la 80-aj jaroj, la
itala kolektivo Wu Ming montris sin identigita kun la rajtocedo por uzi
ĝin kiel bastionon en sia defendo kontraŭ la intelekta propraĵo. La
unuaj tekstoj kaj intervjuoj de la kolektivo al ĵurnalistoj, kiuj
menciis la aferon datiĝas de 2002 kaj 2003; speciale <cite>Copyright e
maremoto</cite>[^158], teksto publikigita de membro de la kolektivo (Wu
Ming 1), klopodas defendi la malfermitkodon kaj la rajtocedon kiel
strategiojn, kiuj aliancas sin kun la libera kunhavado kontraŭ la kultura
privatigo &mdash; kaj kiuj povus superi la tiaman leĝaron de intelekta
propraĵo. La forto de la rajtocedo venus de la fakto esti jura novaĵo
naskita de malsupro, kiu superas la simplan «piratecon», emfazante la
<i lang="lt">pars construens</i>[^159] de la reala movado[^160].

[^158]: Wu Ming, <cite>Copyright y maremoto</cite>.
[^159]: <i lang="lt">Pars construens</i> estas esprimo, kiu difinas
  «konstruan argumenton» en iu debato, kontraŭe de
  «<i lang="lt">pars destruens</i>». La distingo estis farita de
  Francis Bacon, en 1620. Noto de la teksto de Wu Ming, <cite>Copyright
  e maremoto</cite>.
[^160]: <i lang="lt">Ibidem</i>.

> La malfermitkodo kaj la rajtocedo vastiĝas nune pli malferme ol la
> programa programado: la «malfermitaj licencoj» estas ĉie kaj tendence
> povas iĝi la paradigmo de nova produktada maniero, kiu fine liberigu
> la socian kunlaboron (jam ekzistantan kaj videble praktikitan) de la
> parazita kontrolo, la eksproprietigo kaj de la «rento» profitanta al
> grandaj industriaj kaj firmaaj potenculoj.[^161]

[^161]: <i lang="lt">Ibidem</i>.

En 2005 la teksto <cite>Notas inéditas sobre copyright e copyleft</cite>
aktualigas la temon kaj montras la rajtocedon ne kiel movadon aŭ
ideologion, sed kiel terminon, kiu «enhavas serion de praktikoj,
situacioj kaj komercaj permesiloj kaj kiu korpigas tion, kio necesas por
reformi kaj adapti la aŭtorajn leĝojn al la “daŭripova disvolviĝo”»[^162].

[^162]: Wu Ming, Notas inéditas sobre copyright e copyleft, en <cite>La
  Remezcla</cite>.

Ankaŭ komence de la 2000-aj jaroj, parto de la cifereca aktivismo kaj de
la jura akademio komencas rigardi la movadojn ĉirkaŭ la libera kulturo
kaj unuiĝi kontraŭ la kreskanta severeco de la aŭtorrajtaj leĝoj, ĉefe
en Usono, origina loko de la unuaj personaj komputiloj, de la programoj
por tiuj komputiloj kaj de aliaj teknologiaj inventaĵoj faritaj en
Silicon Valley. Iuj el tiuj ĝisdatigoj en la leĝoj estis la *Digital
Millennium Copyright Act* (DMCA) kaj la *Sonny Bono Copyright Act*
(ankaŭ konata kiel la jam menciita Mickey Mouse Protection Act) &mdash;
en tiu sama jaro Brazilo kreis sian lastan aŭtorrajtan leĝon, kiu,
ankoraŭ aktivas ĝis la eldondato de ĉi tiu libro, pliigis el 60 al 70
jaroj la periodon de aŭtorrajta protekto post la morto de la aŭtoro[^163].

[^163]: Valente, <cite>Implicações jurídicas e políticas do direito
  autoral na internet</cite>, p. 150.

Unu el la ĉefaj voĉoj de la aktivismo kaj de la juro, kiu komencas sin
organizi ĉirkaŭ la nocio de libera kulturo estas Lawrence Lessig,
advokato kaj jura profesoro en Harvard. Membro de la Berkman Center for
Internet & Society, Lessig eldonis <cite>Code and Other Laws of
Cyberspace</cite> (1999), libron kiu igis lin referenco en juro kaj
regado en Interreto, kiam li okupiĝis pri la defendo de Eric Eldred,
organizanto de paĝo en Interreto, kiu disponigis librojn en publika
havaĵo kaj kiu forigis lian retejon el Interreto protestante la pliigon
de kvindek jaroj en la templimo de valideco de la aŭtorrajto proponita
per la Sonny Bono Copyright Act. Konita kiel Eldred kontraŭ Ascroft, la
kazo, de 1999, populariĝis en tiu komunikilo laŭ la atingopovo de la
retejo, kiu en tiu epoko havis pli da 20 mil aliroj tage, kaj per la
formulado por lia defendo proponita de Lessig, kiu kunigis diversajn
organizaĵojn por la defendo de la publikaj interesoj, kiel la Eletronic
Frontier Foundation (EFF), la Free Software Foundation (FSF), la Public
Knowledge, inter aŭtoroj, advokatoj, ekonomikistoj kaj eĉ teknologiaj
firmaoj, kiel Intel[^164].

[^164]: <i lang="lt">Ibidem</i>, p. 151.

Lessig argumentis, ke la grandigo de la templimon de la aŭtorrajtoj
malobservis la Usonan Konstitucion, kiu determinis, kiel Thomas
Jefferson kaj aliaj liberaluloj defendis en la finalo de la 18-a
jarcento, ke la protekto de aŭtorrajtoj havu limigitan
templimon[^165]. Eĉ apelaciante al la plej supera landa dokumento la ago
de Lessig estis neita en ĉiuj instancoj, êc en la Supera Korturo. Tio servis
tamen por montri kaj al Lessig kaj al aliaj aktivuloj, ke la tradiciaj
politikaj kaj juraj vojoj estis malfermitaj por la traktado pri la
moligo de la aŭtorrajtoj kaj «ke la rajtoj de aliro kaj protekto al la
publika havaĵo, en la oficialaj rondoj, estis viditaj kiel enmiksiĝo
malbona por la reta komerco»[^166]. En la fino de la 1990-aj jaroj, la
leĝaroj por Interreto estis adaptitaj bazante sin sur la aŭtorrajtaj leĝoj uzitaj
en la distro kaj en la kulturo, estigitaj post akordoj kiel tiu de Berna
kaj de Parizo, en la 19-a jarcento, en tiu momento ankaŭ jam integritaj en
la Monda Organizaĵo pri Komerco (MOK).

[^165]: En la subfrazo de aŭtorrajtoj kaj de patentoj de la Usona
  Konstitucio, citita en la 55-a noto.
[^166]: Valente, <i lang="lt">op. cit.</i>, p. 154.

La vojo elektita post la juraj malvenkoj estis krei novan temon por
prezenti aliajn vojojn, jurajn kaj politikajn, por la defendo de la scio
kaj la libera kulturo. De tiu movado naskiĝis, en 2001, Creative Commons
(CC), ne-profitcela organizaĵo, kiu klopodis krei alternativajn
permesilojn anstataŭ la limiga «Ĉiuj rajtoj rezervitaj» de la aŭtorrajto.
Ĝi prezentis kiel opcion «iuj rajtoj rezervitaj», en kiu ĉiu kreanto
povus elekti tion, kion ri ŝatus liberigi, el la plej limiga &mdash;
kiu estas sama kiel la jam ekzistanta aŭtorrajto &mdash; al la malplej,
kiel la publika havaĵo[^167]. La projekto komencis gvidita de Lessig,
Hal Abelson kaj Eric Aldred, kun financa subteno de la Center for the
Public Domain, esplorejo ligita al la Universitato Harvard, kie Lessig
laboris, havante la celon «grandigi la malpliigitan publikan havaĵon,
fortigi la sociajn valorojn de kunhavado, de malfermo kaj de
antaŭenirado de la scio kaj de la individua kreemo»[^168]. Ĝi klopodis
esti pragmata alternativo al la aktiva sistemo de aŭtorrajto kaj estis
malkaŝe inspirita en la movado de la libera programaro kaj en la
rajtocedo, kvankam ĝi havis pli grandajn ecojn, kun permesiloj, kiuj
utilis por diversaj specoj de kulturaj verkoj kaj ne por nur unu speco
(la programoj), kiel la GPL.

[^167]: La CC-permesiloj, kiuj ekzistas en 2022, estas montritaj en la
  retejo <https://creativecommons.org/licenses/>.
[^168]: En Bollier, <cite>How the Commoners Built a Digital Republic of
  their Own</cite>, tradicido a partir de Valente,
  <i lang="pt">op. cit.</i>, p. 156.

Kiel multaj el la proponoj, kiuj klopodas grandigi la atingon de specifa
scio, Creative Commons devis simpligi iujn procedojn, kio kaŭzis multajn
kritikojn pri malpolitikiĝo de la iniciato kaj de la ideo de la
aŭtorrajto mem. Kreante ĝian licencaron ekzemple CC pliigis la elektajn
eblojn de la origina rajtocedo proponita en la GPL sen estigi
liberecojn, rajtojn nek fiksitajn kvalitojn &mdash; aŭ sen diferenci
tion, kio estus libera permesilo kaj proprieta permesilo, ambaŭ eblaj inter
la ses permesiloj elekteblaj de la projekto. Do Benjamin Mako Hill,
Florian Kramer, Dimitry Kleiner, Anna Nimus, inter aliaj en tiu epoko,
indikis, ke CC ne estigis etikan pozicion kiel la libera programaro, aŭ
eĉ kiel la malfermitkoda movado[^169] &mdash; skismo kun komerce pli
malrigidaj principoj ol la libera programaro, sed kiu ankaŭ havas, kiel ĝi,
politikajn ideojn difinitajn pri tio, kion ili defendas kaj kion ne.

[^169]: Malfermitkoda programaro
(<i lang="en">free/libre/open source software</i>, akronimo FLOSS
unue adoptita en 2001) estas nomo uzita por speco de programaro, kiu
aperis post la nomita Open Source Initiative (OSI), kreita en 1998
(<https://opensource.org/>) kiel skismo kun principoj iom malpli rigidaj ol
tiuj de la libera programaro (<https://opensource.org/osd>), kio kaŭzis
konsiderindan disvastiĝon kaj de la termino malfermitkodo (<i lang="en">open
source</i>) kaj de projektoj kaj firmaoj, kiuj havas la programaron kiel
produkton kaj motoron de siaj negocoj. OSI havas kiel centran filozofan
tekston <cite>La katedralo kaj la bazaro</cite> (angle <cite>The
Cathedral and the Bazaar</cite>), de Eric Raymond, publikigitan en 1999.
En tiu teksto Raymond traktis la ideon, ke *«per sufiĉaj okuloj ĉiuj
eraroj estas videblaj»*, por diri, ke, se la fontkodo estas disponebla
por publika provo, ekzamenado kaj eksperimentado, la eraroj estos
malkovritaj pli rapide. La originala eseo povas estis tute legita angle
en
<http://www.catb.org/~esr/writings/cathedral-bazaar/cathedral-bazaar>.

Laŭ tiu kritika vidpunkto Creative Commons lasus tro da libereco al la
elektoj por la kreantoj (aŭ konsumantoj), kio servus pli por rezervi la
rajtojn al la *uzantoj* ol al la posedantoj de la aŭtorrajtoj[^170]. En
la kritiko de Nimus: «Creative Commons utilas por helpi al la
produktanto teni la kontrolon sub “ria” verko, kio legitimas la
kontrolon aplikitan de la produktanto anstataŭ rifuzi ĝin kaj trudas la
distingon inter produktanto kaj kaj konsumanto anstataŭ revoki
ĝin»[^171]. Laŭ tiu perspektivo, kiu resonas en multaj el la
praktikoj kontraŭartaj kaj kontraŭaj al la aŭtorrajto de la artaj
avangardoj de la 20-a jarcento, CC estus kiel la pompa versio de la
aŭtorrajto, kiu «ne kontraŭas al la aŭtorrajta reĝimo kiel
tuto nek konservas ĝian juran statuson por renversi la praktikon de la
aŭtorrajto, kiel la rajtocedo faras»[^172].

[^170]: Kramer, O mal-entendido do Creative Commons, en Belisário;
  Tarim, <i lang="lt">op. cit.</i>, p. 180-181.
[^171]: Nimus, <i lang="lt">op. cit.</i>, p. 52.
[^172]: <i lang="lt">Ibidem</i>.

Ne estas surprizo nek malmerito la pragmatika vojo adoptita de Creative
Commons. Kun influo precipe liberala, de la tradicio de John Locke,
Condorcet kaj Thomas Jefferson, Lessig ne volis aboli la aŭtorrajton,
sed reformi ĝin. Lia propono, prezentita per Creative Commons, malkaŝe defendis
la liberecon de la kreantoj, kiu estis atakita per la daŭra
grandigo de la periodo de la daŭro de la aŭtorrajtoj, kio ankaŭ minacis
la prizorgadon de komuna publika havaĵo. Tiel, lia pozicio estis «kunigi
pli da subteno ĉirkaŭ la celoj, kiuj refaras la socian pejzaĝon de la
kreiveco»[^173], kio iĝis la iniciato, almenaŭ en la unuaj jaroj, manka
de ĉiuj la politikaj kaj etikaj principoj kontraŭaj al la aŭtorrajto,
kiujn granda parto de la defendantoj de la libera programaro, de la
rajtocedo kaj de libera kulturo tradicie kontraŭaŭtorrajta kunportis.

[^173]: <i lang="lt">Ibidem</i>.

En kelkaj okazoj Stallman publike diris, ke pro la disvastiĝo de CC en
la 2000-aj jaroj multaj homoj komencis dubi pri la diferenco inter la
rajtocedo kaj Creative Commons. En la termoj proponitaj por la jura
<i lang="en">hack</i> de la rajtocedo, nur unu el la permesiloj estus
konsiderita: la CC BY-SA &mdash; *Kunhavado sub la sama permesilo*[^174],
kiu permesas la reuzon kaj la kunhavadon de la verko, inkluzive por
komercaj finoj, se ĝi konservas en la estonteco la liberecojn
akiritajn por aliaj uzojn, «infektante» la aliajn verkojn kaj
garantiante, ke ili ne estu malfermitaj per aŭtorrajto. Alia permesilo, la
CC BY[^175], kiu donas la samajn liberecojn ol la publika havaĵo, ankaŭ
estas libera permesilo laŭ la terminoj de la GPL kaj de la kvar liberecoj de
la libera programaro, dum la aliaj kvar ĉefaj permesiloj de Creative
Commons &mdash; kiuj povas aŭ ne permesi la modifon de la verko kaj
malpermesi la uzon por komercaj celoj, ekzemple &mdash; ne estas
liberaj.

[^174]: Kompleta teksto de la permesilo disponebla en
  <https://creativecommons.org/licenses/by-sa/4.0/deed.eo>.
[^175]: Tute disponebla en <https://creativecommons.org/licenses/by/4.0/deed.eo>.

Eĉ kun la kritikoj, la strukturo de CC, la praktikeco de ĝia licencaro
kaj ĝia intenco klopodi defendi, kvankam ĝenerale, la kunhavadon kaj la
publikan havaĵon faciligis ĝian disvastigon al diversaj landoj kaj
preter la teknologia mondo. La disputoj ĉirkaŭ la kunhavado de ciferecaj
dosieroj en la 2000-aj jaroj helpis ankaŭ popularigi Creative Commons
kiel realigeblan alternativon por la batalo kontraŭ la diskurso de la
krimigo de la pirateco por kiu elŝutis protektitajn dosierojn
el la Interreto. «Ĉio faciliĝas, kiam vi ne bezonas perantojn» estis frazo
aŭskultita en tiama video de disvastigo de CC[^176], kiu substrekis la
praktikecon, por ke la kreantoj elektu, per anticipita maniero, kiujn
rajtojn ili volis konservi (krom la atribuon kiel aŭtoro, estigitan kiel
normon por ĉiuj verkoj kaj agnoskita en ĉia leĝaro de intelekta propraĵo)
kaj kiujn ili volis liberigi. La rajto de adaptado aŭ libera kunhavado
de muzikaĵo ekzemple faciligus ĝian disvastiĝon per malsamaj versioj
remiksitaj &mdash; ekzempla okazo en tiu aspekto estas tiu de la disko
citita en tiu sama video de prezentado de CC, nomita «Redd Blood Cells»,
en kiu la basgitaristo Steven McDonald, de la bando Redd Kross,
reregistris en version kun basgitaro ĉiujn muzikaĵojn de la disko «White
Blood Cells» de White Stripes, bando de nur gitaro, voĉo kaj drumo.

[^176]: <cite>Get Creative!</cite>, la video, ankoraŭ videblas per la
  ligilo <https://www.youtube.com/watch?v=SUblaElbybE>.

Post 2003 kaj 2004 la disvastiĝo de CC naskigis grupojn, kiuj tradukis
kaj adaptis ĝiajn permesilojn por la lokaj realaĵoj en landoj kiel Japanio,
Sud-Koreio, Meksiko, Kroatio, Portugalio, Hispanio, Germanio, Argentino,
Urugvajo, ĝenerale organizitaj per esploraj institucioj kaj universitatoj
aŭ aŭtonomaj grupoj. En Brazilo la unuaj jaroj de la jardeko koincidis
kun la alveno de Lula al la landa Prezidanteco, en 2002, kaj de Gilberto
Gil kiel ministro de Kulturo, en 2003. Centra persono de la brazila
muziko, Gil kun la antropologo Hermano Vianna renkontiĝis kun Lessig
kaj, laŭ estas registrita, «[li] rapide komprenis la projekton kaj
subtenis la kaŭzon»[^177]. En la analizo de Hermano Vianna, amiko kaj
partnero de la brazila muzikisto, «la kulturo de la kunhavado kaj ĉefe
tiu de la <i lang="en">sampling</i> estis tiel ligita al la tropikismo,
ke la kompreno de la neceso pensi la liberan kulturon estis tuja por
Gil»[^178]. Kiom da rekombinita jam estis la tropikismo, kiam
<cite>Tropicália ou panis et circensis</cite>, modela albumo de la
movado de 1968, kunigis Vicente Celestino, John Cage, popularan kaj
erudician kulturon strategie irante tra la antropofagismo proponita de
Oswald de Andrade[^179]?

[^177]: Valente, <i lang="lt">op. cit.</i>, p. 156.
[^178]: Bollier, <i lang="lt">op. cit.</i>, tradukita de Valente,
  <i lang="lt">op. cit.</i>, p. 157.
[^179]: En Viveiros de Castro, <i lang="lt">op. cit.</i>, p. 81.

La aliĝo de la Kultura Ministerio (portugale Ministério da Cultura,
MinC) gvidita de Gil al Creative Commons okazis post agoj kiel la
disvolvigo de la permesilo CC-GPL, en 2003, kiu tradukis la komencan
tekston de la GPL al la portugala, kaj de la adopto de la permesiloj en la
materialoj produktitaj de la MinC. Tio komencis ankaŭ momenton de
sindevontigo de la ministerio kun la libera programaro, kio rezultigis
projektojn kiel Pontos de Cultura, kiu, post 2004, disdonis komputilarojn
kun liberaj operaciumoj por etaj kulturaj produktantoj en tuta Brazilo.
Stranga publika politiko, kiu kunigis liberan teknologion kaj popularan
kulturon, la [projekto] Cultura Viva[^180], kiel konatiĝis la projekto,
fortigis la disvastigon de la libera programaro kaj kulturo en la
lando kaj igis Brazilon, tiam, unu el la ĉefaj disvolvigantaj kaj
konsumantaj ejoj de teknologioj kaj libera kulturo de la mondo. Gil,
samtempe iĝis proksima de Lessig kaj publika defendanto de CC;
diskonigis ĝin kiel demokratigantan kaj societumantan ilon &mdash; la
unika Kultura ministro de iu ajn lando, kiu faris tion, kio ankaŭ
kontribuis por doni mondan videblecon al la projekto[^181].

[^180]: La [projecto] Cultura Viva estas «kultura politiko destinita al
  agnosko kaj la subteno de aktivecoj kaj kulturaj procezoj jam
  disvolvitaj, stimulante la socian partoprenadon, la kunlaboron kaj la
  kunhavitan administradon de publikaj politikaĵoj en la kultura medio».
  Kvankam, en la momento de ĉi tiu teksto, la projekto estas ĉesigita
  kaj la Kultura Ministerio de Brazilo ne ekzistas, ĝi povas esti detale
  konita en la retejo <https://web.archive.org/web/20210302234024/http://culturaviva.gov.br/>.
[^181]: Valente, <i lang="lt">op. cit.</i>, p. 157.

Paciga starpunkto, proponita de la antropologo Eduardo Viveiros de
Castro, resumas la influon de Creative Commons en Brazilo kaj en la
mondo per vidpunkto kaj koncepta kaj pragmata.

> Ĝi estas klopodo, laŭ mia opinio tre meritplena. Ili klopodas eviti,
> ke la cifereca mondo estu malfermita, tiel kiel la geografia mondo
> estis. Ke ĝi estu privatigita. Ĝi estas klopodo konservi la informon
> kiel havaĵon publikan. La granda afero por Creative Commons estas, ke la
> informo ne sekvas la sistemon de nula sumo, ke ĝi povas antaŭeniri kaj
> ne malkreski pro tio. Tio ne signifas, ke aŭtoro devas esti
> plagiatita; la celo estas faciligi la disvastiĝon. [...] La ideo
> estas, ke la aŭtorrajto signifas
> «<i lang="en">all rights reserved</i>» kaj Creative Commons signifas
> «<i lang="en">some rights reserved</i>». Kaj vi diras, kiuj estas
> ili. Estas kelkaj formuloj, kelkaj specoj de malfermitaj permesiloj.
> Ĝi temas pri krei manieron de kunvivado en la informa medio, kiu estu
> tolerema, kaj kiu evitu tion, kio okazas, kio estas la kontrolo de la
> informo per la grandaj firmaoj. Nun ĉi ĉio ankoraŭ estas, iel,
> paliativo. Creative Commons povas esti vidita, kiel ĝi estas efektive
> de la plej, ni diru tion, radikalaj, kiel kapitalisma strategio. La
> vera anarkiisto ne volas scii pri Creative Commons nek pri rajtocedo,
> estas tute radikala. Unue mi estas kun ili, mi pensas, ke la privata
> proprieto estas monstraĵo, estu ĝi intelekta aŭ ne, sed mi ankaŭ
> scias, ke oni ne antaŭeniras frapante muron, kovrante la sunon per
> kribrilo. Mi pensas, ke vi devas cedi, devas fari ian
> intertraktadon[^182].

[^182]: Viveiros de Castro, <i lang="lt">op. cit.</i>, p. 93-94.

La disvastiĝo de la libera kulturo en la 2000-aj jaroj havis, krom la
rajtocedon kaj la permesilojn de Creative Commons, alian gravan eron: la
publikigo de <cite>Free Culture</cite> (<cite>Libera kulturo</cite>), de
Lawrence Lessig, en 2004. La libro savas la historion de la intelekta
propraĵo per emblemaj kazoj, iuj el ili jam ĉi tie komentitaj &mdash;
kiel la bataloj en la anglaj tribunaloj de la 17-a jarcento, kiuj estigis
la aŭtorrajton kaj la uzon de historioj de publika havaĵo fare de Disney.
Inspirita per la libera programaro, la verko defendas koncepton de
libera kulturo kiel tiun, kiu devas esti kiel eble plej minimume
limigita, por ebligi ĝian kunhavadon, disdonadon, kopion kaj uzon, sen
ke tio afektu al la intelekta propraĵo de la kulturaj havaĵoj. Per tio
ĝi helpas disvastigi vidon de la kulturo, kiu organizas movadon favoran
al modifoj de la nuntempaj aŭtorrajtaj leĝoj, kiuj, laŭ Lessig kaj aliaj
aktivuloj, malfaciligas la kreemecon kaj disvastigas «kulturon de
permeso», en kiu ĉiu kreanto devas
peti permeson, se ri volus uzi specifan verkon, kio ajn estu la celo.
Movado por la libera kulturo, kiel komencas esti identigita en tiu
epoko, batalis por konservi publikan havaĵon fortan kaj alireblan al
ĉiuj, kreante, krom leĝojn, ankaŭ teknologiojn, strategiojn kaj
taktikojn por konservi la kreaĵojn liberaj, ne necese «senkostaj»,
parafrazante la konatan frazon de Stallman uzitan en la kunteksto de la
libereco de la libera programaro: «<i lang="en">Think free
as in free speech, not free beer</i>»[^183].

[^183]: «Pensu pri libera kiel en parollibereco, ne kiel en senkosta biero».
  Tre konata en la mondo de la libera programaro, la frazo estas atribuita al
  Stallman de Lessig unue en 2006, disponebla en
  <https://www.wired.com/2006/09/free-as-in-beer>.

<cite>Libera kulturo</cite>, la libro, ankaŭ prezentas praktikajn
proponojn de defendo de la publika havaĵo. Iuj el ili estis diskutitaj
kaj ankoraŭ hodiaŭ estas konsideritaj por reformistoj, kvankam oni havas
la nocion, ke, por la intereso de la grupoj de protekto de la aŭtorrajtoj en
la tuta mondo, ili ankoraŭ estas viditaj kiel tro radikalaj. La
malkreskigo de la templimo de daŭro de la aŭtorrajto ekzemple estas
propono, kiu ĉiam ekzistis kaj kiu Lessig daŭrigas en la libro por
konsideri ĝin bazante sin sur la ideo, ke tiu templimo «devus esti tiel daŭra kiel
necesa por instigi la kreadon, ne pli»[^184]. Kio, krom faciligi la
aliron kaj konservi verkojn pli da tempo en publika havaĵo, evitus ankaŭ
la neceson krei kontinuajn jurajn esceptojn, kiuj malfaciligas la
komprenon, por la granda publiko, kiu ne estas advokato, pri kio estas
protektita kaj kio estas malfermita. Lessig diras, ke, ĝis 1976, la
averaĝa periodo de daŭro de aŭtorrajto en Usono estis de 32,2 jaroj kaj
ke eble tiu averaĝa periodo estus adekvata.

[^184]: Lessig, <i lang="en">op. cit.</i>, p. 235.

> Sendube la ekstremistoj nomos ĉi tiujn ideojn «radikalaj». (Mi ja
> nomas ilin «ekstremistoj»). Sed, denove, la templimo, kiun mi
> rekomendis estas pli daŭra ol la templimo sub Richard Nixon. Kiom da
> «radikala» estas peti aŭtorrajtan leĝon pli donaceman ol tiu, kiun
> Nixon prezidis?[^185]

[^185]: <i lang="lt">Ibidem</i>, p. 236.

Aliaj ideoj prezentitaj de Lessig en la 2004-a eldono sonis kiel
antaŭvidoj de la sekvaj jardekoj, kiel la rilata al la kunhavado de
dosieroj Interrete.

> Kiam estos facilege konekti al servoj, kiuj permesu aliron al enhavo,
> estos pli facile konekti al servoj, kiuj permesu aliron al enhavo,
> ol elŝuti kaj konservi enhavon en la multaj iloj, kiujn ni havos
> por «ludi» enhavojn. Estos pli facile, alivorte, subskribi ol iĝi
> administranto de datumbazo, kiu estas esence ĉiu en la mondo de
> elŝuto kaj kunhavado de teknologioj similaj al Napster. La enhavaj
> servoj konkuros kontraŭ kunhavado de enhavo, eĉ se la servoj postulas
> monon kontraŭ la enhavo, al kiu ili donas aliron.[^186]

[^186]: <i lang="lt">Ibidem</i>, p. 239.

### IV.

La Interreto de la 1990-aj kaj 2000-aj jaroj, periodo en kiu la libera
kulturo disvastiĝis, estis momento de ekstrema libereco kaj imago,
manifestita per la reganta optimismo ĉirkaŭ la ebloj, kiujn la reto
aperigis, kaj per la libereco kunhavigi ebligita en la diversaj retejoj,
kiuj disponebligis la plej multespecajn dosierojn de kulturaj havaĵoj de
la planedo. Kiel reto bazita sur la interŝanĝo de informo, la Interreto
ekde sia komenco ebligis kaj faciligis la liberan kunhavadon de
dosieroj. Kiam ĝi ankoraŭ estis niĉo ĉefe uzita de sciencistoj,
militistoj kaj reprezentantoj de la kontraŭkulturo, en la 1980-aj jaroj
kaj en la unuaj jaroj de la 1990-a jardeko, la libera disvastigo de
informo ne multe ĝenis la industriojn bazitajn sur la intelekta propraĵo
&mdash; ĉar, en tiu epoko, nur eblis sendi dosieretojn, bitojn da
informo, kiuj rondiris inter malmultaj homoj. La dosierfomo de kodigo de
sondosiero ekzemple nur ŝanĝis muzikaĵon en datumojn, kiuj povis esti
libere senditaj tra la Interreto post 1993, kiam MP3[^187] estis publikita, unu
el la unuaj specoj de densigoj de sondosieroj kun perdo de informo
preskaŭ neperceptebla por la homa aŭdado. Tamen pasis iujn jarojn ĝis la
dosierformo populariĝis kaj la ebleco de transdono de datumoj en la
Interreto atingis transporti muzikaĵon sen superŝarĝi la reton.

[^187]: Ĝia bitrapido estas de la kiomo de *kbps* (kilobitoj dum
  sekundo), 128 kbps estante la defaŭlta bitrapido, en kiu la malpliigo
  de la dosiergrando estis de preskaŭ 90% &mdash;la dosiergrando iĝas
  1/10 de la origina grando&mdash;. Fonto: <https://en.wikipedia.org/wiki/MP3>.
  Kiel ĉiuj teknologioj cititaj en ĉi tiu libro, ĝi estas frukto de multaj
  eksperimentoj kaj longaj jaroj de sciencaj esploroj, kiuj referencas
  multajn manierojn sendi altkvalitajn sonojn kaj ĉifri aŭdaĵojn, kiuj
  estigis la MPEG-an dosierfomon kaj, poste, la MP3-n (MPEG3), historio
  detale rakontita en la artikolo «Genesis of the MP3 Audio Coding
  Standard» de H. G. Musmann, de la Universitato de Hannover, en
  Germanio, disponebla en
  <https://ieeexplore.ieee.org/document/1706505>.

Post la komenco de la komerca Interreto en la mondo post 1994 (en
Brazilo en 1995), miloj de homoj komencis podi libere alŝuti kaj elŝuti
dosierojn, protektitaj aŭ ne per aŭtorrajto, pere de la samtavolaj
komunikadoj (<i lang="en">peer to peer</i>, ankaŭ mallongigita kiel
<i lang="en">p2p</i>), kiel *torento*, malcentra procedo de kunhavado, kiu
faciligas la elŝuton, por ke ĉiu uzanto povu elŝuti partojn de dosiero
el aliaj partoj disdonitaj en diversaj komputiloj &mdash;ju pli da
aparatoj, des pli rapida la procedo. La facileco de la disvastigo de
informo ebligita per Interreto eksponente kreskis pro la pliiĝo de la
rapideco de la konektoj; la ŝaltilaj retoj de 56 kbps, ordinaraj en
1995[^188], post malmultaj jaroj iĝis de 1 000 kbps pro la popularigo de
la servo konata kiel ADSL (<i lang="en">Assymmetric Digital Subscriber
Line</i>, Malsimetria Cifereca Abonula Linio[^189]), kiu utilis por plej
granda parto de la aliro al la Interreto por personaj komputiloj jam en
la komenco de la 2000-a jardeko. Per pli da rapideco por elŝuti pli
grandajn dosierojn en la reto, ago timita kaj kontraŭita ekde la komenco
de la aŭtorrajto denove estis la centro de la publika atento:
la piratado.

[^188]: Per tiu rapideco MP3-a muzikdosiero (3,5 megabajtoj) ekzemple
  bezonus averaĝe inter 15 kaj 30 minutoj por esti elŝutita por persona
  komputilo, dum video de malalta kvalito (700 megabajtoj), inter 28 kaj
  42 horoj. Ĉar la datumoj per la Interreto kaj la voĉo per la telefono
  estis senditaj tra la sama kanalo, nur unu ago povis esti samtempe
  farita: elŝuti MP3-dosiero okupus la telefonlineon dum ĝis 30 minutoj,
  kiu signifis, por fakturadaj celoj, loka konekto de du horoj. Ago,
  kiu dependante de la valoro de la pulso aŭ de la minuto povus kreskigi
  la valoron de la telefona fakturo per centoj de realoj en Brazilo dum
  la unuaj jaroj de la komerca Interreto (Foletto, <cite>Um mosaico de
  parcialidades na nuvem coletiva</cite>, p. 117-8).
[^189]: Ĝenerale la ADSL ankaŭ funkcias pro la telefonaj lineoj kaj
  kabloj, sed kun la malsameco, ke la datumoj estas dividitaj en tri,
  kiam ili estis senditaj: la elŝutaj datumoj, tio estas, el la kabloj,
  kiuj kondukas la Interretan informon al la komunikaj centroj, kaj el
  tiuj al la komputilo; alŝutaj datumoj, el la komputilo al la kabloj,
  al la centroj kaj al la Interreto; kaj la voĉo per telefono, kiu estas
  disigita de la aliaj informoj per ilo nomita dividilo, instalita kaj en
  la uzula lineo kaj en la telefona centro. La samtempa sendado de tiuj
  tri specoj de datumoj okazas en malsamaj frekvencoj, sed en la samaj
  kabloj: la telefonlineo servas kiel «strato» por la movado de
  tri-specaj datumoj. Ne estas plu alvoko al specifa nombro por estigi
  konekton, kiel la pertelefona konekto, kiu liveras la telefonon kaj ne
  signifas pagon de pulsoj nek de aliron al la Interreto, igante la
  servon pli malmultekosta (Foletto, <i lang="lt">op. cit.</i>, p. 121).

Por la industrioj bazitaj sur la intelekta havaĵo, la problemoj kun la
piratado komencis gravi per Napster, programo kreita en 1999 &mdash;
ankaŭ jaro, en kiu la dosierformo de disvastigo de muziko MP3 iĝis
ordinara &mdash; de juna kodumulo nomita Shawn Fanning. Ĝi funkciis
tiel: uzanto elŝutis programon, aliris al serĉa interfaco, serĉis
muzikaĵon kaj, se ri trovis dosieron disponeblan kun muzikaĵo (aŭ
albumo) disponebligita per unu aŭ pli komputiloj ankaŭ kun la programo,
ri elektis ĝin por ĝin elŝuti kaj atendis. La hejmaj Interretaj retoj en
1999 kaj 2000 estis malrapidaj, kun rapideco ekvivalenta al inter 1/10
kaj 1/300 de la rapideco de post du jardekoj; tiam la atendo por la
elŝuto de muzikaĵo povus daŭri kelkfoje horojn; por libro, kelkajn
dekojn da minutoj; kaj por filmo, tagojn aŭ semajnojn. Per ĉiuj ajn
rapidecoj la eblo elekti estis ega kaj la dosiero venis senpage.

La ideo de Fanning kaj de lia kunfondinto Sean Parker (kiu poste estis
unu el la unuaj akciuloj de Facebook) estis krei programon kun facile
uzebla grafika interfaco, facile elŝutebla al la tiamaj komputiloj, por
ke ĉiu ajn povu serĉi siajn muzikaĵojn, en MP3, laŭ artista nomo,
albumo, muzikaĵo kaj eĉ tutaj ĝenroj, kaj elŝuti kopion por sia
maŝino[^190]. Tio estis la kutimo kunhavi muzikaĵojn, popularigita en la
registroj en la kasedaj bendoj de post la 1970-aj jaroj, disvastigita
tutmonde danke al formo, kiu permesis samtempe kunhavi muzikon kaj teni
ĝin kun vi en la fiksitaj diskoj, KD-j kaj tiamaj distekoj. Muzikaĵo en MP3 elŝutita
el Napster portis ankaŭ kiel novaĵo esti «havaĵo sen rivalo»[^191], kiu
signifas diri, ke povis kunekzisti en malsamaj kopioj kaj esti
transportita en ĉiun ajn aparaton, kiu kapablis legi (do *ludi*) la
kombinaĵojn de 0-j kaj 1-j, kiuj densigis muzikaĵon, sendepende de ĝia
komplekseco, en eta dosiero, kiu havis pli malpli 4 MB da informo. En
tiu epoko ne nur la persona komputilo ludis la dosierformon, sed multaj
aparatoj de ciferecaj sonoj kaj pli malgrandaj iloj, precipe nomitaj
«MP3-ludiloj», disvastigitaj post la iPod, de Apple, eldonita en 2001.
Kaj mi ankoraŭ ne parolis pri la lasere kompaktaj diskoj (CD-RW [KD legebla kaj skribebla]), kiuj &mdash;
kiel la bendoj antaŭe, sed kun la kapableco konservi pli ol 10 horoj da
centoj da muzikaĵoj kaj ne nur 60 minutojn &mdash; iĝis popularaj kiel
malmultekosta maniero fizike disdoni dosierojn (enhaveco: 700 MB) en tiu
epoko, poste anstataŭitaj per la Digital Video Disc (DVD), kun iom pli
ol sesoblo de la enhaveco de la KD (4,7 GB), kaj la poŝmemoriloj, kun eĉ
pli da spaco (5, 10, 15 GB kaj pli).

[^190]: Deak; Foletto, Ambiente digital de difusão: por onde circula a
cultura online?, <cite>BaixaCultura</cite>, 14-an jun. 2019. Disponebla en
<https://baixacultura.org/ambiente-digital-de-difusao-por-onde-circula-a-cultura-online/>.
[^191]: Alia koncepto, kreita en Brazilo de la profesoro de komputiko de
  la USP Imre Simon kaj de la esploristo Miguel Said Vieira, parolis pri
  «<i lang="pt">rossio não rival</i>» &mdash; vidu Simon; Vieira, O
  rossio não-rival (The Non-Rival Commons), <cite>Revista da
  USP</cite>. Ili argumentis, ke la plej bona traduko por
  <i lang="en">commons</i> estus <i lang="pt">rossio</i>, kiu, laŭ la
  vortaro Houaiss, estas «tereno komune falĉita kaj uzita». Laŭ
  Savazoni, «la celo de la klopodo farita de la aŭtoroj estis trovi
  manieron traduki terminon, kiu ne trovas en la portugala idealan
  tradukon, kaj tio igas ĝin tre malfacile asimilebla. Fine la ideo ne
  akiris multajn subtenantojn. Tiom malmulte, ke kelkaj aŭtoroj preferis
  teni la originalan esprimon angle, «commons», kreante anglismon, kiu
  laŭ mia opinio, tenis la koncepton apartigitan en la politika-kulturaj
  debatoj en la portugala (Savazoni; <cite>O comum entre nós: da cultura
  digital à democracia do século XXI</cite>).

La senpaga elŝuto de MP3-j estis la unua granda eblo de frakaso, en la
Interreto, de la sistemo bazita sur la vendo de kulturaj havaĵoj
starigita per la ekspluatado de la intelekta propraĵo en la 19-a
jarcento. Sen rekompenci la aŭtorojn per la elŝuto, tiu sistemo,
estante Napster la unua kazo, estis rapide atakita: jam je la fino de
1999, la Record Industries Association of America (RIAA) komencis
proceson kontraŭ la programaro de Fanning kaj Parker, kiu en ĝia unua
jaro de funkciado devis respondi en la tribunaloj pro la akuzo de
piratado kaj defendi sin kontraŭ kompensopeto de 100 mil dolaroj pro
muzikaĵoj elŝutitaj. Eĉ kun ĉiu la subteno akirita tiam Napster perdis
la proceson kaj, la sekvan jaron, devis ĉesi la kunhavadon de verkoj
registritaj kun aŭtorrajto, kio ne signifis, ke ĝi devis tute fermi
la servojn. Sed ĝi ne trovis solvon, kiu filtris inter verkoj kun
kaj sen aŭtorrajto &mdash; kio estus tre malfacile sen enmiksiĝi en la
aŭtonomeco kaj en la datumoj de ĉiuj homoj, kiuj igis enhavon disponebla
en la programo &mdash;, kaj, en julio de 2001, interrompis siajn agojn,
por en la sekva jaro remalfermi kiel servo de subskriboj de elŝutoj de
muzikaĵoj kaj tiel resti ĝis hodiaŭ[^192].

[^192]: Sen la aliro, kiu havis en siaj unuaj du jaroj, la retejo estis
  aĉetita de servo nomita Rhapsody en 2011 kaj ĝi funkcias kiel
  pagosubskribo en la adreso <https://us.napster.com/home>.

La efiko, kiun la kazo havis inter la artistoj[^193] kaj
retaj aktivuloj; la pli ol cent aktivaj uzantoj, kiujn Napster havis
registritaj en 1999, plejparte maljunuloj el la tuta mondo,
kiuj estis novaj uzantoj de la Interreto; la kovrilo de la revuo
<cite>Time</cite> de oktobro de 2000 kun la frazo «<i lang="en">What’s
Next for Napster?</i>» («kio sekvas por Naspter?») kaj bildo de la
junulo (19-jara) Fanning kun ĉapo kaj ega aŭdilo; ĉiuj indicoj, ke la
proceso ne finus tie. Programoj, kiuj funkciis simile, bazitaj sur la
<i lang="en">p2p</i>-kunhavado, disvastiĝis tra la reto, kiel okazis per
Gnutella, Grokster, Kazaa, FreeNet, Morpheus, Soulseek, inter aliaj,
kiuj antaŭenigis la samajn procedojn de libera kunhavado de dosieroj,
dum la RIAA daŭrigis kaj intensigis la malpopularajn procesojn kontraŭ
uzantoj, kiuj kunhavis dosierojn en tiuj programoj[^194].

[^193]: Unu el la kazoj plej emblemaj de tiu epoko estis la proceso,
  kiun Metallica, havante kiel proparolanton ĝian drumiston kaj
  komponiston Lars Ulrich, komencis en 2000 kontraŭ Napster serĉante ne
  nur la monon de Fanning kaj lia programaro, sed ankaŭ tiun de la
  uzantoj, kiuj elŝutis la muzikaĵojn de la bando, adorantoj de la sono
  de la bando. Se ne estis io senprecedenca, minimume estis strange vidi
  artiston, kiu procesis siajn proprajn adorantojn. Pri tiu ĉi kazo vidu
  <https://en.wikipedia.org/wiki/Metallica_v._Napster,_Inc>.
[^194]: Nur en 2004 estis 264 juĝaj procesoj komencitaj de la RIAA,
  procesoj kiuj, laŭ Valente, <i lang="lt">op. cit.</i>, p. 82, estis
  elektitaj ekzemplodone: «La usona leĝo, origine direktita al ĵuraj
  personoj, kaj ne fizikaj, antaŭdiris kompenson el 750 al 30 mil
  dolaroj, kreskante al 150 mil pro dolaj kondutoj, po verko kies
  aŭtorrajtoj estu maloebitaj. Tiuj agoj kaŭzis grandan publikan
  tumulton, ĉefe pro la grandaj valoroj, kiujn ili implicis, sed ankaŭ
  ĉar la identigo de uzantoj per IP-adreso kaŭzis erarojn kaj igis, ke
  RIAA komencis procesojn kontraŭ malĝustaj homoj, kiel estis la okazo de
  proceso kontraŭ morta homo kaj avino, kiu ne sciis elŝuti muzikon.
  La rezulto por la industrio de la enhavo estis antipatio kaj
  sekva malfacileco sin pozicii en la publika spaco».

En la sekvaj jaroj la malgrandiĝo de la ciferecaj aparatoj kaj sekve ĝia
preza malpliiĝo, malfermis eĉ pli da spaco en KD-ROM, DVD-j, FD-j kaj
poŝmemoriloj por konservado de dosieroj. La populariĝo de novaj
teknologioj de sendo de datumoj, kiel la jam nomita ADSL (kiu
popularigis la koncepton de *larĝbenda*[^195]), per kabla televido,
radioondoj kaj satelitoj, kaj poste la sistemoj 2, 3 kaj 4G ankaŭ por la
poŝtelefonoj, triobligis la rapidecon de la Interreto kaj malpliigis la
tempon de elŝuto kaj alŝuto de enhavoj, dum la aliro al la reto iĝis pli
malmultekosta kaj pli facila en la tuta planedo &mdash; precipe en la
monda nordo. En ĉi tiu situacio la batalo pro la libera kunhavado de
dosieroj iĝis neevitebla diskuto. La libera kulturo disvastiĝis kiel
simbolo de la libereco de aliro kaj disvastiĝo de informo kaj trovis
spacon por iĝi forta en la servoj de kunhavado de dosieroj kaj inter la
homoj, kiuj elŝutis enhavon (kun aŭ sen aŭtorrajto, multaj ne sciis aŭ
ne vidis diferencon) libere kaj volis konservi tiun praktikon. En tiu
epoko Lessig diris, ke «dum en la analoga mondo la vivo malhavas
aŭtorrajton, en la cifereca mondo la vivo estas ligita al la aŭtorrajta
leĝo»[^196], frazo kiu montras iun animon de tiuj jaroj, en kiuj la unua
politika kaj jura afero en la reto estis ĉirkaŭ la elŝuto: ĝia laŭleĝeco
aŭ ne, ĝia efiko en la konstruado de la scio, en la aliro al la informo,
en la produktĉeno de la artoj, en la daŭripovo de kulturaj projektoj, en
la neceso de reformo de la aŭtorrajtaj leĝoj, por ke ĉi tiuj ne plu
krimigu kutiman praktikon de milionoj da homoj.

[^195]: Larĝbenda estas koncepto utiligita por ĝenerale difini konektojn
  pli rapidajn ol tiuj ŝaltitaj per analogaj modemoj de kbps. La
  rekomendo de la Internacia Telekomunika Unio difinas larĝbendon kiel
  kapablecon elsendi pli grandan ol 2 aŭ 5 megabitoj dum sekundo. La
  vario de tiu, kiu estas konsiderita larĝbenda ĉirkaŭ la mondo estas
  tamen diversa; Kolombio estigis minimuman rapidecon de 1024 kbps kaj
  Usono de 25 mbps ekzemple. En Brazilo ankoraŭ ne estas konsento, kiu
  diras, kiu estas la minimuma rapideco, por ke konekto estu konsiderita
  larĝbenda. Fonto: <https://pt.wikipedia.org/wiki/Banda_larga>.
[^196]: Lessig, <cite>Code and Other Laws of Cyberspace</cite>, p. 192.

La grandaj perantoj jam cititaj, reprezentitaj de organizoj, kiuj havis
sufiĉe da mono por dungi diversajn advokatojn kaj iri ĝis la fino en ĉiu
ajn jura proceso, naskigis en la Ĵuro iujn idolojn de la libera kunhavado
en la reto, kiel okazis kun la retejo de *torrents* The Pirate Bay
(TPB). Premitaj de firmaoj ligitaj al Motion Pictures Association
(MPAA), prokuroroj svedaj, hejmlando de The Pirate Bay, prezentis
akuzojn je la 31-a de januaro de 2008 kontraŭ Fredrik Neij, Gottfrid
Svartholm kaj Peter Sunde, kiuj administris la retejon, kaj Carl
Lundström, sveda komercisto, kiu financis ekde la komenco TPB, por helpi
disponebligi enhavojn kun aŭtorrajtoj. Ili estis kondamnitaj je la 17 de
aprilo de 2009 per mallibereja puno de unu jaro kaj per pago de 2,7
milionoj da eŭroj al la firmaoj reprezentitaj de MPAA, kiel 20th
Century Fox, Columbia Pictures, Warner Bros, EMI, inter aliaj. La kazo
havis apelacion en 2010, kiu malpliigis la malliberejan tempon de ĉiuj
la akuzitoj (4 ĝis 10 monatoj). Post kelkaj jaroj forkurante ili
plenumis siajn punojn kaj ekde 2015 estas liberaj. La retejo, kiu kune
aldonis ligilojn, sed ne gastigis la enhavojn protektitajn per
pretenditaj aŭtorrajtoj, estas aktiva danke al diversaj speguloj[^197].

[^197]: Pri la kazo vidu la dokumentan filmon <cite>The Pirate Bay: Away
  from the Keyboard</cite>, direktitan de Simon Klose kaj eldonitan en
  2013, tute disponeblan en
  <https://www.youtube.com/watch?v=eTOKXCEwo_8>, kaj la paĝo en
  Vikipedio <https://en.wikipedia.org/wiki/The_Pirate_Bay_trial>.

En la 2000-a jardeko la organizoj ligitaj al la peranta industrio igis
ankaŭ kutimaj kontraŭpiratecajn kampanjojn, en kiuj ili insistis kompari
dosieron, kopieblan kaj ne rivalan, kun fizika havaĵo kiel KD aŭ DVD; ke
filmo elŝutita estis unu DVD malpli vendita kaj, tial, oni helpis
malsate «mortigi» artistojn[^198]; ke la pirateco «finis la emocion»,
ĉar la elŝutita dosiero ne havis la saman kvaliton kiel tio, kion oni
vidas en DVD aŭ en la kino[^199]; ke, «Kiam vi elŝutas MP3-dosierojn, vi
ankaŭ elŝutas komunismon», en bildo hodiaŭ historia, en kiu Lenin
vestita kun armea uniformo kaj diabla kapo aperas apud blanka junulo kun
aŭdilo kontraŭ komputilo. Estis aliaj similaj motoj en kampanjoj, sed
neniu ĉesigis la kunhavadon de dosieroj; retejo fermita estis kiel
mortigi kapon de Hidro de Lerno, alia kreskis en ĝia loko. Sed tamen tio
servis por kaŭzi la detruon de multaj kopioj, retejojn malfermitajn,
homojn procesitajn kaj, ĉefe, por montri al la industrio de la kultura
perantado &mdash; precipe kinaj kaj videaj studioj kaj distribuistoj por
televido, registristoj kaj distribuistoj de muziko kaj libraj
eldonistoj&mdash;, ke ne estus tiel, kiel ili finos la kunhavadon de
dosieroj.

[^198]: Ekzemplo disponeblas en
  <https://baixacultura.org/propagandas-antipirataria-3>.
[^199]: Moto de kampanjo de Honour Intellectual Property (HPI), kiu
  prezentis kiel heroon Superviron, la Feran Homon kaj aliajn savante la
  mondon kun la vortoj, en la angla, «<i lang="en">Piracy
  kill the real thrill</i>». Pli da detaloj en
  <https://baixacultura.org/propagandas-antipirataria-o-retorno-2>.

En la 2000-a jardeko ankaŭ disvastiĝis la ideo liberigi jam ekzistantajn
kulturajn kaj edukadajn havaĵojn por ilia uzo, kunhavado kaj
reproprigo. En la edukado, post 2002, la jam citita internacia komunumo
REA naskiĝis kun celo instigi la aliron, uzon kaj reuzon de edukadaj
havaĵoj. En la muzeoj, librejoj kaj memorinstitucioj estis simila movado
per la adopto de la permesiloj Creative Commons speciale kiel moto por igi
la kolektojn de tiuj institucioj pli alireblaj, konektitaj kaj
disponeblaj, por ke la uzantoj povu kontribui, partopreni kaj kunhavi
ilin[^200], en la movado nomita Open GLAMP (Gallery, Library, Archive,
Museum). Kun radikoj en la etikaj principoj de la libera programaro kaj
tiuj rekombinaj de la libera kulturo, la iniciatoj ambaŭ konkeris spacon
en diversaj institucioj kaj registaroj en diversaj lokoj el la planedo
kaj de la plej diversaj ideologioj. Ili estis, en 2020, post multe da
organizado, malvenkoj kaj lernado dum la vojo, la ejoj, kie oni pleje
leĝe trovas liberajn verkojn.

[^200]: En «Os 5 princípios do Open Glam», <cite>Creative Commons
  br</cite>, 24 sep. 2019. Disponebla en
  <https://br.creativecommons.org/os-5-principios-do-open-glam>.

### V.

La movado de la libera kunhavado de dosieroj en la reto krimigita kiel
pirateco nur komencis malgrandigi sian forton en la sekva jardeko, kiam
eniris du grandaj agantoj, kiuj kune ŝanĝis la Interreton en ion tre
malsaman ol tiu de la unuaj jaroj. Unue estis la servoj de
<i lang="en">streaming</i>, kiuj malkutimaj en la 2000-aj jaroj, iĝis
baza monata investo, kiel akvo kaj lumo, por milionoj da mezklasaj
familioj en diversaj lokoj de la mondo post la 2010-aj jaroj; kaj kiuj
havis ankaŭ, estas grave substreki tion, kiel helpon la konsiderindan
kreskon de la rapideco en la Reto en tiu periodo, kun fibrooptiko,
kiuj permesas rapidecon almenaŭ centfoje pli grandaj ol en la komenco de
la 2000-aj jaroj.

La sama industrio, kiu iniciatis kontraŭpiratecajn kampanjojn, sciis
aŭskulti peton faritan de kelkaj, el kiuj uzis la <i lang="en">torrents</i>
por havi aliron al diversaj mondaj kulturaj kreaĵoj: *faru pli bone kaj
mi pagos*[^201]. Ili kreis (aŭ alligis al) platformojn kun multe da
muziko, filmoj kaj serioj facile, malmultekoste disponeblaj en amika
interfaco, jam subtekstigitaj en multaj lingvoj (okazo de la filmoj kaj
serioj), kun algoritmoj pli kaj pli potencaj, kiuj lernis la ŝatojn de
la homoj kaj indikis aliajn produktojn, kiujn la abonanto povus voli pli
kaj pli precize. Ili funkciis eĉ en la diversaj aparatoj (poŝtelefonoj,
tabulkomputiloj), kiuj iĝis pli kaj pli malgrandaj, pli potencaj kaj
popularaj kaj tial atingis kaj gajni tiujn, por kiuj estis malfacile
elŝuti filmon (aŭ muzikaĵon) kaj legalizi la retan kulturan konsumon,
ĉar ĉio, kio estas en Netflix, en Spotify, en Amazon Prime kaj en
Deezer, kelkaj el la plej popularaj el tiuj servoj en 2020, estas
disponebligita ene de la leĝo[^202]. Ili ne ĉesigis la samtavolan
elŝuton, per <i lang="en">torrent</i>, sed igis tiun opcion pli peniga,
limigita al pli malgrandaj grupoj &mdash; en la komenco de la 2020-a
jarcento ankoraŭ nombro rimarkinda (kaj malfacile mezurebla) de homoj,
sed konsiderinde malpli granda ol en la antaŭaj jardekoj.

[^201]: Pri tiu ideo legu «Faça melhor que eu pago: desafio à
  indústria», <cite>Leo Germani</cite>, 10 jan. 2010. Disponebla en
  <https://web.archive.org/web/20210226091135/https://leogermani.com.br/2010/01/10/faca-melhor-que-eu-pago-desafio-a-industria/>.
[^202]: Deak; Foletto, <i lang="lt">op. cit.</i>

La dua aganto, kiu aperis, kaj malpliigis la movadon de la libera
kunhavado en la Interreto estis la sociaj retejoj, unue Orkut
(eldonjaro: 2004), poste MySpace (inter 2005 kaj 2008, la plej populara
retejo de la planedo) kaj fine, kaj en pli granda skalo, Facebook (100
milionoj da uzantoj en 2008, 2500 milionoj en 2020). Retumi en la
Interreto estis komuna frazo en la 1990-aj kaj 2000-aj jaroj por difini la
kutimo eniri en retejon kaj el tiu iri al alia, kaj al alia, kaj al
alia, ĝis perdiĝi, horojn poste, en paĝo, al kiu oni ne bone sciis kiel
alvenis. <i lang="fr">Flanêur digital</i> estis alia esprimo uzita por
identigi tiun promenanton sen direkto tra la Reto, kiu perdiĝis en la
anguloj de la blogoj kiel piediranto tra la stratoj de la grandaj urboj.
Facebook speciale ŝanĝis tiun movadon; alportis la tutan urbon, por ke
la promenanto piediru sen eliri el la loko. Urbo konstruita de unu sola
privata firmao, kiu per ĉiu movo farita de iliaj loĝantoj produktis
datumon, kiu rekombinita kun aliaj miloj iĝis tre profita por esti
komercigita de la firmao &mdash; la «nafto» de la 21-a jarcento, laŭ
esprimo, kiu iĝis kliŝo en la paroloj de regantoj kaj studantoj de la
estonteco apud alia kiu ankaŭ iĝis ĝenerala post la 2010-aj jaroj:
<i lang="en">big data</i>.

Paroli kun homoj, skribi, publikigi, foti, vidi videojn kaj labori,
aktivecoj kiuj antaŭ estis faritaj en malsamaj lokoj de la reto, komencis
povi esti faritaj en sola loko, Facebook &mdash; kiu poste, kun planoj
ĉiam pli ambiciaj krei paralelan Interreton en siaj domajnoj,
transformiĝis en du, per la aĉeto de Instagram (en 2012, kontraŭ 1000
milionoj da usonaj dolaroj), kaj en tri, per WhatsApp (en 2014, kontraŭ
16 000 milionoj da usonaj dolaroj[^203]). Kune kun aliaj de la nomitaj
kiel *big techs* (Google, Amazon, Apple kaj Microsoft), la firmao kreita
de Mark Zuckerberg ŝanĝis la manieron, kiel la homoj produktis kaj
konsumis informon en la Interreto. Ĝi komencis diri kie, kiel kaj per kiu
formo la informo moviĝu en la Reto &mdash; kaj ne plu estis la retejoj,
<i lang="en">torrents</i> kaj blogoj kreitaj por la libera kunhavado de
dosieroj, sed sola spaco fermita, rigardita, monopoligita, ilo de
*modulado* de opinioj kaj kondutoj laŭ la vojo proponita per la pli kaj
pli kompleksaj (kaj sekretaj) algoritmoj[^204].

[^203]: Fonto:
  <https://tecnoblog.net/151547/facebook-compra-whatsapp-16-bilhoes-de-dolares>
[^204]: Vidu Souza; Avelino; Amadeu; <cite>A sociedade de controle:
  manipulação e modulação nas redes digitais</cite>.

Tio estis la fino de la mallonga somero de la libera Interreto[^205] kaj
la komenco de ia *postebrio de Interreto*[^206], en kiu kritikoj al iuj
naivaj kondutoj adoptitaj en la du unuaj jardekoj de la reto iĝis
kutimaj &mdash; inter ĉi tiuj al la libera kulturo kaj specife al
rajtocedo. La hispana sociologo César Rendueles, en libro, kiu estas
entute analizo pri la kredo ciferecfetiĉisma, ke la Interreto solvos
ĉiujn niajn sociajn, ekonomikajn kaj politikajn problemojn
(<cite>Sociofobia</cite>, 2016), savas gravan aspekton en tiu postebria
kritiko: la libera disvastiĝo de informo kaj kunhavado de dosieroj ankaŭ
povas esti rigardita kiel kompleta senreguligo, proksima al tio, kio
okazas en la libera merkato &mdash; kiu estis la kialo de la kreado de
la aŭtorrajto kaj de la unua propono de Lessig pri libera kulturo. Ĝi
havas do rilato kun la universaligo de la kapitalisma merkato
disvolvita post la 19-a jarcento kaj disvastigas la «dogmon, ke la
socia kunlaboro naskiĝas spontanee el individua interago egoisma, sen
neceso de iu institucia mediacio»[^207].

[^205]: Ĉi tiu esprimo devenas el <i lang="en">remix</i> de <cite>O curto
  verão da anarquia</cite>, de Hans Magnus Enzensberger, adaptita de
  Paulo José Lara (vulgo Pajeh) el drinkeja konversacio en 2019 en São
  Paulo, kun la nomo de *iu projekto, kiu venos*.
[^206]: En 2018 mi resumis tiun ideon en teksto nomita «Ressaca da
  internet, espírito do tempo», skribita en BaixaCultura. Peco: «Mi ne
  sciis, aŭ mi ne volis kredi, aŭ mi ne volis skribi nek publike paroli,
  ke mi ne kredis, ke la grandaj agantoj de la Interreto ŝanĝis la
  Interreton en tion, kio hodiaŭ ĝi estas: fermita spaco, kie ni mem
  estas malliberaj en algoritmaj privataj vezikoj, kies funkciado
  malmulte aŭ neniel scias &mdash; kaj nur en unu jaron, per Trump kaj la
  Briteliro, ni komencas vidi la fatalajn eblecojn de tiu funkciado inter
  homoj kaj teknikaj sistemoj kiel Facebook por la politiko».
  Disponebla en <http://baixacultura.org/ressaca-da-internet-espirito-do-tempo>.
[^207]: Rendueles, <cite>Sociofobia: mudança política na era da utopia
  digital</cite>, p. 94.

La ciferecfetiĉisma kredo kritikita de Rendueles estis tre populara en
la unuaj jaroj de la Interreto kaj formis manieron pensi ankoraŭ hodiaŭ
dominantan en la teknologiaj novaĵoj kaj en la diskurso de la ciferecaj
<i lang="en">startups</i>. Konata teksto de tiu epoko montras tion
klare: «La deklaracio de sendependenco de la Retlando»[^208],
publikigita je la 8-a de februaro de 1996, skribita de John Perry Barlow
&mdash; unu el la kreintoj de EFF kaj instiganto de Creative Commons
&mdash; por respondi al ago, kiu reguligis la telekomunikadojn en Usono
kaj unue inkludis la Interreton[^209]. «Registaroj de la Industria
Mondo, pezecaj gigantoj de karno kaj ŝtalo, mi venas de Retlando, la
nova hejmo de Menso. En la nomo de la estonteco, mi petas al vi de la
pasinteco, lasu nin trankvilaj. Vi ne estas bonvenaj ĉe ni. Vi ne havas
suverenecon, kie ni kunvenas»[^210].

[^208]: Barlow, «Deklaracio de sendependenco de la Retlando» en la Monda
  Ekonomia Forumo, Davos, Svislando, 8-a feb. 1996. Disponebla en
  <https://eo.wikisource.org/wiki/Deklaracio_de_sendependenco_de_la_Retlando]>.
[^209]: Estinte unu el la kreintoj de la Electronic Frontier Foundation
  (EFF) en 1990, Barlow akompanis kaj skribis pri ekonomiaj, politikaj
  kaj teknologiaj aferoj de la Interreto en tiu epoko. La deklaracio
  estas teksto, kiu servis kiel bazo por alia rimarko nomita «La
  ekonomio de la ideoj», publikigita en januaro de 1994 en la revuo
  <cite>Wired</cite> (<https://www.wired.com/1994/03/economy-ideas>),
  kaj li estis invitita fari projekton nomitan 24 Hours in Cyberspace,
  evento, kiu havis kiel celon kunvenigi fotistojn, ĵurnalistojn,
  eldonistojn, programistojn kaj desegnistojn por krei, en la tago 8-a de
  februaro de 1996, kunlaboran «tempokapsulon» de la interreta vivo de la
  epoko. Fonto:
  <https://en.wikipedia.org/wiki/24_Hours_in_Cyberspace#cite_note-4>.
[^210]: <i lang="lt">Ibidem</i>.

Ĝiaj unuaj vortoj jam portas, kiel en manifesto, utopian kaj idealigitan
vidon, ke Interreto estas io ekstera al socio, esprimitan pli evidente
en alia tekstparto: «Ni formas nian propran socialan kontrakton. Tiu
rego leviĝos laŭ kondiĉoj de nia mondo, ne de via. Nia mondo estas
malsama. [...] Viaj juraj konceptoj de posedaĵo, esprimo, identeco,
movado, kaj kunteksto ne aplikeblas al ni. Ĉiuj ĉi bazas sur materio,
kaj ĉi tie ne ekzistas materio»[^211].

[^211]: <i lang="lt">Ibidem</i>.

Barlow, ankaŭ poeto kaj kantisto de unu el la grupoj plej konataj de la
hipia kontraŭkulturo de la Okcidenta Marbordo de Usono, Grateful Dead,
sciis interpreti la novaĵon, kiu la Interreto signifis en la historio de
la homaro kaj ĝojis pri la promeso, ke tiu libereco bonen transformu la
tutan socion.

> Ni kreas mondon, kien ĉiuj rajtas eniri sen privilegio aŭ antaŭjuĝo
> akordita per raso, ekonomia potenco, milita forto, aŭ naskiĝloko.
>
> Ni kreas mondon, kie iu ajn, ie ajn rajtas esprimi siajn kredojn,
> egale kiel neordinaraj, sen timo iĝi devigita al silentado aŭ
> konformeco.
>
> [...]
>
> Niaj identecoj ne havas korpojn, do, malkiel vi, ni ne povas atingi
> ordon per fizika devigado. Ni kredas, ke de etiko, de klerigita
> memintereso, kaj de la komuna boneco, nia registaro altiĝos. Niaj identecoj
> eblas disiĝataj trans multaj de viaj jurisdikcioj.[^212]

[^212]: <i lang="lt">Ibidem</i>

Alia tiama tekso, skribita por cifereca angla revuo nomita
<cite>Mute Magazine</cite>[^213] en 1994, iĝis konata analizante tiun
teknoutopian ideon: <cite>La kalifornia ideologio</cite>, de Richard
Barbrook kaj Andy Cameron. La ideologio mem estas miksaĵo de
kontraŭaŭtoritataj kaj bohemiaj sintenoj de la hipia kontraŭkulturo de
la Okcidenta Marbordo de Usono kaj de la teknologia utopiemo (alia nomo
por ciferecfetiĉismo) kaj la ekonomia (nov)liberalismo. Iom nekutima
miksaĵo &mdash; «kiu pensus, ke miksaĵo tiel kontraŭdira de teknologia
determinismo kaj libertarianisma individuismo iĝus la hibrida
ortodokseco de la cifereca epoko?»[^214] &mdash;, kiu formas la spiriton
de la <i lang="en">big techs</i> de la 1990-aj jaroj kaj poste kaj nutras
la komprenon, ke ĉiu povas esti «<i lang="en">hip and rich</i>» [moderna kaj riĉa]. Por tio
sufiĉis kredi en via laboro kaj havi fidon, ke la novaj informaj
teknologioj emancipos la homon pliigante la liberecon de ĉiuj kaj
malpliigante la povon de la burokrata ŝtato[^215].

[^213]: Barbrook; Cameron, The Californian Ideology. <cite>Mute</cite>,
v. 1, n. 3, 1-an sep. 1995. Disponebla en
<https://www.metamute.org/editorial/articles/californian-ideology>.
[^214]: Barbrook; Cameron, <cite>A ideologia californiana</cite>, p. 10.
[^215]: <i lang="lt">Ibidem</i>.

La vortoj de Barbrook kaj Cameron en 1995 estas, preskaŭ du jardekoj
poste, precizaj kaj antaŭvidaj:

> Unuflanke ĉi tiuj pintoteknologiaj metiistoj ne nur kutimas esti bone
> pagitaj, sed ili ankaŭ havas konsiderindan aŭtonomecon pri sia labora
> ritmo kaj laborejo. Kiel rezulto la kultura limo inter hipio kaj la
> «homo de organizo» iĝis tre malklara. Tamen aliflanke tiuj laboristoj
> estas ligitaj al la kondiĉoj de siaj kontraktoj kaj ne havas garantion
> de daŭra laboro. Sen la libera tempo de la hipioj la laboro iĝis la
> unua vojo de memkontenteco por granda parto de la «virtuala
> klaso».[^216]

[^216]: <i lang="lt">Ibidem</i>.

La kalifornia ideologio reflektas kaj la disciplinoj de la merkata
ekonomio kaj la liberecoj de la «hipia metiisto», hibrido kunigita per
la fido, kelkfoje blinda, ke la cifereca teknologio solvos la problemojn
kaj kreos egalecan socion, kaj ke sen privilegioj aŭ antaŭjuĝoj, kie, kiel
tre bone reprezentas «La deklaracio de sendependenco de la Retlando», de
Barlow, ĉiuj povu esprimi siajn opiniojn sen gravi, kiel neordinaraj kaj
malsamaj ili estu.

Per la pliboniĝo de la elsendfluo kaj de la sociaj retejoj oni vidis pli
klare, ke socio, kie la teknologioj de la informo enrete konektitaj
ĉiujn decidas, ne necese estas pli bona, kaj povas esti multe pli
malbona. Forta algoritma sistemo, kiu, kiel
<i lang="lt">deus ex machina</i>, estu alvokita por solvi ĉion fine
defendas kredon ankaŭ konatan kiel tendencon al teknologiaj solvoj &mdash;
ideo, ke sufiĉas programo, algoritmo, *pli da teknologio*, por solvi kaj
ripari ĉiujn la problemojn de la mondo. Estas la serĉo de magia, rapida
kaj supozeble sendolora eliro, kiu forĵetas la instituciajn
alternativojn aŭ tiujn kreitajn de la organizado de la civila socio, pli
malrapidajn kaj kompleksajn, kaj kiu povas esti aĉetita finita,
proponita de firmaoj kreitaj aŭ iel konektitaj al la servoj provizitaj
de la <i lang="en">big techs</i>. Vojo, kiu dum la pandemio de la nova
kronviruso en 2020 iris tra specon de akcela rapidega tunelo, kun la
disvastiĝo de programoj, kiuj taksis ekzemple la movojn de la homoj en
kvaranteno, aŭ spuris kaj elektis, kiu povus aŭ ne eliri el hejmo uzante
kolektitan kaj per privataj algoritmoj pritraktitan datumaron[^217]. La
samaj datumoj uzitaj por io konsiderita kiel pozitiva, ĉar trafas la
sanon de la tuta socio &mdash; la kontrolon de la movado de homoj, kiuj
povas transdoni viruson &mdash;, povas ankaŭ esti uzitaj por eĉ pli
granda entrudiĝo de reklamoj de tajloritaj produktoj. Kio generas eĉ pli
da klasifiko &mdash; kaj sekva ekskludo &mdash; de homoj laŭ iliaj
konsumaj kutimoj en Interreto kaj akcelas la enretan gvatadon de ĉiuj la
kutimoj de homo en la reto.

[^217]: Raporto de Sam Biddle, «Coronavírus traz novos riscos de abus de
  vigilância digital sobre a população». <cite>The Intercept</cite>,
  6-an apr. 2020, disponebla en
  <https://theintercept.com/2020/04/06/coronavirus-covid-19-vigilancia-privacidade>,
  diras, ke en Sud-koreio, en Tajvano kaj en Israelo, la aŭtoritatoj
  uzis poziciajn datumojn de saĝtelefono por devigi individuajn
  kvarantenojn. Palantir, firmao dungita de la NSA de Usono, helpis la
  Nacian Sanservon de Britio spuri infektojn. Programoj, kiuj profitas el
  la granda precizeco de la sensiloj estantaj en la saĝtelefonoj por
  devigi socian malproksimiĝon aŭ mapumi la movojn de la infektitaj,
  estis uzitaj en Singapuro, Pollando kaj Kenjo. En Brazilo, unu el la
  ĉefaj provizantoj de retkonekto, Vivo, diras, ke ĝi malspurebligas la
  poziciajn datumojn de siaj uzantoj, sed praktike eblas lokalizi ilin,
  kio malobservas la individuajn rajtojn influante la privatecon de ĉiu
  homo sen permeso nek scio, kiel montras raporto de Tatiana Dias en
  <cite>The Intercept Brasil</cite>: «Vigiar e lucrar»
  (<https://theintercept.com/2020/04/13/vivo-venda-localizacao-anonima>).
  Tiuj samaj poziciaj datumoj estis donitaj al firmaoj kaj registaroj
  por la monitorado de la pandemio en Brazilo.

Krom riski eĉ pli la privatecon de la uzantoj, teknologiaj finitaj
solvoj, produktitaj de privataj firmaoj kaj aĉetitaj kiel savantaj fare de
registaroj, ne tuŝas tion, kio estiĝis kiel la centra institucio de la
moderna vivo: la merkato[^218]. La kritiko de Rendueles al la aŭtorrajto
estas ankaŭ en la fakto, ke la ĉefa solvota problemo per ĝi estas la
rompo de la baroj de libera disvastiĝo de la informo kaj la aliro al
kulturaj havaĵoj sen, tamen kaj plejfoje, tuŝi la sociajn kondiĉojn de
tiu merkato. La maniero kiel la jam citita viva energio de la homoj
komencas esti ekspluatita de firmaoj, kiuj ne traktas ilin plu kiel
laborantojn, sed kiel kontribuantojn, interŝanĝante laborajn rajtojn
historie konkeritajn kontraŭ supoza libereco elekti viajn laborajn
horojn, emis esti, dum granda parto de la ekzisto de la aŭtorrajto ĝis
hodiaŭ, malĉefa problemo. La fonto de la problemoj elektita ne estis la
informa merkato nek la labora merkato, sed la limoj al la disvastiĝo kaj
la uzo de la informo[^219]. Alia aspekto de la kritiko al la rajtocedo
estas, ke pli da aliro al la informo aŭ pli da verkoj elŝutitaj ne
necese signifas kritikan konsciencon. La misinformado kaj la kreskado de
merkato de malveraj informoj (<i lang="en">fake news</i>) estis, tiel
kiel la disvastiĝo de la cifereca aktivismo[^220], unu el la rezultoj de
la «liberigo de la informosendanta loko», por ke ĉiu ajn &mdash; kun
aliro al Interreto &mdash; povu paroli en blogo, retejo aŭ profilo en
sociaj retejoj. Rezulto, kiu estas direkta frukto, ke la merkato
(speciale de programaroj kaj teknologiaj produktoj) restu netuŝebla, sen
ŝtata reguligo aŭ ekstera ekvilibro, kio en la lastaj jaroj estigis
sekvojn kiel la disvastiĝon de la malveraj informoj kaj la uzon de tiuj en
la marionetado de homaroj por politikaj-balotadaj celoj, okazo de la
balotoj kaj de Donald Trump en Usono en 2016 kaj de Jair Bolsonaro en
Brazilo en 2018.

[^218]: Morozov, Solucionismo, nova aposta das elites globais,
  <cite>Outras Palavras</cite>, 23-an apr. 2020. Disponebla en
  <https://outraspalavras.net/tecnologiaemdisputa/solucionismo-nova-aposta-das-elites-globais>.
[^219]: Rendueles, <i lang="lt">op. cit.</i>, p. 87.
[^220]: Cifereca aktivismo estas difino, kiu, popularigita en la 90-aj
  kaj 2000-aj jaroj, rilatas kun la respondaj movadoj, kiuj «pliigis la
  publikan konscion pri la influo de la amaskomunikiloj kaj instigis la
  postulojn por demokratiiĝo kaj publika aliro al la komunikiloj», kiel
  diras la esploristino Stepania Milan, «When Algorithms Shape
  Collective Action: Social Media and th Dynamics of Cloud Protesting»,
  en <cite>Social Media + Society</cite>. En kelkaj okazoj ĝi estas
  uzita kiel sinonimo de alternativaj komunikiloj kaj civitanaj
  komunikiloj, inter aliaj terminoj. Pri la eblaj difinoj de cifereca
  aktivismo mi skribis tekston (Foletto, «Midiativismo, mídia
  alternativa, radical, livre, tática: um inventário de conceitos
  semelhantes»).

La rompiĝo de ĉiuj eniraj baroj de aliro al la parolo en la reto nutris
&mdash; kaj estis nutrita &mdash; per tio, kion sciencisto kaj verkisto
Jaron Lanier nomas <i lang="en">Bummer</i> (<i lang="en">Behaviors of
User Modified Made into an Empire for Rent</i>[^221]),
statistika maŝino (estanta en la sociaj retejoj kaj en la algoritmoj de
elsendfluo ekzemple), kiu vivas en la komputilaj nuboj kaj sub la
preteksto organizi la informon de la mondo, modifis la kondutojn de
miloj da homoj. La <i lang="en">Bummer</i>, diras Lanier, klopodas
«optimumigi la vivon», kaj farante tion egaligas ĉiajn informojn: tio,
kio gravas, estas la disvastiĝo de datumoj, estu ili kio ajn. Estas en
tiu kunteksto, kie la disvastiĝo de la malveraj informoj iĝas kutima
akirante pli interŝanĝvaloron ol la veraj, estante pli malmultekostaj
produkti kaj eble pli facile disvastigi, direktitaj laŭ la interesoj de
specifaj grupoj por plifortigi iliajn antaŭajn perspektivojn pri la
realeco[^222]. En tiu okazo «pli proksima al reakcia koŝmaro ol al
komunumeco», kiel diras Rendueles[^223], ne hazarde estis diskutitaj la
manieroj ŝtate reguli la <i lang="en">big techs</i>, speciale en la
Eŭropa Unio kaj en Usono, uzante leĝojn de protekto de personaj datumoj,
proponojn de moderigo de la disvastiĝo de malveraj informoj en la
sociaj retejoj kaj taksadon al la profitoj de la
<i lang="en">big techs</i>[^224]. Estas kurioze rimarki, ke tiuj
proponoj de reguligo jam estis, inter aliaj lokoj, en diversaj
pecoj de la jam citita eseo de Barbrook kaj Cameron, de 1995, kiu
indikas ciferecan estoncon kiel miksaĵon de «Ŝtata interveno,
kapitalisma entreprenado kaj faru-mem-kulturo»[^225].

[^221]: Lanier ĝin difinas en <cite>Dez argumentos para você deletar
  agora suas redes sociais</cite>, libro kiu proponas ferocan kritikon
  al la sociaj retejoj, la ĉefaj ekzemploj de agado de tiu statistika
  maŝino. En la p. 44 li uzas kuriozan formulon por memori la erojn de
  <i lang="en">Bummer</i>, disvolvante ĉiun en la sekvaj paĝoj de lia
  libro: «A de akiro de atento, kiu levas al aĉula supereco; B de
  enmiksiĝi [<i lang="en">butt into</i>] en la vivo de ĉiuj; C de premi [<i lang="en">cramming</i>]
  enhavon en la gorĝon de la homoj suben; D de direkti la konduton de la
  homoj per la maniero plej ruza kiel eblas; E de gajni
  [<i lang="en">earning</i>] monon permesante, ke la plej malbonaj aĉuloj
  ĉagrenu ĉiujn aliajn; F de falsa grego kaj falsanta socio».
[^222]: La analizo de la valoro de la vera ĵurnalisma informo kaj la
  disvastiĝo de malveraj novaĵoj per la oligopoloj de la ciferecaj
  teknologioj estas unu el la fokusoj, kiun la profesoro kaj esploristo
  Elias Machado laboris por sia libera instrua disertacio en la UFSC. Li
  ĝin publike diskutis en siaj propraj sociaj retejoj kaj antaŭe diris
  ĝin al mi, en iuj virtualaj konversacioj, el kiuj mi vortigis tiun
  pecon.
[^223]: Rendueles, <i lang="lt">op. cit.</i>, p. 102.
[^224]: Pri impostado: en 2020 Eŭropo estigis impostan pakon, kiu havis
  kiel instrumentojn garantii, ke la landoj de la bloko interŝanĝu
  informon pri la enspezoj generitaj de vendoj en retaj platformoj, kiu
  estos efektivigita en la sekvaj jaroj. Fonto:
  <https://www1.folha.uol.com.br/mercado/2020/07/europa-lanca-pacote-tributario-para-apertar-cerco-a-gigantes-digitais.shtml>.
[^225]: Barbrook; Cameron, <i lang="lt">op. cit.</i>, p. 37.

### VI.

La elekto de la movado de la libera kulturo trakti la baron al la
aliro al la informo kaj al la scio kiel ĝia ĉefa afero havas kelkajn
motivojn, iujn jam prezentitaj ĉi tie, granda parto de tiuj rilata al
la afero de la fako de deveno de ĝia termino, la produktado de programoj. Por
Rendueles, kaj ankaŭ Dimitry Kleiner en <cite>The Telekommunist
Manifesto</cite> (2010), la rajtocedo fiaskis ne kompreninte la
implicitajn diferencojn inter la programaro kaj la libera kulturo. En la
unua la sociaj kondiĉoj de pago de la programistoj de programoj ekzemple
ne kutimas esti dependaj de la vendo po unuo de produkto (programo), sed
de daŭra servo, disvolviĝo, agordigo kaj prizorgado, inter aliaj
manieroj pli kompleksaj, kiuj implicas la vendon ligitan al aliaj
produktoj. Estas praktiko en la fako de programado de programoj liberigi
kodon kaj vendi servojn sur ĝi ankaŭ, ĉar tiu procedo, krom aparteni al
maniero kunlabora kaj fragmentigita, per kiu programo estas produktita
ekde ĝia komenco[^226], ne malhelpas la rekompencon de iliaj
programintoj. Ne nur eblas liberigi kodon kaj vendi servojn sur ĝi
paralele, sed ankaŭ ekzistas merkato de malfermitaj teknologioj,
inspirita en la ideo de la malfermitkoda movado, en kiu unu el la ĉefaj
agantoj estas Microsoft, historia oponanto al la libera
programaro[^227].

[^226]: «La programado de programoj povas kaj devas esti fragmentigita.
  Estas granda mito pri sendependaj programistoj laborantaj en iliaj
  garaĝoj nokte, sed la vero estas, ke la divido de la projekto en
  problemaro kolektive solvota en speco de muntĉeno ne estas opcio, sed
  estas teknika neceso». Rendueles, <i lang="lt">op. cit.</i>, p.  90.
[^227]: Teksto de la Linux Foundation klarigas kiel funkcias la
  malfermitkoda laboro de Microsoft: Baker,
  The Open Source Programa at Microsoft: How Open Source Thrives,
  <cite>The Linux Foundation</cite>, 2-an mar. 2018, disponebla en
<https://www.linuxfoundation.org/blog/2018/03/open-source-program-microsoft-open-source-thrives>.

En tiu aspekto la situacio de la kulturo estas iom malsama. Laboristoj
de la branĉo, kiel muzikistoj, plejparte nesalajritaj aŭtonomoj (malsame
ol la programistoj de programoj), havas kiel ĉefan fonton de enspezoj la
apartan ludadon de siaj verkoj tujelsende (<i lang="en">shows</i>) kaj
po elcenton por verko vendita[^228]. La liberigo de muzikaĵo kaj la vendo
de servoj sur ĝi, tiel kiel per la programoj, eblas ankoraŭ &mdash; kaj
praktiko de multaj artistoj, estu pro etikaj principoj aŭ, ĉefe, kiel
«logaĵa» maniero por vendi <i lang="en">shows</i>. Sed, ne estante
salajritaj kiel la programistoj de programoj, estas pli malfacile, ke
ekzistu arigita servo ofertota, kiu kompensu sian koston. Por Rendueles
kaj Kleiner liberigi senpage la informon uzitan por la produktado de
programo &mdash; aŭ kodo &mdash; ne ŝanĝas la rekompencon (en la
plejparto de la okazoj jam garantiitan) de iliaj produktantoj, dum
tute kaj senkoste disponebligi muzikan informaĵon &mdash; muzikaĵon aŭ
diskon &mdash; modifus la gajnojn de sendependa muzikisto. Ĉi tiu
argumento, diras Rendueles, estis unu el tiuj, kiuj limigis la
disvastiĝon de la liberaj permesiloj bazitaj sur la rajtocedo por la
kultura branĉo.

[^228]: Rendueles, <i lang="lt">op. cit.</i>, p. 86.

Estas ankaŭ en tiu situacio grava escepto: licenci kulturan verkon por
modifado kaj ankaŭ por komercaj uzoj, kiel la rajtocedo proponas ĉefe
por la programaro, povas iĝi, praktike, malmulte racia por muzikistoj
kaj aliaj sendependaj artistoj. Kiom ajn da kreemeco, kiu estu en la
skribado de kodlinioj, ĝi estas komandaro plenumota per maŝino,
intelekta produkto, kiu havas specifan funkcion, kiu dependas de alia
aĵo por esti farita. Ne bezonas diri, ke artverko kiel muzikaĵo aŭ filmo
havas kiel celon estetikan aŭ distran aprezon, kiu ne kutimas esti nur
funkcia. Programo kaj kultura verko estas aĵoj de malsamaj naturoj,
produktitaj per manieroj kaj por celoj malsamaj, kio signifus, ke ankaŭ
iliaj produktantoj ne devus esti traktitaj same.

Stallman mem komentas la aferon: «por la noveloj, kaj precipe por
verkoj uzataj kiel distro, la teksta nekomerca redistribuo povus esti
sufiĉa libereco por la legantoj»[^229]. La proponanto de la rajtocedo
argumentas, ke tia verko, kiel ankaŭ laboroj, kiuj informas pri la
pensoj de homo (memoraĵoj, opiniaj kaj sciencaj artikoloj), devus havi
limigitajn eblecojn de uzo, ĉar ili estas malsamaj ol verkoj, kiujn li
kategoriigas kiel «funkciajn», al kiuj apartenas receptoj, edukadaj
verkoj kaj la programoj. Li do defendas, ke, en la okazoj de
verkoj estetikaj kaj kiuj informas pri la penso de iu, havi liberecon
fari kopiojn ja sufiĉus, por ke ĉiu ajn povus kunhavigi kiel kaj kie ri
ŝatu, malpermesante la komercan uzon kaj iujn eblecojn de modifo de la
verko, kiuj povu modifi aŭ misprezenti la vidmanieron proponitan de ĝia
aŭtoro. Tiu perspektivo de Stallman prezentas la rajtocedon kiel ideon,
kiu ne volas detrui la aŭtorrajton, sed reformi ĝin, inkluzive kun
kelkaj argumentoj, kiuj reprenas denove ĝian komencon en la 18-a
jarcento &mdash; okazo de la propono, defendita de la kreanto de la
libera programaro en la sama teksto, ŝanĝi al dek jaroj la templiman
daŭron de la aŭtorrajto[^230]. Per tiu kompreno la aŭtoroj havus teorie
manierojn garantii, ke iliaj ideoj ne estu misprezentitaj kaj ke iliaj
gajnoj ne estu multe tuŝitaj.

[^229]: En «Malinterpretar el copyright: una sucesión de errores»,
  Stallman, <cite>Software libre para una sociedad libre</cite>, p. 119.
[^230]: <i lang="lt">Ibidem</i>. Kaj kiel Aracele Torres detalas: «La
  defendo por redukti la monopolon pri la kopio al dekjara templimo
  estas, ke tiu redukto havus malmulte da influo pri la hodiaŭa
  publikigo de verkoj, ĉar li konsideras tiun tempon sufiĉa, por ke
  sukcesa verko estu profitdona. Krome, laŭ li, la verkoj ĝenerale
  kutimas esti for de katalogo multe antaŭ tiu templimo»
  (Torres, <i lang="lt">op. cit.</i>, p. 168).

Alia maniero, pli praktika, ekvilibrigi la ekvacion rekompencado de
la aŭtoroj kontraŭ respekto al la «originaj» formoj de la ideoj
kontraŭ publika aliro al la kreaj havaĵoj de la homaro estas la vido,
kiun Kleiner[^231] prezentas per la koncepto de
<i lang="en">copyfarleft</i>[^232]-permesiloj, kiuj havas regulon por la uzo en la
kolektiva produktado kaj alia por la uzo de tiu, kiu uzu dungitan laboron en
sia produktado. Al la laboristoj ekzemple estus permesata la uzo,
inkluzive komerca, de la kultura verko, sed ne al tiuj, kiuj ekspluatu
la dungitan laboron, kiuj estus devigitaj negoci la aliron[^233]. Laŭ
ĝia propono «eblus konservi komunan stokon da kulturaj havaĵoj
disponeblan por sendependaj produktantoj de la grandaj industrioj de
perantado jam nomitaj, sed samtempe malhelpi ĝian eksproprietigon fare de
privataj agentoj»[^234].

[^231]: Kleiner, <cite>The Telekommunist Manifesto</cite>.
[^232]: Ĝisdatigita versio de la sama ideo estis nomita
<i lang="en">copyfair</i> kaj estis difinita de Michel Bauwens,
fondinto de la P2P Foundation, kiel «principo, kiu klopodas reenkonduki
reciprokecajn postulojn en la merkataj agoj». Ĝi faras tion konservante
la rajton kunhavigi scion sen limoj, sed ĝi klopodas submeti la
komercadon de tiuj komunaj varoj al ia kontribuo por tiuj komunaj havaĵoj
(<https://wiki.p2pfoundation.net/Copyfair>). Unu el la permesiloj kreitaj
kiel ekzemplo de tiu koncepto kaj kun la samaj limigoj de vendo kiel la
<i lang="en">copyfarleft</i> estas la Peer Production License
(<https://wiki.p2pfoundation.net/Peer_Production_License>), uzata en la
branĉo de la komunaĵoj &mdash; en Brazilo ĝi estas tiu uzita en la
projekto Biblioteca do Comum (<https://web.archive.org/web/20220405230116/http://www.bibliotecadocomum.org/>).
[^233]: Foletto; Martins, Luna, Encontro on-line cultura livre do sul: a
  produção cultural comunitária para a construção do comum,
  <cite>Contratexto</cite>, p. 114.
[^234]: <i lang="lt">Ibidem</i>.

Komuna stoko da kulturaj havaĵoj. Publika havaĵo. Ĉi tie ni alvenas al
pli granda diskutkampo, en kiu, ekde la mezo de la 2000-aj jaroj, la
libera kulturo multe disvastiĝis: la komuno (<i lang="es">procomún</i>
en la hispana; <i lang="en">commons</i> en la angla[^235]).
Vasta koncepto de longa historia tradicio, kiu direktas al la
grekaj[^236], la komuno, historie, difinis kaj resursaron (arbarojn,
akvon, aeron, kampojn) kaj aferojn (ilon, maŝinon) kiel socian produkton
kaj praktikon. En <cite>O comum entre nós</cite>, Rodrigo Savazoni uzas
la vortojn de Massimo De Angelis,
«<i lang="en">there is no commons without commoning</i>» [ne estas
komuno sen komunigo][^237], kaj
tiujn de la brazila esploristo Miguel Said Vieira por difini la komunon
kiel «substantivon» (la aro de kunhavigitaj havaĵoj) kaj «verbon» (la
ago kunhavigi; <i lang="en">commoning</i>, «komunigi»)[^238].

[^235]: Savazoni, <i lang="lt">op. cit.</i>, p. 29-30.
[^236]: Savazoni, <i lang="lt">op. cit.</i>, p. 45.
[^237]: De Angelis, Introduction. <cite>The Commoner</cite>, n. 11, p.
  1.
[^238]: Savazoni, <i lang="lt">op. cit.</i>, p. 39.

Estas multaj la esploristoj, kiuj laboras pri la ideo de komuno.
Komencante per «La tragedio de la komunaĵoj», publikigita en 1968 de la
ekologo Garret Hardin, kiu analizante la komunan uzon de pâstejo de
diversaj gregoj, argumentas, ke komuna administrado de tiu kaj aliaj
liberaj komunaĵoj levus al ĝia detruo &mdash; la solvo estus la
privatigo aŭ la ŝtatigo. Kelkaj jardekoj poste Elinor Ostrom alfrontas
tiun ideon kaj, per metodaj esploroj de la modeloj de memstara
administrado de komunaj havaĵoj kiel alternativo al la privata
aŭ ekskluzive ŝtata administrado de la naturaj havaĵoj, gajnis la
Nobel-premion de Ekonomiaj Sciencoj en 2009[^239]. Ankaŭ post la jaroj
1990-aj kaj 2000-aj la komuno (re)alproksimiĝas al la politika lukto de la
fino de la 19-a jarcento, speciale en la maldekstro de aŭtonoma origino
kaj post verkoj (kiel <cite>Homamaso</cite>) de Michael Hardt kaj Antonio
Negri, kiu disvolvis koncepton de komuno «kiel rezulton de biopolitika
praktiko de la homamaso, kiu konstituas sin kiel “malfermitan kaj
ekspansiantan” reton, multoblan kaj misforman, larĝan kaj multnombran, kiu agas, por ke
ni povu “labori kaj vivi komune”»[^240]. Estas ankaŭ la verko de Sílvia
Federici, kiu, precipe en <cite>Caliban and the Witch</cite> [Kalibano kaj la sorĉistino] (2004)
kaj <cite>Revolución en punto cero</cite> (2013), elvokas la gravecon de
la ina laboro por konservi la komunaĵojn: «La inoj estis antaŭe en la
lukto kontraŭ la fermigoj, kaj en Anglio kaj en la “Nova Mondo”, kaj
estis la defendantoj plej energiaj de la komunaj kulturoj, kiujn la
eŭropa koloniigo klopodis detrui»[^241].

[^239]: Poste ŝi kreas la Internacian Asocion por la Studado de la komuno
  (IASC), kiu hodiaŭ kunigas esploristojn kaj aktivulojn de la komuno en
  malsamaj landoj. Retejo: <https://iasc-commons.org>.
[^240]: Savazoni, <cite>O comum entre nós: da cultura digital à
  democracia do século XXI</cite>, p. 46.
[^241]: En Federici, <cite>Revolución en punto cero</cite>, p. 248.
  Ĝis nun estas inaj grupoj tiuj, kiuj konservas la komunan proprieton
  de la kolektivaj vivmanieroj en la montoj de Peruo kaj de afrikaj
  farmistinoj de vivtenado (kiuj, laŭ Federici, produktas 80% de la
  manĝaĵoj, kiujn la loĝantaro de la kontinento konsumas), citante du
  ekzemplojn. La laboro de la historiistino proksimigas la feminismajn
  studojn de la komuno kaj metas la materiajn reproduktadajn ilojn
  kiel ĉefajn mekanismojn por «komunigi».

La komuno komencas rilati kun pli da ofteco kun havaĵoj kiel programoj,
scio kaj la dosieroj de kulturaj havaĵoj en la reto, kaj ankaŭ kun la
sendependaj manieroj administri tiujn havaĵoj de la komunumoj, en la
mezo de la 2000-a jardeko, danke al la disvastiĝo de la ciferecaj teknologioj
kaj de Interreto. Transformate la programojn en scion de komuna uzo,
produktado kaj admnistrado la rajtocedo igis la liberan programaron
intelekta komunaĵo, diras Benkler en <cite>The Wealth of Networks</cite>
(2006), unu el la unuaj, kiuj adresis la komunon de la ciferecaj
teknologioj en reto. La intelektaj komunaĵoj estus bazitaj en la
cifereca informo disvastigita en Interreto, havaĵoj ne rivalaj, kiuj,
konsumitaj aŭ uzitaj de unu homo, ne iĝas nedisponeblaj por esti
konsumitaj aŭ uzitaj de aliuloj[^242]. Pro tiu trajto ili kreus novan
kunlaboran kaj sur la komunaj kavaĵoj bazitan manieron produkti la scion
(<i lang="en">commons-based peer production</i>, CBPP), kiu, laŭ la
opinio de la aŭtoro, kreus novan ekonomion pli demokratian kaj
distribuan ol tiu de la industria periodo.

[^242]: Torres, <i lang="lt">op. cit.</i>, p. 137.

La ellaborado de Benkler estis uzita en la sekvaj jaroj de kelkaj
«ekonomistoj de la komuno», inter ili Michel Bauwens, kreinto de la P2P
Foundation, kiu «indikas, ke la ekonomio de la paroj naskigas trian
manieron de produktado, de kunregado kaj de proprieto, kiu sekvas la
proverbon “de ĉiu laŭ riaj kapabloj, al ĉiu laŭ riaj bezonoj”»[^243]. La
libera kulturo tiusence estus la reprezentanto de la kulturaj havaĵoj de
tiu tria produktadmaniero (la aliaj du estus, ĝenerale, la kapitalismo
kaj la socialismo), kiu «reorganizus la produktadan sistemon ĉirkaŭ la
prizorgadon kaj la solidarecon, ĉirkaŭ la egalan interŝanĝon inter paroj kaj
bazitan sur la ago de civitanaj entreprenistoj kies fina celo ne estas la
maksimumigo de la profitoj, sed la plibonigo de la sociaj kondiĉoj de
ĉiuj»[^244].

[^243]: Savazoni, <i lang="lt">op. cit.</i>, p. 49.
[^244]: <i lang="lt">Ibidem</i>, p. 49.

