Ĉi tiu libro estis skribita, en ĝia fina skribado, inter septembro de
2019 kaj septembro de 2020, periodo de la komenco de la nova kronvirusa
pandemio kaj de kvaranteno, kiu daŭris multe pli ol 40 tagoj. Multaj
homoj permesis kaj helpis al mi skribi ĝin. Estus malfacile citi ĉiujn;
iuj el tiuj estas en la referencoj kaj en la piednotoj.

Mi povas danki al aliaj ĉi tie. La interŝanĝoj de ideoj kun Elias
Machado kaj Leonardo Retamoso Palma estis gravaj por multaj de la
diskutoj de ĉi tiu verko. En malsamaj momentoj kaj per malsamaj manieroj
estis interparolantoj, instigantoj kaj kunlaborantoj de la ideoj ĉi tien
alportitaj en la lastaj jaroj Rodrigo Savazoni, Mariana Valente, Felipe
Fonseca, André Deak, Pedro Markun, Evelyn Gomes, Lívia Ascava, Sheila
Uberti, Janaína Spode, Carolina Dalla Chiesa, Aline Bueno, Fabrício
Solagna, Leonardo Roat, Augusto Paim, Luís Eduardo Tavares, Aracele
Torres, Guilherme Flynn, Pablo Ortellado, Sérgio Amadeu, Eduardo
Viveiros de Castro, Marcelo Träsel, Rubens Velloso, Gustavo Torrezan,
Sávio Lima Lopes, Reuben da Cunha Rocha, Edson Andrade, Victor
Wolfenbüttel, William Araújo, Pedro Jatobá, Rodrigo Troian, Joel
Grigolo, Iuri Martins, Thiago Almeida, Douglas Freitas, Márcia, Veiga,
Angelo Kirst Adami kaj Tatiana Dias (kiu faris multvalorajn sugestiojn en
la teksta fina fazo). Mi dankas ankaŭ al Daniel Santini kaj Cauê
Seignermartin Ameni, pro la subteno kaj la fido al la projekto; kaj al
Beatriz Martins, Carlos Lunna, Jorge Gemetto, Mariana Fossati, Dani
Cottilas, Barbi Couto kaj al la reto Cultura Livre do Sul, kiu estis kiel
esplorejo de multaj vortoj ĉi tie skribitaj, kaj ankaŭ al la Rede das
Produtoras Culturais Colaborativas, ambaŭ kolektivoj kiuj praktikas iujn
el la manieroj kompreni la liberan kulturon.

