<figure>
    ![Leonardo Feltrin Foletto](bildoj/Leonardo-Feltrin-Foletto.png)
    <figcaption>Foto: Sheila Uberti</figcaption>
</figure>

Leonardo Feltrin Foletto naskiĝis en Taquari, interno de Rio Grande do
Sul. Trejnita kiel ĵurnalisto en la UFSM, en Santa Maria (Rio Grande do
Sul), li iĝis majstro de Ĵurnalismo en la UFSC kaj doktoriĝis en
Komunikado en la UFRGS, kun esploroj pri komunikado, teknologio kaj
aktivismo. Kiel redaktanta ĵurnalisto li estis en <cite>A Razão</cite>,
de Santa Maria, kaj en la <cite>Folha de S.Paulo</cite> (ilustrita).
Estis vizitanta profesoro en kelkaj universitatoj (PUC-RS, UCS,
Unisinos, PUC-SP kaj Unochapecó) kaj skribis <cite>Efêmero revisitado:
conversas sobre teatro e cultura digital</cite>, per stipendio de la
Fundação Nacional das Artes (Funarte) &mdash; nacia arta fondaĵo &mdash;
en 2011. Ekde 2007 li laboras pri cifereca komunikado, libera kulturo kaj
teknopolitiko en Brazilo kaj Iberameriko en projektoj kiel Casa da
Cultura Digital (São Paulo kaj Porto Alegre), Ônibus Hacker, Festival
BaixoCentro, Fórum Internacional do Software Livre (FISL), Rede de
Produtoras Culturais Colaborativas, kodumulejo Matehackers, Labhacker,
Creative Commons Brasil kaj LabCidade (FAU-USP), inter aliaj. Aldone
al *BaixaCultura* ([baixacultura.org](https://baixacultura.org/)),
retejo pri libera kulturo kaj cifereca (kontraŭ)kulturo aktiva ekde
2008, esplorejo kaj etendaĵo de multaj el la ideoj ĉi tie prezentitaj.
