La vorto «kulturo» havas tiom da signifoj dum la historio, ke ni
komencos serĉante difinon por povi aldoni al ĝi la vorton *liberan*, kiu estas
en la nomo de la libro. La unua ĉapitro de la libro <cite>Micropolíticas:
cartografias do desejo</cite> (1984), de Felix Guattari kaj Suely
Rolnik, havas la titolon «Kulturo: revoluciema koncepto?», teksto, kiu
provizas malsamajn signifojn de libera kulturo, kiuj povas helpi nin: la
*senco A* estas difinita kiel *kulturo-valoro* kaj konformas al
valorjuĝo, kiu determinas tiujn, kiu havas kaj kiu ne havas kulturon.
Oni manifestas ĝin ekzemple en iuj kutimaj dialogoj, en kiuj oni diras,
ke «tiu homo estas ĝentila, studis en karaj lernejoj, vojaĝis por la
tuta mondo, *havas kulturon*.

La *senco B* estas tiu de *kulturo-kolektiva animo*, io, kion, malsama ol
la unua, ĉiuj havas: estas nigra kulturo, kvira kulturo, subgrunda
medio. Ĝi estus la aro de produktaĵoj, valoroj, manieroj fari kaj vivi,
«speco de animo iom malklara, malfacile ĝin kapti, kaj kiu kaŭzis
ĉiuspecajn ambiguecojn dum la historio»[^2]. Al ĉiu kolektiva animo
(la popoloj, etnoj, sociaj grupoj) estas atribuita kulturo; en multaj
okazoj ĝi ankaŭ estas sinonimo de civilizo, io kio iĝis tre problema
en la antropologio, fako en kiu la kulturo estas la ĉefa fokuso kaj kiu,
pro tio mem, havas multajn konceptojn kaj debatojn[^3]. La *senco C*
proponita de Guattari kaj Rolnik estas tiu de *kulturo-varo*, kiel
produkto disvastigita en merkato de mona trafiko. Ĝi estas pli objektiva
ol la du antaŭaj, ĉar ĝi rilatas al io, kion ni povas tuŝi kaj vidi:
libron, bildon, ekzemple. Ni povus uzi ĉi tiun sencon por indiki alian
nocion, tiun de kulturaj varoj, kiuj estus tiuj aĵoj disvastigitaj en
*merkato*, kiu enhavas aliajn homojn krom ria kreanto. Kelkaj ekzemploj
estas desegno publikigita en blogo en la Interreto, video farita de kvar
personoj en poŝtelefono kaj disponebligita en elsendflua retejo,
politikaj tekstoj aranĝitaj en zina formo, por ke ili estu venditaj aŭ
disdonitaj en strata vendejeto, poezia libro de eldonejo, eseo pri arto
en monata revuo. Estas diversaj aliaj; sufiĉas plenumi la neceson esti
aranĝitaj en iu agnoskita formo kaj disvastigi ilin al diversaj homoj.

[^2]: Guattari; Rolnik, <cite>Micropolíticas: cartografias do desejo</cite>, p. 19.
[^3]: Kiel multaj aliaj antropologoj, Clifford Geertz, ekzemple,
  parolas pri kulturo kiel diversaj vivmanieroj kaj malsamaj manieroj
  sin esprimi (<cite>A interpretação das culturas</cite>); Marshall
  Sahlins parolas pri kulturo kiel praktika racio (<cite>Cultura e razão
  prática</cite>); kaj Roy Wagner parolas pri la kreema eblo en la
  diferenco (<cite>A invenção da cultura</cite>).

En la senco A, ne estas kiel paroli pri la libereco de kulturo, kiu estas
rigardita kiel valoro, ĉar, kvankam eblas skribi, ne estas logike paroli
pri «libera valoro» opozicie al «malfermita valoro», ekzemple. Ulo
konsiderita kiel iu, kiu havas kulturon, ne estas distingita kiel
*tenanto* de libera kulturo. En la senco B, kulturo kiel «kolektiva
animo», ĝi estas libera antaŭe; ne estas *subgrundaj medioj*, kiuj ne
estas liberaj, nek kulturo kiel la sambo aŭ hiphopo, ekzemple, kiu estas
tute malfermita kaj proprieto de unu firmao. Sed estas kulturaj varoj
produktitaj en la agokampo de tiuj kulturoj, kiuj ne estas liberaj, aĵoj
kiuj uzas la nomitaj kolektivaj animoj kaj ekdisvastiĝas en merkato kaj
iĝas proprieto de iuj.

Estas fine pri la senco C de «kulturo» pri kiu ni parolos ĉi tie pri
libera kulturo: kiel kulturo, kiu disvastigita rezulte de difinitaj
kulturaj varoj en specifa merkato, varoj kiuj estas libere alireblaj,
dissendeblaj, adapteblaj kaj valoreblaj &mdash; ĉiuj trajtoj, kiuj estos
traktitaj en ĉi tiu libro. Kvankam tiu *kulturo* estu *varo*,
konsiderita entute kiel distinga *valoro* kaj frukto de *kolektiva
animo*, kiu enhavas siajn politikojn kaj sociajn rilatojn, ĉi tiu
distingo situas nin nun en koncepto dum la sekvaj paĝoj.

Difinita mola nocio de kulturo kaj de libera kulturo, ni povas koncentri
nin sur aliaj konceptoj, kiuj estas grave, ke estu, kvankam en minimuma
versio, en ĉi tiu antaŭparolo. La nocio, ke teksto, libro, teatraĵo,
bildo povus esti *vendita* kontraŭ difinita valoro ne estas io, kio ĉiam
okazis en la homa historio, sed koncepto rigardata kiel komuna senco
post la 17-a kaj 18-a jarcentoj, dum la naskiĝo de la unuaj monopoloj
donitaj al presantoj, de la intelekta propaĵo kaj la aŭtorrajtoj. Antaŭ
tio, estis kompreneble produktado de libroj, desegnoj, pentraĵoj,
skulptaĵoj, teatraĵoj faritaj kaj disvastigitaj por malsamaj publikoj,
sed ne estis konsento pri, kiuj el tiuj verkoj disvastiĝus kontraŭ
specifa kvanto, kiu estus pagita al ria *posedanto*, aŭ al kiu produktis
ilin. Kaj ne estis pro kelkaj kialoj: unue ĉar la disvastiĝo estis
limigita pro la malfacileco okazi (en la okazo de la libro, ekzemple);
due ĉar la maniero ĝui tiujn verkojn estis kutime kolektiva kaj parola,
ne individua; kaj trie ĉar ne estis tre klare la senco, ke specifa verko
havu iun posedanton aŭ eĉ *aŭtoron*, kiel oni diras en la 1-a ĉapitro
[«Parola kulturo»](#parola-kulturo).

Nur komencas havi sencon la rilato de la kulturaj havaĵoj kun la varoj
kun specifa prezo kaj aŭtoro, kiam en la 15 jarcento estas kreita presa
maŝino, kiu disvastigis specifajn specojn de kulturaj varoj al publikoj
multe pli grandaj ol tiuj, kiuj estis ĝis tiam. Post tio establiĝas
manieroj kontroli la disvastigo de tiuj varoj, kiel leĝoj, kiel la
kopirajtoj, rajto ekskluzive donita al iu por produkti kaj reprodukti
verkon, kiel oni priskribas en la 2-a ĉapitro [«presa
kulturo»](#presa-kulturo). Post naskiĝas la nocio de intelekta propraĵo,
kiu solidigis en la sekvaj jarcentoj kiel branĉo de la civila juro, kiu
penas reguli kreaĵojn de la homa intelekto, kiel oni montras en la 3-a
ĉapitro [«proprieta kulturo»](#proprieta-kulturo), sekvante rilaton, ĝis
hodiaŭ kontestita, kun la fizika proprieto.

Post la 19-a jarcento la intelekta propraĵo solidiĝas en du branĉoj. Unu
el ili estas la *aŭtorrajto*, fondita sekvante la kopirajton, en la 18-a
jarcento, en la Klerisma Francio, kiel prerogativaro leĝe donita al homo
aŭ firmao, al kiu oni atribuas la kreadon de intelekta verko. La
aŭtorrajtoj estos siavice dividitaj per aliaj du branĉoj: la *moralaj
rajtoj*, koncerna al leĝoj, kiuj regas la aŭtorecon de verko kaj ĝian
integrecon, tio estas, la eblon ŝanĝi aŭ ne ŝanĝi specifan kreaĵon; kaj la
*posedrajtoj*, kiuj regas la komercajn produktadon kaj reproduktadon de
tiu verko. En tiu epoko jam eblas vidi, ke estis situacio pli kompleksa
pri la disvastigo de verko por multe pli da homoj; ke pro tio la ĝuado
de kulturaj havaĵoj iĝis malpli kolektiva kaj iom post iom pli
individua; kaj ke ankaŭ la aŭtoro de specifa verko povas esti identigita
kiel tiu «kiu permesas superi la kontraŭdirojn, kiuj povas okazi en serio
da tekstoj»[^4].

[^4]: Kiel Michel Foucault konceptis en sia konata eseo «O que é um
  autor?» [«kio estas aŭtoro?»] en 1969 en <cite>Ditos e
  escritos</cite>, v. 3.

Fine de la 19-a jarcento kaj dum la 20-a jarcento, kiam tiuj nocioj
solidiĝis en la komuna senco kaj en jura sistemo de intelekta propraĵo,
estas sennombraj la manieroj, speciale en arto kaj kontraŭkulturo,
respondi al tio starigita. «Ĉu mi bezonas pagi al iu por legi libron?»,
«Ĉu mi estas posedanto de ĉi tiu teksto?», «Kiu diris, ke mi ne povas
uzi pecon de verko por fari alian, aŭ por krei novan artmanieron, novajn
kulturajn havaĵojn?». Iuj avangardaj movadoj, artistoj kaj kolektivoj
alfrontas la tiaman staton de la aŭtorrajto kaj de la aŭtoreco, kaj pro
tio ili iĝas defendantoj de libera kulturo antaŭ, ke la termino
populariĝis, kaj ankaŭ estas aliaj, kiuj dubas pri la kondiĉo de
originaleco de specifa verko en epoko de disvastiĝo de teknikaj maŝinoj
de reproduktado kiel oni informas en la 4-a ĉapitro
[«rekombina kulturo»](#rekombina-kulturo).

La alia branĉo, en kiu dividatas la intelekta propraĵo, estas la nomita
industria propraĵo. Ĝi estas ligita al la produktado kaj uzado de iaj
produktoj en industria skalo, kiu pliigas la juran kontrolon de kreado
por procezoj, inventaĵoj, desegnoj, identigitaj kiel utilaj verkoj
&mdash; tio estas, kiuj estas uzitaj por konkreta celo en specifa
merkato, en opozicio al la aŭtorrajto, kiu regas la artan, sciencan,
muzikan, literaturan kreadon kaj kiu, per tiu elpenso, ne estus utilaj.
La industriaj propraĵoj havas kiel sia ĉefa registranta elemento la
patenton, publika koncesio &mdash; do provizita de iu ŝtata organo
&mdash;, por ke certa posedanto komerce ekspluatu, ekskluzive kaj
tempolimigite, specifan kreaĵon. El la inkandeska lampo al la
filma fotografilo, el la fonografo de Thomas Edison ĝis la
programaro, la patentoj estas monopoloj de komerca ekspluato de ideo,
kiuj generas multe da mono kaj ankaŭ pro tio multajn batalojn kaj kritikajn
dubojn, speciale ekde la 19-a jarcento.

La ekspansio de la cifereca teknologio kaj ĝia preskaŭ ĉieesteco en la
vivo de granda parto de la pli ol sep miliardoj da homoj, kiuj loĝas en
la planedo Tero en la 21-a jarcento, kaŭzas kondiĉojn pli kompleksajn de
produktado, disvastiĝo kaj komercado de kulturaj havaĵoj. Pro tio alia
nocio, kiun traktas ĉi tiu verko iĝas eĉ pli fleksebla: fine kio estas
kopio kaj kio estas originalo? Se la Interreto nur funkcias per kopio de
datumoj kaj dosieroj, kiuj estas transigitaj kaj kunhavitaj, ĉu eblas
kontroli la ludadon de muziko milionon da fojoj kopiita kaj kiu samtempe
ankoraŭ ekzistas *egale* en ĉiuj el la milionoj da kopioj? La diskuto
pri la kunhavigo de dosieroj rete kaj ĝiaj sekvoj rezultas en la 5-a
ĉapitro [«Libera kulturo»](#libera-kulturo), ne hazarde la plej ampleksa
el ĉiuj.

Miljaraj tradicioj en la Ekstrema Oriento kaj en iuj originaj popoloj de
Latin-Ameriko montras al ni, ke la mondo ne estas nur kion ni nomas
okcidentan kaj ke la perspektivo, pri kio estas kopio, originalo, libera
kaj kolektiva havas signifajn diferencojn inter diversaj kulturoj. Ili
estas ideoj, kiuj invitas al ni malkoloniigi nian okcidentan rigardon
aplikitan al historioj, filozofioj kaj manieroj pensi la aferojn kaj la
mondon kiel ni ĝin konas, kaj serĉi malsamajn manierojn vidi tiujn
aferojn, kiel estas la okazo de la koncepto <i lang="zh">shanzai</i> en
Ĉinio, sinonimo de falsa, <i lang="en">fake</i>, sed estas ankaŭ maniero vidi
la kulturajn havaĵojn kiel ĉiam transformantaj elementoj laŭ ĉiu
kunteksto, celo kaj fino, sen unika kaj sankta origino. Kaj ankaŭ la
perspektivo de iuj indianaj popoloj, kiu ne apartigante la individuon de
la objekto, igas la vortaron disvolvigitan pri proprieto kaj aŭtorrajto
nesufiĉa por esti uzita ĉe tiuj popoloj, kiel oni prezentas en la 6-a
ĉapitro [«kolektiva kulturo»](#kolektiva-kulturo). Post la mikso de iuj
el tiuj neokcidentaj spertoj cititaj kaj de rigardo el la nomita monda
sudo, fine de tiu ĉapitro ni montras iujn alternativojn por la
disvastigo kaj la defendo de libera kulturo hodiaŭ.

