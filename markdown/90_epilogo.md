Post paroli tiom pri kreo, reproprigado, proprieto, kopio, komuno,
rajtocedo kaj aŭtorrajto tra la tempoj, de malsamaj lokoj kaj mondaj
vidoj, indas demandi: kaj ĉi tiu libro, kiu estas la marko de la aŭtoro
por kopiado, uzado (privata aŭ publika), citado kaj reproprigo? Ĉu oni
adoptas iun licencon? Kiun?

Mia &mdash; *nia*, ĉar, kvankam estas nomo antaŭ ĉi tiu verko, ĝi ne
ĉesas esti kolektiva, kiel vi rimarkis dum la legado &mdash; elekto
estas la licenco, kiun la rajtocedo reprezentas: Creative Commons
CC BY-SA[^298].

[^298]: Ĝia teksto estas tute disponebla ĉi tie: <https://creativecommons.org/licenses/by-sa/4.0/>.

<figure>
    ![Licenco Creative Commons BY-SA](bildoj/Creative_Commons_BY-SA.png)
    <figcaption></figcaption>
</figure>

Ĝi diras, ke ĉi tiu verko povas esti *kunhavita* &mdash;kopiita kaj
redistribuita &mdash; per ĉia ajn komunikilo kaj formo kaj *adaptita*
&mdash; remiksita, ŝanĝita &mdash; por ĉia celo; se estas *atribuo* de
aŭtoreco, kio signifas, ke ĉiu uzo devas mencii tiun, kiu skribis ĉi
tiun verkon kaj kie ĝi estis modifita &mdash; mi supozas, ke kiu volus
kunhavi, uzi kaj adapti ĉi tiun libron faros tion racie &mdash;, kaj ke
ĉiuj ajn modifitaj verkoj de ĉi tiu estas kunhavita per la sama licenco
priskribita ĉi tie, garantio kiu ne permesas la malfermadon de ĉi tiu
verko per licenco, kiu restriktu ĉiujn la indikojn antaŭe nomitajn.

La atingo de ĉi tiu licenco aplikas al la materiaj manieroj per kiuj ĉi
tiu verko disvastiĝas: presita kiel libro, en dosierformo de cifereca
libro kaj publikigita en partoj en retejoj. La elekto de ĉi tiu naskiĝas de
la supozo, ke ĉi tiu verko nur ekzistas, ĉar multaj aliaj ekzistas, kaj
ke instigi la kreon de novaj verkoj estos laŭdo al la ideoj, kiuj ĉi tie
troviĝas. Ni scias pri la ebloj de malkonvena kaj pigra proprigado,
kiun multaj jam faris kun similaj verkoj, sed ni elektas preni riskon
por garantii, ke ĉi tiu libro estu libera por pluraj celoj, inkluzive la
komerca.

Tiurilate ni instigas la uzon, la reproprigon kaj la (re)vendon de ĉi
tiu verko por fortigi malgrandajn eldonejojn kaj alternativajn
markojn, se la gvidoj jam montritaj estas respektitaj; se vi volas
fari tion, ni iĝus feliĉaj, se vi sciigus nin. Ni memorigas tamen, ke la
laboro de sendependaj eldonejoj kiel ĉi tiu bezonas esti rekompencita,
por ke ĝi ekzistu plu. Pro tio konsideru aĉeti ĝin presitan kaj tial
valorigi la eldonajn kaj grafikajn elektojn faritajn ĉi tie, kaj ankaŭ
la monan investon faritan &mdash; tio estas kio kaŭzos, ke aliaj verkoj
kiel ĉi tiu estu publikigitaj. Fine ni memorigas, ke la plej bona sperto
legi ĉi tiun tekston &mdash; kiel multajn aliajn &mdash; estas tiu
ebligita per ĉi tiu invento de miloj da jaroj nomita *presa libro*, kun
la papera odoro penetrante en la naztruojn kaj instigante malrapidan
legon, per diversaj notoj kaj substrekoj, kiuj instigas dialogojn kaj
ebligas al ni unikan kaj neordinaran sperton scii.

