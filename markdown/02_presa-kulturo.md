<section class="citas">
<i>Ĉio, kion al mi oni skribis pri tiu mirinda homo vidita en Frankfurto,
veras. Mi ne vidis kompletajn Bibliojn, sed nur nebinditaj kajeraroj aŭ
kelkaj libroj de la Biblio. La tipografio estis tre eleganta kaj
legebla, neniel malfacile sekvebla &mdash; vi povus legi ĝin senpene, kaj
fakte sen okulvitroj.</i>

<div class="right-align">
Enea Silvio Bartolomeo Piccolomini, estonta papo Pio la 2-a, en letero al
kardinalo Carvajal, 1455
</div>

<br>

<i>Konsiderante, ke presistoj, bibliotekistoj kaj aliaj homoj kutime
en la lastaj jaroj prenis la liberecon presi, represi kaj eldoni, aŭ
fari, ke oni presu kaj represu kaj ke oni eldonu librojn kaj aliajn
skribaĵojn sen la konsento de la aŭtoroj aŭ proprietuloj de tiuj libroj
kaj skribaĵoj, malprofitante ilin, kaj kun tro da kutimo ruinigante ilin
kaj iliajn familiojn: por eviti do tiujn praktikojn en la estonteco kaj por
instigi al instruitaj homoj komponi kaj skribi utilajn librojn; ke bonvole
de via moŝto povu esti proklamita ĉi tiu Statuto.</i>

<div class="right-align">
Statuto de Anne, Anglio, 1710
</div>

<br>

<i>Mi ne vidas kialon por doni nun pli daŭran periodon, ke devigu al
ni doni ĝin denove senhalte, kiam la antaŭaj eksvalidiĝu; se tiu
projekto estas aprobita, ĝi kreos sume eterna monopolo, io ege leĝe
malaminda; ĝi estos granda obstrukco por la komerco, granda baro por la
lernado, kiu donos neniun profiton al la aŭtoroj, sed ĝenan imposton al la
publiko, nur por pligrandigi la privatajn gajnojn de la librovendistoj.</i>

<div class="right-align">
Angla parlamento, neante la peton de la librovendistoj por pliigi la
daŭron de la aŭtorrajta templimo, 1735
</div>

<br>

<i>La aŭtorrajto apartenas al la aŭtoro; la aŭtoro tamen ne posedas
presajn maŝinojn; tiuj maŝinoj apartenas al la eldonisto; do aŭtoro
necesas la eldoniston. Kiel reguli ĉi tiun neceson? Simple: la aŭtoro,
interesita ke ria verko estu eldonita, liveras la rajtojn al la
eldonisto dum certa periodo. La ideologia defendo ne plu baziĝas sur
cenzuro, sed sur la merkata neceso.</i>

<div class="right-align">
Wu Ming, <cite>Novaj rimarkoj pri la aŭtorrajto kaj la rajtocedo</cite>, 2005
</div>
</section>

### I.

Ĉirkaŭ la jaro 1455, la estonta papo Pio la 2-a, Enea Silvio Bartolome
Piccolomini, promenis tra la stratoj de la regiono de Frankfurto, kiam li
vidis montrofenestron kun kelkaj presitaj kajeroj de teksto, kiun li
bonege konis. En letero al la kardinalo Carvajal, li rakontis tiel la
epizodo: «Ĉio, kion al mi oni skribis pri tiu mirinda homo vidita en Frankfurt,
veras. Mi ne vidis kompletajn Bibliojn, sed nur nebinditajn kajerarojn aŭ
kelkajn librojn de la Biblio. La tipografio estis tre eleganta kaj
legebla, neniel malfacile sekvebla &mdash; vi povus legi ĝin senpene, kaj
fakte sen okulvitroj»[^33]. La nomita «Biblio de Gutenberg», ankaŭ
konata kiel «Biblio de 42 linioj», estis unue presita en 1455 kaj havis
inter 158 kaj 180 kopiojn. Ĝi konsistis el la hebrea Malnova Testamento
kaj el la greka Nova, kiel la kristana Biblio hodiaŭ estas konata,
skribitaj en la latina, kun 42 (en iuj 40) linioj, parte presita en
pergameno, parte en normala papero de grando *duobla folio*, kun du
paĝoj en ĉiu flanko de la papero (po kvar paĝoj por folio).

[^33]: «<i lang="en">All that has been written to me about that marvelous man seen at
Frankfurt is true. I have not seen complete Bibles but only a number
of quires of various books of the Bible. The script was very neat and
legible, not at all difficult to follow—your grace would be able to
read it without effort, and indeed without glasses</i>». Disponebla en
<https://pablozeta.com/posts/the-birth-of-movable-type/>.

Naskita en Majenco, sudokcidento de Germanio, en 1398, homo nomita
Johannes Gensfleisch zur Laden zum Gutenberg estis juvelisto kaj lerta
komercisto, antaŭ ol labori en la presado kaj disvolvi procezon, kiu
faciligis la disvastigon de la ideoj multe pli rapide ol tio, kio ekzistis
ĝis tiam. La sistemo de moveblaj tipoj per kiu Gutenberg presis la
Biblion, kaj kiu populariĝis post tiu periodo, ne estis &mdash; kiel
neniam estas &mdash; inventaĵo kreita el nenio. La permana produktada
procezo de libroj komencis, ekde la 12-a jarcento, havi firmajn modifojn;
la paperoj redisvastiĝis tra Eŭropo, kaj la presiloj, grandaj lignaj
blokoj, kiuj prenis inkon kaj registris en surfacon, komencis igis la
presadon industria procezo pli rapida ol la manoj de la kopiistaj
monaĥoj kaj pli malmultekosta ol la malnovaj libroj de papiruso de la
Antikveco[^34]. La kontribuoj de Gutenberg al la sistemo jam uzita en la
epoko por presado estis ĉefe la inventado de amasproduktada procezo de
movebla tipo, farita per alojo, kiu enhavis plumbon, stanon kaj kupron
kaj povis esti reuzita; la uzo de inko bazita sur oleo, kiu adaptiĝis pli
bone al papero pli mola kaj sorbanta provita de li; kaj la uzo de presa
modelo, kiu estis simila al tiu de la ŝraŭbo uzita en la tiama
terkulturo kaj tial aĵo, kiu estis pli konata en la kutima kampara
regiona vivo.

[^34]: En Ĉinio estas registroj de uzoj de presmanieroj similaj al tiuj
  de Gutenberg ekde la 11-a jarcento; Bi Sheng, en 1040, uzis moveblajn
  tipojn por presi en argilon, materialon malmulte rezistan kaj facile
  rompiĝeman; Wang Zhen, en 1298, laboris kun sistemo movebla, ankoraŭ
  prilaborita el ligno, iom pli rezista ol la argilo. La presado en ligno,
  tekniko konata kiel ksilografio, estis vaste uzita en la Ĉinio de tiu
  periodo kaj estas registrita, ke pro la propreco de la ĉinaj signoj,
  daŭre estis la plej efika kaj malmultekosta maniero presi. Vidu
  Briggs; Burke, <cite>Uma história social da mídia: de Gutenberg a
  Diderot</cite>; kaj Tsien Tsuen-Hsuin; Joseph Needham. <cite>Paper and
  Printing: Science and Civilisation in China</cite>, v. 5, p. 158.

La naskiĝo de la procezo de amasproduktado de eldonaĵoj, kiun hodiaŭ ni
nomas presado, akcelis procezon de populariĝo de la skribita
kulturo[^35]. La progresoj favoritaj per la presado faciligis la
disvastigon de ĉiaj ideoj, ne nur tiuj liturgiaj kaj religiaj, kiuj
superregis en tiu epoko.  La kreskanta moviĝo de eldonoj plifortigis la
kreadon de legantoj kaj komencis ŝanĝi la kutimojn ĝui la kulturajn
havaĵojn. Estis komenco, iom post iom, de interŝanĝo de la kolektiva
parola sperto, bazita sur la agado de tiu, kiu prezentis ĝin, kaj sur la
enhavo, kiun oni volis komuniki, per la individua sperto, silenta kaj
izolita, registrita en papero pli daŭre kaj fiksite ol tiu en la libera
aero, alkutimigita al la libereco de aldonoj, alproprigoj kaj diversaj
improvizoj de tiu, kiu ĝin prezentis. «La tendenco al pli individuismaj
sintenoj estis instigita per la eblo presi, kiu samtempe helpis fiksi
kaj disvastigi tekstojn»[^36].

[^35]: Estas interese rimarki ĉi tie, kiel faras Eisenstein, en
  <cite>The Printing Revolution in Early Modern Europe</cite>, kaj
  Martins, en <cite>Autoria em rede</cite>, ke la paso el la manuskripta
  libro al la presita libro ne okazis tuje; «male, ĝi estis procezo de
  traktado kaj miksado inter du lingvoj, kio plifortigas la teorion, ke
  la kreado de unu nova rimedo okazas per la adaptigo de antaŭa rimedo»
  (Martins, <i lang="lt">op. cit.</i>, p. 68).
[^36]: Briggs; Burke, <i lang="lt">op. cit.</i>, p. 140.

La kresko de la cirkulado de presitaj eldonoj kaj la stimulo al
individuismo favorigita per la ebleco de legado en soleco aglutinis sin al
renesanca humanismo por ankaŭ modifi la ideon de tiama aŭtoreco. Se, dum
granda parto de la Mezepoko, la kulturo estis parola kaj la aŭtoreco
estis kolektiva kaj difuza, esprimo de dia deziro aŭ fiksita en iu
populara kulturo, kaj la libroj estis limigitaj cirkuli por metiista
produktado de preĝejoj, tiam estis eroj por la transformado de la
koncepto de tiu, kiu estus aŭtoro de verko. Translokinte la homon al la
mezo (antropocentrismo) de la mondo, la humanismo komencis valori la
nocion de originaleco kaj individueco, kio estis esprimita en la
valorigo de la *stilo* kaj en la rekono de nova fokuso de ĉiu aŭtoro,
kontraŭe al la granda teksta dependeco de la kutima tradicio de la
Mezepokaj verkoj[^37]. Havinte identigitan individuan aŭtoron la
eldonoj ankaŭ iĝis pli fermitaj, kun malpli da malfermeco al aldonoj aŭ
komentoj, kiel ĝis tiam kutimis okazi en la *ĉirkaŭaj notoj* de la
mezepokaj libroj.

[^37]: <i lang="lt">Ibidem</i>, p. 116.

Antaŭ la populariĝo de la presado de moveblaj tipoj, la produktado de
libro estis malfacila tasko, multekosta kaj metiista, praktike restrikta
al la agokampo de la Katolika Eklezio kaj iliaj kopiistaj monaĥoj. Post
Gutenberg la libro povis esti presita industrie de komercistoj kaj
entreprenistoj, kiuj havu monon por aĉeti la necesajn maŝinojn kaj
organizi siajn produktadajn modelojn, kio jam estis konsiderinda ŝanĝo
en la tiama sistemo de scia cirkulado: tio ebligis la disvastigon de ideoj,
kulturaj havaĵoj kaj informoj trans la kontrolo de la Eklezio. Ne
hazarde en tiu periodo okazas la Protestanta Reformacio, religia movado,
kiu pridubis la dogmojn de la tiama katolikismo, inaŭgurita post la
famaj 95 tezoj skribitaj de Marteno Lutero en Vitenbergo, en Germanio,
en oktobro de 1517. La malgrandaj presejoj, multfoje kaŝaj, estis la
arterioj de disvastigo de la reformismaj ideoj tra la tuta Eŭropo.

La *galaksio* (aŭ *revolucio*) inaŭgurita de Gutenberg plifortigis la
ideon de merkato por kulturaj havaĵoj kaj li donis al tiuj specifajn
ecojn laŭ la kondiĉoj de amasproduktado. Presi libron ankoraŭ estis
multekosta procezo, kiu bezonis konsiderindan kvanton da mono por okazi.
Sed, per la novaĵo de la presilo de moveblaj tipoj, tio iĝis ankaŭ
profitdona negoco; sola libro, kiu postulis monatojn por esti metiiste
produktita en la monaĥejoj, iĝis 500, 1&nbsp;000 aŭ pli da ekzempleroj
presitaj en malmultaj tagoj kaj disdonitaj en la ĉefaj tiamaj urboj, kio
naskigis potencan reton, kiu allogis bankistojn por financi la
presistojn, vendistojn por komerci la verkojn, vojaĝantajn vendistojn por
transporti ilin kaj novajn legantojn, multfoje alfabetigitajn danke al la
publikaĵoj, kiuj komencis cirkuli en tiu epoko.

Ĉar la Eklezio kaj la eŭropaj monarkioj ne volis perdi la kontrolon de
la disvastigo de ideoj, la konfliktoj estis neeviteblaj. En la unua
okazo la timo al la disvastiĝo de la principoj de la Protestanta
Reformacio kaŭzis persekutadojn al diversaj tiamaj presistoj, kio en
postaj jaroj kaŭzis la kreadon, en 1559, de la <cite>Index</cite>, listo
de publikaĵoj konsideritaj kiel herezaj kaj kiuj estis malpermesitaj de
la Katolika Eklezio, per la nuligo de ĝiaj eldonistoj[^38]. La kreado de
eldona merkato siavice kaŭzis, ke la tiamaj monarkiaj registaroj estigu
regulojn por kontroli la rilatojn inter tiu kiu skribis libron, tiu kiu
vendis kaj tiu kiu legis. Ĝis tiam ĉiu ajn, kiu havis aliron al presmaŝino
aŭ iu, kiu ĝin havis, povis presi kopiojn de tio, kion ri volis, sen ke
neniu jure pretendu la produktadan ekskluzivecon kaj cirkuladon de la
verkoj presotaj. Krome kutimis, ke verko, bone vendita en iu regiono,
estis eldonita kiel novaĵo en alia pro la diversaj tradukoj kaj adaptoj
sen nenia kontrolo. En epoko, en kiu en Eŭropo, Portugalio, Hispanio,
Anglio kaj Francio komencis sin organizi, dum urboŝtatoj kaj la
nuntempaj Germanio, Italio, Belgio, Aŭstrio, Pollando, inter aliaj,
estis dividitaj en centoj da sendependaj urboŝtatoj, ne estis leĝaroj,
kiuj regulis la cirkuladon de verkoj en ĉiuj tiuj regionoj; maksimume
ĉiu urbo aŭ regiono havis siajn proprajn regulojn, kiuj ne validis por
aliaj. Nenia distingo ekzistis inter tiu, kiu estus «oficiala», kaj tiu,
kiu estus «pirata» verko.

[^38]: Realigita de la Papo Paŭlo la 4-a, Gian Pietro Carafa, ĝi estis
  listo konservita ĝis la 20-a jarcento, nur ĉesigita en 1966. Pri la
  cenzuraj kaj persekutadaj agoj al la eldonistoj kaj radikalaj pastoroj
  de la Protestanta Reformacio, de Carafa, la grandioza novelo
  <cite>Q: o caçador de hereges</cite>, de Luther Blisset (1999),
  eldonita en Brazilo en 2002 de Conrad, estas bonega fonto.

### II.

Koncernis al la Venecia Respubliko kaj al Anglio la unuaj laboroj plej
solidigitaj oferi ekskluzivajn licencojn al kelkaj eldonistoj por la
publikigo de certaj libroj. En la itala urbo, konata per ĝia mara
komercado kun azianoj, araboj, bizancanoj, afrikanoj kaj per la
diversa cirkulado de nobluloj, bankistoj, maristoj, krimuloj kaj
vendistoj de la plej malsamaj ejoj, en 1486 estiĝis la unua privilegio
por la ekskluziva eldono de libro. La elekta verko,
<cite>Rerum venetarum ab urbe condita opus</cite>, estas historia resumo
de la *Plej Serena*, kiel estis konata Venecia, skribita de Marcus
Antonius Coccius Sabellicus, itala historiisto, al kiu la konsilistaro,
kiu administris la urbon, donis specialan permeson por elekti solan
eldoniston de la libro en la venecia teritorio[^39]. Kelkajn jarojn poste,
la reĝaj privilegioj al certaj presistoj estis uzitaj por pli da verkoj
en Venecia (1498) kaj disvastiĝis ankaŭ al aliaj italaj urboj, kiel
Florenco kaj Romo, kiel ankaŭ al Francio kaj aliaj germanaj urboŝtatoj,
havante la saman celon: garantii al certaj presistoj la eldonadan
ekskluzivecon de specifaj libroj, por ke nur ili povu gajni monon per
ĝia komercado[^40].

[^39]: En Armstrong, <em>Before Copyright: The French Book-Privilege
  System 1498-1526</em>.
[^40]: En Martins, <i lang="lt">op. cit.</i>, p. 38, kaj ankaŭ
  Woodmansee, <cite>The Author, Art, and the Market: Rereading the History of Aesthetics</cite>.

En Anglio 1557 estas la jaro de la unuaj licencoj donitaj al presistoj,
ceditaj de la reĝino Mary al londona grupo konata kiel Stationers
Company, formita en 1403[^41] de metiistoj rilatitaj al la cirkulado kaj
vendo de libroj kaj aliaj presaj materialoj. Ĉar ĝi estis unu el la
unuaj organizitaj grupoj laborintaj en la nova negoco, ili puŝis al la
angla monarkio por havi produktadan kaj vendan ekskluzivecon de
publikaĵoj kaj atingis privilegion, kiu praktike donis al Stationers
Company la monopolon de la kopiado kaj cirkulado de libroj. Poste
eblis nur leĝe presi en Anglio verkojn, kiuj havu reĝan rajtigon kaj kiuj
estu listigitaj en la oficiala registro kun la nomo de iu eldonisto
ligita al la firmao. Tio estis rajto kopii
(<i lang="en">right to copy</i>) donita al iuj presistoj, kiuj per tio
iĝis la unikaj kun privilegioj sur specifaj verkoj. Ne estis mencio al
proprietaj, moralaj aŭ estetikaj rajtoj de la aŭtoroj de iu specifa
verko.

[^41]: Kiel aperas en la oficiala retejo de la organizo, hodiaŭ ankoraŭ
  aktiva, disponebla en
  <https://www.stationers.org/company/history-and-heritage>.

Post jarcento kaj duono da monopolo, Stationers Company iĝis pli kaj pli
minacita de la librovendistoj de provincoj malproksimaj de Londono
&mdash; skotoj kaj irlandanoj ĉefe &mdash;. La firmao petis tiam al la
angla Parlamento novan leĝon por plilongigi sian ekskluzivan rajton sur
la kopio de libroj. La respondo estis la kreo de la Statuto de Anne,
aprobita en 1710 de la brita Parlamento kaj konsiderita kiel la unua
aŭtorrajta leĝo de la mondo kaj bazo por parto de la leĝaroj ĝis hodiaŭ,
pli ol tri jarcentojn poste. Estis forta frapo kontraŭ la privilegio de
Stationers Company, ĉar la leĝo proklamis la aŭtorojn (kaj ne plu la
eldonistojn) kiel la proprietuloj de iliaj verkoj. La jura teksto
komencis tiel:

> Konsiderante, ke presistoj, bibliotekistoj kaj aliaj homoj kutime en
> la lastaj jaroj prenis la liberecon presi kaj represi, kaj eldoni aŭ
> presigi, represigi kaj publikigi librojn kaj aliajn skribaĵojn, sen la
> permeso de la aŭtoroj aŭ proprietuloj de tiuj libroj kaj skribaĵoj,
> damaĝante ilin multe, kaj tro ofte ruinigante ilin kaj iliajn
> familiojn: por eviti do tiujn praktikojn en la estonteco kaj por igi, ke
> la kleraj homoj verku kaj skribu utilajn librojn; ke bonvole
> de via moŝto povu esti proklamita ĉi tiu Statuto..[^42]

[^42]: En la angla originala: «Whereas Printers, Booksellers, and
  other Persons, have of late frequently taken the Liberty of Printing,
  Reprinting, and Publishing, or causing to be Printed, Reprinted, and
  Published Books, and other Writings, without the Consent of the
  Authors or Proprietors of such Books and Writings, to their very
  great Detriment, and too often to the Ruin of them and their Families:
  For Preventing therefore such Practices for the future, and for the
  Encouragement of Learned Men to Compose and Write useful Books; May it
  please Your Majesty, that it may be Enacted this Statute». Disponebla
  en <https://en.wikipedia.org/wiki/Statute_of_Anne>.

Antaŭe ekskluzivaj de la membroj de Stationers Company, la rajtoj pri la
presado kaj la represado de libroj komencis esti de la aŭtoro &mdash; aŭ
de alia homo, al kiu ri elektu por licenci &mdash;, tuj kiam ĝi estu
eldonita. Grava limigo estis, ke la leĝo donis tiun rajton nur dum
specifa tempo: 14 jaroj, renovigebla nur unufoje, se la aŭtoro estu
viva; kaj 21 jaroj por verkoj eldonitaj ĝis tiam. Je la fino de tiu
periodo la aŭtorrajtoj eksvalidiĝis kaj la verko estis tiam libera por
esti publikigita de iu ajn. La puno por kiu ne obeis la statuton estis
la detruo de la kopioj kaj la pago de monpuno al la proprietulo de la
rajtoj.

Por kelkaj aŭtorrajtaj esploristoj kaj historiistoj la intenco de la
leĝo estis fini la monopolon de Stationers Company &mdash; kaj ne doni
kopiajn kaj presadajn rajtojn al la aŭtoro. Estis premo de diversaj
lokoj por forpreni la firmaan monopolon, akuzitan «vendi la liberecon de
Anglio por garantii iliajn gajnojn»[^43]. La angla verkisto John
Milton, aŭtoro de <cite>Perdita paradizo</cite> (1667), diris en tiu
epoko, ke la presistoj de Stationers Company estis «monopolantoj de la
negocon vendi librojn, homoj, kiuj neniam laboris en honestaj profesioj
kaj malestimis la scion»[^44]. La fama povo, kiun la libristoj aplikis
al la scia dissemigo per la monopoloj, damaĝus ĝian liberan disvastigon.

[^43]: Kiel rakontas la advokato kaj profesoro Lawrence Lessig en
  <cite>Cultura livre: como a grande mídia usa a tecnologia e a lei para
  bloquear a cultura e controlar a criatividade</cite>, p. 90.
[^44]: Wittenberg, <cite>The Protection and Marketing of Literary
  Property</cite>, citita en Lessig, <i lang="lt">op. cit.</i>, p. 80.

Aprobinte la Statuton de Anne, la britia Parlamento ankaŭ klopodis
pliigi la konkuradon inter la libristoj kaj tiel teorie instigi pli da
cirkulado de verkoj. Per tiu perspektivo, limigi la aŭtorrajtan periodon
estis necesa por garantii, ke la publikaĵoj iĝus malfermitaj, por ke ĉiu
ajn distribuisto publikigu ilin post iom da tempo. «La tempa determino
por ekzistantaj verkoj de nur 21 jaroj estis maniero batali kontraŭ la
povo de la libristoj, maniero nedirekta garantii la konkuradon inter la
distribuistoj kaj, tial, la kulturan konstruadon kaj pligrandiĝon»[^45].

[^45]: Lessig, <i lang="lt">op. cit.</i>, p 81.

### III.

Publikigita en Anglio, la vero estas, ke la Statuto de Anne ne estis
obeita tuje. Ĝi naskiĝis kiel leĝo, kies interpretoj estis disputitaj en
la tribunaloj ankoraŭ dum multaj jardekoj, eble pro la noveco de la
koncepto, kiun ĝi enkondukis, kio donas montron, ankoraŭ hodiaŭ
koncernan, pri kiuj interesoj estas en risko, kiam oni parolas pri
konfliktoj inter produktantoj, perantoj kaj publiko. Stationers Company
kaj aliaj libristoj poste naskitaj ignoris la leĝaron kaj daŭre insistis
pri la eterna rajto kontroli siajn publikaĵojn kiel ajn ili volus dum
jardekoj.

En 1735, jam pasintaj la unuaj 21 jaroj de eksvalidiĝo de verkoj sekvante
la Statuton (1710 + 21), la libristoj klopodis persvadi la Parlamenton
plilongigi la periodojn, por legalizi la komercan ekspluaton de la
verkoj dum pli da tempo. La teksto, en kiu la Parlamento komunikis sian
decidon &mdash; nean &mdash; kunportas ian <i lang="de">zeitgeist</i>
(spirito de la epoko) kritikan al la monopoloj, precipe de la angla
Monarkio, tre ĉeesta en la lando en la 17-a kaj 18-a jarcentoj. Oni devas
memori, ke la nomata Angla Civila Milito (1642-1651), stranga periodo,
en kiu Anglio ne havis monarkon kiel sian ĉefan reganton, estis parte
kaŭzita per la monarkiaj agoj subteni monopolojn.

> Mi ne vidas kialon por koncedi nun novan limtempon, kio ne evitus, ke
> oni koncedu denove kaj denove, tiel ofte, kiel eksvalidiĝas la antaŭa;
> do se tiu ĉi leĝo estas aprobita, ĝi fakte estigos eternan monopolon,
> aferon merite abomenan antaŭ la leĝaj okuloj; ĝi estos granda malhelpo
> al komerco, malkuraĝigo al lernado, neniu profito al la aŭtoroj, sed
> ĝenerala Imposto sur la Publikoj kaj ĉi tio nur por grandigi la
> privatajn gajnojn de la libristoj.[^46]

[^46]: Citita en Lessig, <i lang="lt">op. cit.</i>, p 82.

Ne akirinte la plidaŭrigon de la komenca aŭtorrajta periodo la
eldonistoj ankoraŭ estis en disputoj en la anglaj tribunaloj dum iuj
jardekoj. En la defendo de la procesoj komencitaj kontraŭ iuj kaj aliaj
ili komencis ankaŭ alvoki la rajtojn, kiujn la aŭtoroj havis sur la
verkoj, kiel argumenta strategio por garantii la komercan ekspluatadon de
iliaj verkoj dum pli da tempo. Ili uzis jurajn ruzojn por tio, kiel
montras unu el la plej konataj kazoj de tiu periodo, Millar kontraŭ
Taylor, en 1769. Millar estis libristo aginta en Londono ligita al
Stationers Company, kiu en 1729 aĉetis la kopiajn rajtojn de la poemo de
la verkisto James Thomson <cite>La sezonoj</cite>, paginte tiam £105.
Post la fino de la aŭtorrajta periodo, 14 jaroj laŭ la Statuto de Anne,
Robert Taylor, alia angla eldonisto, komencis vendi eldonon de la poemoj
en la londonaj bazaroj, kiu konkuris kun la Millar-a &mdash; kiu ne
ŝatis tion kaj, kun la subteno de la firmao, al kiu li apartenis,
denuncis Taylor-on. La juĝa argumentado uzita en la proceso estis, ke
Millar, paginte la aŭtoron, havis la daŭran rajton sur la verko.

Konata angla juĝisto, sinjoro Mansfield, juĝis favore al la kazo de
Millar. Laŭ lia kompreno ĉiu ajn protekto donita per la Statuto de Anne
al la libristoj ne nuligis la rajtojn de la angla
<i lang="en">common law</i>, jura sistemo, en kiu juĝaj decidoj kaj
precedencaj leĝoj &mdash; nomitaj jurisprudenco &mdash; havis pli da
graveco ol la leĝaj aŭ plenumaj agoj, okazo de la statuto. En tiu
sistemo decido farota en kazo dependas de aliaj faroj adoptitaj en
antaŭaj kazoj, lasante al juĝisto la finan decidon. Se la juĝisto ne
vidas taŭgan jurisprudencon por situacio, ri tiam havas la povon krei
unu kaj estigi precedencon, kiu ekestas nomita
<i lang="en">common law</i> kaj estas ligita al ĉiuj estontaj decidoj.

En la kazo de Millar kontraŭ Taylor la daŭra rajto de la eldonistoj
kopii, presi kaj represi verkon estis rigardita kiel
<i lang="en">common law</i> de la juĝisto Mansfield. Li defendis, ke tiu
leĝo garantiis aŭtorprotekton kontraŭ estontaj «pirataj» eldonistoj, kio
laŭ la interpretado uzita en la proceso, povus eviti, ke la dua eldono
de <cite>La sezonoj</cite> farita de Taylor estu eldonita sen la
rajtigo de la eldonisto de la unua, Millar. La decido de la juĝisto
Mansfield malutiligis la Statuton de Anne kaj donis al la libristoj
daŭran rajton kontroli la eldonon de ĉiuj libroj, kies aŭtorrajton ili
havu.

Kvin jarojn poste tamen la decido estis revokita en alia fama tiama
kazo, Donaldson kontraŭ Beckett[^47]. Millar mortis iom post sia venko
vendinte sian akiraĵon al sindikato de librodisdonistoj, kiu enhavis
ulon nomatan Thomas Beckett. Aliflanke Alexander Donaldson estis skoto
libristo, kiu publikigis malmultekostajn eldonojn kies aŭtorrajta
periodo eksvalidiĝis, kio igas, ke li estu konsiderita kiel «pirata»
eldonisto de la angloj de Londono. Post la morto de Millar, la skoto
lanĉis nerajtigitan eldonon de la laboroj de la poeto Thomson; Beckett,
bazante sin sur la antaŭa decido favora al Millar, ricevis juran
ordonon kontraŭ li. Donaldson tiam apelaciis al la Ĉambro de Lordoj,
speco de tiama supera Kortumo, kiu trafis decidojn, kiuj kutime
kunvenigis «adeptojn» en ambaŭ flankoj. Per plimulto de du kontraŭ unu
la Ĉambro de Lordoj decidis favore al Donaldson kontraŭ la argumento de
la daŭraj aŭtorrajtoj &mdash; kiujn, kvin jarojn antaŭe, la juĝisto
Mansfield atakis favore al Millar. La lordoj tiam akceptis la pledon de
la advokatoj de la skota libristo: ĉiuj ajn rajtoj, kiuj antaŭe
ekzistis, bazitaj sur la <i lang="en">common law</i>, finiĝis per la
Statuto de Anne, kiu tiam ekestigis la unuan juran regulon por la kopirajto
de presitaj eldonoj. Post la fino de la periodo fiksita per la statuto
(14 aŭ 21 jaroj, depende de la kazo) la laboroj, kiuj origine estis
protektitaj &mdash; tiuj de aŭtoroj kiel William Shakespeare kaj John
Milton ekzemple &mdash; perdis tiun protekton kaj povis esti libere
uzitaj, adaptitaj kaj komercitaj, ĉar ili iĝis publika havaĵo
&mdash; nocio, kiu kvankam ekzistas ekde la grekaj kaj romiaj[^48],
komencis en tiu momento esti validigita unue en la historio de la jura
angla-saksa sistemo.

[^47]: Detalita en Lessig, <i lang="lt">op. cit.</i>, p. 83.
[^48]: Estas malsamaj versioj de la deveno de la publika havaĵa ideo en
  Okcidento. Unu el la plej akceptitaj referencas la proprietajn rajtojn
  en Romio, kie estis difinoj de <i lang="lt">res
  nullius</i> («aferoj, kiuj ne povas esti proprigitaj»), <i lang="lt">res
  communes</i> («aferoj, kiuj povus esti komune ĝuitaj de la homaro,
  kiel la aero, la sona lumo kaj la maro), <i lang="lt">res
  publicae</i> («aferoj, kiuj estis kunhavigitaj de ĉiuj la civitanoj»)
  kaj <i lang="lt">res universitatis</i> («aferoj, kiuj estis proprieto
  de la municipoj de Romio»). La termino devenas el tiuj konceptoj kaj
  estis disputita konkurante kun aliaj similaj, kiel
  <i lang="lt">publici juris</i> aŭ <i lang="fr">propriété publique</i>,
  en la 18-a jarcento, ĝis disvastiĝi kaj esti leĝe adoptita post la
  Konvencio de Berna (vidu sekvan ĉapitron). Pri la origino de la
  publika havaĵo, vidu Huang, <cite>On Public Domain in Copyright Law,
  Frontiers of Law in China</cite>, v. 4, p. 178-195, kaj Torremans,
  <cite>Copyright Law: a Handbook of Contemporary Research</cite>.

### IV.

La nocio de aŭtorrajto, kiu aperis en la epoko de la Statuto de Anne
estis specifa: malpermesis al aliuloj reeldoni presitan libron. Ĝi estis
rajto ligita al havaĵo, kiu siavice direkte rilatis al teknologio, kiu
ne estis produktita &mdash; en tiu epoko presmaŝinoj de moveblaj
tipoj. En la 18-a jarcenta Anglio la aŭtorrajto ankoraŭ limiĝis determini
kiu, kaj dum kiom da tempo, povus kopii kaj distribui kulturan havaĵon
en presita formo. Ĝi ne menciis rajtojn por la aŭtoroj, kiel la pago
kontraŭ verko aŭ la eblo adapti ĝin, nek citis aliajn artojn aŭ formojn.
Kvankam la anglaj libristoj parolis pri la aŭtora protekto en siaj juĝaj
defendoj, ĝi estis pli unu ruzo por protekti interesojn de certaj
grupoj, kiuj komenciĝis industriiĝi, ol jura protekta sistemo por kiu
kreis[^49].

[^49]: Wu Ming, <cite>Notas inéditas sobre copyright e copyleft</cite>,
  <i lang="lt">op. cit.</i>

Por la itala kolektivo Wu Ming, la aŭtorrajta leĝo de la Statuto de Anne
naskiĝis pro la cenzura preventa neceso kaj limigo de aliro al la
kulturaj produktadrimedoj &mdash; do pro la reteno de la cirkulado de
ideoj. La intenco de la presistoj klopodante la kreon de la Statuto de
Anne de la angla Parlamento estus agnoski la legitimecon de iliaj
interesoj kaj krei regularon, kiu laborus favore al ili. La argumento ĉi
tie estas: «la aŭtorrajto apartenas al la aŭtoro; la aŭtoro tamen ne
posedas presmaŝinojn; tiuj maŝinoj apartenas al la eldonisto; do
aŭtoro necesas la eldoniston. Kiel reguli ĉi tiun neceson? Simple: la
aŭtoro, interesita ke ria verko estu eldonita, liveras la rajtojn
al la eldonisto dum certa periodo. La ideologia defendo ne plu baziĝas
sur cenzuro, sed sur la merkata neceso»[^50].

[^50]: Nimus, <cite>Copyright, copyleft e os creative
  anti-commons</cite>, p. 42.

La kreo de sistemo, kiu regulis ne nur la ekskluzivajn rajtojn kopii,
presi kaj vendi iun verkon, sed ankaŭ la *proprieton* de la ideoj,
naskiĝis preskaŭ en la sama periodo, sed en la alia flanko de la Manika
Markolo.

