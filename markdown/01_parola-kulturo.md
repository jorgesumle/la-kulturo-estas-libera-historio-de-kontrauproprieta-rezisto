<section class="citas">
<i>Mi konfidas al vi, Kvintiano, miajn libretojn. Se mi povas nomi miaj
tiujn, kiujn deklamas poeto amiko via. Se ili plendas pri siaj doloraj
sklavecoj, helpu ilin tute. Kaj kiam tiu proklamis sin sia posedanto,
diru, ke ili estas miaj kaj ke ili estis liberigitaj. Se vi parolas
sufiĉe laŭte tri- aŭ kvarfoje, vi faros, ke la plagiatisto hontu.</i>

<div class="right-align">
Marko Valerio Marcialo, <cite>Epigramoj</cite>, 1-a jc.
</div>

<br>

<i>La imitado estas esenca, la fabrikado estas danĝera, la materio estas
kolektiva proprieto.</i>

<div class="right-align">
Iu, 1-a jc.
</div>

<br>

<i>La plej bonaj ideoj estas de ĉiuj. Do ĉar tio, kio estas de ĉiuj, estas
ankaŭ de ĉiu, ĉiu vero apartenas al mi; ĉio, kio estis bone dirita de iu,
estas ankaŭ mia.</i>

<div class="right-align">
Seneko, 2-a jc.
</div>

<br>

<i>La fortigado de la divina atribuo al la aŭtoreco iĝas normo en la
Mezepoko post la 7-aj kaj 8-aj jarcentoj. Estas tendenco progresema
konsideri ĉiun tekston kiel parton de granda diskurso, lingva komunumo,
kiu diluis la specifan en ĝenerala signifo favora al analogio, la
stereotipita esprimo de la rekombinado de antaŭekzistantaj aroj.</i>

<div class="right-align">
Kevin Perromat, <cite>El plagio en las literaturas hispánicas</cite>, 2010
</div>
</section>

<br>

### I.

Ŝajnas, ke la artaĵoj produktitaj en la greka kaj romia antikva epoko ne
donis specialan atenton al la afero pri la proprieto ligita al la
kulturo. Ne estas vestiĝoj de referencoj al juraj kodoj, protektoj,
sankcioj aŭ rajtoj pri produktado kaj disvastigado de kulturaj verkoj
similaj al tiuj, kiujn ni hodiaŭ havas. Kiel skribas Kevin Perromat[^5]
en sia vasta studo pri plagiato en la hispana literaturo, estas diversaj
faktoroj, kiuj igus, ke ni deduktu, ke ambaŭ civilizacioj, kun
verkistoj, dramistoj kaj filozofoj, kiuj post pli ol du mil jaroj
ankoraŭ estas konataj kaj legataj en la tuta mondo, kun abundo da
artistoj, komercistoj kaj precipe juristoj (speciale la romianoj), povus
klopodi reguli la produktadon kaj disvastigon de kulturaj havaĵoj. Sed
ili ne tion faris &mdash; aŭ tion faris per maniero, per kiu registroj
ne konserviĝis ĝis hodiaŭ. Kial?

[^5]: Perromat, <cite>El plagio en las literaturas hispánicas</cite>, p.
  24.

La unua kialo estas, ke en la romia kaj greka civilizacioj, la rakontoj
apartenis al komuna tradicio, kaj tio permesis rilatojn laŭ iliaj
diversaj proparolantoj kaj la situacioj, en kiuj ili estas rakontitaj.
Ne eblis spuri iliajn precizajn originojn kaj multe malpli malhelpi ilian liberan
disvastigon. La poeta kreado estis ekzemple nature flua, kaj kvankam la
rakontisto de specifa historio povus esti rekonita, sia kontribuo ne
estis konsiderita kiel frukto de sia individueco, sed de kolektiva
kulturo, el kiu li estis parto. Tio, kion oni povus aldoni al la poemo
ne estis registrita por la estonteco; ne estis tiu zorgo[^6], kiel ankaŭ
ne estis kontrolo sur la produktadon kaj la disvastigon de verko.


[^6]: Martins, <cite>Autoria em rede: os novos processos autorais através das redes eletrônicas</cite>, p. 28.

En socio precipe analfabeta, la celo de kultura verko &mdash; kaj
teatraĵo kaj muzikaĵo aŭ poemo, sed ankaŭ politikaj tekstoj &mdash;
estis la publika disvastigo en placoj, teatroj, stratoj, tribunoj. Kaj
la skribo kaj la lego estis por malmultaj; la ideoj kaj la kulturo
disvastiĝis per la voĉo &mdash; kaj per la reproprigado &mdash; de ĉiu,
kiu parolis. La greka filozofo Sokrato, unu el la patroj de la okcidenta
filozofio kaj kiun ni konas nur per tekstoj de aliaj, ja diras en
<cite>Fedro</cite>, dialogo kompilita de Platono ĉirkaŭ 370 a.K., ke la
skribo estis malgajno rilate al la parolo, laŭ li pli taŭga por konservi
viva la penson. Sokrato parolis pri la minaco, kiun la skribo
reprezentis por la konservado de la funkcioj de la memoro, kiu iĝus
subuzita kaj des pli perdus sian povon ju pli la registroj estus
translokitaj al la papero[^7].

[^7]: <i lang="lt">Ibidem</i>, p. 78.

Ankaŭ ne estis solida nocio de aŭtoro, almenaŭ en la jura medio, kio nur
okazis post la 18-a jarcento. La aŭtoreco estis ĉefe kolektiva, atribuita
al specifa kulturo aŭ al la dioj, frukto de dia inspiro aŭ de komunuma
konstruo, en kiu pli gravis la enhavo kaj kion ĝi povus lernigi ol ĝia
proparolanto. Homero, al kiu oni atribuas la skribo de la klasikaĵoj
<cite>Iliado</cite> kaj <cite>Odiseado</cite> ĉirkaŭ la 8-a kaj 7-a
jarcentoj a.K. estas ekzemplo de tiu periodo: ne estas pruvoj pri la
dato de kreado de tiuj du verkoj nek ke estis homo nomita Homero, kiu
ilin skribis. La rakonto de la Troja Milito kaj la diversaj malfacilaĵoj
de la vojaĝo de Odiseo revene al lia naskiĝa Itako, rakontaj kernoj de
la <cite>Iliado</cite> kaj la <cite>Odiseado</cite> respektive, estas
historioj, kiuj kondensas manierojn vidi la mondon kaj instruojn, kiuj
reprezentas komunan manieron pensi ĉe la grekoj, radikitan en la tiama
kulturo. Homero estas arketipo, konstruita poste, kaj kiu plenumis
funkciojn kiel tiun de *lingva horizonto* aŭ «patro kaj avo» de la
poetoj, figuro preskaŭ mistika, kiun koncernis rakonti historion, kiun,
konatan kaj kreitan de multaj, estis konsiderita kiel de aŭtoreco de
dioj, muzoj, naturaj estaĵoj[^8].

[^8]: Perromat, <i lang="lt">op. cit.</i>, p. 24.

Kun la disvastiĝo de la skribo post la 7-a jarcento a.K., naskiĝas,
paralele kun malferma kaj kolektiva speco de kreado, registroj de
individuigita esprimo kaj de deziro de aŭtoreca agnosko, kiu transiris
la klopodon de unua provo de kontrolo de la disvastigo de verko[^9]. Unu
el la unuaj, kiuj klopodis kontroli la disvastigon de ria produktaĵaro
en tiu periodo estas Teognis de Megara, greka poeto, kiu vivis en la 6-a
jarcento kaj kiu aldonis, al siaj manuskriptaj eldonoj, sigelon: «Ĉi
tiuj estas versoj de Teognis de Megara». Lia intenco per tiu
malnova <i lang="en">trademark</i> (registrita varmarko) ne estas gajni
profiton per la vendo de siaj verkoj, sed ke oni ne modifu ilin kaj
pensu, ke ili ne estis liaj, kio forigus la agnoskon por la laboro, kiun
li faris[^10].

[^9]: <i lang="lt">Ibidem</i>, p. 30.
[^10]: <i lang="lt">Ibidem</i>, p. 27, citante ankaŭ historiiston de la
  3-a jc., Klemento de Aleksandrio (kies verko estis eldonita poste,
  <cite>Omnia quae extant opera</cite>).

En tiu epoko la aŭtoroj komprenis, ke la plej bona maniero esti
financita de regantoj kaj de la greka aristokrataro estis esti
konsideritaj «specialistoj» kaj havi verkojn atribuitajn al ili. Jen
dua spuro, kiu klarigas la kialon, ke la grekoj kaj romianoj ne
speciale atentis la aferon de la proprieto ligita al la kulturo: la
manko de merkato por kulturaj havaĵoj. La aŭtoroj ne vivtenis per la
rekta enspezo de siaj verkoj; ili loĝis provizante servojn de instruado
por la aristokrataro, konsilojn por la regantoj, lernigadojn de diversaj
aferoj &mdash; ĉiuj agoj ligitaj al la ĉeesto kaj parola komuniko. En la
diskursa greka-romia tradicio la rekompencoj al la aŭtoroj estis
simbolaj, de socia agnosko de ago, kiu ne ĉesis esti elita kaj
aristokrata[^11] &mdash; malmultaj ja povis legi &mdash;, sed ankaŭ de
specifaj specoj de nerektaj agnoskoj, kiel premioj en oficialaj
konkursoj kaj materiaj avantaĝoj venintaj de la premioj.

[^11]: <i lang="lt">Ibidem</i>, p. 29.

### II.

En Grekio en la epoko konata kiel helenisma (4a a.K.-2a a.K.) ne estis
kontrolo pri la destino de libro post esti eldonita, estu rilate al la
nombro de ekzempleroj aŭ al la modifo de ĝia enhavo. Krom esti kulturo,
kiu valoris la kolektivan kaj parolan ĝuon kaj kreadon, aliaj faktoroj
klarigis tiun mankon de kontrolo: la formo de la tiama libro. Ĉi tiu
havis formaton konatan kiel *volumon*, konsistantan el longa strio de
papiruso aŭ pergameno, kiu estis legita dum ĝi estis malvolvita de iu,
per du manoj, povante havi kelkajn metrojn de longeco kaj pezi iujn
kilojn. Formo peza kaj multekosta, estis malfacile reprodukti, prizorgi
kaj stoki ĝin, kio igis la libron varo relative malabunda en tiu epoko,
kiel estas montrita en tekstoj de Aristotelo kaj de Platono[^12].

[^12]: Putnam, <cite>Authors and their Public in Ancient Times</cite>,
  citita en Perromat, <i lang="lt">op., cit.</i>, p. 32.

La apero de la unuaj bibliotekoj en la greka-romia mondo, post la 4-a
jarcento, estigas komencon por politiko de kontrolo de la tekstoj kaj
manuskriptoj. Por kontroli la tekstojn kaj akiri kopion necesas
garantii, kio estas aŭtentika kaj kio ne, kio motivas al la bibliotekojn
esplori por atribui ĝuste la aŭtorecon de la eldonoj[^13]. Kreita en tiu
periodo, la legenda Biblioteko de Aleksandrio, en Egipto, estis unu el
la ĉefaj iniciatoj, kiu celis organizi la scion kaj la kulturon
produktitan ĝis tiam; ĝi faras tion per la elekto de *kanono* de verkoj
de la greka kulturo de la saĝuloj kaj bibliotekistoj distingitaj en la
nomita *Skolo de Aleksandrio*[^14]. Per sistemo de kritika redaktado,
kiu komparis malsamajn partojn de verko serĉante ĝian tekstan koherecon,
ili estigis la versiojn, kiujn ni konas, de multaj tekstoj de la greka
kulturo, kiuj estus ankaŭ la bazo por la «rustika Romio», kiu heredas la
«kulturan tavolon» de la antikvaj grekaj kaj dum la sekvaj jarcentoj
igis tiun kulturon propra, miksante ĝin kun aziaj kaj afrikaj referencoj.

[^13]: Long, <cite>Openness, Secrecy, Authorship: Technical Arts and the
  Culture of Knowledge from Antiquity to the Renaissance</cite>, p. 30.
[^14]: Skolo de Aleksandrio estas kolektiva nomado por iuj tendencoj en
  literaturo, filozofio, medicino kaj sciencoj, kiuj disvolviĝas en la
  helena kulturejo de la same nomita urbo, en Egiptio, dum la greka kaj
  romia periodoj. Kalimako (310-240 a.K.) estis la unua bibliotekisto,
  al kiu estas atribuita la prilaboro de la katalogo de la Biblioteko de
  Aleksandrio.

Ankaŭ en Romio oni vidas kreski la povon de la aŭtora nomo kiel sigelo
de kredebleco por produktitaj kulturaj havaĵoj, kio ankoraŭ ne signifas
juran protekton nek kontrolon de la aŭtoro pri la disvastigo de ria
verko. Estas ekzempla de tiu momento la historio de la poeto Vergilio,
kiu, en la 1-a jarcento, estis dungita de la imperiestro Augusto por krei
epopeon pri la fondado de Romio imitante la modelojn de la
<cite>Iliado</cite> kaj la <cite>Odiseado</cite>. Premante la poemojn
atribuitajn al Homero kiel bazon, li rekreas la vojaĝo de Uliso per
Eneo, batalanto kiu, post partopreni en la Troja Milito, venas al la
Itala Duoninsulo kaj tie alfrontas aron de aventuroj por transloĝiĝi.
Estas konata la historio, en kiu en la fino de lia vivo Vergilio ordonis
la detruon de la <cite>Eneado</cite> pro konsideri ĝin verko de politika
propagando de Aŭgusto kaj pro ne havi poetan perfektecon, kiun li ŝatus.
Tamen la <cite>Eneado</cite> ne estis detruita; lia rajto kiel aŭtoro
kontroli lian produktaĵon ne estis respektita, kio estas reprezenta de
la sento de epoko, en kiu la rajto morala kaj estetika de komunumo ĝui
verkon de la artisto estis prioritata super aŭtora volo[^15].

[^15]: Perromat, <i lang="lt">op. cit.</i>, p. 32.

La komuna penso de la romia socio rilate al la posedo kaj la aŭtoreco
de la kulturaj havaĵoj montris sin en tri konceptoj identigitaj per klasika
esploro de la angla Harold Ogen White[^16]: «La imitado estas esenca, la
fabrikado estas danĝera, la materio estas kolektiva proprieto». Tiuj
nocioj videblas en esprimoj kiel «<i lang="lt">Oratio publicata, res
libera est</i>» («tio publikigita apartenas al ĉiuj»), atribuita al
Aŭrelio Kvinto Simako, verkisto de la Romio de la 4-a jarcento p.K.; aŭ
kiel la frazo «la plej bonaj ideoj estas de ĉiuj. Do ĉar tio, kio estas
de ĉiuj, estas ankaŭ de ĉiu, ĉiu vero apartenas al mi; ĉio, kion estis
bone dirita de iu, estas ankaŭ mia»[^17], de Seneko, filozofo kaj
verkisto de la 1-a jarcento p.K.

[^16]: White, <cite>Plagiarism and Imitation During the English
  Renaissance: A Study in Critical Distinctions</cite>, citado em
  Perromat, <i lang="lt">op. cit.</i>, p. 44.
[^17]: <i lang="lt">Ibidem</i>

Tamen la serĉo por la kontrolo de la aŭtenteco de la verkoj komencas,
dum la sekvaj jarcentoj, kaŭzi ŝanĝojn, kiuj kontrastis kun la rekombina
kulturo de parola tradicio, kiu konsideris la ideojn kaj la verkojn kiel
komunan proprieton. Inter iuj tiamaj romiaj filozofoj kaj verkistoj,
kiel Cicerono, Horacio kaj Seneko, la diskuto ĉirkaŭ la ideo, ke nur la
imitado sole ne sufiĉas, necesas, ke estu krea imitado[^18]. La plagiato
kiel diskuto kaj kiel vorto aperas en tiu periodo sekve de Marko Valerio
Marcialo (40-104). Protektita de la aristokrataro kaj de la
imperiestroj, li estis poeto tre populara en tiu periodo, sed, kiel ĉiuj
artistoj samtempaj de li, li ne vivis el la vendo de sia verko. Liaj
satiraj kaj mem-referencaj diskursoj famiĝis &mdash; iuj el tiuj parolas pri
limigi la aliron al liaj skribaĵoj kontraŭ pago, kio anticipas la ideon
de artaĵo en merkataj konceptoj[^19]. Registro de lia pozicio estas en
unu el liaj miloj de epigramoj, speco de komento &mdash; mallonga kiel
<i lang="en">tweet</i> de la 21-a jarcento &mdash; ironia, multfoje
eskatologia kaj obscena, pri iu aŭ iu okazaĵo: «La popolo diras, ke vi,
Fidentino, deklamas miajn libretojn kvazaŭ ili estus viaj. Se vi volas,
ke oni diru, ke ili estas miaj, ni sendos al vi senkoste la poemojn. Se vi
volas, ke oni diru, ke estas viaj, aĉetu ilin, por ke ili ne plu estu
miaj»[^20].

[^18]: White, <cite>Plagiarism and Imitation During the English
  Renaissance: A Study in Critical Distinctions</cite>, citita en
  Perromat, <i lang="lt">op. cit.</i>, p. 44.
[^19]: Perromat, <i lang="lt">op. cit.</i>, p. 42.
[^20]: Marcial, <cite>Epigramas</cite>, en trad. de Rodrigo Garcia
  Lopes.

Laŭ Perromat[^21], la invento de la termino *plagiato* laŭ la moderna
signifo estas atribuita al Marcialo, ĉar antaŭ tia ago, kiam eltrovita,
estis nomita «ŝtelo» aŭ «rabo» de ideoj. La romia verkisto kreas la
esprimaĵon sekve de la latina verbo <i lang="lt">plagiare</i>, kiu en la
latina signifas «fraŭde revendi sklavon aŭ idon de iu kiel propran»,
delikto kiu en tiu epoko estis punita per skurĝado. Marcialo uzas la
terminon kun la nova signifo en alia el siaj epigramoj: «Mi konfidas al
vi, Quinciano, miajn libretojn. Se mi povas nomi miaj tiujn, kiujn
deklamas poeto amiko via. Se ili plendas pri siaj doloraj sklavecoj,
helpu ilin tute. Kaj kiam tiu proklamis sin sia posedanto, diru, ke ili
estas miaj kaj ke ili estis liberigitaj. Se vi parolas sufiĉe laŭte tri-
aŭ kvarfoje, vi faros, ke la plagiatisto hontu»[^22].

[^21]: Perromat, <i lang="lt">op. cit.</i>
[^22]: Neoficiala traduko de la hispana versio: «Te encomiendo,
  Quinciano, mis libritos. Si es que puedo llamar míos los que recita un
  poeta amigo tuyo. Si ellos se quejan de su dolorosa esclavitud, acude
  en su ayuda por entero. Y cuando áquel se proclame su dueño, di que
  son míos y que han sido liberados. Si lo dices bien alto tres o cuatro
  veces, harás que se avergüence el plagiario». 53-a Epigramo, en
  Marcialo, <cite>Épigrammes</cite>, p. 70-71, tradukita el la franca al
  la hispana de Perromat, <i lang="lt">op. cit.</i>, p. 47.

Tamen la kreskanta diskuto ĉirkaŭ la plagiato en tiu epoko estis farita
en la medio de la moralo kaj la estetiko, ne en la jura aŭ kriminala
senco. Multaj el la intelektulaj debatoj, kiuj okazis en la tiama romia
socio montris kreskantan negativan juĝon pri la imitado sen kreo, la
pura kaj simpla kopio, sed ili ne iĝis centraj en la romia literatura
kritiko[^23]. Ne estas registro de leĝoj, kiuj estis faritaj por reguli
aŭ puni tiujn agojn. Tio povis okazi, kaj en Grekio kaj en Romio, ankaŭ
pro la materiala malfacileco de reproduktado, financa obstaklo
konsiderinda por la disvastigo de verkoj konsideritaj kiel plagiatitaj
kaj kiuj verŝajne ne helpis instigi la kreadon de reguloj por tio. Alia
faktoro estas, ke la tiamaj juraj sistemoj ne konsideris la artajn verkojn
kaj iliajn materialojn aparte. Por la grekoj ekzemple, kiu aĉetu verkon
&mdash; kontraŭ financa kosto, verŝajne biblioteko aŭ ano de
aristokrataro &mdash; povis uzi kaj modifi verkon laŭvole, sen ke la
aŭtoro enmiksiĝus iel. La kolektiva poseda rilato de la kulturaj havaĵoj
kaj la malesto de leĝoj, kiuj regulu kaj punu la agojn konsideritajn
kiel rabojn de ideoj kaj publikaĵoj, daŭris ĝis la 16-a jarcento, kiam unue
okazis ŝtata koncesio (monopolo), kiu garantiis privilegiojn por presi
tekston &mdash; por Stationer’s Company, en Anglio &mdash; kaj estis
dekretita la Statuto de Anne, la unua leĝo de intelekta propraĵo, de
1710, ankaŭ angla.

[^23]: Perromat, <i lang="lt">op. cit.</i>, p. 43.

### III.

La periodo, kiu daŭras el la fino de la Romia Imperio ĝis la Frua
Mezepoko, estas tiu de la alveno de la kristanismo kaj la alfronto kaj
posta asimilado de la greka-romia civilizacio. Por la kultura
produktado, la rezulto estis diversa. Perromat kaj aliaj
historiistoj[^24] diras, ke preskaŭ ĉiuj versioj, kiujn ni hodiaŭ
konservas de la grandaj verkistoj kaj pensuloj de Grekio kaj Romio
estas adaptoj kaj modifoj faritaj en tiu periodo, se ili ne estas
falsaĵoj, kiuj poste estis atribuitaj al grandaj famuloj de la Antikva
Epoko pro la prestiĝo, kiun tiu montro poste ekhavis.

[^24]: <i lang="lt">Ibidem</i>, p. 50.

Alia grava kialo tiam por la ŝanĝo de la manieroj produkti kaj
disvastigi la kulturajn havaĵojn en tiu epoko okazas pro la transformado
de la materia formo de la verkoj. La falo de imperio, kiu etendiĝis al
preskaŭ la tuta Eŭropo, kiun ni hodiaŭ konas, signifas ankaŭ rompiĝon de
komercaj itineroj kaj malabundeco de iuj resursoj, kio okazas kun la
ĉefa materialo uzita por la fabrikado de libroj en la greka-romia mondo,
la papiruso, eltirita el la planto <i lang="lt">Cyperus papyrus</i>, de
la familio de la ciperacoj. Materialo multekosta kaj de malforta
rezisto, venis el la komerco kun la nordo de Afriko (precipe Egiptio)
kaj kun Mezoriento, kiu malkreskis konsiderinde post la falo de la
Romia Imperio. Do, ĉirkaŭ la 4-a kaj 5-a jarcentoj, la libroj komencis
esti produktitaj ĉefe en pergamenoj, nomo donita al besta haŭto, precipe
de kapro, ŝafiĉo, ŝafido aŭ ŝafino, preparita por skribi sur ĝi
&mdash;materialo, kiu kvankam nedaŭra kaj multekosta, komencis esti la
ĉefa utiligita en la fabrikado de libroj. Ankaŭ en tiu periodo, ili ĉesis
esti produktitaj kiel ruloj, sed en *kodeksa*[^25] formo, proksima al la
libro kiel ni ĝin konas en la 20-a jarcento. La historiisto Alberto
Manguel diras, ke en la 12-a jarcento la teknologio por la fabrikado de
Biblia volumeno (Malnova kaj Nova Testamentoj) bezonis haŭtojn de preskaŭ
ducent bestoj[^26].

[^25]: Ĝi konsistas el kajeroj falditaj, kudritaj kaj ligitaj, skribitaj
  en ambaŭ flankoj de folio nombrita, formato ĝis hodiaŭ normo por la
  produktado de libroj.
[^26]: Manguel, <cite>Una historia de la lectura</cite>, p. 198.

La fortiĝo de la kristanismo en Eŭropo en la sekvaj jarcentoj ankaŭ
venigas kelkajn ŝanĝojn pri la identigo de la aŭtoro. En religio, por
kiu la Biblio estis la (sola) sankta libro kaj ĝia aŭtoreco[^27] estis,
finfine, de Dio, la volo de la verkistoj kaj ilia supozita unueco
&mdash;kiu eĉ instigis komencajn diskutojn inter grekoj kaj romianoj
&mdash; komencis dependi de la vero de sola «aŭtoro», Dio. La fortiĝo de
la dia atribuo de la aŭtoreco iĝas normo en la Mezepoko post la 7-a kaj
8-a jarcentoj; estas pli kaj pli tendenco konsideri ĉiun tekston parto de
unu granda diskurso, «unu “lingva komunumo”, kiu diluis la apartan en
ĝenerala signifo ebla por antologio, la stereotipa esprimo de la
(re)kombinado de antaŭe ekzistantaj eroj»[^28], maniero skribi kaj legi
kun elstare kolektiva karaktero.

[^27]: Librara bazo de la kristianismo kaj de la judismo, la Biblio
  estas kialo de multaj disputoj pri aŭtoreco. Klopodante la firmiĝon de
  la juda-kristanajn dogmojn, multaj el la unuaj bibliaj skribantoj
  bazis sur aliaj tekstoj, grekaj kaj de aliaj religioj, kompilante ilin
  kaj adaptante ilin laŭ la celoj de la evangeliado de la loĝantaro
  &mdash; por tio ili estos sukcesaj en la sekvaj jarcentoj, atingante
  la disvastiĝon de tiuj dogmoj al granda parto de la mondo. Vidu
  <https://en.wikipedia.org/wiki/Bible>.
[^28]: Perromat, <i lang="lt">op. cit.</i>, p. 60.

Solidiĝas diskurso, en kiu diversaj homoj adaptas liberege tekstojn (kaj
nun ni povas ankaŭ diri muzikon kaj teatron) por specifaj celoj, precipe
por konvinki uzante iun ideon kaj estigi moralan kaj etikan sekvotan
ekzemplon. Paul Zumthor, en sia <cite>Essai de poétique
médievale</cite>[^29], diras, ke la teksto en tiu periodo funkcias
memstare de siaj cirkonstancoj; la aŭskultanto (la plejmulto de la
verkoj tiam ankoraŭ estis parolaj aŭ paroligitaj) atendis nur ĝian
laŭvortecon, tio estas, la signifon de la «mesaĝo», kaj ne kondamnis la
formajn ŝanĝojn, kiuj konservu tiun signifon &mdash; estas pli verŝajne,
ke, antaŭ analfabeta plimulto, ŝanĝoj ne estu perceptitaj. Do la
mezepokaj aŭtoroj havis kiel metodon la kutiman uzon de materialoj de
aliuloj per aludo, interpolado aŭ parafrazado kaj, precipe, ne specifis
iliajn devenojn. Konsiderindaj tekstaj fragmentoj, kelkfoje tre antaŭaj,
estis simple enmetitaj en novajn verkojn, antaŭestantaj poemoj estis
tute enmetitaj en literaturaj verkoj[^30]. Sen dokumentoj kaj sen scio
pri la kanono de tiamaj aŭtoroj, iuj el tiuj citaĵoj kaj enmetaĵoj
neniam estis agnoskitaj.

[^29]: Zumthor, <cite>Essai de poétique médievale</cite>.
[^30]: <i lang="lt">Ibidem</i>.

Post la 5-a jarcento, la kristanismo ne nur iĝis la plej disvastigita
fido en Okcidento, sed ankaŭ ĝi regis la tiamajn kulturajn verkojn. Unu
el la sekvoj de tiu kontrolo estis la malapero en la okcidenta Eŭropo,
inter la jaroj 500 kaj 700, de diversaj klasikaj grekaj kaj romiaj
verkoj &mdash; en Bizanco (Konstantinoplo, nuna Istanbulo, en Turkio)
kaj teritorioj kun pli granda islama influo en la sudo kaj oriento tiuj
verkoj restis. Anstataŭante la verkojn konsideritajn kiel nenecesajn
(precipe paganajn) per aliaj religiaj, tiuj kiuj iĝis kaj posedantoj de
la produktadrimedoj kaj de scioj por publikigi kaj kontroli la kulturajn
havaĵojn &mdash; la mezepokaj Eklezio kaj kopiistaj monaĥoj, en la okazo
de la libroj &mdash; kontribuis por malpliigi la kanonon de la Antikva
Epoko[^31]. Per tiu senco ĝis hodiaŭ oni parolas pri la Mezepoko kiel
periodo de kultura mallumigo, dominita de la kristanaj interesoj,
kvankam pri tiu bildo oni dubas de longe fare de historiistoj kiel la franca
Patrick Boucheron, kiu memoras la periodon kiel la «adoleskeco de la
moderna epoko, ĝia naiva aŭ ribelema aĝo» kaj diras, ke, rigardante la
nomitan «Malhelan Epokon» pli proksime, eblas aŭskulti la «akrasona[n]
tintado[n] de ĝoja kaj senorda bruo»[^32]. Nur post la 14-a kaj 15-a
jarcentoj estas, kiam ia klasika tradicio estis reintegrita en la
eŭropan tradicion, speciale tra komercaj regionoj, kiuj konservis azian,
islaman kaj bizancan influon, kiel okazis en la sudo de la Ibera Duoninsulo
kaj de italaj regionoj kiel Sicilio, Napolo kaj Venecio.

[^31]: Por montro de malaperitaj verkoj en tiu periodo vidu Diringer,
<cite>The Book Before Printing: Ancient, Medieval and Oriental</cite>.
[^32]: En Boucheron <cite>Como se revoltar?</cite> Detale ekzamenante,
  kiel la franca historiisto faras en tiu libreto pri la ago ribeli en
  la Mezepoko, ni povas trovi iujn faktojn, kiuj montras la supervivon de
  restaĵoj de ia klera kaj ribelema tradicio kontraŭ la religiaj dogmoj
  en tiu periodo. Ĝi estas ankaŭ la okazo de verkoj de Carlo Ginzburg
  kiel <cite>O queijo e os vermes e Os andarilhos do bem</cite> kaj de
  tio, kio konvencie estis nomita mikro-historio. Bedaŭrinde ĉar
  malmultaj dokumentoj de tiu epoko supervivis, la savo estas malmola
  kaj malrapida laboro.

