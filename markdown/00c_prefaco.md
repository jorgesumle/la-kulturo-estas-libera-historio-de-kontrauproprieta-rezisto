La konceptaro malfermita per ĉi tiu libro, kiun vi havas en viaj manoj,
iras de Okcidento al Oriento, traktante ĉiun trajektorion de la
konjektoj pri la kontrastaj nocioj de intelekta propraĵo kaj publika
havaĵo, el la malnovaj Grekio kaj Romio kaj la Imperia Ĉinio sub la
influo de la konfuceismo, irante tra la Mezepoko kaj la renesanca kaj
klerisma eŭropaj mondoj, tra la moderna tutmondiĝa epoko de ekspansio de
la mondaj horizontoj de la epoko de eltrovoj &mdash; la ekspansio al
Ameriko, Afriko kaj Azio &mdash;, ĝis niaj tagoj, kun la
eksterordinaraj influoj de la modernaj ciferecaj teknologioj en la
produktado kaj disvastiĝo de kulturaj verkoj en la tuta mondo.

Ĉi tiu konceptaro troviĝas, en ĉi tiu libro, detale ilustrita per kelkaj
historiaj rakontoj, kiuj donis korpon kaj animon al la konstruo de la
nomitaj «rajtoj de intelekta propraĵo». La libro traktas tiun konstruon
per multaj rimarkoj pri kiel «homoj, grupoj kaj movadoj renversis staton
de siaj epokoj, de la kreado kaj disvastiĝo de kulturo kaj arto».
Ĝi enhavas ilustraĵojn el la priskriboj pri teknikoj de la papirusa uzo por
la kreado de la unuaj libroj en malproksimaj imperiaj epokoj, tra la
revolucio de la presilo malferminte la postmezepokajn tempojn (kun
Gutenberg), konstatante la novajn ekonomiajn-politikajn-socialajn
diktojn en Britio kaj Francio &mdash; de la 16-a ĝis la 18-a jarcentoj
&mdash; en iliaj transiroj de absolutaj monarkioj al konstituciaj
reĝimoj (kiam naskiĝis la kopirajto kaj la aŭtorrajto), rigardante la
alvenon de la radio (kun Guglielmo Marconi), ĝis fini en la nuntempa
epoko de kino, televidilo kaj Interreto kun ĉio kiu rilatas al ni,
hodiaŭ, kun la nomita *libera kulturo* (la libera programaro, la
specimenilo, la diversaj manieroj kunhavi, ktp.).

Mia rilato kun la vasta koncepta aliro de la libro venas de mia deziro,
ke ĉi tiu estu legita per multfokusa lenso, kiun ĝi bezonas, necesa
por trakti la vastan ambician diversecon de la aŭtoro por trakti unu
el la aferoj plej kompleksaj de la homa kultura historio. La implicoj de
la ekzisto de unu aŭ multaj nocioj de proprieta rajto, rilate al niaj
artaj kaj intelektaj agadoj dum kelkaj periodoj de la disvolviĝo de nia
civilizo, estas fundamenta por la kompreno kiel ni alvenis ĉi tien
kaj kien ni iras kiel homa socio. Ĉi tiu libro, kvankam centrita en
la aŭtorrajto fronte al la publika havaĵo de la ideoj &mdash; afero sole
sufiĉa por okupi tutan konjektan universon &mdash;, informas al ni pri
scio kaj racio, helpas al ni mezuri nian horizonton de homa disvolviĝo per
larĝeco de rigardodiverseco. La libro rigardas al la intelekta
propraĵo, sed montras multe pli: la propran historian nocion de
proprieto, mondon de mankoj kaj riĉecoj de la posedantoj kaj la
senposedigitaj.

Vasta libro pri kulturo, politiko, sociologio, antropologio kaj
historio. Libro de modereca elokvento pri aferoj preskaŭ neniam eĉ iom
moderaj en la homaj diskutoj. Libro por la nuntempo, por la postmoderneco
kaj por la civilizanta estonteco. Por profiti ni legu.

<br>
<p class="right-align">Gilberto Gil</p>

