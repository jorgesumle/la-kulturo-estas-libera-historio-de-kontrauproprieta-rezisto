# La kulturo estas libera: historio de kontraŭproprieta rezisto

Tio ĉi estas traduko de la libro
<a href="https://baixacultura.org/download/13637/"><i lang="pt">A Cultura é
Livre: Uma história da resistência antipropriedade</i></a> de Leonardo
Foletto, kiu eldonis ĝin sub la  <a
href="https://creativecommons.org/licenses/by-sa/4.0/deed.eo"><abbr
title="Atribuite-Samkondiĉe 4.0 Tutmonda">CC BY-SA 4.0</abbr>-licenco</a>.

## Krei la libron

### HTML kaj EPUB

```
sudo apt install pandoc
make html epub
```

### PDF

[Instalu ĉinan tiparon](https://tex.stackexchange.com/questions/168732/how-to-install-correctly-simhei-ttf-and-simsun-ttc-for-pdflatex-on-tex-live-2013) kaj plenumu la sekvajn komandojn:

```
sudo apt install fonts-noto-core fonts-sil-gentium-basic make pandoc texlive texlive-lang-arabic texlive-lang-chinese texlive-lang-other texlive-xetex
make pdf
```

## Kunlabori

La traduko estas skribita per Markdown por poste aliformigi ĝin al kelkaj
dosierformoj per [pandoc](https://pandoc.org/). Ĉiu sekcio de la libro
troviĝas en aparta `.md`-dosiero.

La `Makefile`-dosiero utilas por krei la finajn dosierojn.
`index.html` estas la
[retpaĝo](https://freakspot.net/libro/la-cultura-es-libre/), kiu ligas al
la diversaj formoj de la libro.

## Eraroj de la originala

- Estas 'Teognis de [Mégara](https://pt.wikipedia.org/wiki/M%C3%A9gara)',
  ne de *'Megarar'.
- En \*«<i lang="pt">não existe evidência a respeito da data de criação
  dessas duas obras nem que houve uma pessoa chamada Homero que a
  escreveu</i>», ĝi devus diri «[...] <i lang="pt">as escreveu</i>»,
  ĉar estas du (plurale).
- En la paĝo 66, en \*«Um dos mais importantes divulgadores dessa
  ideias» ĝi devus diri «Um dois mais importantes divulgadores dessas
  ideias» (concordancia).
- En la página 68, en «entre eles o de impressores, estabelecidos desde
  meados do século XVI» no se entiende bien la oración explicativa si no
  se refiere a un singular (el derecho establecido).
- En la página 68, en «os chamados “privilégios do autor” (*privilèges
  d’auteur*), que, diferente dos», debería decir «diferentes dos» para
  concordar.
- En la página 75, el título «III» debería tener un tamaño más grande.
- En la paĝo 81, estas «calótipo», ne \*«celótipo».
- En la paĝo 81, estas «William Henry Fox», ne \*«William Heny Fox».
- En la página 82, es «cinetoscópios», no \*«cinestocópios».
- En la página 89, es «calótipo», no \*«celótipo».
- En la página 107, falta un espacio después del punto en
  «poème.Découpez» (nota 103).
- En la paĝo 107, en la noto 103, estas «<i lang="fr">dans un sac</i>», ne \*«<i lang="fr">dans um sac</i>».
- En la página 109, en la nota 106, la URL a la que quiere hacer
  referencia el autor es
  <https://www.nealumphred.com/hugo-ball-sound-poetry-gadji-beri-bimba/>.
- En la paĝo 111, la ĝustaj datoj de la verko de Walter Benjamin estas
  1935 (skribo) kaj 1936 (eldono).
- En la paĝo 124, estas «King Crimson», ne \*«King Crinsom».
- En la paĝo 126, en la enumeración de la cita
  («<i lang="pt">condições de elaborar a montagem de seus projetos,
  fundindo planos; linhas e sombras, sem qualquer instrumento auxiliar
  que a técnica do desenho exige</i>.»)no tiene sentido que haya un
  punto y coma.
- En la página 143 (nota 142), arregla el título del libro de Pekka
  Himanen. Es «espíritu», y no \*«espírito».
- En la página 143 (nota 143), arregla el nombre de la editorial (es
  «Traficantes de Sueños», no \*«Traficante de Sueños»).
- En la página 145 (nota 146), arregla el título del libro, mostrando
  solo el citado en castellano.
- En la página 150 «do» no debería ir en cursiva ya que no forma parte
  del extranjerismo «<i lang="lt">status quo</i>».
- En la página 151, es «Open Access», no \*«Open Acess».
- En la página 153 (nota 159) sobran unas comillas dobles, antes de
  «<i lang="pt">A distinção</i>».
- En la página 154, es «Electronic Frontier Foundation», no \*«Eletronic
  Frontier Foundation».
- En la página 156 (nota 168), el título del libro es <cite>How the
  Commoners Built a Digital Republic of their Own,</cite> (no
  \*«Republico»).
- En la página 163 (nota 183), arregla la URL (es «wired.com», no
  \*«wired.C.om»).
- En la página 166, es «<i lang="pt">tipos de compressão de áudio</i>»,
  y no\*«<i lang="pt">tipos de compreensão de áudio</i>».
- En la página 169 (nota 191), sobran unas comillas de cierre después de
  «<i lang="pt">português</i>.
- En la página 172 (nota 195), es redundante decir «<i lang="pt">mbps
  por segundo</i>».
- En la paĝo 180 la parto ne dirita («[...]») devus esti skribita post
  «<i lang="pt">Nosso mundo é diferente</i>».
- En la página 184, la frase «<i lang="pt">impulsiona a vigilância
  on-line de todos os hábitos de uma pessoa na rede</i>» es redundante.
- En la página 186, los algoritmos de <i lang="en">streaming</i> solo se
  encargan del <i lang="en">streaming</i>, el autor debe de referirse a
  los de la plataforma de <i lang="en">streaming</i>.
- En la paĝo 192, estas Michael Hardt, ne \*'Michel Hardt'
- En la paĝo 223, ne eblas diri \*<i lang="la">res commune</i>, nur
  eblas diri «res communa» (singulara) aŭ «res communes» (plurala).
- En la página 225 (nota 292), arregla en enlace.
- Las palabras estranjeras como <i lang="en">software</i> y
  <i lang="en">hardware</i> se escriben en cursiva.
- En la página 235 arregla el enlace de <cite>The Economy of
  Ideas</cite>.
- En la página 240 arregla el título del libro de Pekka Himanen. Es
  *espíritu*, y no \*espírito.
- En la página 245 no es \*'Madri', sino 'Madrid'.
- En la sección «Referências» la editorial no es \*'Traficante de Sueños', sino 'Traficantes de Sueños'.
- El artículo enlazado en la nota a pie de página 33 ya no existe, así
  que lo he reemplazado por otro que contiene el mismo texto.

## Modifoj

- A diferencia del PDF original, los enlaces a Internet son interactivos.
- En el capítulo 5 no hay apartado IV, Pasa del III al V, así que lo he
  ajustado para que no haya tal salto.
- Cita del libro <cite>Cultura libre: Cómo los grandes medios
  usan la tecnología y las leyes para encerrar la cultura y controlar la
  creatividad</cite> (traducción española, en vez de traducción
  portuguesa, que me parece de peor calidad).
- En el enlace de la página 116 (nota 121), pone el enlace con HTTPS
  para evitar redirección
  (<https://enciclopedia.itaucultural.org.br/termo3654/situacionismo>).
- En la paĝo 142 (piednoto 141) la manifesto ankoraŭ ne estas tradukita
  al Esperanto.
- En la paĝo 157 (piednoto 169) la eseo ankoraŭ ne estas tradukita al
  Esperanto.
- En la paĝo 159 (piednoto 176), la video ankoraŭ ne estas dublita al
  Esperanto.
- En la paĝo 231 oni uzas la oficialan nomon de Creative Commons
  «BY-SA».
- Mi ĝisdatigis jarojn, kiam eblis. Ekzemple «La CC-permesiloj, kiuj
  ekzistas en 2022, estas montritaj en la [...]».
