MARK_DIR=markdown/
SECTIONS=$(MARK_DIR)00a_kovrilo.md $(MARK_DIR)00b_enhavlisto.md $(MARK_DIR)00c_prefaco_tit.md $(MARK_DIR)00c_prefaco.md $(MARK_DIR)00d_prezento_tit.md  $(MARK_DIR)00d_prezento.md $(MARK_DIR)00e_enkonduko_tit.md $(MARK_DIR)00e_enkonduko.md $(MARK_DIR)01_parola-kulturo_tit.md $(MARK_DIR)01_parola-kulturo.md $(MARK_DIR)02_presa-kulturo_tit.md $(MARK_DIR)02_presa-kulturo.md $(MARK_DIR)03_proprieta-kulturo_tit.md $(MARK_DIR)03_proprieta-kulturo.md $(MARK_DIR)04_rekombina-kulturo_tit.md $(MARK_DIR)04_rekombina-kulturo.md $(MARK_DIR)05_libera-kulturo_tit.md $(MARK_DIR)05_libera-kulturo.md $(MARK_DIR)06_kolektiva-kulturo_tit.md $(MARK_DIR)06_kolektiva-kulturo.md $(MARK_DIR)90_epilogo_tit.md $(MARK_DIR)90_epilogo.md $(MARK_DIR)91_referencoj_tit.md $(MARK_DIR)91_referencoj.md $(MARK_DIR)92_dankoj_tit.md $(MARK_DIR)92_dankoj.md $(MARK_DIR)93_pri-la-aŭtoro_tit.md $(MARK_DIR)93_pri-la-aŭtoro.md
SECTIONS_PDF=$(MARK_DIR)00c_prefaco.md $(MARK_DIR)00d_prezento.md $(MARK_DIR)00e_enkonduko.md $(MARK_DIR)01_parola-kulturo.md $(MARK_DIR)02_presa-kulturo.md $(MARK_DIR)03_proprieta-kulturo.md $(MARK_DIR)04_rekombina-kulturo.md $(MARK_DIR)05_libera-kulturo.md $(MARK_DIR)06_kolektiva-kulturo.md $(MARK_DIR)90_epilogo.md $(MARK_DIR)91_referencoj.md $(MARK_DIR)92_dankoj.md $(MARK_DIR)93_pri-la-aŭtoro.md
SECTIONS_EPUB=$(MARK_DIR)00c_prefaco_tit.md $(MARK_DIR)00c_prefaco.md $(MARK_DIR)00d_prezento_tit.md  $(MARK_DIR)00d_prezento.md $(MARK_DIR)00e_enkonduko_tit.md $(MARK_DIR)00e_enkonduko.md $(MARK_DIR)01_parola-kulturo_tit.md $(MARK_DIR)01_parola-kulturo.md $(MARK_DIR)02_presa-kulturo_tit.md $(MARK_DIR)02_presa-kulturo.md $(MARK_DIR)03_proprieta-kulturo_tit.md $(MARK_DIR)03_proprieta-kulturo.md $(MARK_DIR)04_rekombina-kulturo_tit.md $(MARK_DIR)04_rekombina-kulturo.md $(MARK_DIR)05_libera-kulturo_tit.md $(MARK_DIR)05_libera-kulturo.md $(MARK_DIR)06_kolektiva-kulturo_tit.md $(MARK_DIR)06_kolektiva-kulturo.md $(MARK_DIR)90_epilogo_tit.md $(MARK_DIR)90_epilogo.md $(MARK_DIR)91_referencoj_tit.md $(MARK_DIR)91_referencoj.md $(MARK_DIR)92_dankoj_tit.md $(MARK_DIR)92_dankoj.md $(MARK_DIR)93_pri-la-aŭtoro_tit.md $(MARK_DIR)93_pri-la-aŭtoro.md
LANG=eo
MARKDOWN_ONE_FILE=la-kulturo-estas-libera.md
HTML_OUTPUT=la-kulturo-estas-libera.html
TEX_OUTPUT=la-kulturo-estas-libera.tex
TITLE=La kulturo estas libera: historio de la kontraŭproprieta rezisto
STYLESHEET=style.css

epub:
	cat $(SECTIONS_EPUB) > $(MARKDOWN_ONE_FILE)
	sed -i "s/## /# /g" $(MARKDOWN_ONE_FILE)
	sed -i "s/### /## /g" $(MARKDOWN_ONE_FILE)
	pandoc $(MARKDOWN_ONE_FILE) -f markdown --metadata title="$(TITLE)" -t epub -s -o la-kulturo-estas-libera.epub metadata.yaml --toc -V lang=$(LANG)
	rm $(MARKDOWN_ONE_FILE)

latex:
	cat $(SECTIONS_PDF) > $(MARKDOWN_ONE_FILE)
	sed -i "s/<[\/]*\(i\|cite\)[^>]*>/*/g" $(MARKDOWN_ONE_FILE)
	pandoc $(MARKDOWN_ONE_FILE) -f markdown -t latex -s -o $(TEX_OUTPUT) -V lang=$(LANG)
	./latex_personalizado/ajustes_latex.sh
	rm $(MARKDOWN_ONE_FILE)

html:
	pandoc $(SECTIONS) -f markdown -H header.html -t html --css $(STYLESHEET) --metadata pagetitle="$(TITLE)" -s -o $(HTML_OUTPUT) -V lang=$(LANG)
	sed -i 's|<head|<head prefix="og: http://ogp.me/ns# book: http://ogp.me/ns/book#"|g' $(HTML_OUTPUT)

pdf: latex
	xelatex $(TEX_OUTPUT)

publish: epub html pdf
	scp -r index.html bildoj/ la-kulturo-estas-libera.* style.css root@185.112.144.134:/var/www/html/eo/libro/la-kulturo-estas-libera/

