TEX_OUTPUT=la-kulturo-estas-libera.tex

sed -i '/{article}/r latex_personalizado/documentclass.tex' $TEX_OUTPUT
sed -zi 's/\\documentclass\[[[:space:]]*esperanto,\n*\]{article}/\\documentclass\[\n  11pt,esperanto,twoside\n]{article}/' $TEX_OUTPUT
sed -i 's/\\setotherlanguage\[\]{english}/\\setotherlanguages{arabic, english}\n\\newfontfamily\\arabicfont\[Script=Arabic,Scale=1.1\]{Noto Sans Arabic}/' $TEX_OUTPUT
sed -i 's/\\usepackage{hyperref}/\\usepackage[colorlinks,linktoc=all]{hyperref}\n\\hypersetup{linkcolor=black}/' $TEX_OUTPUT
sed -i '/begin{document}/r latex_personalizado/begin.tex' $TEX_OUTPUT
sed -i 's/La konceptaron malfermitan de ĉi tiu libro, kiun vi havas en viaj manoj,/\\noindent &/' $TEX_OUTPUT
sed -i '/^Gilberto Gil/r latex_personalizado/despues-de-prefacio.tex' $TEX_OUTPUT
sed -i 's/Ĉi tiu libro naskas el peno date de 2008, jaro de la kreado de/\\noindent &/' $TEX_OUTPUT

sed -i '/São Paulo, vintro de 2020/r latex_personalizado/despues-de-presentacion.tex' $TEX_OUTPUT
sed -i 's/^Gilberto Gil/\\\begin{flushright}\n&\\\end{flushright}/' $TEX_OUTPUT
sed -i 's/Leonardo Foletto/\\\begin{flushright}\n&/' $TEX_OUTPUT
sed -i 's/São Paulo, vintro de 2020/&\\\end{flushright}/' $TEX_OUTPUT
sed -i '/libera kulturo hodiaŭ/r latex_personalizado/antes-cap-1.tex' $TEX_OUTPUT
sed -i 's/Marko Valerio Marcialo, \\emph{Epigramoj}, 1-a jc\./\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/Iu, 1-a jc\./\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/Seneko, 2-a jc\./\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break\\newpage/' $TEX_OUTPUT
sed -i '/El plagio en las literaturas hispánicas}, 2010/r latex_personalizado/antes-cap-cit.tex' $TEX_OUTPUT
sed -i 's/Kevin Perromat, \\emph{El plagio en las literaturas hispánicas}, 2010/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/La vorto «kulturo» havas tiom da signifoj dum la historio, ke ni/\\noindent &/' $TEX_OUTPUT

sed -i '/Sicilio, Napolo kaj Venecio\./r latex_personalizado/antes-cap-2.tex' $TEX_OUTPUT
sed -i '/rajtocedo}, 2005/r latex_personalizado/antes-cap-cit.tex' $TEX_OUTPUT
sed -i 's/Enea Silvio Bartolomeo Piccolomini, estonta papo Pio la 2-a, en letero/\\\begin{flushright}\n&/' $TEX_OUTPUT
sed -i 's/al kardinalo Carvajal, 1455/&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/Statuto de Anne, Anglio, 1710/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/Angla parlamento, neante la peton de la librovendistoj por pliigi la/\\\begin{flushright}\n&/' $TEX_OUTPUT
sed -i 's/daŭron de la aŭtorrajta templimo, 1735/&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/Wu Ming, \\emph{Novaj rimarkoj pri la aŭtorrajto kaj la rajtocedo}, 2005/\\\begin{flushright}\n&\\\end{flushright}/' $TEX_OUTPUT

sed -i '/Markolo\./r latex_personalizado/antes-cap-3.tex' $TEX_OUTPUT
sed -i 's/Markizo de Condorcet, \\emph{Fragmentoj pri la gazetarlibereco}, 1776/\\\begin{flushright}\n&\\\end{flushright}/' $TEX_OUTPUT
sed -i '/Thomas Jefferson, en la letero al Isaac McPherson, 1813/r latex_personalizado/antes-cap-cit.tex' $TEX_OUTPUT
sed -i 's/Thomas Jefferson, en la letero al Isaac McPherson, 1813/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT

sed -i '/konataj en la 20-a jarcento kaj ĝis hodiaŭ\./r latex_personalizado/antes-cap-4.tex' $TEX_OUTPUT
sed -i 's/Grafo de Lautréamont (Isidore Lucien Ducasse), \\emph{Poesias}, 1870/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -zi 's/\\emph{Rajto esti tradukita, ludita kaj deformita en ĉiuj lingvoj.}/\\\begin{flushright}\n&\n\\\end{flushright}/' $TEX_OUTPUT
sed -i 's/Oswald de Andrade, \\emph{Serafim Ponte Grande}, 1933/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -zi 's/Guy Debord; Gil Wolman, \\emph{Um guia para os usuários do detournamènt},\n1956/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/\\emph{La rimedo estas la mesaĝo\.}/\\\begin{flushright}\n&\n\\\end{flushright}/' $TEX_OUTPUT
sed -i 's/Marshall McLuhan, \\emph{Understand Media}, 1964/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i '/Friedrich A. Kittler, \\emph{Gramofone, filme, typewritter}, 1986/r latex_personalizado/antes-cap-cit.tex' $TEX_OUTPUT
sed -i 's/Friedrich A. Kittler, \\emph{Gramofone, filme, typewritter}, 1986/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT

sed -i '/^konitaj\./r latex_personalizado/antes-cap-5.tex' $TEX_OUTPUT
sed -i 's/Richard Stallman, \\emph{La manifesto de GNU}, 1985/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/Richard Barbrook; Andy Cameron, \\emph{The Californian Ideology}, 1995/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -zi 's/John Perry Barlow, \\emph{Deklaracio de sendependeco de la retlando},\n1996/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/Creative Commons, \\emph{Get Creative!}, 2000/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -zi 's/Record Industry Association of America, \\emph{Kontraŭpirateca kampanjo},\n2000-aj jaroj/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/Wu Ming, \\emph{Copyright e maremoto}, 2002/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i '/Cohn, \\emph{Cultura digital\.br}, 2009/r latex_personalizado/antes-cap-cit.tex' $TEX_OUTPUT
sed -zi 's/Eduardo Viveiros de Castro, Economia da cultura digital, en Savazoni;\nCohn, \\emph{Cultura digital\.br}, 2009/\\\begin{flushright}\n&\n\\\end{flushright}/' $TEX_OUTPUT
# Per pdflatex
# sed -i 's/لخوارزمية/\\<&>/' $TEX_OUTPUT
sed -i 's/لخوارزمية/\\textarabic{&}/' $TEX_OUTPUT

sed -i '/\\emph{Ibidem}, p\.\~49\./r latex_personalizado/antes-cap-6.tex' $TEX_OUTPUT
sed -i 's/^\\emph{Mi estas, ĉar ni estas\.}/\\\begin{flushright}\n&\n\\\end{flushright}/' $TEX_OUTPUT
sed -i 's/^Ubuntu$/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/^Konfuceo, la \\emph{Analektoj}, ĉirk. s\. IV-II a\.K\./\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -zi 's/Marcela Stockler Coelho de Souza, \\emph{The Forgotten Pattern and the\nStolen Design: Contract, Exchange and Creativity among the Kĩsêdjê},\n2016/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i '/Evelin Heidel (Scann), \\emph{Que se callen las musas}, 2017/r latex_personalizado/antes-cap-cit.tex' $TEX_OUTPUT
sed -i 's/Evelin Heidel (Scann), \\emph{Que se callen las musas}, 2017/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
# Per pdflatex
# sed -i 's/儒學/\\begin{CJK*}{UTF8}{zhsong}&\\end{CJK*}/' $TEX_OUTPUT
# sed -i 's/魯鎮/\\begin{CJK*}{UTF8}{zhsong}&\\end{CJK*}/' $TEX_OUTPUT
# sed -i 's/論語/\\begin{CJK*}{UTF8}{zhsong}&\\end{CJK*}/' $TEX_OUTPUT
# sed -i 's/专利/\\begin{CJK*}{UTF8}{zhsong}&\\end{CJK*}/' $TEX_OUTPUT
sed -i '/remiksita en vintro de 2020/r latex_personalizado/antes-epilogo.tex' $TEX_OUTPUT
sed -zi 's/Interreto, Iberoameriko,\n\n\\sout{monda sudo, 23-a de novembro de 2018}\n\nremiksita en vintro de 2020/\\\begin{flushright}\n&\n\\\end{flushright}/' $TEX_OUTPUT

sed -i 's/Post paroli tiom pri kreo, reproprigado, proprieto, kopio, komuno,/\\noindent &/' $TEX_OUTPUT
sed -i '/kaj neordinaran sperton scii\./r latex_personalizado/antes-referencias.tex' $TEX_OUTPUT
sed -i 's/Ĉi tiu libro estis skribita, en ĝia fina skribado, inter septembro de/\\noindent &/' $TEX_OUTPUT
sed -i '/{\[}1972{\]} 2000\./r latex_personalizado/antes-agradecimientos.tex' $TEX_OUTPUT

sed -zi 's/\(\\includegraphics{bildoj\/Leonardo-Feltrin-Foletto\.png}\)\n\n\(Foto: Sheila Uberti\)/\\hspace\*{-0.65cm}\\rotatebox{90}{\\footnotesize \2} \1/' $TEX_OUTPUT
sed -i 's/Leonardo Feltrin Foletto naskiĝis en Taquari, interno de Rio Grande do/\\noindent &/' $TEX_OUTPUT
sed -i '/iujn el la manieroj kompreni la liberan kulturon\.$/r latex_personalizado/antes-sobre-el-autor.tex' $TEX_OUTPUT
